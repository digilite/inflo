<?php get_header(); ?>
  <div class="container custom-archive modules-archive">
    <h1><?php post_type_archive_title(); ?></h1>
    <div class="row">
      <div class="col-md-8 modules-header">
        <?php
          if ($title = get_field("modules_subtitle","option")):
            echo "<h2 class='h4'>". $title ."</h2>";
          endif;
          if ($content = get_field("modules_content","option")):
            echo "<p>". $content ."</p>";
          endif;
        ?>
      </div>
    </div>
    <div class="icons-container d-flex flex-wrap justify-content-between">
      <svg viewBox="0 0 1246 161" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
          <defs>
              <linearGradient x1="0%" y1="50%" x2="100%" y2="50%" id="linearGradient-1">
                  <stop stop-color="#FF2F40" offset="0%"></stop>
                  <stop stop-color="#FF7400" offset="42.5570101%"></stop>
                  <stop stop-color="#FF9600" offset="67.4039274%"></stop>
                  <stop stop-color="#00B92F" offset="100%"></stop>
              </linearGradient>
          </defs>
          <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
              <g id="3-Modules" transform="translate(-97.000000, -355.000000)" stroke="url(#linearGradient-1)" stroke-width="6">
                  <path d="M100,431.275 L100,363 C100,360.238576 102.238576,358 105,358 L250,358 C252.761424,358 255,360.238576 255,363 L255,508 C255,510.761424 257.238576,513 260,513 L405,513 C407.761424,513 410,510.761424 410,508 L410,363 C410,360.238576 412.238576,358 415,358 L560,358 C562.761424,358 565,360.238576 565,363 L565,508 C565,510.761424 567.238576,513 570,513 L715,513 C717.761424,513 720,510.761424 720,508 L720,363 C720,360.238576 722.238576,358 725,358 L870,358 C872.761424,358 875,360.238576 875,363 L875,508 C875,510.761424 877.238576,513 880,513 L1025,513 C1027.76142,513 1030,510.761424 1030,508 L1030,363 C1030,360.238576 1032.23858,358 1035,358 L1180,358 C1182.76142,358 1185,360.238576 1185,363 L1185,508 C1185,510.761424 1187.23858,513 1190,513 L1335,513 C1337.76142,513 1340,510.761424 1340,508 L1340,435.5 L1340,435.5" id="Path-5"></path>
              </g>
          </g>
      </svg>
      <?php
        $args = array( "post_type" => "modules_type", "posts_per_page" => 8);
        $posts = get_posts($args);
        foreach( $posts as $post ):
      ?>
        <div class="modules-card-container">
          <div class="modules-card d-flex justify-content-center align-items-center">
            <?php echo wp_get_attachment_image( get_field("module_icon"), "full", false, array("class"=>"big-icon-image")); ?>
          </div>
          <h2 class="h6"><?= $post->post_title ?></h2>
        </div>
      <?php endforeach; wp_reset_postdata(); ?>
    </div>
  </div>
  <div class="modules">
    <div class="container">
      <div class="row">
        <?php foreach( $posts as $post ): ?>
          <div class="col-md-6 modules-container">
            <div class="modules-card-full expandable-content">
            <?php echo wp_get_attachment_image( get_field("module_icon"), "full", false, array("class"=>"medium-icon-image")); ?>
              <h2 class="h3"><?= $post->post_title ?></h2>
              <p><?= $post->post_content ?></p>
              <?php if($qoute = get_field("modules_quote")): ?>
                <div class="extra-modules">
                  <?= $qoute; ?>
                  <div class="row extra-footer">
                    <div class="col-md-6">
                      <strong><?php the_field("modules_quote_author") ?></strong>
                    </div>
                    <div class="col-md-4 offset-md-2">
                      <img src="<?php the_field("modules_quote_image") ?>" class="img-fluid" alt="testimonials logo">
                    </div>
                  </div>
                </div>
                <a href="#" class="learn-more"><svg class="arrow-blue" width="5px" height="8px" viewBox="0 0 5 8"><use xlink:href="#blue-arrow"></use></svg> <span>Learn more</span></a>
              <?php endif; ?>
            </div>
          </div>
        <?php endforeach; wp_reset_postdata(); ?>
      </div>
    </div>
  </div>

<?php get_footer(); ?>
