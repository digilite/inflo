<?php
  get_header();
  if (have_posts()): while (have_posts()): the_post();
?>
<div class="careers-content">
  <div class="container">
    <h1 class="h2"><?php the_title(); ?></h1>
  </div>
  <div class="right-aligned overflow-image">
    <div class="container">
      <?php $img_obj = get_field("featured_image"); ?>
      <div class="featured-image cover d-flex" style="background-image: url(<?= $img_obj["sizes"]["page-header-img"]; ?>)"></div>
      <div class="row">
        <div class="col-md-1 offset-md-1">
          <img src="<?php the_field("left_side_icon"); ?>" class="big-icon-image" alt="heart icon image">
        </div>
        <div class="col-md-4 first-column">
          <h2 class="h4"><?php the_field("first_column_title"); ?></h2>
          <?php the_field("first_column_text"); ?>
        </div>
        <div class="col-md-4 offset-md-1 second-column">
          <h2 class="h4"><?php the_field("second_column_title"); ?></h2>
          <?php the_field("second_column_text"); ?>
          <?php if( have_rows("icon_points") ): while ( have_rows("icon_points") ) : the_row(); ?>
            <div class="row points">
              <div class="col-md-1 col-2">
                <img src="<?php the_sub_field("icon"); ?>" class="very-small-icon-image" alt="<?php the_sub_field("point"); ?>">
              </div>
              <div class="col-md-11 col-10"><strong><?php the_sub_field("point"); ?></strong></div>
            </div>
          <?php endwhile; endif; ?>
        </div>
      </div>
    </div>
  </div>

  <?php if( ($title = get_field("testimonial_title")) && ($content = get_field("testimonial_content"))): ?>
    <div class="testimonial-container">
      <div class="container">
        <div class="row">
          <div class="col-md-10 offset-md-1">
            <div class="testimonial-card">
              <h3 class="h4"><?= $title; ?></h3>
              <?php
                echo "<p>{$content}</p>";
                if ($name = get_field("testimonial_name")) {
                  echo "<strong>{$name}</strong>";
                }
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <?php if($jobs = get_field("select_job_posts")): ?>
    <section class="job-posts">
      <div class="container">
        <h2 class="h3"><?php the_field("section_title"); ?></h2>
        <div class="row">
          <?php 
            foreach($jobs as $job): 
            if (get_post_status($job) == 'publish' ):
          ?>
            <div class="col-md-6">
              <div class="job-card-container">
                <span class="package-overlay"></span>
                <div class="job-card">
                  <div class="d-md-flex justify-content-md-between">
                    <strong><?= get_the_title($job); ?></strong>
                    <span>Deadline: <?php the_field("end_date",$job) ?></span>
                  </div>
                  <span><svg class="pin" width="13px" height="16px" viewBox="0 0 13 16"><use xlink:href="#pin"></use></svg><?php the_field("location",$job) ?></span>
                  <a href="<?= get_permalink($job) ?>" class="learn-more"><svg class="arrow-blue" width="5px" height="8px" viewBox="0 0 5 8"><use xlink:href="#blue-arrow"></use></svg> More about the opportunity</a>
                </div>
              </div>
            </div>
            <?php endif; endforeach; ?>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="gdpr-container">
              <a href="<?php the_field("gdpr_link"); ?>" class="absolute" target="_blank"></a>
              <svg width="26px" height="26px" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <g id="12-Careers" transform="translate(-127.000000, -1798.000000)" stroke="#0477DD">
                    <g id="Group-9" transform="translate(129.000000, 1800.000000)">
                      <circle id="Oval" stroke-width="2.5" cx="11" cy="11" r="11"></circle>
                      <g id="Group-10" transform="translate(9.000000, 6.000000)" stroke-width="2">
                          <line x1="2.5" y1="3.5" x2="2.5" y2="8.5" id="Path-8"></line>
                          <line x1="0" y1="8.5" x2="5" y2="8.5" id="Path-12"></line>
                          <line x1="0" y1="4.5" x2="3" y2="4.5" id="Path-12-Copy"></line>
                          <line x1="2.5" y1="0" x2="2.5" y2="2" id="Path-8"></line>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
              <strong>GDPR</strong>
              <?php the_field("gdpr_text"); ?>
            </div>
          </div>
        </div>
      </div>
    </section>
  <?php endif; ?>
  <section class="career-footer">
    <div class="container">
      <h3 class="h1 text-md-center"><?php the_field("main_title"); ?></h3>
    </div>
    <div class="whats-offer gray-background">
      <div class="container">
        <div class="row">
          <div class="col-md-5">
            <h4 class="h3"><?php the_field("secondary_title"); ?></h4>
            <?php the_field("section_text"); ?>
          </div>
          <div class="col-md-5 offset-md-1">
            <?php $img_obj = get_field("offers_image"); ?>
            <div class="featured-image cover d-flex" style="background-image: url(<?= $img_obj["sizes"]["medium_large"]; ?>)">
              <span class="absolute overlay-mask"></span>
            </div>
          </div>
        </div>
        <?php if( have_rows("offers") ): ?>
          <div class="row">
            <?php while ( have_rows("offers") ) : the_row(); ?>
              <div class="col-lg-3 col-sm-6 feature-card-container">
                <div class="feature-card">
                  <img src="<?php the_sub_field("icon") ?>" class="small-icon-image" alt="<?php the_sub_field("title"); ?>">
                  <strong><?php the_sub_field("title"); ?></strong>
                  <p><?php the_sub_field("content"); ?></p>
                </div>
              </div>
            <?php endwhile; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </section>

</div>
<?php
  endwhile;
  endif;
  get_footer();
?>
