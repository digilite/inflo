<?php
  get_header();
  if (have_posts()): while (have_posts()): the_post();
?>
<div class="about-inflo">
  <div class="container">
    <h1 class="h2"><?php the_title(); ?></h1>
    <div class="row header-margin">
      <div class="col-md-8">
        <h2 class="h5"><?php the_field("subtitle"); ?></h2>
      </div>
      <div class="col-md-2 offset-md-2">
        <img src="<?php the_field("right_icon"); ?>" class="img-fluid" alt="Hybrid Intelligence">
      </div>
    </div>
  </div>
  <section class="inflo-benefits gray-background">
    <div class="right-aligned overflow-image">
      <div class="container">
        <?php $img_obj = get_field("main_image"); ?>
        <div class="featured-image cover d-flex" style="background-image: url(<?= $img_obj["sizes"]["page-header-img"]; ?>)"></div>
        <h3 class="h4"><?php the_field("benefit_title"); ?></h3>
        <?php if( have_rows("benefits") ): ?>
          <div class="row">
            <?php while ( have_rows("benefits") ) : the_row(); ?>
              <div class="col-lg-4 col-sm-6 feature-card-container">
                <div class="feature-card">
                  <img src="<?php the_sub_field("icon") ?>" class="small-icon-image" alt="<?php the_sub_field("title"); ?>">
                  <strong><?php the_sub_field("title"); ?></strong>
                  <p><?php the_sub_field("content"); ?></p>
                </div>
              </div>
            <?php endwhile; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </section>
  <section class="inflo-explained">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <h2 class="h2"><?php the_field("inflohi_video_title"); ?></h2>
        <?php if($subtitle = get_field("inflohi_video_subtitle")): ?>
          <p><?= $subtitle; ?></p>
        <?php endif; ?>
      </div>
      <div class="col-md-7 offset-md-2">
        <?php the_field("inflohi_video"); ?>
      </div>
    </div>
  </div>
</section>


</div>
<?php
  endwhile;
  endif;
  get_footer();
?>
