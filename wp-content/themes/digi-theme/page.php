<?php
  get_header();
  if( have_posts() ): while ( have_posts() ) : the_post();
?>
<div class="container">
  <h1 class="offset-md-2 h2"><?php the_title(); ?></h1>
  <div class="row">
    <div class="col-md-8 offset-md-2">
      <div class="user-content">
        <?php the_content(); ?>
      </div>
    </div>
  </div>
</div>
<?php
  endwhile;
endif;
  get_footer();
?>
