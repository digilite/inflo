<div class="search-form-container">
  <h2 class="h5">Get up to speed on our latest news and industry insights.</h2>
  <div class="form-content">
    <span class="package-overlay"></span>
    <form action="<?php bloginfo("url"); ?>" method="get" id="search" class="form-inline transition">
      <input class="search-bar transition" type="search" value="<?php (isset($_GET["s"]) ? _e($_GET["s"]) : ""); ?>" type="search" name="s" autocomplete="off" placeholder="Search">
    </form>
  </div>
  <div class="search-tags">
    <?php
      $tags = get_tags();
      foreach ( $tags as $tag ):
        $tag_link = get_tag_link( $tag->term_id );
        if ( single_tag_title("", false) == $tag->name) {
          echo "<a href='{$tag_link}' title='{$tag->name} Tag' class='current-tag {$tag->slug}'>{$tag->name}</a>";
        } else {
          echo "<a href='{$tag_link}' title='{$tag->name} Tag' class='{$tag->slug}'>{$tag->name}</a>";
        }
      endforeach;
    ?>
  </div>
</div>
