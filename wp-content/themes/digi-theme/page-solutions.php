<?php
  get_header();
  if (have_posts()): while (have_posts()): the_post();
?>
  <div class="container">
    <h1 class="h2"><?php the_title(); ?></h1>
    <div class="row">
      <div class="col-md-10">
        <p class="h4"><?php the_field("main_subtitle"); ?></p>
      </div>
    </div>
  </div>
  <?php
    $counter = 1;
    if( have_rows("solution_sections") ): while ( have_rows("solution_sections") ) : the_row();
    $section_class = "section-content";
    $row_class = "row";
    $col_first = "col-md-4";
    $col_second = "col-md-6 offset-md-2";
    if($counter % 2 == 0) {
      $section_class .= " gray-background";
      $row_class .= " reverse-order";
      $col_first .= " offset-md-2";
      $col_first = "col-md-4 offset-md-2";
      $col_second = "col-md-6";
    }
  ?>
    <div class="<?= $section_class; ?>">
      <div class="container">
        <div class="<?= $row_class; ?>">
          <div class="<?= $col_first; ?>">
            <div class="image-overlay-container">
              <?php echo wp_get_attachment_image( get_sub_field("section_image"), "image-600" , false,  array("class"=> "img-fluid"))  ?>
              <span class="overlay-mask absolute"></span>
            </div>
          </div>
          <div class="<?= $col_second; ?>">
            <h2 class="h3"><?php the_sub_field("title"); ?></h3>
            <strong class="secondary-subtitle"><?php the_sub_field("subtitle"); ?></strong>
            <?php get_template_part("templates/accordion"); ?>
          </div>
        </div>
      </div>
    </div>
  <?php $counter++; endwhile; endif; ?>
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <h2 class="h4"><?php the_field("customers_section_title"); ?></h2>
      </div>
      <div class="col-md-4 nav-control d-md-flex justify-content-md-end"></div>
    </div>
  </div>
  <div class="customer-section">
    <?php $logos = get_field("logos_slider");  ?>
    <?php foreach( $logos as $image ): ?>
      <div class="item">
          <img src="<?= $image['sizes']['image-600']; ?>" alt="<?= (empty($image['alt'])) ? "logos" : $image['alt']; ?>" />
      </div>
    <?php endforeach; ?>
  </div>
<?php
  endwhile;
  endif;
  get_footer();
?>
