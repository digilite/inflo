<?php
  get_header();
  $image_obj = get_field("featured_image","option");
  $imagee_url = $image_obj["sizes"]["page-header-img"];
?>
<div class="container custom-archive features-archive">
  <h1><?php post_type_archive_title(); ?></h1>
  <div class="row">
    <div class="col-md-8 modules-header">
      <?php
        if ($content = get_field("features_subtitle","option")):
          echo "<h2 class='h4'>". $content ."</p>";
        endif;
      ?>
    </div>
  </div>
</div>
<div class="features shifted-container">
  <div class="shifted-image-container">
    <div class="container">
      <div class="shifted-image">
        <div class="cover absolute" style="background-image: url(<?= $imagee_url; ?>)"><span class="cover absolute overlay-mask"></span></div>
      </div>
      <div class="features-content">
        <div class="row">
          <div class="col-md-3">
            <div id="pills-tab" class="features-tab nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
              <?php
                $args = array( "post_type" => "features_type", "posts_per_page" => -1);
                $posts = get_posts($args);
                $counter = 1;
                $html ="";
                $tab_class = "";
                $tab_content_class = "";
                foreach( $posts as $post ):
                  if ($counter == 1 ) {
                    $tab_class = "nav-link with-icon active";
                    $tab_content_class = "tab-pane fade show active";
                  } else {
                    $tab_class = "nav-link with-icon";
                    $tab_content_class = "tab-pane fade";
                  }
                  $counter++;
              ?>
                <a class="<?= $tab_class; ?>" id="pills-tab-<?= $post->post_name; ?>" data-toggle="pill" href="#pills-<?= $post->post_name; ?>" role="tab" aria-controls="pills-<?= $post->post_name; ?>" aria-selected="true">
                  <h2 class="h6 nav-container">
                    <img src="<?php the_field("icon"); ?>" class="icon" alt="<?= $post->post_title ?>">
                    <?= $post->post_title ?>
                  </h2>
                  <span class="package-overlay"></span>
                </a>
                <div class="not-moved <?= $tab_content_class; ?>" id="pills-<?= $post->post_name; ?>" role="tabpanel" aria-labelledby="pills-tab-<?= $post->post_name ?>">
                  <?php if( have_rows("feature_row") ): while ( have_rows("feature_row") ) : the_row(); ?>
                  <div class="row content-row <?php if ( get_row_layout() == "image_text" ) { echo "reverse-order"; } ?>">
                    <div class="col-md-6 text-tab">
                      <h3 clas="h4"><?php the_sub_field("feature_title"); ?></h3>
                      <?php the_sub_field("feature_content"); ?>
                    </div>
                    <div class="col-md-6">
                      <?php echo wp_get_attachment_image( get_sub_field("feature_image"), "full" , false,  array("class"=> "rounded-img img-fluid"))  ?>
                    </div>
                  </div>
                  <?php endwhile; endif; ?>
                </div>
              <?php
                endforeach;
                wp_reset_postdata();
              ?>
            </div>
          </div>
          <div class="col-md-8 offset-md-1">
            <div class="tab-content" id="pills-tabContent">

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
