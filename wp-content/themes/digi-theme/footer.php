    </main>
    <?php
      if(!is_page_template("landing.php")):
        if(!is_page("contact")):
          get_template_part("templates/cta-block");
        endif;
      endif;
    ?>
    <footer itemscope itemtype="http://schema.org/WPFooter">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-6">
            <div  class="footer-first-col d-flex flex-column">
              <h3 class="h4">Subscribe to stay updated</h3>
              <?php echo do_shortcode("[formidable id=2]"); ?>
              <div class="social-pages">
                <?php get_template_part("social"); ?>
              </div>
              <div class="privacy-cookies">
                <?php if($privacy = get_field("data_privacy_page","option")): ?>
                  <a href="<?= $privacy->guid; ?>"><svg width="12" height="15" class="lock" viewBox="0 0 11.5 14.5"><use xlink:href="#lock"></use></svg> <?= $privacy->post_title; ?></a>
                <?php endif; ?>
                <?php if($cookies = get_field("cookie_policy","option")): ?>
                  <a href="<?= $cookies->guid; ?>"><svg width="14" height="14" class="settings" viewBox="0 0 14 14"><use xlink:href="#settings"></use></svg> <?= $cookies->post_title; ?></a>
                <?php endif; ?>
              </div>
              <span class="copyright">
                &copy; <?php echo date("Y") . " " . get_bloginfo("name"); ?>. All rights are reserved. Developed by <a href="//digilite.com.au" target="_blank">Digilite</a>
              </span>
            </div>
          </div>
          <div class="col-lg-2 offset-lg-2 col-md-3 offset-md-3">
            <?php wp_nav_menu([ 'theme_location' => 'footer-menu', 'menu_class' => 'footer-menu nav navbar-nav', 'container' => '']); ?>
          </div>
          <div class="col-md-2 menu-with-titles">
            <h4 class="h6">Modules</h4>
            <?php wp_nav_menu([ 'theme_location' => 'footer-modules', 'menu_class' => 'modules-menu nav navbar-nav', 'container' => '']); ?>
          </div>
          <div class="col-md-2 menu-with-titles">
            <h4 class="h6">Features</h4>
            <?php wp_nav_menu([ 'theme_location' => 'footer-features', 'menu_class' => 'features-menu nav navbar-nav', 'container' => '']); ?>
          </div>
        </div>
      </div>
		</footer>
		<?php wp_footer(); ?>
	</body>
</html>
