<?php
/**
* Template Name: Contact Page
*
* @package WordPress
* @subpackage Digilite Theme
*/
  get_header();
?>
<div class="container">
  <?php if (have_posts()): while (have_posts()): the_post(); ?>
    <h1>Get in touch with Inflo!</h1>
    <span class="subtitle"><a href="mailto:<?php the_field("email","option"); ?>"><?php the_field("email","option"); ?></a></span>
    <div class="row">
      <div class="col-md-5 contact-details">
        <?php the_content(); ?>
        <div class="social-pages">
          <?php get_template_part("social"); ?>
        </div>
      </div>
      <div class="col-md-7 form-button-right">
        <?= do_shortcode("[formidable id=1]"); ?>
      </div>
    </div>
  <?php endwhile;	endif; ?>
</div>
<?php get_footer(); ?>
