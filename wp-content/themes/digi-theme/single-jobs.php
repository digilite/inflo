<?php
  get_header();
  if( have_posts() ): while ( have_posts() ) : the_post();
?>
<div class="container">
  <h1 class="h2"><?php the_title(); ?></h1>
  <div class="row">
    <div class="col-md-10">
      <h2 class="h5"><?= get_the_content(); ?></h2>
    </div>
  </div>
  <div class="features-content">
    <div class="row">
      <div class="col-md-2">
        <div id="pills-tab" class="features-tab nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
          <?php
            $counter = 1;
            $html ="";
            $tab_class = "";
            $tab_content_class = "";
            while ( have_rows("tabs") ) : the_row();
              if ($counter == 1 ) {
                $tab_class = "nav-link active";
                $tab_content_class = "tab-pane fade show active";
              } else {
                $tab_class = "nav-link";
                $tab_content_class = "tab-pane fade";
              }
              $counter++;
              $rand = rand(1,200);
          ?>
            <div class="<?= $tab_class; ?>" id="pills-tab-<?= $rand; ?>" data-toggle="pill" href="#pills-<?= $rand; ?>" role="tab" aria-controls="pills-<?= $rand; ?>" aria-selected="true">
              <h2 class="h6 nav-container">
                <?php the_sub_field("tabs_title"); ?>
              </h2>
              <span class="package-overlay"></span>
            </div>
            <div class="not-moved <?= $tab_content_class; ?>" id="pills-<?= $rand; ?>" role="tabpanel" aria-labelledby="pills-tab-<?= $rand ?>">
              <h3 class="h4"><?php the_sub_field("tabs_title"); ?></h3>
              <?php the_sub_field("tabs_content"); ?>
            </div>
          <?php
            endwhile;
            wp_reset_postdata();
          ?>
        </div>
      </div>
      <div class="col-md-6 offset-md-1">
        <div class="tab-content" id="pills-tabContent">
        </div>
      </div>
      <div class="col-md-2 offset-md-1">
        <div class="contact-us-cta">
          <strong>Apply now</strong>
          <span>Deadline: <?php the_field("end_date") ?></span>
          <span><svg class="pin" width="13px" height="16px" viewBox="0 0 13 16"><use xlink:href="#pin"></use></svg><?php the_field("location") ?></span>
          <p><?php the_field("right_sidebar_content"); ?></p>
          <a href="<?php the_field("apply_now"); ?>" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 14H4V8l8 5 8-5v10zm-8-7L4 6h16l-8 5z"/></svg>
          Apply Now</a>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
  endwhile;
  endif;
  get_footer();
?>
