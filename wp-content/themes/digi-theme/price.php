<?php
/**
* Template Name: Pricing Page
*
* @package WordPress
* @subpackage Digilite Theme
*/
  get_header();
  if( have_posts() ): while ( have_posts() ) : the_post();
  $cleasses = "no-thumbnail";
  if (has_post_thumbnail()) {
    $post_thumbnail_id = get_post_thumbnail_id();
    $image_url = wp_get_attachment_image_url( $post_thumbnail_id, "page-header-img");
    $cleasses = "has-thumbnail";
  }
?>

<div class="pricing-header <?= $cleasses; ?>">
  <?php if(!empty($image_url)): ?>
  <div class="absolute cover" style="background-image: url(<?= $image_url; ?>);">
    <span class="header-overlay absolute"></span>
  </div>
  <?php endif; ?>
  <div class="container user-content">
    <?php the_content(); ?>
  </div>
</div>
<?php if( have_rows("pricing_packages") ): ?>
  <div class="packages">
    <div class="container">
      <div class="row">
        <?php while ( have_rows("pricing_packages") ) : the_row(); ?>
        <div class="col-lg-3 col-md-6 package-card <?php if (get_sub_field("highlighted")) { echo "highlighted"; } ?>">
          <span class="package-overlay"></span>
          <div class="package-card-container d-flex flex-column">
            <div class="package-info">
              <strong class="package-name"><?php the_sub_field("package_name"); ?></strong>
              <div class="package-price">
              <?php
                $price = get_sub_field("package_price");
                if ( get_sub_field("package_monthly")):
                  echo "<p class='h4'>". $price ."</p>";
                else:
                  echo "<span class='number'>". $price ."</span><span class='per'>/mo</span>";
                endif;
              ?>
              </div>
              <strong class="package-duration"><?php the_sub_field("package_duration"); ?></strong>
            </div>
            <div class="package-description">
              <?php the_sub_field("package_description"); ?>
            </div>

            <?php if($add_info = get_sub_field("additional_info")): ?>
              <div class="package-add-info">
                <strong><?= $add_info; ?></strong><?php the_sub_field("icon"); ?>
              </div>
            <?php endif; ?>
            <?php if( have_rows("features") ): while ( have_rows("features") ) : the_row(); ?>
              <span class="feature-bullet"><?php the_sub_field("feature"); ?><svg class="tic" viewBox="0 0 14 12"><use xlink:href="#tic"></use></svg></span>
            <?php endwhile; endif; ?>
            <div class="button-container">
              <a href="<?php the_sub_field("button_link"); ?>" class="button orange-button"><?php the_sub_field("button_text"); ?></a>
            </div>
          </div>
        </div>
        <?php endwhile; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>
  <div class="inlcuded-features">
    <div class="container">
      <h2 class="h4">All plans include these amazing features</h2>
      <?php
        $args = array(
          "post_type" => "features_type",
          "posts_per_page" => -1,
        );
        $features = new WP_Query( $args );
        $link = get_page_link(10);
        if ( $features->have_posts()):
      ?>
      <div class="row">
        <?php while ($features->have_posts()) : $features->the_post(); ?>
          <div class="col-lg-4 col-sm-6 feature-card-container">
            <div class="feature-card">
              <img src="<?php the_field("icon") ?>" class="small-icon-image" alt="<?php the_title(); ?>">
              <strong><?php the_title(); ?></strong>
              <p><?php the_field("short_description"); ?></p>
              <a class="learn-more" href="<?= $link . "/#{$post->post_name}"; ?>"><svg class="arrow-blue" width="5px" height="8px" viewBox="0 0 5 8"><use xlink:href="#blue-arrow"></use></svg> Learn more</a>
            </div>
          </div>
        <?php endwhile; ?>
      </div>
      <?php endif; wp_reset_postdata(); ?>
    </div>
  </div>

<?php
  endwhile;
  endif;
  get_footer();
?>
