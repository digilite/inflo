<?php
  get_header();
  if (have_posts()): while (have_posts()): the_post();
?>
<div class="about-inflo">
  <div class="container">
    <h1 class="h2"><?php the_field("page_title"); ?></h1>
    <div class="row">
      <div class="col-md-10">
        <h2 class="h4"><?php the_field("page_subtitle"); ?></h2>
      </div>
    </div>
  </div>
  <div class="right-aligned overflow-image">
    <div class="container">
      <?php $img_obj = get_field("featured_image"); ?>
      <div class="featured-image cover d-flex" style="background-image: url(<?= $img_obj["sizes"]["page-header-img"]; ?>)">  </div>
    </div>
  </div>
  <section class="transform-inflo">
    <div class="container">
      <div class="row">
        <div class="col-md-5 offset-md-1"><strong><?php the_field("first_column"); ?></strong></div>
        <div class="col-md-5">
          <?php the_field("second_column"); ?>
          <?php the_field("third_column"); ?>
        </div>
      </div>
      <?php if (($left_img = get_field("left_column_image")) && ($right_img = get_field("right_column_image"))): ?>
      <div class="row">
        <div class="col-md-6">
          <div class="featured-image cover d-flex" style="background-image: url(<?= $left_img["sizes"]["image-600"]; ?>)">
            <span class="absolute overlay-mask"></span>
          </div>
        </div>
        <div class="col-md-6">
          <div class="featured-image cover d-flex" style="background-image: url(<?= $right_img["sizes"]["image-600"]; ?>)">
            <span class="absolute overlay-mask"></span>
          </div>
        </div>
      </div>
      <?php endif; ?>
    </div>
  </section>
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <h2 class="h4"><?php the_field("customers_section_title"); ?></h2>
      </div>
      <div class="col-md-4 nav-control d-md-flex justify-content-md-end"></div>
    </div>
  </div>
  <div class="customer-section">
    <?php $logos = get_field("logos_slider");  ?>
    <?php foreach( $logos as $image ): ?>
      <div class="item">
          <img src="<?= $image['sizes']['image-600']; ?>" alt="<?= (empty($image['alt'])) ? "logos" : $image['alt']; ?>" />
      </div>
    <?php endforeach; ?>
  </div>
</div>
<?php
  endwhile;
  endif;
  get_footer();
?>
