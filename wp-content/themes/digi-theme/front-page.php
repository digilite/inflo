<?php
  get_header();
  if (have_posts()) :
    while (have_posts()) : the_post();
?>
<section class="header-section right-aligned overflow-image">
  <div class="container">
    <div class="row reverse-order">
      <div class="col-md-7 offset-md-1">
        <?php $img_obj = get_field("page_header_background_image"); ?>
        <div class="featured-image cover d-flex" style="background-image: url(<?= $img_obj["sizes"]["page-header-img"]; ?>)"></div>
      </div>
      <div class="col-md-4">
        <?php
          if($title = get_field("page_header_title")):
            echo "<h2 class='h1'>". $title ."</h2>";
          endif;
          if($content = get_field("page_header_subtitle")):
            echo "<p>". $content ."</p>";
          endif;
          get_template_part("templates/buttons");
        ?>
      </div>
    </div>
  </div>
</section>
<section class="home-modules">
  <div class="container">
    <h2 class="h4">These are our modules</h2>
    <div class="d-flex flex-wrap justify-content-between">
      <?php
        $args = array( "post_type" => "modules_type", "posts_per_page" => 8);
        $posts = get_posts($args);
        foreach( $posts as $post ):
      ?>
        <div class="modules-card-container">
          <div class="modules-card d-flex justify-content-center align-items-center">
            <a href="<?= get_post_type_archive_link("modules_type") ?>" class="absolute"></a>
            <?php echo wp_get_attachment_image( get_field("module_icon"), "full", false, array("class"=>"big-icon-image")); ?>
          </div>
          <h2 class="h6"><?= $post->post_title ?></h2>
        </div>
      <?php endforeach; wp_reset_postdata(); ?>
    </div>
  </div>
</section>
<section class="inflo-explained">
  <div class="container">
    <div class="row reverse-order">
      <div class="col-md-7 offset-md-2">
        <?php the_field("2_video"); ?>
      </div>
      <div class="col-md-3">
        <h2 class="h2"><?php the_field("2_section_title"); ?></h2>
        <?php if($subtitle = get_field("2_call_to_action")): ?>
          <p><?= $subtitle; ?></p>
        <?php endif; ?>
        <?php if(($btn_text = get_field("2_button_text")) && ($btn_url = get_field("2_button_url"))): ?>
          <a href="<?= $btn_url; ?>" class="button orange-button"><?= $btn_text; ?></a>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>
<section class="video-section">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <h2 class="h2"><?php the_field("section_title_quote"); ?></h2>
        <?php if($subtitle = get_field("section_subtitle_quote")): ?>
          <p><?= $subtitle; ?></p>
        <?php endif; ?>
      </div>
      <div id="video-control" class="col-md-4 nav-control d-md-flex justify-content-md-end"></div>
    </div>
  </div>
  <div class="video-slider">
    <?php
      if( have_rows("videos") ): while ( have_rows("videos") ) : the_row();
        $page_id = get_sub_field("select_conversations");
    ?>
      <div class="item">
        <a href="<?= get_page_link($page_id); ?>" class="absolute"><i class="fas fa-play-circle"></i></a>
        <?= get_the_post_thumbnail( $page_id, "conversations-thumbnail", array("class"=>"img-fluid")); ?>
        <div class="caption">
          <strong><?php the_sub_field("author_name") ?></strong>
          <p><?php the_sub_field("title"); ?></p>
        </div>
      </div>
    <?php endwhile; endif;  ?>
  </div>
</section>
<section class="why-inflo overflow-image">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <h2 class="h2"><?php the_field("1_section_title"); ?></h2>
        <?php if($subtitle = get_field("1_section_subtitle")): ?>
          <p><?= $subtitle; ?></p>
        <?php endif; ?>
      </div>
      <div class="col-md-7 offset-md-2">
        <div class="featured-image cover d-flex" style="background-image: url(<?php the_field("background_image"); ?>)"></div>
      </div>
    </div>
    <?php if( have_rows("1_key_points") ): ?>
      <div class="row key-points">
      <?php while ( have_rows("1_key_points") ) : the_row(); ?>
        <div class="col-md-6 key-points-container">
          <div class="points">
            <img src="<?php the_sub_field("icon"); ?>" class="small-icon-image" alt="<?php the_sub_field("key_point_title"); ?>">
            <h3 class="h5"><?php the_sub_field("key_point_title"); ?></h3>
            <p><?php the_sub_field("key_point_content"); ?></p>
          </div>
        </div>
      <?php endwhile; endif; ?>
    </div>
  </div>
</section>
<div class="container">
  <div class="row">
    <div class="col-md-8">
      <h2 class="h4"><?php the_field("customers_section_tiltle"); ?></h2>
    </div>
    <div id="customer-control" class="col-md-4 nav-control d-md-flex justify-content-md-end"></div>
  </div>
</div>
<div class="customer-section">
  <?php $logos = get_field("logos_slider");  ?>
  <?php foreach( $logos as $image ): ?>
    <div class="item">
        <img src="<?= $image['sizes']['image-600']; ?>" alt="<?= (empty($image['alt'])) ? "logos" : $image['alt']; ?>" />
    </div>
  <?php endforeach; ?>
</div>
<?php
    endwhile;
  endif;
  get_footer();
?>
