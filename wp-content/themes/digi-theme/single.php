<?php
  get_header();
  if( have_posts() ): while ( have_posts() ) : the_post();
?>

<div class="container single-content">
  <h1 class="h2"><?php the_title(); ?></h1>
  <?php get_template_part("templates/post-meta"); ?>
  <div class="tag-container">
    <?php the_tags(""); ?>
  </div>
  <?php
    if(has_post_thumbnail()):
      $post_thumbnail_id = get_post_thumbnail_id();
      $image_url = wp_get_attachment_image_url( $post_thumbnail_id, "full");
      echo "<div class='featured-image hero-image cover' style='background-image: url(". $image_url .");'><span class='overlay-mask absolute'></span></div>";
    endif;
  ?>
  <div class="row">
    <div class="col-lg-6 offset-lg-2 col-md-7">
      <div class="user-content">
        <?php the_content(); ?>
      </div>
    </div>
    <div class="col-lg-2 offset-lg-2 col-md-3 offset-md-1 teams-card author-column">
      <?php
        if ($author_id = get_field("select_author")):
        $image_url = wp_get_attachment_image_url(get_field("leadership_team_picture",$author_id), "image-600", false, array("class"=>"img-fluid"));
      ?>
        <div class="team-image" style="background-image: url(<?= $image_url; ?>)"><span class="absolute team-overlay"></span></div>
        <h2 class="h5"><?= get_the_title($author_id); ?></h2>
        <span class="team-position"><?php the_field("leadership_team_role",$author_id) ?></span>
        <br>
        <a href="<?= get_post_type_archive_link("leadership_team") ?>" class="learn-more"><svg class="arrow-blue" width="5px" height="8px" viewBox="0 0 5 8"><use xlink:href="#blue-arrow"></use></svg> More</a>
      <?php endif; ?>
    </div>
  </div>
  <?php get_template_part("templates/related-posts"); ?>
</div>

<?php
  endwhile;
endif;
  get_footer();
?>
