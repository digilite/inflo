<?php
  $prefix = "";
  if (is_post_type_archive("modules_type")) { $prefix = "modules_"; }
  if (is_post_type_archive("leadership_team")) { $prefix = "team_"; }
  if (is_post_type_archive("features_type")) { $prefix = "features_"; }
  $text = get_field("{$prefix}cta_text","option");
  $button_link = get_field("{$prefix}cta_link","option");
  $button_text = get_field("{$prefix}button_text","option");

  if(get_field("custom_cta")) {
    $text = get_field("cta_text");
    $button_link = get_field("cta_url");
    $button_text = get_field("cta_button_text");
  }

  if (!is_single() && (!empty($text) || !empty($button_link) ) ):

?>
  <section class="call-to-action">
    <div class="cta-content text-center">
      <?php if (!empty($text)): ?>
        <h3 class="h4"><?= $text; ?></h3>
      <?php endif; ?>
      <?php
        if (!empty($button_link)):

          echo "<a class='button orange-button' href='". $button_link ."'>". $button_text ."</a>";
        endif;
      ?>
    </div>
  </section>
<?php endif; ?>
