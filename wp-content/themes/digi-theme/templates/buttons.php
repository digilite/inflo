<?php
    if( have_rows("buttons") ): ?>
    <div class="button-container">
      <?php while ( have_rows("buttons") ) : the_row(); ?>
        <a href="<?php the_sub_field("button_link") ?>" class="button <?= (get_sub_field("button_style")) ? "orange-button" : "white-button"; ?>">
          <?php the_sub_field("button_text"); ?>
        </a>
      <?php endwhile; ?>
    </div>
<?php endif; ?>
