<div class="number-pagination clearfix"> <?php
  global $wp_query;
	$big = 999999999; //need an unlikely integer
	$args = array(
		"base" => str_replace($big, "%#%", esc_url(get_pagenum_link($big))),
		"format" => "?page=%#%",
		"total" => intval($wp_query->max_num_pages),
		"current" => max(1, get_query_var("paged")),
		"show_all" => false,
		"end_size" => 2,
		"mid_size" => 2,
		"prev_next" => True,
		"prev_text" => __("<i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>"),
		"next_text" => __("<i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i>"),
		"type" => "list",
	);
	echo paginate_links($args); ?>
</div>
