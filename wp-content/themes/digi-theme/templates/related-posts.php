<?php
  $post_type = get_post_type();
  $args = array(
    "post_type" => $post_type,
    "posts_per_page" => 3,
    "post__not_in" => array( get_the_ID())
  );
  $related = new WP_Query( $args );
  if ( $related->have_posts()):
?>
<section class="related-container">
  <h4 class="h4">You might be interested</h4>
  <div class="row">
  <?php
    while ($related->have_posts()) : $related->the_post();
      get_template_part("templates/cards/resource-card");
    endwhile;
  ?>
  </div>
</section>
<?php endif; wp_reset_postdata(); ?>
