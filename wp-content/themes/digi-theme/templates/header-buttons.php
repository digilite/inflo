<?php
  $login_text = get_field("login_button_text","option");
  $login_url = get_field("login_button_url","option");
  $inflo_text = get_field("inflo_button_text","option");
  $inflo_url = get_field("inflo_button_url","option");
  if ( !empty($login_text) || !empty($login_url) || !empty($inflo_text) || !empty($inflo_url) ):
?>
  <div class="header-buttons">
    <?php if(!empty($login_text) && !empty($login_url)): ?>
      <a href="<?= $login_url; ?>" class="button white-button"><?= $login_text; ?></a>
    <?php endif; ?>
    <?php if(!empty($inflo_text) && !empty($inflo_url)): ?>
      <a href="<?= $inflo_url; ?>" class="button orange-button"><?= $inflo_text; ?></a>
    <?php endif; ?>
  </div>
<?php endif; ?>
