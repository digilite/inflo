<?php if( have_rows("accordion") ): ?>
  <?php $random = rand(1,200); ?>
  <div id="accordion-<?php echo $random; ?>" class="accordion">
      <?php $accordion_counter = 2; while ( have_rows("accordion") ) : the_row(); ?>
        <div class="panel panel-group">
          <h3 id="heading-<?php echo $accordion_counter* $random; ?>" class="h6 panel-title <?php if($accordion_counter != 2) { echo "collapsed"; } ?>" data-toggle="collapse" data-target="#collapse-<?php echo $accordion_counter*$random; ?>" aria-expanded="false">
              <?php the_sub_field("accordion_title"); ?>
              <svg class="arrow-blue" width="16px" height="16px" viewBox="0 0 5 8"><use xlink:href="#blue-arrow"></use></svg>
          </h3>
          <div id="collapse-<?php echo $accordion_counter* $random; ?>" class="<?php if($accordion_counter == 2) { echo "show"; } ?> collapse panel-collapse" aria-expanded="true" aria-labelledby="#heading-<?php echo $accordion_counter* $random; ?>" data-parent="#accordion-<?php echo $random; ?>">
              <?php the_sub_field("accordion_content"); ?>
          </div>
        </div><!-- End of panel -->
      <?php $accordion_counter++; endwhile; ?>
  </div><!-- End of panel-group -->
<?php endif; ?>
