<?php $image_url = wp_get_attachment_image_url(get_field("leadership_team_picture"), "image-600", false, array("class"=>"img-fluid")); ?>
<div class="team-image" style="background-image: url(<?= $image_url; ?>)"><span class="absolute team-overlay"></span></div>
<div class="row">
  <div class="col-md-7">
    <h2 class="h5"><?php the_title(); ?></h2>
    <span class="team-position"><?php the_field("leadership_team_role") ?></span>
  </div>
  <?php if( have_rows("team_social_pages") ): ?>
  <div class="col-md-5">
    <div class="social-pages d-flex justify-content-md-end">
      <?php
        while ( have_rows("team_social_pages") ) : the_row();
          echo "<a href='". get_sub_field("link") ."' style='background-color: ". get_sub_field("color_picker") ."' target='_blank'>". get_sub_field("icon") ."</a>";
        endwhile; ?>
    </div>
  </div>
  <?php endif; ?>
</div>
