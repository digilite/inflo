<?php
  $post_thumbnail_id = get_post_thumbnail_id();
  $image_url = wp_get_attachment_image_url( $post_thumbnail_id, "conversations-thumbnail");
?>
<div class="col-lg-4 col-md-6 resource-card-container">
  <div class="resource-card">
    <div class="featured-image cover" style="background-image: url(<?= $image_url; ?>);">
      <a href="<?php the_permalink(); ?>" class="blog-overlay absolute"></a>
    </div>
    <div class="tag-container">
      <?php the_tags(""); ?>
    </div>
    <?php get_template_part("templates/post-meta"); ?>
    <h2 class="h6"><a href="<?php the_permalink(); ?>"><strong><?php the_title(); ?></strong></a></h2>
  </div>
</div>
