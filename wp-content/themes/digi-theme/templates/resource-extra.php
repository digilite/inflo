<div class="resource-extra">
  <div class="container">
    <div class="row">
      <div class="col-lg-4">
        <h3 class="h1"><?php the_field("title",80); ?></h3>
        <p><?php the_field("subtitle",80); ?></p>
      </div>
      <div class="col-lg-4 col-sm-6">
        <?php $image_url = get_field("cta_image",80); ?>
        <div class="featured-image cover" style="background-image: url(<?= $image_url["sizes"]["medium_large"]; ?>);">
          <a href="<?php the_field("cta_file",80); ?>" class="absolute" target="_blank"></a>
        </div>
      </div>
      <div class="col-lg-4 col-sm-6">
        <?php $image_url = get_field("cta_image_2",80); ?>
        <div class="featured-image cover" style="background-image: url(<?= $image_url["sizes"]["medium_large"]; ?>);">
          <a href="<?php the_field("cta_file_2",80); ?>" class="absolute" target="_blank"></a>
        </div>
      </div>
    </div>
  </div>
</div>
