<?php
/**
* Template Name: Landing Page
*
* @package WordPress
* @subpackage Digilite Theme
*/
  get_header();
?>
<div class="container">
  <?php
    if (have_posts()): while (have_posts()): the_post();
    $form_id = get_field("select_form");
  ?>
    <div class="row">
      <div class="col-md-6 form-button-right d-md-flex flex-column justify-content-md-center">
        <h1 class="h2"><?php the_field("form_title"); ?></h1>
        <?= do_shortcode("[formidable id={$form_id}]"); ?>
      </div>
      <div class="col-md-4 offset-md-2 landing-content">
        <?php the_content(); ?>
        <?php if( have_rows("styles_bullets") ): while ( have_rows("styles_bullets") ) : the_row(); ?>
          <span class="feature-bullet"><?php the_sub_field("bullets"); ?><svg class="tic" viewBox="0 0 14 12"><use xlink:href="#tic"></use></svg></span>
        <?php endwhile; endif; ?>
      </div>
    </div>
  <?php endwhile;	endif; ?>
</div>
<?php get_footer(); ?>
