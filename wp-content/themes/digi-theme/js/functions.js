var $ = jQuery;
var single_slide_nav = {
	loop: true,
  dots: false,
  autoplay: true,
  center: true,
  nav: true,
  lazyLoad: true,
  margin: 65,
  navContainer: "#customer-control",
  navText: ['<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 4.31 6.65"><title>learn_more-left</title><polyline points="3.82 6.15 1.3 3.63 0.99 3.32 3.82 0.49" style="fill:none;stroke:#f57a2e;stroke-width:1.4px"/></svg>','<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 4.31 6.65"><polyline points="0.49 0.49 3.01 3.01 3.32 3.32 0.49 6.15" style="fill:none;stroke:#f57a2e;stroke-width:1.4px"/></svg>'],
	autoplayTimeout: 3000,
	responsiveClass: true,
    responsive:{
        0:{
            items:2,
            autoHeight:true,
        },
        992:{
            items:6,
        },
    }
};
var video_slider = {
    nav: true,
    autoplayTimeout: 7000,
    center: true,
    lazyLoad: true,
    loop: true,
    dots: false,
    margin: 20,
    navContainer: "#video-control",
    navText: ['<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 4.31 6.65"><title>learn_more-left</title><polyline points="3.82 6.15 1.3 3.63 0.99 3.32 3.82 0.49" style="fill:none;stroke:#f57a2e;stroke-width:1.4px"/></svg>','<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 4.31 6.65"><polyline points="0.49 0.49 3.01 3.01 3.32 3.32 0.49 6.15" style="fill:none;stroke:#f57a2e;stroke-width:1.4px"/></svg>'],
    responsive:{
        0:{
            items:2,
            loop: true
        },
        768:{
            items:2
        },
        991:{
            items:4
        }
    }
}


var videos = $(".video-slider");
var slider_section = $(".customer-section");

$(document).ready(function() {
  responsive_ele();

  $("#burger-icon").click(function(){
    $(this).toggleClass("open");
    $("#site-nav").addClass("opend");
  });

  if(slider_section.length) {
    slider_section.owlCarousel(single_slide_nav);
	}
  if(videos.length) {
    videos.owlCarousel(video_slider);
  }

  $(".main-content iframe").each(function(){
    $(this).wrap("<div class='embed-responsive embed-responsive-16by9'></div>");
  });

  $(".move-down").click(function(){
    var hero_height = $(this).parent().parent().height();
    var to_scroll = hero_height + $("header").height() + 34;
    $("html, body").animate({ scrollTop:  to_scroll }, 2000);
  });
  if ($(".grid").length) {
    $(".grid").packery({
      // options
        itemSelector: ".grid-item",
    });
  }

  $(".teams-content .teams-card .learn-more").click(function(e){
    e.preventDefault();
    $("body").css("overflow","hidden");
    $(this).next().addClass("team-opened");
    $(this).parent().find(".team-image").clone().appendTo(".image-popup");
  });

  $(".teams-content .teams-card .close").click(function(e){
    e.preventDefault();
    $(".popup-team-container .image-popup").find(".team-image").detach();
    $(this).parent().parent().removeClass("team-opened");
    $("body").css("overflow","unset");
  });

  $(".modules-container .learn-more").click(function(e){
    e.preventDefault();
    $(this).toggleClass("opened");
    $(this).parent().find(".extra-modules").slideToggle();
    if ($(this).hasClass("opened")) {
      $(this).find("span").html("show less");
    } else {
      $(this).find("span").html("learn more");
    }
  });
  $("#pills-tab-" + location.hash.replace( /#/,"")).tab("show");

});

function responsive_ele() {
  if ($(window).width() < 992 ) {
      $(".nav-link").click(function(){
        $(this).next().slideToggle();
      });
    } else {
      $(".not-moved").each(function(){
        $(this).removeClass("not-moved").appendTo(".tab-content");
      });
    }
}
