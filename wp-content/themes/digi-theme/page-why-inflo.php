<?php
  get_header();
  if (have_posts()): while (have_posts()): the_post();
?>
<div class="growing-value">
  <div class="container">
    <h1 class="h3"><?php the_title(); ?></h1>
    <div class="row">
      <div class="col-md-8">
        <span class="h5"><?php the_field("subtitle"); ?></span>
      </div>
    </div>
  </div>
  <div class="overflow-image">
    <div class="shifted-image-container">
      <div class="container">
        <div class="shifted-image">
          <?php $img_obj = get_field("feature_image"); ?>
          <div class="featured-image cover d-flex" style="background-image: url(<?= $img_obj["url"]; ?>)"></div>
        </div>
      </div>
      <div class="reasons-why">
        <div class="container">
          <h2 class="h4"><?php the_field("section_title"); ?></h3>
          <?php if( have_rows("numbered_sections") ): ?>
            <div class="row">
              <?php while ( have_rows("numbered_sections") ) : the_row(); ?>
                <div class="col-lg-3 col-sm-6 feature-card-container">
                  <div class="feature-card">
                    <span class="number"><img src="<?php the_sub_field("icon"); ?>" class="tiny-icon-image" alt="resons why choose inflo software"></span>
                    <?php if($title = get_sub_field("title")): ?>
                      <strong><?= $title; ?></strong>
                    <?php endif; ?>
                    <p><?php the_sub_field("content"); ?></p>
                  </div>
                </div>
              <?php endwhile; ?>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-5 offset-md-1">
          <?php $img_obj = get_field("mission_image"); ?>
          <div class="featured-image cover d-flex" style="background-image: url(<?= $img_obj["sizes"]["medium_large"]; ?>)">
            <span class="absolute overlay-mask"></span>
          </div>
        </div>
        <div class="col-md-4 offset-md-1">
            <?php the_field("mission_content"); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-7">
          <div class="mission-block gradiant-background">
            <div class="mission-block-container">
              <h3>Mission</h3>
              <p><?php the_field("mission_block"); ?></p>
            </div>
          </div>
        </div>
        <div class="col-md-5">
          <div class="mission-block gradiant-background">
            <div class="mission-block-container">
              <h3>Vision</h3>
              <p><?php the_field("vission_block"); ?></p>
            </div>
          </div>
        </div>
      </div>
      <?php if( have_rows("testimonials") ): ?>
      <div class="testimonial-container">
        <div class="container">
          <div class="row">
            <div class="col-md-10 offset-md-1">
              <?php while ( have_rows("testimonials") ) : the_row(); ?>
                <div class="testimonial-card">
                  <p><?php the_sub_field("content"); ?></p>
                  <strong><?php the_sub_field("title"); ?></strong>
                </div>
              <?php endwhile; ?>
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?>
    </div>
  </section>


</div>
<?php
  endwhile;
  endif;
  get_footer();
?>
