<?php
  get_header();
  if (have_posts()): while (have_posts()): the_post();
?>
  <div class="container">
    <h1 class="h2"><?php the_title(); ?></h1>
    <div class="row justify-content-md-center">
      <div class="col-lg-6 col-md-8">
        <?php get_template_part("searchform"); ?>
        <?php the_tags(); ?>
      </div>
    </div>
    <div class="resource-main-container">
      <div class="row justify-content-center">
        <?php
          remove_all_filters("posts_orderby");
          $tags = get_tags();
          $tag_id = [];
          foreach ( $tags as $key => $tag ): 
            $tag_id[$key] = $tag->name;
          endforeach;
          $paged = ( get_query_var( "paged" ) ) ? get_query_var( "paged" ) : 1;
          $args = array(
            "post_type" => array("blog","webinars", "conversations", "case-studies"),
            "posts_per_page" => 12,
            "tag" => $tag_id,
            "paged" => $paged
          );
          $blogs = new WP_Query( $args );
          if ( $blogs->have_posts()): while ($blogs->have_posts()) : $blogs->the_post();
            get_template_part("templates/cards/resource-card");
          endwhile; endif; wp_reset_postdata(); ?>
      </div>
      <div class="number-pagination clearfix"> <?php
        global $blogs;
        $big = 999999999; //need an unlikely integer
        $args = array(
          "base" => str_replace($big, "%#%", esc_url(get_pagenum_link($big))),
          "format" => "?page=%#%",
          "total" => intval($blogs->max_num_pages),
          "current" => max(1, get_query_var("paged")),
          "show_all" => false,
          "end_size" => 2,
          "mid_size" => 2,
          "prev_next" => True,
          "prev_text" => __("<i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>"),
          "next_text" => __("<i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i>"),
          "type" => "list",
        );
        echo paginate_links($args); ?>
      </div>
    </div>
  </div>
<?php
  get_template_part("templates/resource-extra");
  endwhile;
  endif;
  get_footer();
?>
