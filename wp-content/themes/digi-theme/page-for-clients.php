<?php
  get_header();
  if (have_posts()): while (have_posts()): the_post();
?>
<div class="about-inflo">
  <div class="container">
    <h1 class="h2"><?php the_title(); ?></h1>
    <div class="row header-margin">
      <div class="col-md-8">
        <h2 class="h5"><?php the_field("subtitle"); ?></h2>
      </div>
    </div>
  </div>
  <section class="inflo-benefits gray-background">
    <div class="right-aligned overflow-image">
      <div class="container">
        <?php $img_obj = get_field("main_image"); ?>
        <div class="featured-image cover d-flex" style="background-image: url(<?= $img_obj["sizes"]["page-header-img"]; ?>)"></div>
        <h3 class="h4"><?php the_field("benefit_title"); ?></h3>
        <?php if( have_rows("benefits") ): ?>
          <div class="row">
            <?php while ( have_rows("benefits") ) : the_row(); ?>
              <div class="col-lg-4 col-sm-6 feature-card-container">
                <div class="feature-card">
                  <img src="<?php the_sub_field("icon") ?>" class="small-icon-image" alt="<?php the_sub_field("title"); ?>">
                  <strong><?php the_sub_field("title"); ?></strong>
                  <p><?php the_sub_field("content"); ?></p>
                </div>
              </div>
            <?php endwhile; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </section>
  <section class="inflo-explained">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <h2 class="h2"><?php the_field("inflohi_video_title"); ?></h2>
        <?php if($subtitle = get_field("inflohi_video_subtitle")): ?>
          <p><?= $subtitle; ?></p>
        <?php endif; ?>
      </div>
      <div class="col-md-7 offset-md-2">
        <?php the_field("inflohi_video"); ?>
      </div>
    </div>
  </div>
</section>
<section class="video-section">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <h2 class="h2"><?php the_field("section_title"); ?></h2>
        <?php if($subtitle = get_field("section_subtitle")): ?>
          <p><?= $subtitle; ?></p>
        <?php endif; ?>
      </div>
      <div id="video-control" class="col-md-4 nav-control d-md-flex justify-content-md-end"></div>
    </div>
  </div>
  <div class="video-slider">
    <?php
      if( have_rows("videos") ): while ( have_rows("videos") ) : the_row();
        $page_id = get_sub_field("select_conversations");
    ?>
      <div class="item">
        <a href="<?= get_page_link($page_id); ?>" class="absolute"><i class="fas fa-play-circle"></i></a>
        <?= get_the_post_thumbnail( $page_id, "conversations-thumbnail", array("class"=>"img-fluid")); ?>
        <div class="caption">
          <strong><?php the_sub_field("author_name") ?></strong>
          <p><?php the_sub_field("title"); ?></p>
        </div>
      </div>
    <?php endwhile; endif;  ?>
  </div>
</section>

</div>
<?php
  endwhile;
  endif;
  get_footer();
?>
