<?php
  get_header();
  $image_obj = get_field("team_featured_image","option");
  $imagee_url = $image_obj["sizes"]["page-header-img"];
?>
<div class="container custom-archive team-archive">
  <h1><?php post_type_archive_title(); ?></h1>
</div>
<div class="teams shifted-container">
  <div class="shifted-image-container">
    <div class="container">
      <div class="shifted-image">
        <div class="cover absolute" style="background-image: url(<?= $imagee_url; ?>)"></div>
      </div>
      <?php if ($intro = get_field("team_intro_text","option")): ?>
        <div class="team-intro text-center">
          <h2 class="h4"><?= $intro; ?></h2>
        </div>
      <?php endif; ?>
    </div>
  </div>
  <div class="teams-content">
    <div class="container">
      <div class="row">
        <?php
          $args = array( "post_type" => "leadership_team", "posts_per_page" => -1);
          $blogs = new WP_Query( $args );
          while ($blogs->have_posts()) : $blogs->the_post();
        ?>
          <div class="col-lg-3 col-md-6 teams-card">
            <?php get_template_part("templates/cards/team-card") ?>
            <?php if(get_the_content() != ""): ?>
              <a href="#" class="learn-more"><svg class="arrow-blue" width="5px" height="8px" viewBox="0 0 5 8"><use xlink:href="#blue-arrow"></use></svg> More</a>
              <div class="popup-team-container">
                <div class="popup-team">
                  <span class="close"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/><path d="M0 0h24v24H0z" fill="none"/></svg></span>
                  <div class="row">
                    <div class="col-md-2 offset-md-1 image-popup"></div>
                    <div class="col-md-7 offset-md-1">
                      <h2 class="h2"><?php the_title(); ?></h2>
                      <span class="team-position"><?php the_field("leadership_team_role") ?></span>
                      <?php the_content(); ?>
                      <div class="social-pages d-flex">
                        <?php
                          while ( have_rows("team_social_pages") ) : the_row();
                            echo "<a href='". get_sub_field("link") ."' style='background-color: ". get_sub_field("color_picker") ."' target='_blank'>". get_sub_field("icon") ."</a>";
                          endwhile; ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php endif; ?>
          </div>
        <?php endwhile; wp_reset_postdata(); ?>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
