<?php
/*********************************
Register options pages (ACF)
**********************************/
add_redirect();

// redirect Dashboard to custom options page 'Site Dashboard'
function add_redirect() {
  add_action('load-index.php', 'redirect_dashboard');
}

// disable for posts
add_filter('use_block_editor_for_post', '__return_false', 10);
// disable for post types
add_filter('use_block_editor_for_post_type', '__return_false', 10);

function redirect_dashboard() {
  if (is_plugin_active('advanced-custom-fields-pro/acf.php')) {
    wp_redirect(admin_url('admin.php?page=site-dashboard'));
  }
}

// create custom pages and sub pages within the new dashboard
if (function_exists('acf_add_options_page')) {
  acf_add_options_page(array('page_title'=> 'Site Dashboard',
      'menu_title'=> 'Site Dashboard',
      'menu_slug'=> 'site-dashboard',
      'capability'=> 'edit_posts',
      'redirect'=> false));

  acf_add_options_page(array('page_title'=> 'Site Settings',
      'menu_title'=> 'Site Settings',
      'menu_slug'=> 'site-settings',
      'capability'=> 'edit_posts',
      'redirect'=> false));

  acf_add_options_sub_page(array('page_title'=> 'Header',
      'menu_title'=> 'Header',
      'parent_slug'=> 'site-settings',
    ));

  acf_add_options_sub_page(array('page_title'=> 'Footer',
      'menu_title'=> 'Footer',
      'parent_slug'=> 'site-settings',
    ));
}

/*********************************
Modify the admin bar to remove the things we don't need
**********************************/
function modify_admin_bar() {
  global $wp_admin_bar;

  // define elements to remove
  $nodes=array('wp-logo',
    'about',
    'wporg',
    'documentation',
    'support-forums',
    'feedback',
    'view-site',
    'comments',
    'new-content',
    'customize',
    'customize-widgets',
    'customize-background',
    'themes',
    'dashboard',
    'widgets',
    'menus'
  );

  foreach ($nodes as $node) {
    $wp_admin_bar->remove_node($node);
  }
}

add_action('wp_before_admin_bar_render', 'modify_admin_bar');


/*********************************
Modify the admin area footer
**********************************/
function modify_admin_footer() {
  _e('<span id="footer-thankyou">Developed by <a href="://digilite.ca" target="_blank">Digilite Inc</a></span>', 'themefooter');
}

add_filter('admin_footer_text', 'modify_admin_footer');

/*********************************
Load admin-specific styles. Edit in admin.scss.
**********************************/
function admin_css() {
  wp_enqueue_style('admin-style',
    get_template_directory_uri().'library/dist/css/admin.min.css'
  );
}

add_action('admin_enqueue_scripts', 'admin_css', 10);

/*********************************
Load custom editor styles
**********************************/
add_editor_style(get_template_directory_uri() . '/library/dist/css/editor-style.min.css');

// Create custom login page
// call in css file. Edit in login.scss
function login_css() {
  wp_enqueue_style('login_css', get_template_directory_uri() . '/library/dist/css/login.min.css', false);
}

// changing the logo link from wordpress.org to your site
function login_url() {
  return home_url();
}

// changing the alt text on the logo to show your site name
function login_title() {
  return get_option('blogname');
}

// calling it only on the login page
add_action('login_enqueue_scripts', 'login_css', 10);
add_filter('login_headerurl', 'login_url');
add_filter('login_headertitle', 'login_title');


/*********************************
Allow editors to edit menus
**********************************/
// get the the role object
$role_object=get_role('editor');

// add $cap capability to this role object
$role_object->add_cap('edit_theme_options');
