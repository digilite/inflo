<?php

// Scripts and Stylesheets!
add_action('after_setup_theme', 'init_scripts_and_styles');

function init_scripts_and_styles() {
  // enqueue base scripts and styles
  add_action('wp_enqueue_scripts', 'scripts_and_styles', 999);
}

function scripts_and_styles() {
	wp_enqueue_style("bootstrap-style", "//stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css", [], "", "all");
	wp_style_add_data( 'bootstrap-style', array( 'integrity', 'crossorigin' ) , array( 'sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T', 'anonymous' ) );
	wp_enqueue_style("font-awesome-5", "//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css", [], "", "all");
	wp_style_add_data( 'font-awesome-5', array( 'integrity', 'crossorigin' ) , array( 'sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU', 'anonymous' ) );
	wp_enqueue_style("stylecss", get_stylesheet_uri());

	wp_enqueue_script("jquery");

	wp_enqueue_style('owl-carousel-css', get_template_directory_uri() .'/css/owl.carousel.min.css','','',false);
	wp_enqueue_script('owl-carousel-js', get_template_directory_uri() .'/js/owl.carousel.min.js', '', '', true);

	wp_enqueue_script("popper-js", "//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js", "", "", true);
	wp_script_add_data( 'popper-js', array( 'integrity', 'crossorigin' ) , array( 'sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1', 'anonymous' ) );
	wp_enqueue_script("bootstrap-4", "//stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js", "", "", true);
    wp_script_add_data( 'bootstrap-4', array( 'integrity', 'crossorigin' ) , array( 'sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM', 'anonymous' ) );

	wp_enqueue_script("functions", get_template_directory_uri() . "/js/functions.js", "", "", true);
	wp_localize_script("functions", "wp_var",
		[
			"ajax_url" => admin_url("admin-ajax.php"),
		]
	);
}

/*********************
DEREGISTER CSS/SCRIPTS
*********************/
// Deregister Contact Form 7 styles
add_action('wp_print_styles', 'cf7_deregister_styles', 100);

function cf7_deregister_styles()
{
    if (! is_page(array('contact', 'trial', 'sign-up', 'demo'))) {
        wp_deregister_style('contact-form-7');
    }
}
