<?php
function features() {
	register_post_type( "features_type",
		array( "labels" => array(
			"name" => __( "Features", "digi-theme" ),
			"singular_name" => __( "Feature", "digi-theme" ),
			"all_items" => __( "All Features", "digi-theme" ),
			"add_new" => __( "Add New", "digi-theme" ),
			"add_new_item" => __( "Add Feature", "digi-theme" ),
			"edit" => __( "Edit", "digi-theme" ),
			"edit_item" => __( "Edit Features", "digi-theme" ),
			"new_item" => __( "New Feature", "digi-theme" ),
			"view_item" => __( "View Feature", "digi-theme" ),
			"search_items" => __( "Search Feature", "digi-theme" ),
			"not_found" =>  __( "Nothing found in the Database.", "digi-theme" ),
			"not_found_in_trash" => __( "Nothing found in Trash", "digi-theme" ),
			"parent_item_colon" => ""
		),
			"description" => __( "Inflo incorporates different automated technologies for accountants and auditors helping in effective financial analysis and project management solutions.
			", "digi-theme" ),
			"public" => true,
			"publicly_queryable" => true,
			"exclude_from_search" => false,
			"show_ui" => true,
			"query_var" => true,
			"menu_position" => 8,
			"rewrite"	=> array( "slug" => "features" ),
			"has_archive" => "features",
			"capability_type" => "post",
			"hierarchical" => false,
			"supports" => array( "title", "editor", "revisions", "sticky")
		)
	);
}
add_action( "init", "features");

function leadership_team_post_type() {
  register_post_type( "leadership_team",
    array( "labels" => array(
      "name" => __( "Leadership Team", "digi-theme" ),
      "singular_name" => __( "Team Member Post", "digi-theme" ),
      "all_items" => __( "All Team Members Posts", "digi-theme" ),
      "add_new" => __( "Add New", "digi-theme" ),
      "add_new_item" => __( "Add Team Member", "digi-theme" ),
      "edit" => __( "Edit", "digi-theme" ),
      "edit_item" => __( "Edit Team Member", "digi-theme" ),
      "new_item" => __( "New Team Member Post", "digi-theme" ),
      "view_item" => __( "View Team Member Post", "digi-theme" ),
      "search_items" => __( "Search Team Member", "digi-theme" ),
      "not_found" =>  __( "Nothing found in the Database.", "digi-theme" ),
      "not_found_in_trash" => __( "Nothing found in Trash", "digi-theme" ),
      "parent_item_colon" => ""
      ),
      "description" => __( "Check out Inflo’s Leadership team and board including our CEO and COO", "digi-theme" ),
      "public" => true,
      "publicly_queryable" => true,
      "exclude_from_search" => false,
      "show_ui" => true,
      "query_var" => true,
      "menu_position" => 8,
      "rewrite"	=> array( "slug" => "leadership-team" ),
      "has_archive" => "leadership-team",
      "capability_type" => "post",
      "hierarchical" => false,
      "supports" => array( "title", "editor", "revisions")
    )
  );
}
add_action( "init", "leadership_team_post_type");

function jobs_post_type() {
  register_post_type( "jobs",
    array( "labels" => array(
      "name" => __( "Jobs", "digi-theme" ),
      "singular_name" => __( "Job Post", "digi-theme" ),
      "all_items" => __( "All Jobs Posts", "digi-theme" ),
      "add_new" => __( "Add New", "digi-theme" ),
      "add_new_item" => __( "Add Job", "digi-theme" ),
      "edit" => __( "Edit", "digi-theme" ),
      "edit_item" => __( "Edit Job", "digi-theme" ),
      "new_item" => __( "New Job Post", "digi-theme" ),
      "view_item" => __( "View Job Post", "digi-theme" ),
      "search_items" => __( "Search Job", "digi-theme" ),
      "not_found" =>  __( "Nothing found in the Database.", "digi-theme" ),
      "not_found_in_trash" => __( "Nothing found in Trash", "digi-theme" ),
      "parent_item_colon" => ""
      ),
      "description" => __( "This is the Jobs post type", "digi-theme" ),
      "public" => true,
      "publicly_queryable" => true,
      "exclude_from_search" => true,
      "show_ui" => true,
      "query_var" => true,
      "menu_position" => 8,
      "has_archive" => false,
      "capability_type" => "post",
      "hierarchical" => false,
      "supports" => array( "title", "editor", "revisions")
    )
  );
}
add_action( "init", "jobs_post_type");


function modules() {

	register_post_type( "modules_type",

		array( "labels" => array(
			"name" => __( "Modules", "digi-theme" ),
			"singular_name" => __( "Module", "digi-theme" ),
			"all_items" => __( "All Modules", "digi-theme" ),
			"add_new" => __( "Add New", "digi-theme" ),
			"add_new_item" => __( "Add New Module", "digi-theme" ),
			"edit" => __( "Edit", "digi-theme" ),
			"edit_item" => __( "Edit Modules", "digi-theme" ),
			"new_item" => __( "New Module", "digi-theme" ),
			"view_item" => __( "View Module", "digi-theme" ),
			"search_items" => __( "Search Module", "digi-theme" ),
			"not_found" =>  __( "Nothing found in the Database.", "digi-theme" ),
			"not_found_in_trash" => __( "Nothing found in Trash", "digi-theme" ),
			"parent_item_colon" => ""
			),
			"description" => __( "This is the example custom Module", "digi-theme" ),
			"public" => true,
			"publicly_queryable" => true,
			"exclude_from_search" => false,
			"show_ui" => true,
			"query_var" => true,
			"menu_position" => 8,
			"rewrite"	=> array( "slug" => "modules"),
			"has_archive" => "modules",
			"capability_type" => "post",
			"hierarchical" => false,

			"supports" => array( "title", "editor", "revisions")
		)
	);
}
add_action( "init", "modules");

function conversations_post_type() {
	register_post_type( "conversations",
		array( "labels" => array(
			"name" => __( "Conversations", "digi-theme" ),
			"singular_name" => __( "Conversation", "digi-theme" ),
			"all_items" => __( "All Conversations", "digi-theme" ),
			"add_new" => __( "Add New", "digi-theme" ),
			"add_new_item" => __( "Add New Conversation", "digi-theme" ),
			"edit" => __( "Edit", "digi-theme" ),
			"edit_item" => __( "Edit Conversations", "digi-theme" ),
			"new_item" => __( "New Conversation", "digi-theme" ),
			"view_item" => __( "View Conversation", "digi-theme" ),
			"search_items" => __( "Search Conversation", "digi-theme" ),
			"not_found" =>  __( "Nothing found in the Database.", "digi-theme" ),
			"not_found_in_trash" => __( "Nothing found in Trash", "digi-theme" ),
			"parent_item_colon" => ""
			),
			"description" => __( "Inflo is broken down into eight modules that each help make a specific process more valuable for accounting firms and organisations.", "digi-theme" ),
			"public" => true,
			"publicly_queryable" => true,
			"exclude_from_search" => false,
			"show_ui" => true,
			"query_var" => true,
			"menu_position" => 8,
			"rewrite"	=> array( "slug" => "resources/conversations", "with_front" => false ),
			"has_archive" => "resources/conversations",
			"capability_type" => "post",
			"hierarchical" => false,
      "taxonomies" 			=> array("post_tag"),
			"supports" => array( "title", "editor", "revisions", "thumbnail")
		)
	);
}

	add_action( "init", "conversations_post_type");
function case_studies_post_type() {

	register_post_type( "case-studies",

		array( "labels" => array(
			"name" => __( "Case Studies", "digi-theme" ),
			"singular_name" => __( "Case Study", "digi-theme" ),
			"all_items" => __( "All Case Studies", "digi-theme" ),
			"add_new" => __( "Add New", "digi-theme" ),
			"add_new_item" => __( "Add New Case Study", "digi-theme" ),
			"edit" => __( "Edit", "digi-theme" ),
			"edit_item" => __( "Edit Case Studies", "digi-theme" ),
			"new_item" => __( "New Case Study", "digi-theme" ),
			"view_item" => __( "View Case Study", "digi-theme" ),
			"search_items" => __( "Search Case studies", "digi-theme" ),
			"not_found" =>  __( "Nothing found in the Database.", "digi-theme" ),
			"not_found_in_trash" => __( "Nothing found in Trash", "digi-theme" ),
			"parent_item_colon" => ""
			),
			"description" => __( "This is the case studies custom post type", "digi-theme" ),
			"public" => true,
			"publicly_queryable" => true,
			"exclude_from_search" => false,
			"show_ui" => true,
			"query_var" => true,
			"menu_position" => 8,
			"rewrite"	=> array( "slug" => "resources/case-studies", "with_front" => false ),
			"has_archive" => "resources/case-studies",
			"capability_type" => "post",
			"hierarchical" => false,
      "taxonomies" 			=> array("post_tag"),
			"supports" => array( "title", "editor", "revisions", "thumbnail")
		)
	);
}
add_action( "init", "case_studies_post_type");

function webinars_post_type() {
	register_post_type( "webinars",
		array( "labels" => array(
			"name" => __( "Webinars", "digi-theme" ),
			"singular_name" => __( "Webinar", "digi-theme" ),
			"all_items" => __( "All Webinars", "digi-theme" ),
			"add_new" => __( "Add New", "digi-theme" ),
			"add_new_item" => __( "Add New Webinar", "digi-theme" ),
			"edit" => __( "Edit", "digi-theme" ),
			"edit_item" => __( "Edit Webinars", "digi-theme" ),
			"new_item" => __( "New Webinar", "digi-theme" ),
			"view_item" => __( "View Webinar", "digi-theme" ),
			"search_items" => __( "Search Webinar", "digi-theme" ),
			"not_found" =>  __( "Nothing found in the Database.", "digi-theme" ),
			"not_found_in_trash" => __( "Nothing found in Trash", "digi-theme" ),
			"parent_item_colon" => ""
			),
			"description" => __( "This is the webinars custom post type", "digi-theme" ),
			"public" => true,
			"publicly_queryable" => true,
			"exclude_from_search" => false,
			"show_ui" => true,
			"query_var" => true,
			"menu_position" => 8,
			"rewrite"	=> array( "slug" => "resources/webinars", "with_front" => false ),
			"has_archive" => "resources/webinars",
			"capability_type" => "post",
			"hierarchical" => false,
      "taxonomies" 			=> array("post_tag"),
			"supports" => array( "title", "editor", "revisions", "thumbnail")
		)
	);
}
add_action( "init", "webinars_post_type");

function blog_post_type() {
	register_post_type( "blog",
		array( "labels" => array(
			"name" => __( "News & Blog", "digi-theme" ),
			"singular_name" => __( "Post", "digi-theme" ),
			"all_items" => __( "All Posts", "digi-theme" ),
			"add_new" => __( "Add New", "digi-theme" ),
			"add_new_item" => __( "Add New Post", "digi-theme" ),
			"edit" => __( "Edit", "digi-theme" ),
			"edit_item" => __( "Edit Post", "digi-theme" ),
			"new_item" => __( "New Post", "digi-theme" ),
			"view_item" => __( "View Post", "digi-theme" ),
			"search_items" => __( "Search Post", "digi-theme" ),
			"not_found" =>  __( "Nothing found in the Database.", "digi-theme" ),
			"not_found_in_trash" => __( "Nothing found in Trash", "digi-theme" ),
			"parent_item_colon" => ""
			),
			"description" => __( "This is the news & blog custom post type", "digi-theme" ),
			"public" => true,
			"publicly_queryable" => true,
			"exclude_from_search" => false,
			"show_ui" => true,
			"query_var" => true,
			"menu_position" => 8,
			"rewrite"	=> array( "slug" => "resources/news-and-blogs", "with_front" => false ),
			"has_archive" => false,
			"capability_type" => "post",
			"hierarchical" => false,
      "taxonomies" 			=> array("post_tag"),
			"supports" => array( "title", "editor", "revisions", "thumbnail")
		)
	);
}
add_action( "init", "blog_post_type");
