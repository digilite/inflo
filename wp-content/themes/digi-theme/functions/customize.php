<?php

/* WYSIWYG defaults */
/** change tinymce's paste-as-text functionality */
function paste_as_text($mceInit, $editor_id) {
  //turn on paste_as_text by default
  //NB this has no effect on the browser's right-click context menu's paste!
  $mceInit["paste_as_text"]=true;
  return $mceInit;
}

add_filter("tiny_mce_before_init", "paste_as_text", 1, 2);

/** Set the Attachment Display Settings, This function is attached to the 'after_setup_theme' action hook. */
function default_attachment_display_setting() {
  update_option("image_default_align", "left");
  update_option("image_default_link_type", "none");
  update_option("image_default_size", "large");
}

add_action("after_setup_theme", "default_attachment_display_setting");

// CUSTOM MENUS
function custom_menus() {
  register_nav_menus([
      "primary-menu"=> __("Primary Menu"),
      "footer-menu"=> __("Footer Menu"),
      "footer-modules"=> __("Footer Modules"),
      "footer-features"=> __("Footer Features"),
    ]);
}

add_action("init", "custom_menus");

// create an ID from a user entered string and removing any unwanted symbols
function create_id($string) {
  $new_id=preg_replace('/[^a-zA-Z]/', '', $string);
  $new_id=strtolower(str_replace(" ", "", $new_id));
  return $new_id;
}

function post_back_link() {
  if (wp_get_referer()) {
    $prev_url=$_SERVER['HTTP_REFERER'];
    return "<a href='". $prev_url ."' class='back-link'><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' aria-hidden='true' focusable='false' style='-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);' preserveAspectRatio='xMidYMid meet' viewBox='0 0 512 512'><path d='M216.4 163.7c5.1 5 5.1 13.3.1 18.4L155.8 243h231.3c7.1 0 12.9 5.8 12.9 13s-5.8 13-12.9 13H155.8l60.8 60.9c5 5.1 4.9 13.3-.1 18.4-5.1 5-13.2 5-18.3-.1l-82.4-83c-1.1-1.2-2-2.5-2.7-4.1-.7-1.6-1-3.3-1-5 0-3.4 1.3-6.6 3.7-9.1l82.4-83c4.9-5.2 13.1-5.3 18.2-.3z'></path><rect x='0' y='0' width='512' height='512' fill='rgba(0, 0, 0, 0)'></rect></svg></a>";
  }
}

function acf_load_color_field_choices( $field ) {
	global $wpdb;
	$forms = $wpdb->get_results('SELECT * FROM inf_frm_forms WHERE status="published"');
	$ids = array();
	$values = array();
	$i=0;
	if ( $forms != NULL ) {
		foreach($forms as $form){
			$ids[$i] = $form->id;
			$values[$i] = $form->name;
			$i++;
		}
		$form_assoc = array_combine($ids, $values);
		if( is_array($form_assoc) ){
			foreach( $form_assoc as $key=>$match ){
					$field["choices"][ $key ] = $match;
			}
		}
		// return the field
		return $field;
	} else {
		return false;
	}

}
add_filter("acf/load_field/key=field_5e95dbf2f2abc", "acf_load_color_field_choices");
