<?php // theme_features!
add_action('after_setup_theme', 'theme_features');

function theme_features() {

  // theme_featuresing this stuff after theme setup
  theme_support();

  // adding sidebars to Wordpress
  add_action('widgets_init', 'register_custom_sidebars');
}

/* end theme features */

/****************************************
Thumbnail size options
****************************************/
function extra_image_sizes() {
  foreach ( get_intermediate_image_sizes() as $size ) {
      if ( !in_array( $size, array( 'thumbnail', 'medium', 'medium_large', 'large' ) ) ) {
          remove_image_size( $size );
      }
  }
  add_image_size('image-600', 600, 600, true);
  add_image_size('image-300', 300, 300, true);
  add_image_size('page-header-img', 1600, 300);
  add_image_size('conversations-thumbnail', 600, 338, true);
}

add_action('init', 'extra_image_sizes');



/* custom read more and excerpt heigh.
 * usage: echo my_excerpts(20, $post) or echo my_excerpts(20)
 * or use wp_trim_words() @ https://developer.wordpress.org/reference/functions/wp_trim_words/
 */
function custom_excerpt($excerpt_length, $content=false) {
  global $post;
  $mycontent=$post->post_excerpt;
  $link=$post->guid;

  $mycontent=__($post->post_content);
  $mycontent=strip_shortcodes($mycontent);
  $mycontent=str_replace("]]>", "]]&gt;", $mycontent);
  $mycontent=strip_tags($mycontent);
  $words=explode(" ", $mycontent, $excerpt_length + 1);
  if(count($words) > $excerpt_length):
    array_pop($words);
  array_push($words, "...");
  $mycontent=implode(" ", $words);
  endif;

  // Make sure to return the content
  return $mycontent;
}

// Display dash icons
function ww_load_dashicons() {
  wp_enqueue_style('dashicons');
}

add_action('wp_enqueue_scripts', 'ww_load_dashicons', 999);

// Sidebars & Widgetizes Areas
function register_custom_sidebars() {
  register_sidebar(array('id'=> 'sidebar1',
      'name'=> __('Sidebar 1', 'mainthemee'),
      'description'=> __('The first (primary) sidebar.', 'mainthemee'),
      'before_widget'=> '<div id="%1$s" class="widget %2$s">',
      'after_widget'=> '</div>',
      'before_title'=> '<h4 class="widgettitle">',
      'after_title'=> '</h4>',

    ));
}

/*********************
Theme support
*********************/

// support all of the theme things
function theme_support() {

  // wp thumbnails (sizes handled in functions.php)
  add_theme_support('post-thumbnails');


  // rss thingy
  add_theme_support('automatic-feed-links');


  // Title tag
  add_theme_support('title-tag');

  // Enable support for HTML5 markup.
  add_theme_support('html5', array('comment-list',
      'comment-form',
      'search-form',
      'gallery',
      'caption'

    ));

  /*
    * POST FORMATS
    * Ahhhh yes, the wild and wonderful world of Post Formats.
    * I've never really gotten into them but I could see some
    * situations where they would come in handy. Here's a few
    * examples: https://www.competethemes.com/blog/wordpress-post-format-examples/
    *
    * This theme doesn't use post formats per se but we need this
    * to pass the theme check.
    *
    * We may add better support for post formats in the future.
    *
    * If you want to use them in your project, do so by all means.
    * We won't judge you. Ok, maybe a little bit.
    *
    */

  add_theme_support('post-formats', array('aside', // title less blurb
      'gallery', // gallery of images
      'link', // quick link to other site
      'image', // an image
      'quote', // a quick quote
      'status', // a Facebook like status update
      'video', // video
      'audio', // audio
      'chat' // chat transcript

    ));
}

/* end theme support */
