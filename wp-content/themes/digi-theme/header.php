<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="dns-prefetch" href="//fonts.googleapis.com" />
	<title><?php echo wp_get_document_title(); ?></title>
  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
  <link href="https://fonts.googleapis.com/css2?family=Baloo+2:wght@400;700&family=Lato:ital,wght@0,300;0,400;0,700;0,900;1,400;1,700&display=swap" rel="stylesheet">
  <link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo("template_url"); ?>/img/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo("template_url"); ?>/img/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo("template_url"); ?>/img/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo("template_url"); ?>/img/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo("template_url"); ?>/img/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo("template_url"); ?>/img/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo("template_url"); ?>/img/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo("template_url"); ?>/img/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo("template_url"); ?>/img/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="<?php bloginfo("template_url"); ?>/img/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo("template_url"); ?>/img/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo("template_url"); ?>/img/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo("template_url"); ?>/img/favicon-16x16.png">
  <link rel="manifest" href="<?php bloginfo("template_url"); ?>/img/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="<?php bloginfo("template_url"); ?>/img/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({"gtm.start": new Date().getTime(),event:"gtm.js"});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!="dataLayer"?"&l="+l:"";j.async=true;j.src="https://www.googletagmanager.com/gtm.js?id="+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,"script","dataLayer","GTM-TND8VFH");</script>
  <!-- End Google Tag Manager -->
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TND8VFH" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
	<svg style="display: none;">
    <?php get_template_part("templates/svg-symbols") ?>
	</svg>
	<header itemscope itemtype="http://schema.org/WPHeader">
		<div class="container flex-container d-flex justify-content-between">
			<div itemscope itemtype="http://schema.org/Organization" id="logo">
				<a itemprop="url" href="<?php echo bloginfo('url') ?>">
					<img itemprop="logo" src="<?php the_field("logo","option"); ?>" alt="inflo logo">
				</a>
      </div>
      <?php if(!is_page_template("landing.php")): ?>
        <div class="navbar-expand-lg">
          <nav id="site-nav" class="collapse navbar-collapse" itemscope itemtype="http://schema.org/SiteNavigationElement">
            <?php get_template_part("templates/header-buttons"); ?>
            <?php wp_nav_menu([ 'theme_location' => 'primary-menu', 'menu_class' => 'main-menu nav navbar', 'container' => '']); ?>
          </nav>
        </div>
        <?php get_template_part("templates/header-buttons"); ?>
        <div id="burger-menu" data-toggle="collapse" data-target="#site-nav" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <div id="burger-icon"><span></span><span></span><span></span><span></span><span></span><span></span></div>
        </div>
      <?php endif; ?>
    </div>
  </header>
  <main class="main-content">
