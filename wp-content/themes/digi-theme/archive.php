<?php
  get_header();
  $tag = get_queried_object();
  remove_all_filters("posts_orderby");
  $paged = ( get_query_var( "paged" ) ) ? get_query_var( "paged" ) : 1;
  $args = array(
    "post_type" => array("blog","webinars", "conversations", "case-studies"),
    "posts_per_page" => -1,
    "tag__in" => $tag->term_id,
    "paged" => $paged
  );
  $blogs = new WP_Query( $args );
  if ( $blogs->have_posts()):
?>
  <div class="container">
    <h1 class="h2"><?php single_tag_title(); ?></h1>
    <div class="row justify-content-md-center">
      <div class="col-lg-6 col-md-8">
        <?php get_template_part("searchform"); ?>
      </div>
    </div>
    <div class="resource-main-container">
      <div class="row justify-content-center">
        <?php
           while ($blogs->have_posts()) : $blogs->the_post();
            get_template_part("templates/cards/resource-card");
          endwhile;?>
      </div>
    </div>
  </div>
<?php
  get_template_part("templates/resource-extra");
  endif; wp_reset_postdata();
  get_footer();
?>
