<?php
/*
 Template Name: Trial Page Step 2
*/
?>


<?php get_header(); ?>

	<div id="content">

		<main id="main" class="clearfix" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

        <div class="trial-background">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-12 col-md-12 col-lg-10 col-xl-9">
                <div class="demo-contact-form demo-contact-form--large-margin">
                  <?php echo do_shortcode( '[contact-form-7 id="1398" title="Start Trial (2 of 2)"]' ); ?>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
		</main>

	</div>

<?php get_footer(); ?>
