<?php
/*
 Template Name: Trial Page Version 3 US
*/
?>

<?php get_header('standard'); ?>
  <div class="row">
    <div class="col-12 col-md-12 col-lg-5 v__banner">
      <div class="v__banner__content">
        <a class="vertical__banner__logo" href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/library/src/img/TR-Logo.png" alt="Thomson Reuters" /></a>
        <p class="header__subtitle vertical__banner__subtitle">Using Inflo is free. Forever.</p>
      </div>
    </div>

    <div class="col-12 col-md-12 col-lg-7">
      <div class="signup__wrapper">
        <h1 class="header__title">The future of audit and accounting, now.</h1>
        <p class="hp-section__subtitle">Now you can get hands-on with our next-generation software for free. With Inflo we offer you unlimited planning data loads, giving immediate value to your clients and improving your planning work. You are in complete control, only upgrade when you want to and to the level you need.</p>
        <?php echo do_shortcode( '[contact-form-7 id="1520" title="Trial V3 US" html_class="versionthree__signup__form"]' ); ?>

      </div>
    </div>
  </div>
<?php get_footer(); ?>
