<?php
/*
 Template Name: Contact Page
*/
?>

<?php get_header(); ?>

	<div id="content" class="container section-body section-body--sidebar">

    <div class="row">

      <main id="main" class="col-12 col-md-8 col-lg-8" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

          <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

          	<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

          		<section class="entry-content clearfix" itemprop="articleBody">
                <div class="row">
                  <div class="col-12 col-lg-5">
                    <div class="row">
                      <div class="col contact-page__section">
                        <h2 class="contact-page__heading"><?php the_field('contact_details_title') ?></h2>
                        <?php if( get_field('contact_details_text') ): ?>
                          <p><?php the_field('contact_details_text'); ?></p>
                        <?php endif; ?>

                        <?php if( get_field('contact_details_telephone') ): ?>
                        <p><b>T:</b> <a href="tel:<?php the_field('contact_details_telephone'); ?>"><?php the_field('contact_details_telephone'); ?></a></p>
                        <?php endif; ?>

                        <?php if( get_field('contact_details_fax') ): ?>
                        <p><b>F:</b> <?php the_field('contact_details_fax'); ?> </p>
                        <?php endif; ?>

                        <?php if( get_field('contact_details_email') ): ?>
                        <p><b>E:</b> <a href="mailto:<?php the_field('contact_details_email'); ?>"><?php the_field('contact_details_email'); ?></a></p>
                        <?php endif; ?>

                        <?php if( get_field('contact_details_address') ): ?>
                          <address>
                            <?php the_field('contact_details_address'); ?>
                          </address>
                        <?php endif; ?>

                        <div class="contact-page__form contact-page__form--mobile">
                          <?php echo do_shortcode( '[contact-form-7 id="4" title="General Enquiries"]' ); ?>
                          <p class="small-txt"><?php the_field('gdpr_disclaimer'); ?></p>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col contact-page__section">
                        <h2 class="contact-page__heading"><?php the_field('connect_title'); ?></h2>
                        <?php if( get_field('connect_additional_text') ): ?>
                          <p><?php the_field('connect_additional_text'); ?></p>
                        <?php endif; ?>
                        <?php include( 'templates/social-media-links.php' ); ?>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-lg-7 contact-page__form contact-page__form--desktop">
                    <?php echo do_shortcode( '[contact-form-7 id="4" title="General Enquiries"]' ); ?>
                    <p class="small-txt"><?php the_field('gdpr_disclaimer'); ?></p>
                  </div>
                </div>

          		</section> <?php // end article section ?>

          		<footer class="article-footer clearfix">

                <?php if( get_field('cta_button_url') ): ?>
                  <div class="row">
                    <div class="col cta">
                      <p class="cta__text"><?php the_field('call_to_action'); ?></p>
                      <a href="<?php the_field('cta_button_url'); ?>" class="btn btn--orange-500"><?php the_field('cta_button_text'); ?></a>
                    </div>
                  </div>
                <?php endif; ?>

          		</footer>

          	</article>

          <?php endwhile; endif; ?>


        </main>

        <div class="col-12 col-md-4 col-lg-4">
  	      <?php get_sidebar(); ?>
        </div>

        </div>



  	</div>

  <?php get_footer(); ?>
