function initMap() {
  var locations = [
      ['<b> AMH workspace ltd</b> <br>   3 Bridgewater Road<br>  Hertburn<br>   Washington<br>  Tyne & Wear<br>  NE37 2SG ', 54.9079061, -1.5142965, 1],
    ];

    var markerImage = {
      url:  "http://amhworkspace.com/amh/wp-content/themes/amh-theme/library/dist/img/marker.png",
        size: new google.maps.Size(29, 40),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(15, 40)
      }

    var mapStyles = [
        {
            "featureType": "administrative",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#444444"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [
                {
                  "color": "#f2f2f2"
                }
            ]
        },

        {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "simplified"
                }
            ]
        },

        {
            "featureType": "poi",
            "elementType": "park",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },

        {
            "featureType": "road",
            "elementType": "geometry.fill",
            "stylers": [
                {
                   "saturation": -100
                },
                {
                    //"lightness": 45
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "labels.icon",
            "stylers": [
                {
                   "saturation": -100
                }
            ]
        },

        {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "simplified"
                },
            ]
        },
        {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [
                {
                  "visibility": "off"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "all",
            "stylers": [
                {
                    "color": "#bee0ed"
                },
                {
                    "visibility": "on"
                }
            ]
        },
    ];

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 11,
      center: new google.maps.LatLng(54.9079061, -1.5142965),
      mapTypeId: google.maps.MapTypeId.ROADMAP,

      scrollwheel: false,
      minZoom: 4,
      mapTypeControl: false,
      styles: mapStyles

    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      marker.setIcon(markerImage);

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
}
}
