// Author: Adam Hartwig <adam@adamhartwig.co.uk>

window.device = getDevice();
function getDevice(docReady) {
	
	var t = this, tmp = {}, c = '', d = {};

	t.init = function()
	{
		if(docReady===true)
		{
			t.getReadyProps();
		}else{
			t.beforeProps();
			t.registerRetina();
			t.registerOS();
			t.registerBrowser();
			t.registerPlatform();
			t.registerTouch();
			t.classes();
		}
	};

	t.getReadyProps = function()
	{
		t.afterProps();
		t.registerCapabilities();
		t.registerVendorPrefix();
		tmp = [];
	};

	t.beforeProps = function()
	{
		tmp.ver = navigator.appVersion;
		tmp.ua = navigator.userAgent;
		tmp.msp = navigator.msPointerEnabled;
	};

	t.registerRetina = function()
	{
		d.retina = ((window.devicePixelRatio === undefined)?1:window.devicePixelRatio)>1;
	};

	t.registerOS = function() {d.os = t.checkOS();};
	t.registerBrowser = function()
	{
		d.rubbish = false;
		d.browser = t.checkBrowser();
		if(d.browser==='ie')d.rubbish = true;
	};

	t.registerPlatform = function()
	{
		tmp.plat = {};
		tmp.plat.iPhone = tmp.ua.indexOf('iPhone')!==-1;
		tmp.plat.iPod = tmp.ua.indexOf('iPod')!==-1;
		tmp.plat.iPad = tmp.ua.indexOf('iPad')!==-1;
		tmp.plat.bb = tmp.ua.indexOf('BlackBerry')!==-1;
		tmp.plat.wp = tmp.ua.indexOf('Windows Phone')!==-1;
		tmp.plat.android = tmp.ua.toLowerCase().indexOf('android')!==-1;
		tmp.plat.androidMobile = tmp.plat.android&&(tmp.ua.toLowerCase().indexOf('mobile')!==-1);
		tmp.plat.iOS = tmp.plat.iPhone||tmp.plat.iPod||tmp.plat.iPad;

		d.mobile = (tmp.plat.iPhone||tmp.plat.iPod||tmp.plat.bb||tmp.plat.wp||tmp.plat.android&&tmp.plat.androidMobile);
		d.touch = (tmp.plat.iPhone||tmp.plat.iPod||tmp.plat.bb||tmp.plat.wp||tmp.plat.android||tmp.plat.iPad);
		d.desktop = !d.touch;
		d.platform = tmp.plat;
		if(d.platform.android){d.platform.androidVersion = getAndroidVersion();}
		if(d.platform.android)
		{
			if(d.platform.androidVersion<=4.3)
			{
				d.rubbish = true;
			}
		}
	};

	function getAndroidVersion(ua) {
		ua = ua || navigator.userAgent;
		var match = ua.match(/Android\s([0-9\.]*)/);
		return match ? match[1] : false;
	}

	t.registerTouch = function()
	{
		if(d.touch)
		{
			d.down = tmp.msp?'MSPointerDown':'touchstart';
			d.up = tmp.msp?'MSPointerUp':'touchend';
			d.over = tmp.msp?'MSPointerDown':'touchstart';
			d.out = tmp.msp?'MSPointerUp':'touchend';
			d.instaClick = tmp.msp?'MSPointerDown':'touchstart';
			d.touchMove = tmp.msp?'MSPointerMove':'touchmove';
		}else{
			d.down = 'mousedown';
			d.up = 'mouseup';
			d.over = 'mouseenter';
			d.out = 'mouseleave';
			d.instaClick = 'click';
		}
	};

	t.registerVendorPrefix = function()
	{
		tmp.prefix = ('webkitAnimation' in document.body.style) ? '-webkit-' : ('MozAnimation' in document.body.style)? '-moz-' : ('MsAnimation' in document.body.style? '-ms-' : '');
		device.prefix = tmp.prefix;
	};

	t.classes = function()
	{
		if(d.retina)c+=' retina';
		if(d.mobile)c+=' mobile';
		if(d.touch)c+=' touchscreen';
		if(d.desktop)c+=' desktop';
		document.getElementsByTagName('html')[0].className+=c;
	};

	t.checkOS = function()
	{
		if(tmp.ver.indexOf('Win')!==-1)return 'windows';
		if(tmp.ver.indexOf('Mac')!==-1)return 'mac';
		if(tmp.ver.indexOf('X11')!==-1||tmp.ver.indexOf('Linux')!==-1)return 'other';
		return 'other';
	};

	t.checkBrowser = function()
	{
		tmp.browser = 'other';

		if(window.chrome!==undefined)tmp.browser = 'chrome';
		if(navigator.userAgent.indexOf("Safari") > -1&&tmp.browser!=='chrome'&&!d.touch)tmp.browser = 'safari';
		if(detectIE())
		{
			tmp.browser = 'ie';
			d.browserVersion = getIEVersion();
		}
		if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1)tmp.browser = 'firefox';
		return tmp.browser;
	};

	t.init();

	return d;
}

function getIEVersion()
{
	var str = String(document.getElementsByTagName('html')[0].className),
		ver = 11;

	if(str.search('gt-ie9')>-1)
	{ver = 11;}else if(str.search('lt-ie10')>-1)
	{ver = 9;}else if(str.search('lt-ie9')>-1)
	{ver = 8;}else if(str.search('lt-ie8')>-1)
	{ver = 7;}else if(str.search('lt-ie7')>-1){ver = 6;}

	return ver;
}

function detectIE() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    var trident = ua.indexOf('Trident/');

    if (msie > 0) {
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    if (trident > 0) {
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    // other browser
    return false;
}