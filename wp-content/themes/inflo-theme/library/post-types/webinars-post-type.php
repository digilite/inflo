<?php
// let's create the function for the custom type
function webinars_post_type() {
	// creating (registering) the custom type
	register_post_type( 'webinars', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Webinars', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Webinar', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Webinars', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Webinar', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Webinars', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Webinar', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Webinar', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Webinar', 'bonestheme' ), /* Search Custom Type Title */
			'not_found' =>  __( 'Nothing found in the Database.', 'bonestheme' ), /* This displays if there are no entries yet */
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the webinars custom post type', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
			'rewrite'	=> array( 'slug' => 'resources/webinars', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'resources/webinars', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'revisions', 'sticky')
		) /* end of options */
	); /* end of register post type */

}

	// adding the function to the Wordpress init
	add_action( 'init', 'webinars_post_type');

	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/

	// now let's add custom categories (these act like categories)
	register_taxonomy( 'webinar_custom_cat',
		array('webinars'), /* if you change the name of register_post_type( 'webinars', then you have to change this */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Webinar Categories', 'bonestheme' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Category', 'bonestheme' ), /* single taxonomy name */
				'search_items' =>  __( 'Search Categories', 'bonestheme' ), /* search title for taxomony */
				'all_items' => __( 'All Categories', 'bonestheme' ), /* all title for taxonomies */
				'parent_item' => __( 'Parent Category', 'bonestheme' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Parent Category:', 'bonestheme' ), /* parent taxonomy title */
				'edit_item' => __( 'Edit Category', 'bonestheme' ), /* edit custom taxonomy title */
				'update_item' => __( 'Update Category', 'bonestheme' ), /* update title for taxonomy */
				'add_new_item' => __( 'Add New Category', 'bonestheme' ), /* add new title for taxonomy */
				'new_item_name' => __( 'New Category Name', 'bonestheme' ) /* name title for taxonomy */
			),
			'show_admin_column' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'resources/webinar-categories' ),
		)
	);

?>
