<?php
/* Bones Custom Post Type Example
This page walks you through creating
a custom post type and taxonomies. You
can edit this one or copy the following code
to create another one.

I put this in a separate file so as to
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

Developed by: Eddie Machado
URL: http://themble.com/bones/
*/

// let's create the function for the custom type
function leadership_team_post_type() {
	// creating (registering) the custom type
	register_post_type( 'leadership_team', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Leadership Team', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Team Member Post', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Team Members Posts', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add Team Member', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Team Member', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Team Member Post', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Team Member Post', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Team Member', 'bonestheme' ), /* Search Custom Type Title */
			'not_found' =>  __( 'Nothing found in the Database.', 'bonestheme' ), /* This displays if there are no entries yet */
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the leadership team post type', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
			'rewrite'	=> array( 'slug' => 'about-inflo/leadership-team', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'about-inflo/leadership-team', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'revisions', 'sticky')
		) /* end of options */
	); /* end of register post type */

	/* this adds your post categories to your custom post type */
	register_taxonomy_for_object_type( 'category', 'leadership_team' );
	/* this adds your post tags to your custom post type */
	//register_taxonomy_for_object_type( 'post_tag', 'leadership_team' );

}

	// adding the function to the Wordpress init
	add_action( 'init', 'leadership_team_post_type');

  // now let's add custom categories (these act like categories)
  register_taxonomy( 'leadership_team_cat',
    array('leadership_team'), /* if you change the name of register_post_type( 'case-studies', then you have to change this */
    array('hierarchical' => true,     /* if this is true, it acts like categories */
      'labels' => array(
        'name' => __( 'Team Role', 'bonestheme' ), /* name of the custom taxonomy */
        'singular_name' => __( 'Role', 'bonestheme' ), /* single taxonomy name */
        'search_items' =>  __( 'Search Roles', 'bonestheme' ), /* search title for taxomony */
        'all_items' => __( 'All Roles', 'bonestheme' ), /* all title for taxonomies */
        'parent_item' => __( 'Parent Role', 'bonestheme' ), /* parent title for taxonomy */
        'parent_item_colon' => __( 'Parent Role:', 'bonestheme' ), /* parent taxonomy title */
        'edit_item' => __( 'Edit Role', 'bonestheme' ), /* edit custom taxonomy title */
        'update_item' => __( 'Update Role', 'bonestheme' ), /* update title for taxonomy */
        'add_new_item' => __( 'Add New Role', 'bonestheme' ), /* add new title for taxonomy */
        'new_item_name' => __( 'New Role', 'bonestheme' ) /* name title for taxonomy */
      ),
      'show_admin_column' => true,
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => array( 'slug' => 'about-inflo/leadership-team/role' ),
    )
  );


?>
