<?php get_header(); ?>

	<div id="content" class="section-body section-body--features-single">

      <main id="main" class="" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">



        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        	<article id="post-<?php the_ID(); ?>" <?php post_class('features-holder container clearfix'); ?> role="article" itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting">

              <div class="row">
                <div class="col">


            <?php

            // check if the flexible content field has rows of data
            if( have_rows('feature_row') ):

                 // loop through the rows of data
                while ( have_rows('feature_row') ) : the_row();

                    // Left text and right image layout
                    if( get_row_layout() == 'text_image' ):

                      $img = get_sub_field('feature_image');
                      $size = 'features-img';

                      echo '<div class="row feature feature--txt-img">
                              <div class="col-12 col-md-6 feautre__text-holder">
                                <h2 class="feature__title">';
                      echo         the_sub_field('feature_title');
                      echo      '</h2>';
                      echo       the_sub_field('feature_content');
                      echo   '</div>
                            <div class="col-12 col-md-6 feature__img-holder feature__img-holder--right">';
                      echo     wp_get_attachment_image( $img, $size );
                      echo   '</div>
                            </div>';

                    // Left image right text layout
                    elseif( get_row_layout() == 'image_text' ):

                      $img = get_sub_field('feature_image');
                      $size = 'features-img';

                      echo '<div class="row feature feature--img-txt">
                              <div class="col-12 col-md-6 feature__img-holder feature__img-holder--left">';
                      echo       wp_get_attachment_image( $img, $size );
                      echo    '</div>
                              <div class="col-12 col-md-6 feature__text-holder">
                                <h2 class="feature__title">';
                      echo         the_sub_field('feature_title');
                      echo      '</h2>';
                      echo         the_sub_field('feature_content');
                      echo      '</div>
                            </div>';

                    endif;

                endwhile;

            else :

                // no layouts found

            endif;

            ?>
            </div>
            </div>

        	</article> <?php // end article ?>


          <?php if( get_field('quote') ): ?>
            <div class="feat-quote">
              <div class="container">
                <div class="row">
                  <div class="col-12 col-md-4 col-lg-3">
                    <?php

                      $image = get_field('quote_logo');
                      $size = 'quote-logo-img'; // (thumbnail, medium, large, full or custom size)

                      if( $image ) {
                        echo wp_get_attachment_image( $image, $size );
                      }

                    ?>
  								</div>
                  <div class="col-12 col-md-8 col-lg-9">
                    <blockquote>
                      <p><?php the_field('quote'); ?></p>
                      <footer>
                      <cite><?php the_field('quote_author'); ?></cite>
                      </footer>
                    </blockquote>
                  </div>
                </div>
              </div>
            </div>
          <?php endif; ?>


					<div class="features-cta">
						<div class="features-cta__green"></div>
            <div class="container">
              <div class="features-cta__content row">
                <div class="col-12 col-md-8 features-cta__text-holder">
                  <div class="features-cta__text">
                    <h2 class="features-cta__title"><?php the_field('features_call_to_action'); ?></h2>
                    <p class="features-cta__subtitle"><?php the_field('feautres_cta_extra_copy'); ?></p>
                    <a href="<?php the_field('features_call_to_action_button_url'); ?>" class="btn btn--orange-500"><?php the_field('features_call_to_action_button_text'); ?></a>
                  </div>
    						</div>
    						<div class="col-12 col-md-4 features-cta__related-holder">
                  <div class="features-cta__related">
                    <h2 class="features-cta__related-title">People interested in data ingestion also looked at:</h2>

                    <?php
                      $post_objects = get_field('related_features');

                      if( $post_objects ): ?>
                          <ul>
                          <?php foreach( $post_objects as $post_object): ?>
                              <li>
                                  <a href="<?php echo get_permalink($post_object->ID); ?>"><?php echo get_the_title($post_object->ID); ?></a>
                              </li>
                          <?php endforeach; ?>
                          </ul>
                      <?php endif;
                    ?>

                  </div>
    						</div>
              </div>
              </div>
						<div class="features-cta__white"></div>
					</div>

        <?php endwhile; endif; ?>


			</main>

	</div>

<?php get_footer(); ?>
