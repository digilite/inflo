<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class( 'single-post' ); ?> role="article">

		<header class="single-post__header">

      <h1 class="single-post__title"><?php the_title(); ?></h1>

      <?php
        // Get the byline if blog archive page
        if ( is_singular('blog')): ?>
          <p class="single-post__byline">
            <?php printf( __( 'Posted <time class="updated" datetime="%1$s" itemprop="datePublished">%2$s</time>', 'bonestheme' ), get_the_time( 'Y-m-j' ), get_the_time(get_option('date_format')) );
           ?>
          </p>
      <?php endif; ?>

		</header> <?php // end article header ?>

        <section class="entry-content clearfix" itemprop="articleBody">

        	<?php the_content(); ?>

        </section> <?php // end article section ?>

		<footer class="article-footer">
      <?php if( get_field('cta_button_url') ): ?>
        <div class="row">
          <div class="col cta">
            <p class="cta__text"><?php the_field('call_to_action'); ?></p>
            <a href="<?php the_field('cta_button_url'); ?>" class="btn btn--orange-500"><?php the_field('cta_button_text'); ?></a>
          </div>
        </div>
      <?php endif; ?>
		</footer> <?php // end article footer ?>

	</article> <?php // end article ?>

<?php endwhile; endif; ?>
