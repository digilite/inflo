<?php

if (have_posts()) :

$counter = 1;

while (have_posts()) :

the_post();

if( $counter == 1 && !is_paged()) { ?>

  <a class="conversation-post-full-width col-12" href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
    <article id="post-<?php the_ID(); ?>" <?php post_class('row'); ?> role="article">
      <div class="col-12 col-md-6">
        <div class="conversation-post-full-width__img conversation-post-full-width__img--full-width">
          <?php
            $image = get_field('conversation_video_thumbnail');
            $size = 'conversations-thumbnail'; // (thumbnail, medium, large, full or custom size)
              if ($image) {
                  echo wp_get_attachment_image($image, $size);
              }
          ?>
        </div>
      </div>
      <div class="conversation-post-full-width__content col-12 col-md-6">
        <h3 class="conversation-post-full-width__title"><?php the_title(); ?></h3>
        <?php the_excerpt(); ?>

        <p href="<?php the_permalink() ?>" class="btn btn--orange-500" rel="bookmark" title="<?php the_title_attribute(); ?>">Read More</p>

      </div>
    </article>
  </a>


<?php }  elseif (has_term('coming-soon', 'conversation_custom_cat')) { ?>

  <a class="conversation-post conversation-post--coming-soon col-12 col-md-6 col-xl-3" rel="bookmark" title="<?php the_title_attribute(); ?>">
      <article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">

        <div class="conversation-post__img">
          <?php
            $image = get_field('conversation_video_thumbnail');
            $size = 'conversations-thumbnail'; // (thumbnail, medium, large, full or custom size)
              if ($image) {
                  echo wp_get_attachment_image($image, $size);
              }
          ?>
        </div>

        <section class="conversation-post__text">
          <h3 class="conversation-post__title">Coming soon...<?php the_title(); ?></h3>
        </section>

      </article>
    </a>

  <?php }

     else { ?>

      <a class="conversation-post conversation-post--live col-12 col-md-6 col-xl-3" href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
          <article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">

            <div class="conversation-post__img">
              <?php
                $image = get_field('conversation_video_thumbnail');
                $size = 'conversations-thumbnail'; // (thumbnail, medium, large, full or custom size)
                  if ($image) {
                      echo wp_get_attachment_image($image, $size);
                  }
              ?>
            </div>

            <section class="conversation-post__text">
              <h3 class="conversation-post__title"><?php the_title(); ?></h3>
            </section>

          </article>
        </a>


<?php }


$counter++;

endwhile;

endif;

?>

<?php get_template_part( 'templates/post-navigation'); ?>
