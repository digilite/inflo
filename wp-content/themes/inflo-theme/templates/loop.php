<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

		<header class="article-header">

			<?php get_template_part( 'templates/header', 'title'); ?>

      <p class="__byline entry-meta vcard">
        <?php printf( __( 'Posted <time class="updated" datetime="%1$s" itemprop="datePublished">%2$s</time>', 'bonestheme' ), get_the_time( 'Y-m-j' ), get_the_time(get_option('date_format')) );
                ?>
      </p>

		</header> <?php // end article header ?>

		<section class="entry-content clearfix" itemprop="articleBody">

			<?php the_content(); ?>

		</section> <?php // end article section ?>

		<footer class="article-footer clearfix">

		</footer>

		<?php comments_template(); ?>

	</article>

<?php endwhile; endif; ?>
