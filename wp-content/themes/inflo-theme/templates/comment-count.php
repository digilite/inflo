<p class="footer-comment-count">
	<?php comments_number( __( '<span>No</span> Comments', 'maintheme' ), __( '<span>One</span> Comment', 'maintheme' ), __( '<span>%</span> Comments', 'maintheme' ) );?>
</p>
