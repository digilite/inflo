<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class( 'archive-post' ); ?> role="article">

		<header class="archive-post__header">

      <h3 class="archive-post__title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
      <?php
        // Get the byline if blog archive page
        if ( is_post_type_archive('blog') || is_tax( 'blog_custom_cat')): ?>
          <p class="archive-post__byline">
            <?php printf( __( 'Posted <time class="updated" datetime="%1$s" itemprop="datePublished">%2$s</time>', 'bonestheme' ), get_the_time( 'Y-m-j' ), get_the_time(get_option('date_format')) );
           ?>
          </p>
      <?php endif; ?>
		</header>

		<section class="archive-post__content">

			<?php the_excerpt(); ?>

      <?php
        // Get watch now button if webinar
        if ( is_post_type_archive('webinars') || is_tax( 'webinars_custom_cat')): ?>
        <a href="<?php the_field('webinar_link'); ?>" class="btn btn--green-500" rel="bookmark" title="<?php the_title_attribute(); ?>">Watch now</a>
      <?php endif; ?>
      <a href="<?php the_permalink() ?>" class="btn btn--orange-500" rel="bookmark" title="<?php the_title_attribute(); ?>">Read More</a>

		</section>

	</article>

<?php endwhile; endif; ?>

<?php get_template_part( 'templates/post-navigation'); ?>
