<div class="social-links">
  <?php if( get_field('twitter_btn', options) ): ?>
    <a href="<?php the_field('twitter_btn', options) ?>" target="_blank">
      <div class="social-icon social-icon--twitter">
      <body>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 44 44" enable-background="new 0 0 44 44"><path class="icon" fill="#92C849" d="M43.7 9.2c-1.6.7-3.3 1.2-5.1 1.4 1.8-1.1 3.3-2.8 3.9-4.9-1.7 1-3.6 1.8-5.7 2.2-1.6-1.8-3.9-2.9-6.5-2.9-4.9 0-8.9 4-8.9 8.9 0 .7.1 1.4.2 2-7.4-.4-14-3.9-18.4-9.3-.8 1.3-1.2 2.8-1.2 4.5 0 3.1 1.6 5.8 4 7.4-1.5 0-2.8-.4-4-1.1v.1c0 4.3 3.1 7.9 7.1 8.7-.7.2-1.5.3-2.3.3-.6 0-1.1-.1-1.7-.2 1.1 3.5 4.4 6.1 8.3 6.2-3 2.4-6.9 3.8-11.1 3.8-.7 0-1.4 0-2.1-.1 3.9 2.5 8.6 4 13.7 4 16.4 0 25.3-13.6 25.3-25.3v-1.2c1.7-1.2 3.3-2.7 4.5-4.5z"/></svg>
      </body>
    </div>

    </a>
  <?php endif; ?>

  <?php if( get_field('instagram_btn', options) ): ?>
    <a href="<?php the_field('instagram_btn', options) ?>" target="_blank">
      <div class="social-icon social-icon--instagram">
        <body>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 44 44" enable-background="new 0 0 44 44"><path class="icon" fill-rule="evenodd" clip-rule="evenodd" fill="#92C849" d="M38.6 18.6h-3.8c.3 1.1.4 2.2.4 3.3 0 7.4-6 13.3-13.3 13.3-7.4 0-13.3-6-13.3-13.3 0-1.2.2-2.3.4-3.3h-3.7v18.4c0 .9.7 1.7 1.7 1.7h30c.9 0 1.7-.7 1.7-1.7v-18.4zm0-11.6c0-.9-.7-1.7-1.7-1.7h-5c-.9 0-1.6.7-1.6 1.7v5c0 .9.7 1.7 1.7 1.7h5c.9 0 1.7-.7 1.7-1.7v-5zm-16.6 6.6c-4.6 0-8.3 3.7-8.3 8.3 0 4.6 3.7 8.3 8.3 8.3s8.3-3.7 8.3-8.3c0-4.5-3.7-8.3-8.3-8.3m16.6 30h-33.3c-2.8 0-5-2.2-5-5v-33.3c0-2.8 2.2-5 5-5h33.3c2.8 0 5 2.2 5 5v33.3c0 2.8-2.2 5-5 5"/></svg>
        </body>
      </div>

    </a>
  <?php endif; ?>

  <?php if( get_field('facebook_btn', options) ): ?>
    <a href="<?php the_field('facebook_btn', options) ?>" target="_blank">
      <div class="social-icon social-icon--facebook">
        <body>
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22.8 44" enable-background="new 0 0 22.8 44"><path class="icon" d="M14.8 44v-20.1h6.7l1-7.8h-7.7v-5c0-2.3.6-3.8 3.9-3.8h4.1v-7c-.7-.1-3.2-.3-6-.3-6 0-10.1 3.6-10.1 10.3v5.8h-6.7v7.8h6.8v20.1h8z"/></svg>      </div>

    </a>
  <?php endif; ?>

  <?php if( get_field('linkedin_btn', options) ): ?>
  <a href="<?php the_field('linkedin_btn', options) ?>" target="_blank">
    <div class="social-icon social-icon--linkedin">
      <body>
        <svg enable-background="new 0 0 44 43.9" viewBox="0 0 44 43.9" xmlns="http://www.w3.org/2000/svg"><path class="icon" d="m.7 14.6h9.1v29.3h-9.1zm4.6-14.6c2.9 0 5.3 2.4 5.3 5.3s-2.4 5.3-5.3 5.3-5.3-2.4-5.3-5.3 2.4-5.3 5.3-5.3"/><path class="icon" d="m15.6 14.6h8.7v4h.1c1.2-2.3 4.2-4.7 8.6-4.7 9.2 0 10.9 6.1 10.9 14v16.1h-9.1v-14.3c0-3.4-.1-7.8-4.7-7.8-4.7 0-5.5 3.7-5.5 7.5v14.5h-9.1v-29.3z"/></svg>
      </body>
    </div>

  </a>
  <?php endif; ?>

</div>
