<div class="row">
  <div class="col-12 col-lg-5">
    <div class="row">
      <div class="col contact-page__section">
        <h2 class="contact-page__heading"><?php the_field('contact_details_title') ?></h2>
        <?php if( get_field('contact_details_text') ): ?>
          <p><?php the_field('contact_details_text'); ?></p>
        <?php endif; ?>

        <?php if( get_field('contact_details_telephone') ): ?>
        <p><b>T:</b> <a href="tel:<?php the_field('contact_details_telephone'); ?>"><?php the_field('contact_details_telephone'); ?></a></p>
        <?php endif; ?>

        <?php if( get_field('contact_details_fax') ): ?>
        <p><b>F:</b> <?php the_field('contact_details_fax'); ?> </p>
        <?php endif; ?>

        <?php if( get_field('contact_details_email') ): ?>
        <p><b>E:</b> <a href="mailto:<?php the_field('contact_details_email'); ?>"><?php the_field('contact_details_email'); ?></a></p>
        <?php endif; ?>

        <?php if( get_field('contact_details_address') ): ?>
          <address>
            <?php the_field('contact_details_address'); ?>
          </address>
        <?php endif; ?>

        <div class="contact-page__form contact-page__form--mobile">
          <?php echo do_shortcode( '[contact-form-7 id="4" title="General Enquiries"]' ); ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col contact-page__section">
        <h2 class="contact-page__heading"><?php the_field('connect_title'); ?></h2>
        <?php if( get_field('connect_additional_text') ): ?>
          <p><?php the_field('connect_additional_text'); ?></p>
        <?php endif; ?>
        <?php include( 'templates/social-media-links.php' ); ?>
      </div>
    </div>
  </div>
  <div class="col-12 col-lg-7 contact-page__form contact-page__form--desktop">
    <?php echo do_shortcode( '[contact-form-7 id="4" title="General Enquiries"]' ); ?>
  </div>
</div>
