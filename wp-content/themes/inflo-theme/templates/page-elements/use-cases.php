<div class="services-holder">
  <div class="service">
    <h2 class="service__title"><?php the_field('title_section_1'); ?></h2>
    <p class="service__subtitle"><?php the_field('subtitle_section_1'); ?></p>


    <?php if( have_rows('accordion_section_1') ): ?>

      <div class="">

        <?php while( have_rows('accordion_section_1') ): the_row();

          // vars
          $title = get_sub_field('accordion_title');
          $content = get_sub_field('accordion_content');
          $cta_txt = get_sub_field('accordion_call_to_action');
          $cta_btn = get_sub_field('accordion_call_to_action_button_text');
          $cta_url = get_sub_field('accordion_call_to_action_url');

        ?>

          <div class="accordion-holder">

          <button class="accordion">
            <p class="accordion__title">
              <?php echo $title; ?>
            </p>
          </button>

          <div class="panel">
            <div class="panel__content entry-content">

              <?php echo $content; ?>

              <?php if( get_sub_field('accordion_call_to_action_url') ): ?>
                <div class="cta">
                  <h2 class="cta__text"><?php echo $cta_txt; ?></h2>
                  <a href="<?php echo $cta_url; ?>" class="btn btn--orange-500"><?php echo $cta_btn; ?></a>
                </div>
              <?php endif; ?>

            </div>
          </div>

        </div>

        <?php endwhile; ?>

      </div>

    <?php endif; ?>
  </div>


  <div class="service">
    <h2 class="service__title"><?php the_field('title_section_2'); ?></h2>
    <p class="service__subtitle"><?php the_field('subtitle_section_2'); ?></p>


    <?php if( have_rows('accordion_section_2') ): ?>

      <div class="">

        <?php while( have_rows('accordion_section_2') ): the_row();

          // vars
          $title = get_sub_field('accordion_title');
          $content = get_sub_field('accordion_content');
          $cta_txt = get_sub_field('accordion_call_to_action');
          $cta_btn = get_sub_field('accordion_call_to_action_button_text');
          $cta_url = get_sub_field('accordion_call_to_action_url');

        ?>

          <div class="accordion-holder">

          <button class="accordion">
            <p class="accordion__title">
              <?php echo $title; ?>
            </p>
          </button>

          <div class="panel">
            <div class="panel__content entry-content">

              <?php echo $content; ?>

              <?php if( get_sub_field('accordion_call_to_action_url') ): ?>
                <div class="cta">
                  <h2 class="cta__text"><?php echo $cta_txt; ?></h2>
                  <a href="<?php echo $cta_url; ?>" class="btn btn--orange-500"><?php echo $cta_btn; ?></a>
                </div>
              <?php endif; ?>

            </div>
          </div>

        </div>

        <?php endwhile; ?>

      </div>

    <?php endif; ?>
  </div>


  <div class="service">
    <h3 class="service__title"><?php the_field('title_section_3'); ?></h3>
    <p class="service__subtitle"><?php the_field('subtitle_section_3'); ?></p>


    <?php if( have_rows('accordion_section_3') ): ?>

      <div class="">

        <?php while( have_rows('accordion_section_3') ): the_row();

          // vars
          $title = get_sub_field('accordion_title');
          $content = get_sub_field('accordion_content');
          $cta_txt = get_sub_field('accordion_call_to_action');
          $cta_btn = get_sub_field('accordion_call_to_action_button_text');
          $cta_url = get_sub_field('accordion_call_to_action_url');

        ?>

          <div class="accordion-holder">

          <button class="accordion">
            <p class="accordion__title">
              <?php echo $title; ?>
            </p>
          </button>

          <div class="panel">
            <div class="panel__content entry-content">

              <?php echo $content; ?>

              <?php if( get_sub_field('accordion_call_to_action_url') ): ?>
                <div class="cta">
                  <h3 class="cta__text"><?php echo $cta_txt; ?></h3>
                  <a href="<?php echo $cta_url; ?>" class="btn btn--orange-500"><?php echo $cta_btn; ?></a>
                </div>
              <?php endif; ?>

            </div>
          </div>

        </div>

        <?php endwhile; ?>

      </div>

    <?php endif; ?>
  </div>
</div>
