<?php
/*
 Template Name: Trial Page Version RSM
*/
?>

<?php get_header('standard'); ?>
  <div class="row">
    <div class="col-12 col-md-12 col-lg-5 v__banner">
      <div class="v__banner__content">
        <p class="header__subtitle vertical__banner__subtitle">With Inflo’s collaboration portal, transform your client interactions by taking control of project management with transparent workflow, user responsibilities and clear deadline management.
</p>
 </div>
    </div>

    <div class="col-12 col-md-12 col-lg-7">
      <div class="signup__wrapper">
        <h1 class="header__title">Get your free Inflo Collaboration access now</h1>
        <p class="hp-section__subtitle">You will also receive one free engagement access to Inflo Data Analytics modules
</p>

        <?php echo do_shortcode('[contact-form-7 id="2154" title="SignUp RSM"]'); ?>

      </div>
    </div>
  </div>
<?php get_footer(); ?>

