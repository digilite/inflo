<?php
/*
 Template Name: Trial Page Version ICAEW Webinar 
*/
?>

<?php get_header('standard'); ?>
  <div class="row">
    <div class="col-12 col-md-12 col-lg-5 v__banner">
      <div class="v__banner__content">
<p class="header__subtitle vertical__banner__subtitle">Join <a target="_blank" href="https://www.linkedin.com/in/andrewmoyser/">Andrew Moyser</a>, <a target="_blank" href="https://www.linkedin.com/in/paul-winwood/">Paul Winwood</a> and<a target="_blank" href="https://www.linkedin.com/in/rachel-davis-00590222/"> Rachel Davis<a></p>
        <p class="header__subtitle vertical__banner__subtitle"> to learn about their innovation reasons, implementation approaches and benefits achieved</p>
<p class="header__subtitle vertical__banner__subtitle">February 14th, 2020</p>
<p class="header__subtitle vertical__banner__subtitle">Midday to 1pm GMT</p>
<p class="header__subtitle vertical__banner__subtitle">Also available on demand</p>
      </div>
    </div>

    <div class="col-12 col-md-12 col-lg-7">
      <div class="signup__wrapper">
        <h1 class="header__title">ICAEW & Inflo Webinar</h1>
        <p class="hp-section__subtitle">Big Data Analytics – 3 Implementation Case Studies.</p>
        <p class="hp-section__subtitle">Three firm leaders share their stories of implementing big data analytics and the impact it can have on all sizes of business.</p>
        <?php echo do_shortcode( '[contact-form-7 id="1979" title="Trial V3 ICAEW_copy"]' ); ?>

      </div>
    </div>
  </div>
<?php get_footer(); ?>

