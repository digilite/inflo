<?php
/*
 Template Name: Login Page
*/
?>

<?php get_header(); ?>

	<div id="content" class="section-body section-body--content-bands">

      <main id="main" class="" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

        	<article id="post-<?php the_ID(); ?>" <?php post_class('content-band-holder container clearfix'); ?> role="article" itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting">

              <div class="row">
                <div class="col">
										<h2 class="login__title">Choose Territory</h2>
            		</div>
            	</div>

							<div class="row justify-content-center">
								<div class="col-4 territory">
									<a href="https://audit.inflosoftware.com/">
										<img src="<?php echo get_home_url(); ?>/inf/wp-content/uploads/2018/04/union-jack.png" alt="United Kingdom">
										<p>United Kingdom</p>
									</a>
								</div>
								<div class="col-4 territory">
									<a href="https://us.inflosoftware.com/">
										<img src="<?php echo get_home_url(); ?>/inf/wp-content/uploads/2018/04/usa.png" alt="United States">
										<p>United States</p>
									</a>
								</div>
								<div class="col-4 territory">
									<a href="https://au.inflosoftware.com/">
										<img src="<?php echo get_home_url(); ?>/inf/wp-content/uploads/2018/04/aus.png" alt="Australia">
										<p>Australia</p>
									</a>
								</div>
								<div class="col-4 territory">
									<a href="https://ca.inflosoftware.com/">
										<img src="<?php echo get_home_url(); ?>/inf/wp-content/uploads/2018/04/canada.png" alt="Canada">
										<p>Canada</p>
									</a>
								</div>
                <div class="col-4 territory">
									<a href="https://audit.inflosoftware.com/">
										<img src="<?php echo get_home_url(); ?>/inf/wp-content/uploads/2018/05/europe.png" alt="Europe">
										<p>Europe</p>
									</a>
								</div>
								<div class="col-4 territory">
									<a href="https://audit.inflosoftware.com/">
										<img src="<?php echo get_home_url(); ?>/inf/wp-content/uploads/2018/04/world.png" alt="Rest of World">
										<p>Rest of World</p>
									</a>
								</div>
							</div>

            <?php if( get_field('cta_button_url') ): ?>
              <div class="row">
                <div class="col cta cta--center">
                  <p class="cta__text"><?php the_field('call_to_action'); ?></p>
                  <a href="<?php the_field('cta_button_url'); ?>" class="btn btn--orange-500"><?php the_field('cta_button_text'); ?></a>
                </div>
              </div>
            <?php endif; ?>

        	</article> <?php // end article ?>

			</main>

	</div>

<?php get_footer(); ?>
