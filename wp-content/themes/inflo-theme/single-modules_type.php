<?php get_header(); ?>

	<div id="content" class="container section-body section-body--leadership-single">

    <div class="row">

      <main id="main" class="col-12 col-md-8 col-lg-8" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        	<article id="post-<?php the_ID(); ?>" <?php post_class('leadership-team-member clearfix'); ?> role="article" itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting">

              <div class="entry-content">

                <?php the_content(); ?>

              </div>
                  <?php
                    $post_objects = get_field('modules_key_features');

                    if( $post_objects ): ?>
                    <div class="key-features">
                      <h2 class="key-features__title">Key Features</h2>
                        <ul>
                        <?php foreach( $post_objects as $post_object): ?>
                            <li>
                                <a href="<?php echo get_permalink($post_object->ID); ?>"><?php echo get_the_title($post_object->ID); ?></a>
                            </li>
                        <?php endforeach; ?>
                        </ul>
                      </div>
                    <?php endif;
                  ?>

                <?php if( get_field('modules_quote') ): ?>
                  <div class="module-quote">
                      <div class="row">
                        <div class="col-12 col-lg-4">
                          <?php

                            $image = get_field('modules_quote_image');
                            $size = 'quote-logo-img'; // (thumbnail, medium, large, full or custom size)

                            if( $image ) {
                              echo wp_get_attachment_image( $image, $size );
                            }

                          ?>
        								</div>
                        <div class="col-12 col-lg-8">
                          <blockquote>
                            <p><?php the_field('modules_quote'); ?></p>
                            <footer>
                            <cite><?php the_field('modules_quote_author'); ?></cite>
                            </footer>
                          </blockquote>
                        </div>
                      </div>
                  </div>
                <?php endif; ?>

                <?php if( get_field('cta_button_url') ): ?>
                  <div class="row">
                    <div class="col cta">
                      <p class="cta__text"><?php the_field('call_to_action'); ?></p>
                      <a href="<?php the_field('cta_button_url'); ?>" class="btn btn--orange-500"><?php the_field('cta_button_text'); ?></a>
                    </div>
                  </div>
                <?php endif; ?>



        	</article> <?php // end article ?>

        <?php endwhile; endif; ?>

			</main>

      <div class="col-12 col-md-4 col-lg-4">
	      <?php get_sidebar(); ?>
      </div>

    </div>

	</div>

<?php get_footer(); ?>
