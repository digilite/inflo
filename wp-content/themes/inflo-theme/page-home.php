<?php
/*
 Template Name: Home Page
 *
 * Use this template for a static home page.
 * If you don't need the main loop, remove it
 * and get busy.
*/
?>

<?php get_header(); ?>

	<div id="content">

		<main id="main" class="clearfix" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

			<div class="hp-section hp-section--white hp-section--center hp-section--why-inflo">
        <div class="container">
          <div class="row">
            <div class="col">
              <h2 class="hp-section__title hp-section__title--orange"><?php the_field('1_section_title'); ?></h2>
              <p class="hp-section__subtitle"><?php the_field('1_section_subtitle'); ?></p>

              <div class="row hp-why-inflo">
                <div class="col-12">
                  <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2018/04/why-inflo-mockup.jpg" alt="">
                </div>

                <?php if( have_rows('1_key_points') ): ?>

                  <div class="col-12">
                    <div class="row ">

                      <?php while( have_rows('1_key_points') ): the_row();

                        // vars
                        $title = get_sub_field('key_point_title');
                        $content = get_sub_field('key_point_content');

                        ?>

                        <div class="col-12 col-md-6 hp-why-inflo__block">
                          <p><b><?php echo $title; ?></b></p>
                          <p><?php echo $content; ?></p>
                        </div>

                      <?php endwhile; ?>

                      </div>
                    </div>

                    <?php endif; ?>

              </div>

              <p class="hp-section__cta"><?php the_field('1_call_to_action'); ?></p>
              <a href="<?php the_field('1_button_url'); ?>" class="btn btn--orange-500"><?php the_field('1_button_text'); ?></a>
            </div>
          </div>
        </div>
      </div>

      <div class="hp-section-curve hp-section-curve--bottom">
        <body>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 70" enable-background="new 0 0 1440 70"><style type="text/css">.gray{fill:#e9ecef;}</style><path class="gray" d="M0 70h720c-367.1 0-670.7-52-720-70v70zM720 70h720v-70c-49.3 18-352.9 70-720 70z"/></svg>
        </body>
      </div>

      <div class="hp-section hp-section--quote">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-12 col-lg-6">
              <div class="embed-container">
                <?php the_field('section_video_quote'); ?>
              </div>
            </div>
            <div class="col-12 col-lg-6">
              <h2 class="hp-section__title"><?php the_field('section_title_quote'); ?></h2>
              <p class="hp-section__subtitle"><?php the_field('section_subtitle_quote'); ?></p>
              <blockquote class="hp-section__quote">
                <p><?php the_field('video_quote'); ?></p>
                <footer>
                <cite><?php the_field('video_quote_author'); ?></cite>
                </footer>
              </blockquote>
              <p class="hp-section__subtitle"><?php the_field('call_to_action_quote'); ?></p>
              <a href="<?php the_field('button_url_quote'); ?>" class="btn btn--orange-500"><?php the_field('button_text_quote');?> </a>
            </div>
          </div>
        </div>
      </div>

      <div class="hp-section-curve hp-section-curve--bottom">
        <body>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 70" enable-background="new 0 0 1440 70"><style type="text/css">.orange{fill:#f39200;}</style><path class="orange" d="M0 70h720c-367.1 0-670.7-52-720-70v70zM720 70h720v-70c-49.3 18-352.9 70-720 70z"/></svg>
        </body>
      </div>

      <div class="hp-section hp-section--orange hp-section--center hp-section--inflo-explained">
        <div class="container">
          <div class="row">
            <div class="col">
              <h2 class="hp-section__title"><? the_field('2_section_title'); ?></h2>
              <p class="hp-section__subtitle"><?php the_field('2_section_subtitle'); ?></p>

              <div class="hp-video embed-container">
                <?php the_field('2_video'); ?>
              </div>

              <p class="hp-section__cta"><?php the_field('2_call_to_action'); ?></p>
              <a href="<?php the_field('2_button_url'); ?>" class="btn btn--green-500"><?php the_field('2_button_text'); ?></a>
            </div>
          </div>
        </div>
      </div>

      <div class="hp-section hp-section--inflo-hi">
        <div class="container">
          <div class="row">
            <div class="col-12 col-md-8">
              <h2 class="hp-section__title"><?php the_field('3_section_title'); ?></h2>
              <p class="hp-section__subtitle"><?php the_field('4_section_subtitle'); ?></p>
              <?php the_field('4_content'); ?>
              <?php if( get_field('4_call_to_action') ): ?>
              <p class="hp-section__cta"><?php the_field('4_call_to_action'); ?></p>
              <?php endif; ?>
              <a href="<?php the_field('4_button_url'); ?>" class="btn btn--green-500"><?php the_field('4_button_text'); ?></a>
            </div>
            <div class="col-12 col-md-4 hp-inflo-hi-icon">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 463.8 427.6" enable-background="new 0 0 463.8 427.6"><style type="text/css">.inflo-hi{fill:url(#SVGID_1_);} .st1{fill:#FFFFFF;}</style><linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="59.43" y1="103.624" x2="360.216" y2="404.409"><stop offset=".019" stop-color="#3CAA35"/><stop offset=".081" stop-color="#58AB31"/><stop offset=".198" stop-color="#8BAC27"/><stop offset=".36" stop-color="#C3A711"/><stop offset=".538" stop-color="#F39200"/><stop offset="1" stop-color="#E30413"/></linearGradient><path class="inflo-hi" d="M251.2 0c-117.5 0-212.7 95.2-212.7 212.7 0 33.8 7.9 65.7 22 94.1l-60.5 120.8 128.1-41.5c34.7 24.7 77.2 39.2 123 39.2 117.5 0 212.7-95.2 212.7-212.7s-95.2-212.6-212.6-212.6z"/><path class="st1" d="M124.8 127.5c0-11.6 6.2-17.6 17.9-17.6h21.6c11.6 0 17.6 6 17.6 17.6v56.5h56.8v-56.5c0-11.6 6.2-17.6 17.6-17.6h21.9c11.4 0 17.6 6 17.6 17.6v159.4c0 11.4-6.2 17.6-17.6 17.6h-21.9c-11.4 0-17.6-6.2-17.6-17.6v-54.4h-56.8v54.4c0 11.4-5.9 17.6-17.6 17.6h-21.6c-11.6 0-17.9-6.2-17.9-17.6v-159.4zM318.2 181.6c0-11.6 6.2-17.6 17.6-17.6h21.1c11.6 0 17.9 6 17.9 17.6v105.2c0 11.4-6.2 17.6-17.9 17.6h-21.1c-11.4 0-17.6-6.2-17.6-17.6v-105.2zm1.1-48.5v-5.7c0-11.6 6-17.6 17.6-17.6h19.2c11.4 0 17.6 6 17.6 17.6v5.7c0 11.6-6.2 17.9-17.6 17.9h-19.2c-11.6 0-17.6-6.2-17.6-17.9z"/></svg>
            </div>
          </div>
        </div>
      </div>

		</main>

	</div>

<?php get_footer(); ?>
