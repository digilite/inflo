<?php get_header(); ?>

	<div id="content" class="container section-body section-body--sidebar">

    <div class="row">

      <main id="main" class="col-12 col-md-8 col-lg-8 archive-holder" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

        <div class="archive__catagories">

          <?php if ( is_post_type_archive( 'blog' ) || is_tax( 'blog_custom_cat' ) ) { ?>

            <?php $categories = get_categories('taxonomy=insights_catagories&post_type=insights'); ?>
                <p>Filter by:</p>
                <?php
                $args = array(
                'show_option_all' => 'All',
                'title_li' => '',
                'depth' => 1,
                'echo' => false, // dont echo the results
                'taxonomy' => 'blog_custom_cat'
            );

            $categories = wp_list_categories($args); // store the results in a variable

            if(strpos($categories,'current-cat') == false) { // check if the class exists
                // add the class to the All item if it doesn't exist
                $categories = str_replace('cat-item-all', 'cat-item-all current-cat', $categories);
            }

            echo $categories;
                ?>
        <?php   }  ?>

        </div>

				<?php get_template_part( 'templates/archive', 'loop');  ?>

      </main>

      <div class="col-12 col-md-4 col-lg-4">
        <?php get_sidebar(); ?>
      </div>

      </div>

  </div>

<?php get_footer(); ?>
