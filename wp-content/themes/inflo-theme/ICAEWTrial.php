<?php
/*
 Template Name: Trial Page Version ICAEW 
*/
?>

<?php get_header('standard'); ?>
  <div class="row">
    <div class="col-12 col-md-12 col-lg-5 v__banner">
      <div class="v__banner__content">
        <p class="header__subtitle vertical__banner__subtitle">ICAEW member firms can use Inflo for free.</p> 
        <p class="header__subtitle vertical__banner__subtitle">Forever.</p>
      </div>
    </div>

    <div class="col-12 col-md-12 col-lg-7">
      <div class="signup__wrapper">
        <h1 class="header__title">ICAEW Member Benefit</h1>
        <p class="hp-section__subtitle">As part of our strategic partnership with ICAEW, member firms can get hands-on with our next-generation software and access support resources for free. In our free version you can review our example client and upload interim data from an unlimited number of clients to plan live engagements.</p>
        <p class="hp-section__subtitle"> As an ICAEW member you will also receive £1,500 of initial credit on your Inflo account for free and 20% off your first purchase.</p>
        <?php echo do_shortcode( '[contact-form-7 id="1691" title="Trial V3 ICAEW"]' ); ?>

      </div>
    </div>
  </div>
<?php get_footer(); ?>

