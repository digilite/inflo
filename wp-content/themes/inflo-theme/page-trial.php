<?php
/*
 Template Name: Trial Page
*/
?>


<?php get_header(); ?>

	<div id="content">

		<main id="main" class="clearfix" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">


      <div class="t-section">
        <div class="container">
          <div class="row">
            <div class="col">
              <h2 class="t-section__title t-section__title--green"><?php the_field('trial_page_title'); ?></h2>
            </div>
          </div>

          <div class="row">
            <div class="col t-section-currency-select">
                <p>Select currency</p>
                <img id="gbp" src="<?php echo get_home_url(); ?>/inf/wp-content/uploads/2018/04/gbp-small.png" alt="GBP" class="currency-selector">
                <img id="usd" src="<?php echo get_home_url(); ?>/inf/wp-content/uploads/2018/04/usd-small.png" alt="USD" class="currency-selector">
            </div>
          </div>
          <div class="row justify-content-center t-section-tables">
            </select>
            <div class="col-12 col-sm-10 col-md-6 col-xl-5 t-table">
              <div class="t-table__title-holder t-table__title-holder--gray">
                <h2 class="t-table__product-name"><?php the_field('essential_product_name'); ?></h2>
                <p id="es-gbp" class="t-table__price t-table__price--gbp"><?php the_field('essential_price_gbp'); ?></p>
								<p id="es-usd" class="t-table__price t-table__price--usd"><?php the_field('essential_price_usd'); ?></p>
                <p class="t-table__compatibility"><?php the_field('essential_compatibility'); ?></p>
              </div>


              <div class="t-table__body">
                <?php
                // check if the repeater field has rows of data
                if( have_rows('essential_features') ) :

                  echo '<ul>';

                 	// loop through the rows of data
                    while ( have_rows('essential_features') ) : the_row();

                        // display a sub field value
                        echo '<li>';
                          the_sub_field('essentail_feature');
                        echo '</li>';

                    endwhile;

                  echo '</ul>';

                else :

                    // no rows found

                endif;

                ?>

                <a href="<?php the_field('essential_button_url'); ?>" class="btn btn--orange-500"><?php the_field('essential_button_text'); ?></a>
              </div>
            </div>
            <div class="col-12 col-sm-10 col-md-6 col-xl-5 t-table">
              <div class="t-table__title-holder t-table__title-holder--green">
                <h2 class="t-table__product-name"><?php the_field('professional_product_name'); ?></h2>
                <p id="pro-gbp" class="t-table__price t-table__price--gbp"><?php the_field('professional_price_gbp'); ?></p>
								<p id="pro-usd" class="t-table__price t-table__price--usd"><?php the_field('professional_price_usd'); ?></p>
                <p class="t-table__compatibility"><?php the_field('professional_compatibility'); ?></p>
              </div>
              <div class="t-table__body">
                <?php
                // check if the repeater field has rows of data
                if( have_rows('professional_features') ) :

                  echo '<ul>';

                  // loop through the rows of data
                    while ( have_rows('professional_features') ) : the_row();

                        // display a sub field value
                        echo '<li>';
                          the_sub_field('professional_feature');
                        echo '</li>';

                    endwhile;

                  echo '</ul>';

                else :

                    // no rows found

                endif;

                ?>

                <a href="<?php the_field('professional_button_url'); ?>" class="btn btn--orange-500"><?php the_field('professional_button_text'); ?></a>
              </div>
            </div>
          </div>
        </div>
      </div>


			<div class="t-section t-section--cta-band t-section--green">
        <div class="container">
          <div class="row">
            <div class="col">
              <p><?php the_field('cta_text'); ?></p>
              <a href="<?php the_field('trial_cta_button_url'); ?>" class="btn btn--orange-500"><?php the_field('trial_cta_button_text'); ?></a>
            </div>
          </div>
        </div>
      </div>


      <div class="t-section t-section--cusomters">
        <div class="container">
          <div class="row">
            <div class="col">
              <h2 class="t-section__title"><?php the_field('trusted_by_title'); ?></h2>
              <p class="t-section__subtitle"><?php the_field('trusted_by_subtitle'); ?></p>

            </div>
          </div>
          <div class="row justify-content-space-around align-items-center">
            <?php
            if( have_rows('company_logos') ): ?>

              <?php while( have_rows('company_logos') ): the_row();
                // vars
                $image = get_sub_field('company_logo');
                $size = 'quote-logo-img'; // (thumbnail, medium, large, full or custom size)
              ?>

                  <div class="customers__logo col-sm-auto">
                    <?php echo wp_get_attachment_image($image, $size); ?>
                  </div>

              <?php endwhile; ?>

            <?php endif; ?>
          </div>
        </div>
      </div>


      <div class="t-section t-section--quote">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-12 col-md-4 col-lg-2">
              <?php

                $image = get_field('quote_author_picture');
                $size = 'quote-profile-picture-img'; // (thumbnail, medium, large, full or custom size)

                if( $image ) {
                  echo wp_get_attachment_image( $image, $size );
                }

              ?>            </div>
            <div class="col-12 col-md-8 col-lg-10">
              <blockquote>
                <p><?php  the_field('quote'); ?></p>
                <footer>
                <cite><?php the_field('author'); ?></cite>
                </footer>
              </blockquote>
            </div>
          </div>
        </div>
      </div>


      <div class="t-section t-section--faq">
        <div class="container">
          <div class="row">
            <div class="col">
              <h2 class="t-section__title"><?php the_field('faq_title'); ?></h2>
              <p class="t-section__subtitle"><?php the_field('faq_subtitle'); ?></p>
            </div>
          </div>
          <div class="row justify-content-center">
            <div class="col-12 col-lg-8">
              <?php if( have_rows('accordion_section') ): ?>

                <div class="page-accordion">

                  <?php while( have_rows('accordion_section') ): the_row();

                    // vars
                    $title = get_sub_field('accordion_title');
                    $content = get_sub_field('accordion_content');
                    $cta_txt = get_sub_field('accordion_call_to_action');
                    $cta_btn = get_sub_field('accordion_call_to_action_button_text');
                    $cta_url = get_sub_field('accordion_call_to_action_url');

                  ?>

                    <div class="accordion-holder">

                    <button class="accordion">
                      <p class="accordion__title">
                        <?php echo $title; ?>
                      </p>
                    </button>

                    <div class="panel">
                      <div class="panel__content entry-content">

                        <?php echo $content; ?>

                        <?php if( get_sub_field('accordion_call_to_action_url') ): ?>
                          <div class="cta">
                            <h2 class="cta__text"><?php echo $cta_txt; ?></h2>
                            <a href="<?php echo $cta_url; ?>" class="btn btn--orange-500"><?php echo $cta_btn; ?></a>
                          </div>
                        <?php endif; ?>

                      </div>
                    </div>

                  </div>

                  <?php endwhile; ?>

                </div>

              <?php endif; ?>
            </div>
          </div>
					<?php if( get_field('faq_cta') ): ?>
	          <div class="row">
	            <div class="col t-section__cta">
	              <p class="t-section__subtitle"><?php the_field('faq_cta'); ?></p>
	              <a href="<?php the_field('faq_cta_button_url'); ?>" class="btn btn--orange-500"><?php the_field('faq_cta_button_text'); ?></a>
	            </div>
	          </div>
					<?php endif; ?>
        </div>
      </div>

		</main>

	</div>

<?php get_footer(); ?>
