<?php get_header(); ?>

	<div id="content" class="container section-body section-body--sidebar">

    <div class="row">

      <main id="main" class="col-12 col-md-8 col-lg-8" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

          <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

          	<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

          		<section class="entry-content clearfix" itemprop="articleBody">

                <?php if( is_page( 'accounting-services' ) ) {
                  require_once( 'templates/page-elements/use-cases.php' );
                } else {
                  the_content();
                }?>

          		</section> <?php // end article section ?>

          		<footer class="article-footer clearfix">

                <?php if( get_field('cta_button_url') ): ?>
                  <div class="row">
                    <div class="col cta">
                      <p class="cta__text"><?php the_field('call_to_action'); ?></p>
                      <a href="<?php the_field('cta_button_url'); ?>" class="btn btn--orange-500"><?php the_field('cta_button_text'); ?></a>
                    </div>
                  </div>
                <?php endif; ?>

          		</footer>

          	</article>

          <?php endwhile; endif; ?>

        </main>

        <div class="col-12 col-md-4 col-lg-4">
  	      <?php get_sidebar(); ?>
        </div>

        </div>

  	</div>

  <?php get_footer(); ?>
