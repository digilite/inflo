<?php get_header(); ?>

	<div id="content" class="container section-body section-body--archive">

    <div class="row">

			<main id="main" class="col-12 col-md-8 col-lg-8" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

        <section class="team-members">
          <div class="row">

            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                <div class="col-12 col-sm-6">
                  <a class="team-member" href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
                      <article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">

                        <div class="team-member__img">
                          <?php
                            $image = get_field('leadership_team_picture');
                            $size = 'profile-img'; // (thumbnail, medium, large, full or custom size)
                              if ($image) {
                                  echo wp_get_attachment_image($image, $size);
                              }
                          ?>
                        </div>

                        <section class="team-member__text">
                          <h3 class="team-member__name"><?php the_title(); ?></h3>
                          <p class="team-member__role"><?php the_field('leadership_team_role');?></p>
                        </section>

                      </article>
                    </a>
                </div>

            <?php endwhile; endif; ?>

          </div>
        </section>

        <?php get_template_part('templates/post-navigation'); ?>

			</main>

      <div class="col-12 col-md-4 col-lg-4">
        <?php get_sidebar(); ?>
      </div>

    </div>

	</div>

<?php get_footer(); ?>
