<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php html_schema(); ?> <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
<?php
  global $post;
  $id  = $post->ID;
?>
	<head>
    <title><?php if( get_field('seo_title', $id) ) {
        the_field('seo_title'); ?> - <?php echo get_bloginfo( 'name' );
        } else {
           wp_title('-', true, 'right');
        }
      ?></title>
    <?php if( get_field('seo_description', $id) ): ?>
    <meta name="description" content="<?php the_field('seo_description'); ?>">
    <?php endif; ?>
    <?php if( get_field('seo_keywords', $id) ): ?>
    <meta name="keywords" content="<?php the_field('seo_keywords'); ?>">
    <?php endif; ?>

    <meta charset='<?php bloginfo( 'charset' ); ?>'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta property="og:title" content="<?php if( get_field('seo_title', $id) ) {
      the_field('seo_title'); ?> - <?php echo get_bloginfo( 'name' );
      } else {
         wp_title('-', true, 'right');
      }
    ?>" />
		<meta property="og:url" content="<?php echo home_url(); ?>" />
		<meta property="og:site_name" content="<?php bloginfo('name'); ?>" />
		<meta property="og:type" content="website" />
    <?php
      $description = get_field('seo_description', $id); if($description=='')$description = get_field('social_media_description','options');
    ?>
    <meta property="og:description" content="<?php echo $description ?>" />
    <?php
      $lg_social_banner = get_field('seo_social_banner_large', $id); if($lg_social_banner=='')$lg_social_banner = get_field('social_banner_large','options');
    ?>
		<meta property="og:image" content="<?php echo $lg_social_banner ?>" />

		<meta name="twitter:card" content="summary">
    <?php
      $sm_social_banner = get_field('seo_social_banner_small', $id); if($sm_social_banner=='')$sm_social_banner = get_field('social_banner_small','options');
    ?>
    <meta property="twitter:image" content="<?php echo $sm_social_banner ?>" />

    <link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/library/dist/img/apple-icon-touch-precomposed.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->

    <meta name="application-name" content="<?php echo get_bloginfo( 'name' ); ?>"/>
		<meta name="msapplication-TileColor" content="#ffffff"/>
		<meta name="msapplication-square70x70logo" content="<?php echo get_template_directory_uri(); ?>/library/dist/img/tiny.png"/>
		<meta name="msapplication-square150x150logo" content="<?php echo get_template_directory_uri(); ?>/library/dist/img/square.png"/>
		<meta name="msapplication-wide310x150logo" content="<?php echo get_template_directory_uri(); ?>/library/dist/img/wide.png"/>
		<meta name="msapplication-square310x310logo" content="<?php echo get_template_directory_uri(); ?>/library/dist/img/large.png"/>

		<?php wp_head(); ?>

		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-W3S873D');</script>
		<!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for inflosoftware.com -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:919865,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

    <!-- Tidio Live Chat -->
    <script src="//code.tidio.co/uijpdzjlcvbpx8ofjmhogt3drwnf16hg.js"></script>
	</head>

	<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">

		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W3S873D"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->





