<?php get_header(); ?>

	<div id="content" class="container section-body section-body--sidebar">

    <div class="row">

      <main id="main" class="col-12 col-md-8 col-lg-8 single-holder" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

				<?php // Edit the loop in /templates/single-loop. Or roll your own. ?>
				<?php get_template_part( 'templates/single', 'loop'); ?>

      </main>

      <div class="col-12 col-md-4 col-lg-4">
        <?php get_sidebar(); ?>
      </div>

      </div>

  </div>

<?php get_footer(); ?>
