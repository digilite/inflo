<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php html_schema(); ?> <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
<?php
  global $post;
  $id  = $post->ID;
?>
	<head>
    <title><?php if( get_field('seo_title', $id) ) {
        the_field('seo_title'); ?> - <?php echo get_bloginfo( 'name' );
        } else {
           wp_title('-', true, 'right');
        }
      ?></title>
    <?php if( get_field('seo_description', $id) ): ?>
    <meta name="description" content="<?php the_field('seo_description'); ?>">
    <?php endif; ?>
    <?php if( get_field('seo_keywords', $id) ): ?>
    <meta name="keywords" content="<?php the_field('seo_keywords'); ?>">
    <?php endif; ?>

    <meta charset='<?php bloginfo( 'charset' ); ?>'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta property="og:title" content="<?php if( get_field('seo_title', $id) ) {
      the_field('seo_title'); ?> - <?php echo get_bloginfo( 'name' );
      } else {
         wp_title('-', true, 'right');
      }
    ?>" />
		<meta property="og:url" content="<?php echo home_url(); ?>" />
		<meta property="og:site_name" content="<?php bloginfo('name'); ?>" />
		<meta property="og:type" content="website" />
    <?php
      $description = get_field('seo_description', $id); if($description=='')$description = get_field('social_media_description','options');
    ?>
    <meta property="og:description" content="<?php echo $description ?>" />
    <?php
      $lg_social_banner = get_field('seo_social_banner_large', $id); if($lg_social_banner=='')$lg_social_banner = get_field('social_banner_large','options');
    ?>
		<meta property="og:image" content="<?php echo $lg_social_banner ?>" />

		<meta name="twitter:card" content="summary">
    <?php
      $sm_social_banner = get_field('seo_social_banner_small', $id); if($sm_social_banner=='')$sm_social_banner = get_field('social_banner_small','options');
    ?>
    <meta property="twitter:image" content="<?php echo $sm_social_banner ?>" />

    <link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/library/dist/img/apple-icon-touch-precomposed.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->

    <meta name="application-name" content="<?php echo get_bloginfo( 'name' ); ?>"/>
		<meta name="msapplication-TileColor" content="#ffffff"/>
		<meta name="msapplication-square70x70logo" content="<?php echo get_template_directory_uri(); ?>/library/dist/img/tiny.png"/>
		<meta name="msapplication-square150x150logo" content="<?php echo get_template_directory_uri(); ?>/library/dist/img/square.png"/>
		<meta name="msapplication-wide310x150logo" content="<?php echo get_template_directory_uri(); ?>/library/dist/img/wide.png"/>
		<meta name="msapplication-square310x310logo" content="<?php echo get_template_directory_uri(); ?>/library/dist/img/large.png"/>

		<?php wp_head(); ?>

		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-W3S873D');</script>
		<!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for inflosoftware.com -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:919865,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

    <!-- Tidio Live Chat -->
    <script src="//code.tidio.co/uijpdzjlcvbpx8ofjmhogt3drwnf16hg.js"></script>

	</head>

	<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">

		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W3S873D"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->

		<div id="root" class="clearfix">

      <div class="mean-pusher"></div>

			<?php
        // first, get the image ID returned by ACF acconting for archives and single posts
        if ( is_singular( 'leadership_team' )) {
  				$image_id = get_field('page_header_background_image', 156);
  			} elseif ( is_post_type_archive('leadership_team')) {
  				$image_id = get_field('page_header_background_image', 156);
  			} elseif ( is_post_type_archive('modules_type')) {
  				$image_id = get_field('page_header_background_image', 10);
  			} elseif ( is_post_type_archive('webinars')) {
  				$image_id = get_field('page_header_background_image', 1075);
  			} elseif( is_singular( 'webinars' )) {
  				$image_id = get_field('page_header_background_image', 1075);
  			} elseif( is_tax( 'webinars_custom_cat' )) {
  				$image_id = get_field('page_header_background_image', 1075);
  			} elseif ( is_post_type_archive('blog')) {
  				$image_id = get_field('page_header_background_image', 1068);
  			} elseif( is_singular( 'blog' )) {
  				$image_id = get_field('page_header_background_image', 1068);
  			} elseif( is_tax( 'blog_custom_cat' )) {
  				$image_id = get_field('page_header_background_image', 1068);
  			} elseif ( is_post_type_archive('case-studies')) {
          $image_id = get_field('page_header_background_image', 1070);
        } elseif( is_singular( 'case-studies' )) {
          $image_id = get_field('page_header_background_image', 1070);
        } elseif( is_tax( 'case-studies_custom_cat' )) {
          $image_id = get_field('page_header_background_image', 1070);
        } elseif ( is_post_type_archive('conversations')) {
          $image_id = get_field('page_header_background_image', 1073);
        } elseif( is_singular( 'conversations' )) {
          $image_id = get_field('page_header_background_image', 1073);
        } elseif( is_tax( 'conversations_custom_cat' )) {
          $image_id = get_field('page_header_background_image', 1073);
        } elseif ( is_404()) {
          $image_id = get_field('page_header_background_image', 'option');
        } elseif ( get_field('page_header_background_image')) {
  				$image_id = get_field('page_header_background_image');
  			};
        // and the image size you want to return
        $image_size = 'page-header-img';
        // use wp_get_attachment_image_src to return an array containing the image
        // we'll pass in the $image_id in the first parameter
        // and the image size registered using add_image_size() in the second
        $image_array = wp_get_attachment_image_src($image_id, $image_size);
        // finally, extract and store the URL from $image_array
        $image_url = $image_array[0];
      ?>

      <header class="header" role="banner" itemscope itemtype="http://schema.org/WPHeader" style="background-image:url('<?php echo $image_url; ?>')">

				<div class="wrap clearfix">

					<a class="header__logo text-hide" href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a>

          <div class="header__btns">
            <a href="<?php the_field('header_orange_button_url', 'option'); ?>" class="btn btn--orange-500"><?php the_field('header_orange_button_text', 'option'); ?></a>
            <a href="<?php the_field('header_white_button_url', 'option'); ?>" class="btn btn--white-outline"><?php the_field('header_white_button_text', 'option'); ?></a>
          </div>

					<nav class="clearfix" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">

					<?php // see all default args here: https://developer.wordpress.org/reference/functions/wp_nav_menu/ ?>

						<?php wp_nav_menu(array(
    					         'container' => false,                           // remove nav container
    					         'container_class' => 'menu clearfix',                 // class of container (should you choose to use it)
    					         'menu' => __( 'The Main Menu', 'maintheme' ),  // nav name
    					         'menu_class' => 'header__nav nav top-nav main-menu',     // adding custom nav class
    					         'theme_location' => 'main-nav',                 // where it's located in the theme
						)); ?>

					</nav>

				</div>

        <div class="container header__text">
          <div class="row">
            <div class="col-12 col-sm-10 col-md-10 col-lg-8 col-xl-6">

              <h1 class="header__title">
                <?php if ( is_singular( 'leadership_team' )) {
                   echo get_the_title( '156' );
                }  elseif ( is_post_type_archive('leadership_team')) {
                  the_field('page_header_title', 156);
                } elseif ( is_post_type_archive('modules_type')) {
                  the_field('page_header_title', 10);
                } elseif ( is_post_type_archive('webinars')) {
                  the_field('page_header_title', 1075);
                } elseif ( is_singular( 'webinars' )) {
                   the_field('page_header_title', 1075);
                } elseif ( is_tax( 'webinar_custom_cat' )) {
                   the_field('page_header_title', 1075);
                } elseif ( is_post_type_archive('blog')) {
                  the_field('page_header_title', 1068);
                } elseif ( is_singular( 'blog' )) {
                   the_field('page_header_title', 1068);
                } elseif ( is_tax( 'blog_custom_cat' )) {
                   the_field('page_header_title', 1068);
                } elseif ( is_post_type_archive('case-studies')) {
                  the_field('page_header_title', 1070);
                } elseif ( is_singular( 'case-studies' )) {
                   the_field('page_header_title', 1070);
                } elseif ( is_tax( 'case-studies_custom_cat' )) {
                   the_field('page_header_title', 1070);
                } elseif ( is_post_type_archive('conversations')) {
                  the_field('page_header_title', 1073);
                } elseif ( is_singular( 'conversations' )) {
                   the_field('page_header_title', 1073);
                } elseif ( is_tax( 'conversations_custom_cat' )) {
                   the_field('page_header_title', 1073);
                } elseif ( is_404()) {
                  //the_field('page_header_title', 'option');
                } elseif ( get_field('page_header_title')) {
                  the_field('page_header_title');
                } else {
                  the_title();
                }
                ?>
              </h1>
              <p class="header__subtitle">
                <?php if ( is_singular( 'leadership_team' )) {
                  the_field('page_header_subtitle', 156);
                } elseif ( is_post_type_archive('leadership_team')) {
                  the_field('page_header_subtitle', 156);
                } elseif ( is_post_type_archive('modules_type')) {
                  the_field('page_header_subtitle', 10);
                } elseif ( is_post_type_archive('webinars')) {
                  the_field('page_header_subtitle', 1075);
                } elseif ( is_singular( 'webinars' )) {
                   the_field('page_header_subtitle', 1075);
                } elseif ( is_tax( 'webinar_custom_cat' )) {
                   the_field('page_header_subtitle', 1075);
                } elseif ( is_post_type_archive('blog')) {
                  the_field('page_header_subtitle', 1068);
                } elseif ( is_singular( 'blog' )) {
                   the_field('page_header_subtitle', 1068);
                } elseif ( is_tax( 'blog_custom_cat' )) {
                   the_field('page_header_subtitle', 1068);
                } elseif ( is_post_type_archive('case-studies')) {
                  the_field('page_header_subtitle', 1070);
                } elseif ( is_singular( 'case-studies' )) {
                   the_field('page_header_subtitle', 1070);
                } elseif ( is_tax( 'case-studies_custom_cat' )) {
                   the_field('page_header_subtitle', 1070);
                } elseif ( is_post_type_archive('conversations')) {
                  the_field('page_header_subtitle', 1073);
                } elseif ( is_singular( 'conversations' )) {
                   the_field('page_header_subtitle', 1073);
                } elseif ( is_tax( 'conversations_custom_cat' )) {
                   the_field('page_header_subtitle', 1073);
                } elseif ( is_404()) {
                  //the_field('page_header_subtitle', 'option');
                } elseif ( get_field('page_header_subtitle')) {
                  the_field('page_header_subtitle');
                }
                ?>
              </p>
              <?php

              if ( is_post_type_archive('conversations')) {
               $pageid = 1073;
             } elseif ( is_singular( 'conversations' )) {
                $pageid = 1073;
             } elseif ( is_tax( 'conversations_custom_cat' )) {
                $pageid = 1073;
             } else {
               $pageid = null;
             }

              // check if the flexible content field has rows of data
              if( have_rows('page_header_buttons', $pageid) ):

                   // loop through the rows of data
                  while ( have_rows('page_header_buttons', $pageid) ) : the_row();

                      if( get_row_layout() == 'orange_button' ):

                        echo '<a href="';
                        echo the_sub_field('page_header_orange_standard_button_url');
                        echo '" class="btn btn--orange-500" >';
                        echo the_sub_field('page_header_orange_standard_button_txt');
                        echo '</a>';

                      elseif( get_row_layout() == 'orange_video_button' ):

                        echo '<a data-lity href="';
                        echo the_sub_field('page_header_orange_video_button_url');
                        echo '" class="btn btn--orange-500" >';
                        echo the_sub_field('page_header_orange_video_button_txt');
                        echo '</a>';

                      elseif( get_row_layout() == 'white_video_button' ):

                        echo '<a data-lity href="';
                        echo the_sub_field('page_header_white_video_button_url');
                        echo '" class="btn btn--white-outline" >';
                        echo the_sub_field('page_header_white_video_button_txt');
                        echo '</a>';

                      elseif( get_row_layout() == 'white_button' ):

                        echo '<a href="';
                        echo the_sub_field('page_header_white_standard_button_url');
                        echo '" class="btn btn--white-outline" >';
                        echo the_sub_field('page_header_white_standard_button_txt');
                        echo '</a>';

                      endif;

                  endwhile;

              else :

                  // no layouts found

              endif;

              ?>
            </div>
          </div>
        </div>

        <div class="header__curve">
          <body>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 70" enable-background="new 0 0 1440 70" preserveAspectRatio="xMidYMin slice"><style type="text/css">.st0{fill:#FFFFFF;}</style><path class="st0" d="M0 70h720c-367.1 0-670.7-52-720-70v70zM720 70h720v-70c-49.3 18-352.9 70-720 70z"/></svg>
          </body>
        </div>

			</header>
