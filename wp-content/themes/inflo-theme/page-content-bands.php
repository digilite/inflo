<?php
/*
 Template Name: Content Bands
*/
?>

<?php get_header(); ?>

	<div id="content" class="section-body section-body--content-bands">

      <main id="main" class="" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

        	<article id="post-<?php the_ID(); ?>" <?php post_class('content-band-holder container clearfix'); ?> role="article" itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting">

              <div class="row">
                <div class="col">


            <?php

            // check if the flexible content field has rows of data
            if( have_rows('content_band') ):

                 // loop through the rows of data
                while ( have_rows('content_band') ) : the_row();

                    // Left text and right image layout
                    if( get_row_layout() == 'text_image' ):

                      $img = get_sub_field('content_image');
                      $size = 'features-img';

                      echo '<div class="row content-band content-band--txt-img">
                              <div class="col-12 col-md-6 content-band__text-holder">
                                <h2 class="content-band__title">';
                      echo         the_sub_field('content_title');
                      echo      '</h2>';
                      echo       the_sub_field('content');
                      echo   '</div>
                            <div class="col-12 col-md-6 content-band__img-holder content-band__img-holder--right">';
                      echo     wp_get_attachment_image( $img, $size );
                      echo   '</div>
                            </div>';

                    // Left image right text layout
                    elseif( get_row_layout() == 'image_text' ):

                      $img = get_sub_field('content_image');
                      $size = 'features-img';

                      echo '<div class="row content-band content-band--img-txt">
                              <div class="col-12 col-md-6 content-band__img-holder content-band__img-holder--left">';
                      echo       wp_get_attachment_image( $img, $size );
                      echo    '</div>
                              <div class="col-12 col-md-6 content-band__text-holder">
                                <h2 class="content-band__title">';
                      echo         the_sub_field('content_title');
                      echo      '</h2>';
                      echo         the_sub_field('content');
                      echo      '</div>
                            </div>';

                    endif;

                endwhile;

            else :

                // no layouts found

            endif;

            ?>
            </div>
            </div>

              <div class="row">
                <div class="col cta cta--center">
                  <p class="cta__text"><?php the_field('call_to_action'); ?></p>
                  <?php if( get_field('cta_button_url') ): ?>
                    <a href="<?php the_field('cta_button_url'); ?>" class="btn btn--orange-500"><?php the_field('cta_button_text'); ?></a>
                  <?php endif; ?>
                </div>
              </div>

        	</article> <?php // end article ?>

			</main>

	</div>

<?php get_footer(); ?>
