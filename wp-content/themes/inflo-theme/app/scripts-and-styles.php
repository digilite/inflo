<?php

// Scripts and Stylesheets!
add_action('after_setup_theme', 'init_scripts_and_styles');

function init_scripts_and_styles()
{
  // enqueue base scripts and styles
  add_action('wp_enqueue_scripts', 'scripts_and_styles', 999);
}


/*********************
SCRIPTS & ENQUEUEING
*********************/

// loading modernizr and jquery, and reply script
function scripts_and_styles()
{
    global $wp_styles; // call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

  if (!is_admin()) {

    // call in jQuery from Google's CDN
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', false, '3.1.1');

    // modernizr (without media query polyfill) + device detction (retina support added here)
    wp_enqueue_script( 'detect', get_stylesheet_directory_uri() . '/library/dist/js/detect/detect.min.js', array(), '', false );
    wp_enqueue_script( 'libs', get_stylesheet_directory_uri() . '/library/dist/js/libs/libs.min.js', array('jquery'), '', true);


    // register main stylesheet
    wp_enqueue_style('stylesheet', get_theme_file_uri() . '/library/dist/css/style.min.css', array(), '', 'all');

    // register typekit fonts
    wp_enqueue_style('typekit', 'https://use.typekit.net/hjz7roh.css', array(), '', 'all');

    // ie-only style sheet
    wp_enqueue_style('ie-only', get_theme_file_uri() . '/library/dist/css/ie.min.css', array(), '');

    // comment reply script for threaded comments
    if (is_singular() and comments_open() and (get_option('thread_comments') == 1)) {
        wp_enqueue_script('comment-reply');
    }

    // adding scripts file in the footer
    wp_enqueue_script('js', get_theme_file_uri() . '/library/dist/js/scripts.min.js', array( 'jquery' ), '', true);

      $wp_styles->add_data('ie-only', 'conditional', 'lt IE 9'); // add conditional wrapper around ie stylesheet

    // Extra scripts. Uncomment to use. Or better yet, copy what you need to the main scripts folder or on the page(s) you need it
    // wp_enqueue_script( 'extra-js', get_theme_file_uri() . '/library/dist/js/extras/extra-scripts.js', array( 'jquery' ), '', true );
  }
}

/*
Use this to add Google or other web fonts.
*/

// add_action( 'wp_enqueue_scripts', 'custom_fonts' );

// function custom_fonts() {

//     wp_enqueue_style( 'fonts', '//fonts.googleapis.com/css?family=Open+Sans:400,600,400italic,' );

// }


/*********************
DEREGISTER CSS/SCRIPTS
*********************/
// Deregister Contact Form 7 styles
add_action('wp_print_styles', 'cf7_deregister_styles', 100);

function cf7_deregister_styles()
{
    if (! is_page(array('contact', 'trial', 'sign-up', 'demo'))) {
        wp_deregister_style('contact-form-7');
    }
}

//// Deregister Contact Form 7 JavaScript files on all pages without a form
//add_action('wp_print_scripts', 'cf7_deregister_javascript', 100);
//
//function cf7_deregister_javascript()
//{
//    if (! is_page(array('contact', 'trial', 'sign-up', 'demo'))) {
//        wp_deregister_script('contact-form-7');
//    }
//}
