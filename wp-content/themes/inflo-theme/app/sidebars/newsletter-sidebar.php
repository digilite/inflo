<?php

/**
 * Adds custom widget.
 */
class newsletter_widget extends WP_Widget {

  /**
   * Register widget with WordPress.
   */
  function __construct() {
    parent::__construct(
      'newsletter_widget', // Base ID
      __('Newsletter Signup', 'text_domain'), // Name
      array( 'description' => __( 'Newsletter signup widget', 'text_domain' ), ) // Args
    );
  }

  /**
   * Front-end display of widget.
   *
   * @see WP_Widget::widget()
   *
   * @param array $args     Widget arguments.
   * @param array $instance Saved values from database.
   */
  public function widget( $args, $instance ) {
    echo $args['before_widget'];
    echo '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130 130" enable-background="new 0 0 130 130"><style type="text/css">.mail-icon{fill:#CE132E;} .mail-icon-white{fill:#FFFFFF;}</style><circle class="mail-icon" cx="65" cy="65" r="65"/><path class="mail-icon-white" d="M103.2 39.2c0-.2-.1-1.1-.8-1.7l-.1-.1-.1-.1c-.3-.2-.8-.3-1.2-.3h-72c-.3 0-1.1.1-1.7.8l-.1.1-.1.2c-.2.3-.3.8-.3 1.2v51.4c0 .2.1 1.1.8 1.7l.1.1.2.1c.3.2.8.3 1.2.3h72c.2 0 1.1-.1 1.7-.8l.1-.1.1-.2c.2-.3.3-.8.3-1.2v-51.3l-.1-.1zm-9.4 10.2c-.7.7-8.9 9-16.3 16.5l14.6 14.6c.7.7.7 1.9 0 2.7-.4.4-.9.6-1.3.6-.5 0-1-.2-1.3-.6l-14.6-14.6c-2.6 2.6-4.4 4.5-4.8 4.9-1.6 1.6-3.2 2.3-5 2.3h-.1c-1.8 0-3.4-.7-5-2.3l-4.8-4.8-14.6 14.6c-.4.4-.9.6-1.3.6-.5 0-1-.2-1.3-.6-.7-.7-.7-1.9 0-2.7l14.5-14.6c-7.4-7.5-15.7-15.9-16.4-16.6-.7-.7-.7-1.9 0-2.7.7-.7 1.9-.7 2.7 0 .2.2 22.3 22.7 23.8 24.1 1.2 1.2 1.9 1.2 2.3 1.2h.1c.4 0 1.1 0 2.3-1.2 1.4-1.4 23.5-23.8 23.8-24.1.7-.7 1.9-.8 2.7 0 .7.8.7 2 0 2.7z"/></svg>';
    if ( !empty($instance['title']) ) {
      echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
    }
    echo '<p>' . get_field('sidebar_subtitle', 'widget_' . $args['widget_id']) . '</p>';
    echo '<a class="btn btn--orange-500" href="' . get_field('sidebar_button_url', 'widget_' . $args['widget_id']) . '">' . get_field('sidebar_button_text', 'widget_' . $args['widget_id']) . '</a>';
    echo $args['after_widget'];
  }

  /**
   * Back-end widget form.
   *
   * @see WP_Widget::form()
   *
   * @param array $instance Previously saved values from database.
   */
  public function form( $instance ) {
    if ( isset($instance['title']) ) {
      $title = $instance['title'];
    }
    else {
      $title = __( 'New title', 'text_domain' );
    }
    ?>
    <p>
      <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
    </p>
    <?php
  }

  /**
   * Sanitize widget form values as they are saved.
   *
   * @see WP_Widget::update()
   *
   * @param array $new_instance Values just sent to be saved.
   * @param array $old_instance Previously saved values from database.
   *
   * @return array Updated safe values to be saved.
   */
  public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

    return $instance;
  }

} // class newsletter_widget

// register newsletter_widget widget
add_action( 'widgets_init', function(){
  register_widget( 'newsletter_widget' );
});

?>
