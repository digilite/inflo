<?php

/**
 * Adds custom widget.
 */
class cta_widget_inflohi extends WP_Widget {

  /**
   * Register widget with WordPress.
   */
  function __construct() {
    parent::__construct(
      'cta_widget_inflohi', // Base ID
      __('Call to Action - infloHi', 'text_domain'), // Name
      array( 'description' => __( 'infloHi call to action widget', 'text_domain' ), ) // Args
    );
  }

  /**
   * Front-end display of widget.
   *
   * @see WP_Widget::widget()
   *
   * @param array $args     Widget arguments.
   * @param array $instance Saved values from database.
   */
  public function widget( $args, $instance ) {
    echo $args['before_widget'];
    echo '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 463.8 427.6" enable-background="new 0 0 463.8 427.6"><style type="text/css">.inflo-hi{fill:url(#SVGID_1_);} .st1{fill:#FFFFFF;}</style><linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="59.43" y1="103.624" x2="360.216" y2="404.409"><stop offset=".019" stop-color="#3CAA35"/><stop offset=".081" stop-color="#58AB31"/><stop offset=".198" stop-color="#8BAC27"/><stop offset=".36" stop-color="#C3A711"/><stop offset=".538" stop-color="#F39200"/><stop offset="1" stop-color="#E30413"/></linearGradient><path class="inflo-hi" d="M251.2 0c-117.5 0-212.7 95.2-212.7 212.7 0 33.8 7.9 65.7 22 94.1l-60.5 120.8 128.1-41.5c34.7 24.7 77.2 39.2 123 39.2 117.5 0 212.7-95.2 212.7-212.7s-95.2-212.6-212.6-212.6z"/><path class="st1" d="M124.8 127.5c0-11.6 6.2-17.6 17.9-17.6h21.6c11.6 0 17.6 6 17.6 17.6v56.5h56.8v-56.5c0-11.6 6.2-17.6 17.6-17.6h21.9c11.4 0 17.6 6 17.6 17.6v159.4c0 11.4-6.2 17.6-17.6 17.6h-21.9c-11.4 0-17.6-6.2-17.6-17.6v-54.4h-56.8v54.4c0 11.4-5.9 17.6-17.6 17.6h-21.6c-11.6 0-17.9-6.2-17.9-17.6v-159.4zM318.2 181.6c0-11.6 6.2-17.6 17.6-17.6h21.1c11.6 0 17.9 6 17.9 17.6v105.2c0 11.4-6.2 17.6-17.9 17.6h-21.1c-11.4 0-17.6-6.2-17.6-17.6v-105.2zm1.1-48.5v-5.7c0-11.6 6-17.6 17.6-17.6h19.2c11.4 0 17.6 6 17.6 17.6v5.7c0 11.6-6.2 17.9-17.6 17.9h-19.2c-11.6 0-17.6-6.2-17.6-17.9z"/></svg>';
    if ( !empty($instance['title']) ) {
      echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
    }
    echo '<p>' . get_field('sidebar_subtitle', 'widget_' . $args['widget_id']) . '</p>';
    echo '<a class="btn btn--orange-500" href="' . get_field('sidebar_button_url', 'widget_' . $args['widget_id']) . '">' . get_field('sidebar_button_text', 'widget_' . $args['widget_id']) . '</a>';
    echo $args['after_widget'];
  }

  /**
   * Back-end widget form.
   *
   * @see WP_Widget::form()
   *
   * @param array $instance Previously saved values from database.
   */
  public function form( $instance ) {
    if ( isset($instance['title']) ) {
      $title = $instance['title'];
    }
    else {
      $title = __( 'New title', 'text_domain' );
    }
    ?>
    <p>
      <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
    </p>
    <?php
  }

  /**
   * Sanitize widget form values as they are saved.
   *
   * @see WP_Widget::update()
   *
   * @param array $new_instance Values just sent to be saved.
   * @param array $old_instance Previously saved values from database.
   *
   * @return array Updated safe values to be saved.
   */
  public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

    return $instance;
  }

} // class cta_widget_inflohi

// register cta_widget_inflohi widget
add_action( 'widgets_init', function(){
  register_widget( 'cta_widget_inflohi' );
});

?>
