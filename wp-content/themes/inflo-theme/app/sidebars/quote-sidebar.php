<?php

/**
 * Adds custom widget.
 */
class quote_widget extends WP_Widget {

  /**
   * Register widget with WordPress.
   */
  function __construct() {
    parent::__construct(
      'quote_widget', // Base ID
      __('Quote', 'text_domain'), // Name
      array( 'description' => __( 'Quote widget', 'text_domain' ), ) // Args
    );
  }

  /**
   * Front-end display of widget.
   *
   * @see WP_Widget::widget()
   *
   * @param array $args     Widget arguments.
   * @param array $instance Saved values from database.
   */
  public function widget( $args, $instance ) {
    echo $args['before_widget'];
    $image = get_field('widget_client_photgraph', 'widget_' . $args['widget_id']);
    $image_url = $image['sizes']['quote-profile-picture-img'];
    echo "<img src='". $image_url ."' />";

    echo '<blockquote>
            <p>'. get_field('widget_quote', 'widget_' . $args['widget_id']) . '</p>
          <footer>
          <cite>' . get_field('widget_author', 'widget_' . $args['widget_id']) . '</cite>
            </footer>
          </blockquote>';

    echo $args['after_widget'];
  }

  /**
   * Back-end widget form.
   *
   * @see WP_Widget::form()
   *
   * @param array $instance Previously saved values from database.
   */
  public function form( $instance ) {
    if ( isset($instance['title']) ) {
      $title = $instance['title'];
    }
    else {
      $title = __( 'New title', 'text_domain' );
    }
    ?>
    <p>
      <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
    </p>
    <?php
  }

  /**
   * Sanitize widget form values as they are saved.
   *
   * @see WP_Widget::update()
   *
   * @param array $new_instance Values just sent to be saved.
   * @param array $old_instance Previously saved values from database.
   *
   * @return array Updated safe values to be saved.
   */
  public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

    return $instance;
  }

} // class quote_widget

// register quote_widget widget
add_action( 'widgets_init', function(){
  register_widget( 'quote_widget' );
});

?>
