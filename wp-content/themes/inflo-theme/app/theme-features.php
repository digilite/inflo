<?php

// theme_features!
add_action('after_setup_theme', 'theme_features');

function theme_features()
{

    // theme_featuresing this stuff after theme setup
    theme_support();

    // adding sidebars to Wordpress
    add_action('widgets_init', 'register_custom_sidebars');
} /* end theme features */



/****************************************
Thumbnail size options
****************************************/
// Thumbnail sizes
add_image_size('image-600', 600, 600, true);
add_image_size('image-300', 300, 300, true);
add_image_size('image-300', 150, 150, true);
add_image_size('quote-profile-picture-img', 200, 200, true);
add_image_size('page-header-img', 1440, 800, true);
add_image_size('profile-img', 560, 560, true);
add_image_size('features-img', 540, 540, true);
add_image_size('conversations-thumbnail', 600, 338, true);


/*
to add more sizes, simply copy a line from above
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 100 sized image,
we would use the function:
<?php the_post_thumbnail( 'image-300' ); ?>
for the 600 x 150 image:
<?php the_post_thumbnail( 'pimage-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

add_filter('image_size_names_choose', 'custom_image_sizes');

function custom_image_sizes($sizes)
{
    return array_merge($sizes, array(

        'image-600' => __('600px by 600px', 'mainthemee'),
        'image-300' => __('300px by 300px', 'mainthemee'),
        'image-150' => __('150px by 150px', 'mainthemee'),

        )
    );
}

/*
The function above adds the ability to use the dropdown menu to select
the new images sizes you have just created from within the media manager
when you add media to your content blocks. If you add more image sizes,
duplicate one of the lines in the array and name it according to your
new image size.
*/

/****************************************
Active sidebars
****************************************/

// Sidebars & Widgetizes Areas
function register_custom_sidebars()
{
    register_sidebar(array(

        'id' => 'sidebar1',
        'name' => __('Sidebar 1', 'mainthemee'),
        'description' => __('The first (primary) sidebar.', 'mainthemee'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',

       )
    );

    /*
    to add more sidebars or widgetized areas, just copy
    and edit the above sidebar code. In order to call
    your new sidebar just use the following code:

    Just change the name to whatever your new
    sidebar's id is, for example:

    register_sidebar( array(

        'id' => 'sidebar2',
        'name' => __( 'Sidebar 2', 'mainthemee' ),
        'description' => __( 'The second (secondary) sidebar.', 'mainthemee' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',

       )
    );

    To call the sidebar in your template, you can just copy
    the sidebar.php file and rename it to your sidebar's name.
    So using the above example, it would be:
    sidebar-sidebar2.php

    */
} // don't remove this bracket!



/*********************
Theme support
*********************/

// support all of the theme things
function theme_support()
{

    // wp thumbnails (sizes handled in functions.php)
    add_theme_support('post-thumbnails');

    // default thumb size
    set_post_thumbnail_size(125, 125, true);

    // rss thingy
    add_theme_support('automatic-feed-links');

    // To add another menu, uncomment the second line and change it to whatever you want. You can have even more menus.
    register_nav_menus(array(

          'main-nav' => __('The Main Menu', 'mainthemee'),   // main nav in header
          'footer-features-links' => __( 'Footer Features & Tools Links', 'mainthemee' ), // Feates & Tools links in Footer.
          'footer-resources-links' => __( 'Footer Resources Links', 'mainthemee' ) // Resources links in Footer.

        )
    );

    // Title tag
    add_theme_support('title-tag');

    // Enable support for HTML5 markup.
    add_theme_support('html5', array(

        'comment-list',
        'comment-form',
        'search-form',
        'gallery',
        'caption'

        )
    );

    /*
    * POST FORMATS
    * Ahhhh yes, the wild and wonderful world of Post Formats.
    * I've never really gotten into them but I could see some
    * situations where they would come in handy. Here's a few
    * examples: https://www.competethemes.com/blog/wordpress-post-format-examples/
    *
    * This theme doesn't use post formats per se but we need this
    * to pass the theme check.
    *
    * We may add better support for post formats in the future.
    *
    * If you want to use them in your project, do so by all means.
    * We won't judge you. Ok, maybe a little bit.
    *
    */

    add_theme_support('post-formats', array(

        'aside',             // title less blurb
        'gallery',           // gallery of images
        'link',              // quick link to other site
        'image',             // an image
        'quote',             // a quick quote
        'status',            // a Facebook like status update
        'video',             // video
        'audio',             // audio
        'chat'               // chat transcript

        )
    );
} /* end theme support */


/*********************
Page navi
*********************/

// Numeric Page Navi (built into the theme by default).
// 2018: probably needs updating
function page_navi()
{
    global $wp_query;

    $bignum = 999999999;

    if ($wp_query->max_num_pages <= 1) {
        return;
    }

    echo '<nav class="pagination">';

    echo paginate_links(array(

        'base'         => str_replace($bignum, '%#%', esc_url(get_pagenum_link($bignum))),
        'format'       => '',
        'current'      => max(1, get_query_var('paged')),
        'total'        => $wp_query->max_num_pages,
        'prev_text'    => '&larr;',
        'next_text'    => '&rarr;',
        'type'         => 'list',
        'end_size'     => 3,
        'mid_size'     => 3

        )

    );

    echo '</nav>';
} /* end page navi */


/*
****************************************
Special Functions
****************************************
*/

// Body Class functions
// Adds more slugs to body class so we can style individual pages + posts.
add_filter('body_class', 'custom_body_class');

function custom_body_class($classes)
{
    global $post;

    if (isset($post)) {

        /* $classes[] = $post->post_type . '-' . $post->post_name; *//*Un comment this if you want the post_type-post_name body class */
        $pagetemplate = get_post_meta($post->ID, '_wp_page_template', true);
        $classes[] = sanitize_html_class(str_replace('.', '-', $pagetemplate), '');
        $classes[] = $post->post_name;
    }

    if (is_page()) {
        global $post;

        if ($post->post_parent) {

            # Parent post name/slug
            $parent = get_post($post->post_parent);
            $classes[] = $parent->post_name;

            # Parent template name
            $parent_template = get_post_meta($parent->ID, '_wp_page_template', true);

            if (!empty($parent_template)) {
                $classes[] = 'template-'.sanitize_html_class(str_replace('.', '-', $parent_template), '');
            }
        }

        // If we *do* have an ancestors list, process it
        // http://codex.wordpress.org/Function_Reference/get_post_ancestors
        if ($parents = get_post_ancestors($post->ID)) {
            foreach ((array)$parents as $parent) {

                // As the array contains IDs only, we need to get each page
                if ($page = get_page($parent)) {
                    // Add the current ancestor to the body class array
                    $classes[] = "{$page->post_type}-{$page->post_name}";
                }
            }
        }

        // Add the current page to our body class array
        $classes[] = "{$post->post_type}-{$post->post_name}";
    }

    return $classes;
}

/*
****************************************
SORT LEADERSHIP TEAM PAGE OLD TO NEW
****************************************
*/

add_action( 'pre_get_posts', function ( $q )
{
    if(    !is_admin()
        && $q->is_main_query()
        && $q->is_post_type_archive( 'leadership_team' )
    ) {
        $q->set( 'posts_per_page', 10 );
        $q->set( 'orderby', 'date' );
        $q->set( 'order', 'ASC' );
      }
});

/*
****************************************
DISABLE SEARCH
****************************************
*/
// WordPress Suchfunktion vollst�ndig abschalten
function ah_filter_query( $query, $error = true ) {
if ( is_search() ) {
$query->is_search = false;
$query->query_vars[s] = false;
$query->query[s] = false;
// to error
if ( $error == true )
$query->is_404 = true;
}
}
add_action( 'parse_query', 'ah_filter_query' );
add_filter( 'get_search_form', create_function( '$a', "return null;" ) );

/*
****************************************
ALTERNATE CLASSES FOR MODULES
****************************************
*/
// Add 'odd' and 'even' post classes
function alternating_post_class ( $classes ) {
 global $current_class;
 if( is_post_type_archive('modules_type') ):
 $classes[] = $current_class;
 $current_class = ($current_class == 'module--txt-img') ? 'module--img-txt' : 'module--txt-img';
 endif;
 return $classes;
}
add_filter ('post_class', 'alternating_post_class');
global $current_class;
$current_class = 'module--txt-img';

/*
****************************************
MODULES ARCHIVE SET TO SHOW UNLIMITED POSTS
STARTING WITH THE OLDEST FIRST
****************************************
*/

add_action( 'pre_get_posts', function ( $q )
{
    if(    !is_admin()
        && $q->is_main_query()
        && $q->is_post_type_archive( 'modules_type' )
    ) {
        $q->set( 'posts_per_page', -1 );
        $q->set( 'orderby', 'date' );
        $q->set( 'order', 'ASC' );
      }
});

/*
****************************************
ALLOW EDITORS TO EDIT MENUS
****************************************
*/
// get the the role object
$role_object = get_role( 'editor' );

// add $cap capability to this role object
$role_object->add_cap( 'edit_theme_options' );

/*

/*
****************************************
RESPONSIVE VIDEOS
****************************************
*/
function custom_oembed_filter($html, $url, $attr, $post_ID) {
    $return = '<div class="embed-container">'.$html.'</div>';
    return $return;
}

add_filter( 'embed_oembed_html', 'custom_oembed_filter', 10, 4 ) ;

/*
****************************************
CONVERSATIONS ARCHIVE
****************************************
*/

add_action( 'pre_get_posts', function ( $q )
{
    if(    !is_admin()
        && $q->is_main_query()
        && $q->is_post_type_archive( 'conversations' )
    ) {
        $q->set( 'posts_per_page', 9 );
      }
});

/*
****************************************
REMOVE DEFAULT WIDGETS
****************************************
*/

function unregister_default_widgets() {
  unregister_widget('WP_Widget_Pages');
  unregister_widget('WP_Widget_Calendar');
  unregister_widget('WP_Widget_Links');
  unregister_widget('WP_Widget_Meta');
  unregister_widget('WP_Widget_Archives');
  unregister_widget('WP_Widget_Search');
  unregister_widget('WP_Widget_Recent_Posts');
  unregister_widget('WP_Widget_Categories');
  unregister_widget('WP_Widget_Recent_Comments');
  unregister_widget('WP_Widget_RSS');
  unregister_widget('WP_Widget_Text');
  unregister_widget('WP_Widget_Tag_Cloud');
  unregister_widget('WP_Widget_Media_Audio');
  unregister_widget('WP_Widget_Media_Video');
  unregister_widget('WP_Widget_Media_Image');
  unregister_widget('WP_Widget_Media_Gallery');
  unregister_widget('WP_Widget_Nav_Menu');
 }

 add_action('widgets_init', 'unregister_default_widgets', 11);
