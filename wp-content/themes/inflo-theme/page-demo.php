<?php
/*
 Template Name: Demo Page
*/
?>


<?php get_header(); ?>

	<div id="content">

		<main id="main" class="clearfix" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">


      <div class="t-section t-section--demo-signup">
        <div class="container">
          <div class="row align-items-center">
            <div class="demo-content col-12 col-md-7 col-lg-7 col-xl-8 order-md-2">
              <h2 class="t-section__title t-section__title--orange t-section__title--left"><?php the_field('demo_page_title'); ?></h2>
              <p class="t-section__subtitle t-section__subtitle--left"><?php the_field('demo_page_subtitle'); ?></p>
              <div class="t-section__content">
								<?php the_field('demo_page_content'); ?>
							</div>
            </div>
            <div class="col-12 col-md-5 col-lg-5 col-xl-4 order-md-1">
              <div class="demo-contact-form">
                <p class="demo-contact-form__title">
                  <?php the_field('demo_form_title'); ?>
                </p>
                <?php echo do_shortcode( '[contact-form-7 id="1372" title="Demo Form"]' ); ?>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="t-section">
        <div class="container">
          <div class="row">
            <div class="col">
              <h2 class="t-section__title t-section__title"><?php the_field('experience_team_title'); ?></h2>
              <p class="t-section__subtitle"><?php the_field('experience_team_subtitle'); ?></p>
              <div class="t-section__team container">
<div class="row justify-content-around">
<?php


$args = array(
    'post_type' => 'leadership_team',
    'posts_per_page' => -1,
    'tax_query' => array (
            array (
                'taxonomy' => 'leadership_team_cat',
                'field' =>  'name',
                'terms' => 'Experience Team',
            ),
        ),
    );

$myposts = get_posts( $args );
foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
<div class="col-8 col-md-4">
      <a class="team-member" href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
          <article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">

            <div class="team-member__img">
              <?php
                $image = get_field('leadership_team_picture');
                $size = 'profile-img'; // (thumbnail, medium, large, full or custom size)
                  if ($image) {
                      echo wp_get_attachment_image($image, $size);
                  }
              ?>
            </div>

            <section class="team-member__text">
              <h3 class="team-member__name"><?php the_title(); ?></h3>
              <p class="team-member__role"><?php the_field('leadership_team_role');?></p>
            </section>

          </article>
        </a>
</div>
<?php endforeach;
wp_reset_postdata();?>
</div>

				</div>
            </div>
          </div>
        </div>
      </div>


			<div class="t-section t-section--cta-band t-section--green">
        <div class="container">
          <div class="row">
            <div class="col">
              <p><?php the_field('demo_cta_text'); ?></p>
              <a href="<?php the_field('demo_cta_button_url'); ?>" class="btn btn--orange-500"><?php the_field('demo_cta_button_text'); ?></a>
            </div>
          </div>
        </div>
      </div>


      <div class="t-section t-section--cusomters">
        <div class="container">
          <div class="row">
            <div class="col">
              <h2 class="t-section__title"><?php the_field('existing_customers_title'); ?></h2>
              <p class="t-section__subtitle"><?php the_field('existing_customers_subtitle'); ?></p>
            </div>
          </div>
          <div class="row justify-content-space-around align-items-center">
            <?php
            if( have_rows('company_logos') ): ?>

              <?php while( have_rows('company_logos') ): the_row();
                // vars
                $image = get_sub_field('company_logo');
                $size = 'quote-logo-img'; // (thumbnail, medium, large, full or custom size)
              ?>

                  <div class="customers__logo col-sm-auto">
                    <?php echo wp_get_attachment_image($image, $size); ?>
                  </div>

              <?php endwhile; ?>

            <?php endif; ?>
          </div>
        </div>
      </div>


      <div class="t-section t-section--quote">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-12 col-md-4 col-lg-2">
              <?php

                $image = get_field('quote_author_picture');
                $size = 'quote-profile-picture-img'; // (thumbnail, medium, large, full or custom size)

                if( $image ) {
                  echo wp_get_attachment_image( $image, $size );
                }

              ?>            </div>
            <div class="col-12 col-md-8 col-lg-10">
              <blockquote>
                <p><?php  the_field('quote'); ?></p>
                <footer>
                <cite><?php the_field('author'); ?></cite>
                </footer>
              </blockquote>
            </div>
          </div>
        </div>
      </div>

		</main>

	</div>

<?php get_footer(); ?>
