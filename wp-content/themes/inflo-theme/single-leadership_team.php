<?php get_header(); ?>

	<div id="content" class="container section-body section-body--leadership-single">

    <div class="row">

      <main id="main" class="col-12 col-md-8 col-lg-8" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        	<article id="post-<?php the_ID(); ?>" <?php post_class('leadership-team-member clearfix'); ?> role="article" itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting">

        		<div class="row">
              <div class="col-12 col-lg-3 leadership-team-member__img">
                <?php
                  $image = get_field('leadership_team_picture');
                  $size = 'profile-img'; // (thumbnail, medium, large, full or custom size)
                    if ($image) {
                      echo wp_get_attachment_image($image, $size);
                    }
                ?>

              </div>

              <div class="col-12 col-lg-9 entry-content">
                <h2 class="leadership-team-member__name" itemprop="headline" rel="bookmark"><?php the_title(); ?></h2>
                <p class="leadership-team-member__role"><?php the_field('leadership_team_role'); ?></p>
                <?php the_content(); ?>

                <?php if( get_field('cta_button_url') ): ?>
                  <div class="row">
                    <div class="col cta">
                      <p class="cta__text"><?php the_field('call_to_action'); ?></p>
                      <a href="<?php the_field('cta_button_url'); ?>" class="btn btn--orange-500"><?php the_field('cta_button_text'); ?></a>
                    </div>
                  </div>
                <?php endif; ?>

                </div>
              </div>

        	</article> <?php // end article ?>

        <?php endwhile; endif; ?>

			</main>

      <div class="col-12 col-md-4 col-lg-4">
	      <?php get_sidebar(); ?>
      </div>

    </div>

	</div>

<?php get_footer(); ?>
