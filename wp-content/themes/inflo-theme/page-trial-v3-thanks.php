<?php
/*
 Template Name: Trial Page Version 3 Thanks
*/
?>

<?php get_header('standard'); ?>
  <div class="row">
    <div class="col-12 col-md-5 col-lg-5 v__banner">
      <div class="v__banner__content">
        <a class="header__logo text-hide vertical__banner__logo" href="<?php echo home_url(); ?>"></a>
        <p class="header__subtitle vertical__banner__subtitle">Using Inflo is free. Forever.</p>
      </div>
    </div>

    <div class="col-12 col-md-7 col-lg-7">
      <div class="signup__wrapper">
        <h1 class="header__title">Congratulations for joining Inflo</h1>
        <p class="hp-section__subtitle">Your request has been passed to our onboarding team who are now setting up your account and will be in touch soon. Please keep your eyes open for your activation email.
        <br/><br/>
          <a href="/">Return To Homepage</a>
          <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        </p>

      </div>
    </div>
  </div>
<?php get_footer(); ?>
