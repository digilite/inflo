<?php
/*
 Template Name: Trial Page Version 3
*/
?>

<?php get_header('standard'); ?>
  <div class="row">
    <div class="col-12 col-md-12 col-lg-5 v__banner">
      <div class="v__banner__content">
        <a class="header__logo text-hide vertical__banner__logo" href="<?php echo home_url(); ?>"></a>
        <p class="header__subtitle vertical__banner__subtitle"><?php the_field('trial_page_title'); ?></p>
      </div>
    </div>

    <div class="col-12 col-md-12 col-lg-7">
      <div class="signup__wrapper">
        <h1 class="header__title">Valuable planning – for free </h1>
        <p class="hp-section__subtitle">Want to implement the ten best practices for valuable audit planning in your practice? <br/><br/>
 
Use Inflo on an unlimited number of clients for free by signing up today. </p>
        <?php echo do_shortcode( '[contact-form-7 id="1640" title="Trial V4"]' ); ?>
      </div>
    </div>
  </div>
<?php get_footer(); ?>
