<?php
/*
 Template Name: Trial Page Version EG19
*/
?>

<?php get_header('standard'); ?>
  <div class="row">
    <div class="col-12 col-md-12 col-lg-5 v__banner">
      <div class="v__banner__content">
        <p class="header__subtitle vertical__banner__subtitle">In a time of rapid change, standing still</p>
<p class="header__subtitle vertical__banner__subtitle"> is the most dangerous course of action</p>
 </div>
    </div>

    <div class="col-12 col-md-12 col-lg-7">
      <div class="signup__wrapper">
        <h1 class="header__title">Start Today</h1>
        <p class="hp-section__subtitle"> Sign up to Inflo for free. 
</p>
 <p class="hp-section__subtitle"></p>
        <?php echo do_shortcode( '[contact-form-7 id="1731" title="Audit execution top tips"]'); ?>

      </div>
    </div>
  </div>
<?php get_footer(); ?>

