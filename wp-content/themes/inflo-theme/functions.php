<?php

/* Admin Customisation */
require_once('app/admin.php');

/* Reboot */
require_once( 'app/reboot.php' );

/* Scripts and Styles*/
require_once( 'app/scripts-and-styles.php' );

/* Theme Features */
require_once( 'app/theme-features.php' );

/* Post Types */
require_once( 'library/post-types/leadership-team-post-type.php' );
require_once( 'library/post-types/modules-post-type.php' );
require_once( 'library/post-types/features-post-type.php' );
require_once( 'library/post-types/webinars-post-type.php' );
require_once( 'library/post-types/blog-post-type.php' );
require_once( 'library/post-types/case-studies-post-type.php' );
require_once( 'library/post-types/conversations-post-type.php' );

/* Sidebars */
require_once( 'app/sidebars/standard-sidebar.php' );
require_once( 'app/sidebars/standard-sidebar-green.php' );
require_once( 'app/sidebars/inflohi-sidebar.php' );
require_once( 'app/sidebars/quote-sidebar.php' );
require_once( 'app/sidebars/newsletter-sidebar.php' );


/****************************************
* SCHEMA *
http://www.longren.io/add-schema-org-markup-to-any-wordpress-theme/
****************************************/

function html_schema() {

    $schema = 'http://schema.org/';

    // Is single post
    if( is_single()) {
        $type = "Article";
    }

    // Is blog home, archive or category
    else if( is_home() || is_archive() || is_category() ) {
        $type = "Blog";
    }

    // Is static front page
    else if( is_front_page()) {
        $type = "Website";
    }

    // Is a general page
     else {
        $type = 'WebPage';
    }

    echo 'itemscope="itemscope" itemtype="' . $schema . $type . '"';
}

// ADD EVENT LISTENER FOR Contact Foem 7 DB
add_action( 'wp_footer', 'redirect_cf7' );

function redirect_cf7() {
  ?>
  <script>
    document.addEventListener( 'wpcf7mailsent', function( event ) {
      location = 'https://inflosoftware.com/thank-you-free-trial/';
    }, false );
  </script>
  <?php
}

// Display dash icons
function ww_load_dashicons(){
  wp_enqueue_style('dashicons');
}
add_action('wp_enqueue_scripts', 'ww_load_dashicons', 999);

?>
