<?php get_header(); ?>

	<div id="content">

		<div id="inner-content" class="clearfix">

			<main id="main" class="container clearfix" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

        <div class="modules-intro">
          <p><?php the_field('modules_introduction', '10'); ?></p>
        </div>

<div class="row">
  <div class="col">
        <?php if (have_posts()) : while(have_posts()) : the_post();  ?>

        <a href="<?php the_permalink() ?>"  title="<?php the_title_attribute(); ?>" id="post-<?php the_ID(); ?>" <?php post_class('row module'); ?> >

          <div class="col-12 col-md-6 module__img-holder">
            <?php
              $image = get_field('module_icon');
              $size = 'features_img'; // (thumbnail, medium, large, full or custom size)

              if( $image ) {
                echo wp_get_attachment_image( $image, $size );
              }
            ?>
          </div>

          <div class="col-12 col-md-6 module__text-holder">
            <h2 class="module__title"><?php the_title(); ?></h2>
            <p><?php the_field('page_header_subtitle'); ?></p>
            <p class="btn btn--orange-500">Learn more</p>
          </div>
        </a>

        <?php endwhile; endif; ?>

      </div>
    </div>

			</main>

		</div>

	</div>

<?php get_footer(); ?>
