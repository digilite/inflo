<?php
/*
 Template Name: Trial Page Version ICAEW2 
*/
?>

<?php get_header('standard'); ?>
  <div class="row">
    <div class="col-12 col-md-12 col-lg-5 v__banner">
      <div class="v__banner__content">
        <p class="header__subtitle vertical__banner__subtitle">ICAEW's new initiative in partnership with Inflo.</p> 
        <p class="header__subtitle vertical__banner__subtitle">Member firms will have access to a free, full functionality version of Inflo’s technology</p>
      </div>
    </div>

    <div class="col-12 col-md-12 col-lg-7">
      <div class="signup__wrapper">
        <h1 class="header__title">ICAEW partners with Inflo to support member firm adoption of emerging technologies.</h1>
        <p class="hp-section__subtitle">Inflo will provide each ICAEW member firm with £1,500 of initial credit for free as well as advanced support services tailored to the needs of smaller firms. This will provide firms with access to the change management skills and resources to help evolve their service delivery</p>
        <?php echo do_shortcode( '[contact-form-7 id="1691" title="Trial V3 ICAEW"]' ); ?>

      </div>
    </div>
  </div>
<?php get_footer(); ?>

