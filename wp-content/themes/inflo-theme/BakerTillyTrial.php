<?php
/*
 Template Name: Trial Page Version Baker Tilly
*/
?>

<?php get_header('standard'); ?>
  <div class="row">
    <div class="col-12 col-md-12 col-lg-5 v__banner">
      <div class="v__banner__content">
        <p class="header__subtitle vertical__banner__subtitle">Baker Tilly Firms can use Inflo for free.</p> 
        <p class="header__subtitle vertical__banner__subtitle">Forever.</p>
      </div>
    </div>

    <div class="col-12 col-md-12 col-lg-7">
      <div class="signup__wrapper">
        <h1 class="header__title">Baker Tilly Benefit.</h1>
        <p class="hp-section__subtitle">As part of our strategic partnership with Baker Tilly International, member firms can get hands-on with our next-generation software for free. In our free version you can review our example client and upload interim data from an unlimited number of clients to plan live engagements. 
</p>
 <p class="hp-section__subtitle">If you would to hear about the new-firm starter packages, which include support from our expert team, let us know below. We designed these with Adam Grainger and Paul Winrow based on the experience of other Baker Tilly firms and Global Focus. Discounts are available as a further Baker Tilly benefit.</p>
        <?php echo do_shortcode( '[contact-form-7 id="1696" title="Trial V3 Baker Tilly"]'); ?>

      </div>
    </div>
  </div>
<?php get_footer(); ?>

