<?php
/*
 Template Name: Workshop
*/
?>


<?php get_header(); ?>

	<div id="content">

		<main id="main" class="clearfix" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">


      <div class="t-section t-section--no-bottom-padding">
        <div class="container">
          <div class="row">
            <div class="col">
              <h2 class="t-section__title t-section__title--green"><?php the_field('workshop_page_title'); ?></h2>
              <p class="t-section__subtitle"><?php the_field('workshop_page_subtitle'); ?></p>
            </div>
          </div>
        </div>

        <div class="workshop-background">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-12 col-md-8 col-lg-6">
                <div class="demo-contact-form demo-contact-form--large-margin-small-top">
                  <p class="demo-contact-form__title">
										<?php the_field('workshop_page_form_title'); ?>
                  </p>
                  <?php echo do_shortcode( '[contact-form-7 id="1422" title="Workshop signup"]' ); ?>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

      <div class="t-section t-section--faq">
        <div class="container">
          <div class="row">
            <div class="col">
              <h2 class="t-section__title"><?php the_field('faq_title'); ?></h2>
              <p class="t-section__subtitle"><?php the_field('faq_subtitle'); ?></p>
            </div>
          </div>
          <div class="row justify-content-center">
            <div class="col-12 col-lg-8">
              <?php if( have_rows('accordion_section') ): ?>

                <div class="page-accordion">

                  <?php while( have_rows('accordion_section') ): the_row();

                    // vars
                    $title = get_sub_field('accordion_title');
                    $content = get_sub_field('accordion_content');
                    $cta_txt = get_sub_field('accordion_call_to_action');
                    $cta_btn = get_sub_field('accordion_call_to_action_button_text');
                    $cta_url = get_sub_field('accordion_call_to_action_url');

                  ?>

                    <div class="accordion-holder">

                    <button class="accordion">
                      <p class="accordion__title">
                        <?php echo $title; ?>
                      </p>
                    </button>

                    <div class="panel">
                      <div class="panel__content entry-content">

                        <?php echo $content; ?>

                        <?php if( get_sub_field('accordion_call_to_action_url') ): ?>
                          <div class="cta">
                            <h2 class="cta__text"><?php echo $cta_txt; ?></h2>
                            <a href="<?php echo $cta_url; ?>" class="btn btn--orange-500"><?php echo $cta_btn; ?></a>
                          </div>
                        <?php endif; ?>

                      </div>
                    </div>

                  </div>

                  <?php endwhile; ?>

                </div>

              <?php endif; ?>
            </div>
          </div>
          <?php if( get_field('faq_cta') ): ?>
            <div class="row">
              <div class="col t-section__cta">
                <p class="t-section__subtitle"><?php the_field('faq_cta'); ?></p>
                <a href="<?php the_field('faq_cta_button_url'); ?>" class="btn btn--orange-500"><?php the_field('faq_cta_button_text'); ?></a>
              </div>
            </div>
          <?php endif; ?>
        </div>
      </div>

      <div class="t-section t-section--quote">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-12 col-md-4 col-lg-2">
              <?php

                $image = get_field('quote_author_picture');
                $size = 'quote-profile-picture-img'; // (thumbnail, medium, large, full or custom size)

                if( $image ) {
                  echo wp_get_attachment_image( $image, $size );
                }

              ?>            </div>
            <div class="col-12 col-md-8 col-lg-10">
              <blockquote>
                <p><?php  the_field('quote'); ?></p>
                <footer>
                <cite><?php the_field('author'); ?></cite>
                </footer>
              </blockquote>
            </div>
          </div>
        </div>
      </div>


		</main>

	</div>

<?php get_footer(); ?>
