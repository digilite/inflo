<?php
/*
 Template Name: Trial Page Version Moore2
*/
?>

<?php get_header('standard'); ?>
  <div class="row">
    <div class="col-12 col-md-12 col-lg-5 v__banner">
      <div class="v__banner__content">
        <p class="header__subtitle vertical__banner__subtitle">Moore Members</p>
 </div>
    </div>

    <div class="col-12 col-md-12 col-lg-7">
      <div class="signup__wrapper">
        <h1 class="header__title">Moore Members</h1>
        <p class="hp-section__subtitle">As strategic partners, Moore member firms can sign up to Inflo for free. 
</p>
 <p class="hp-section__subtitle"></p>
        <?php echo do_shortcode( '[contact-form-7 id="1724" title="Trial Basic Moore"]'); ?>

      </div>
    </div>
  </div>
<?php get_footer(); ?>

