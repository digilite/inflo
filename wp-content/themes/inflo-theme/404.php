<?php get_header(); ?>

	<div id="content">

		<div id="inner-content" class="container clearfix">

			<main id="main" class="row clearfix" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

				<article id="post-not-found" class="col clearfix">

					<section class="404-page">

						<h3><?php _e( '404 - Page not found.', 'maintheme' ); ?></h3>
						<p>We couldn't find what you are looking for.</p>

					</section>

				</article>

			</main>

		</div>

	</div>

<?php get_footer(); ?>
