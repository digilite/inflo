<?php get_header(); ?>

	<div id="content" class="container section-body section-body--sidebar">

    <div class="row">

      <main id="main" class="col-12 archive-holder" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

        <div class="archive__catagories">

        <?php if ( is_post_type_archive( 'blog' )) { ?>

          <?php $categories = get_categories('taxonomy=blog_custom_cat&post_type=blog'); ?>
              <p>Filter by:</p>
              <ul>
              <?php foreach ($categories as $category) : ?>
                  <li><a href="<?php echo get_category_link($category->cat_ID); ?>"><?php echo $category->name; ?></a></li>
          <?php endforeach; ?>
          </ul>
      <?php   }

        ?>

        </div>

        <div class="row">
				  <?php get_template_part( 'templates/conversations-archive', 'loop');  ?>
        </div>

        <?php if( get_field('cta_button_url', 1073) ): ?>
          <div class="row">
            <div class="col cta cta--center">
              <p class="cta__text"><?php the_field('call_to_action', 1073); ?></p>
              <a href="<?php the_field('cta_button_url', 1073); ?>" class="btn btn--orange-500"><?php the_field('cta_button_text', 1073); ?></a>
            </div>
          </div>
        <?php endif; ?>

      </main>

      </div>

  </div>

<?php get_footer(); ?>
