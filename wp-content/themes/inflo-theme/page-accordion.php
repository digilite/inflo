<?php
/*
 Template Name: Accordion
*/
?>

<?php get_header(); ?>

	<div id="content" class="container section-body section-body--sidebar">

    <div class="row">

      <main id="main" class="col-12 col-md-8" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

          <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

          	<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

          		<section class="entry-content clearfix" itemprop="articleBody">

          			<?php the_content(); ?>

          		</section> <?php // end article section ?>

                  <?php if( have_rows('accordion_section') ): ?>

                    <div class="page-accordion">

                      <?php while( have_rows('accordion_section') ): the_row();

                        // vars
                        $title = get_sub_field('accordion_title');
                        $content = get_sub_field('accordion_content');
                        $cta_txt = get_sub_field('accordion_call_to_action');
                        $cta_btn = get_sub_field('accordion_call_to_action_button_text');
                        $cta_url = get_sub_field('accordion_call_to_action_url');

                      ?>

                        <div class="accordion-holder">

                        <button class="accordion">
                          <p class="accordion__title">
                            <?php echo $title; ?>
                          </p>
                        </button>

                        <div class="panel">
                          <div class="panel__content entry-content">

                            <?php echo $content; ?>

                            <?php if( get_sub_field('accordion_call_to_action_url') ): ?>
                              <div class="cta">
                                <h2 class="cta__text"><?php echo $cta_txt; ?></h2>
                                <a href="<?php echo $cta_url; ?>" class="btn btn--orange-500"><?php echo $cta_btn; ?></a>
                              </div>
                            <?php endif; ?>

                          </div>
                        </div>

                      </div>

                      <?php endwhile; ?>

                    </div>

                  <?php endif; ?>


          		<footer class="article-footer clearfix">

                <?php if( get_field('cta_button_url') ): ?>
                  <div class="row">
                    <div class="col cta">
                      <p class="cta__text"><?php the_field('call_to_action'); ?></p>
                      <a href="<?php the_field('cta_button_url'); ?>" class="btn btn--orange-500"><?php the_field('cta_button_text'); ?></a>
                    </div>
                  </div>
                <?php endif; ?>

          		</footer>

          	</article>

          <?php endwhile; endif; ?>


        </main>

        <div class="col-12 col-md-4">
  	      <?php get_sidebar(); ?>
        </div>

        </div>



  	</div>

  <?php get_footer(); ?>
