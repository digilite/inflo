<?php
/*
 Template Name: Trial Page Version 2
*/
?>


<?php get_header(); ?>
	<div id="content">

		<main id="main" class="clearfix" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
      <div class="t-section t-section--no-bottom-padding">
        <div class="container">
          <div class="row">
            <div class="col">
              <h2 class="t-section__title t-section__title--green"><?php the_field('trial_page_title'); ?></h2>
              <p class="t-section__subtitle"><?php the_field('trial_page_subtitle'); ?></p>
            </div>
          </div>
        </div>

        <div class="trial-background">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-12 col-md-8 col-lg-6">
                <div class="demo-contact-form demo-contact-form--large-margin-small-top">
                  <p class="demo-contact-form__title">
                    <?php the_field('trial_form_title'); ?>
                  </p>
                    <?php echo do_shortcode( '[contact-form-7 id="1386" title="Start Trail (1 of 2)"]' ); ?>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>


      <div class="t-section t-section--quote">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-12 col-md-4 col-lg-2">
              <?php

                $image = get_field('quote_author_picture');
                $size = 'quote-profile-picture-img'; // (thumbnail, medium, large, full or custom size)

                if( $image ) {
                  echo wp_get_attachment_image( $image, $size );
                }

              ?>            </div>
            <div class="col-12 col-md-8 col-lg-10">
              <blockquote>
                <p><?php  the_field('quote'); ?></p>
                <footer>
                <cite><?php the_field('author'); ?></cite>
                </footer>
              </blockquote>
            </div>
          </div>
        </div>
      </div>


		</main>

	</div>

<?php get_footer(); ?>
