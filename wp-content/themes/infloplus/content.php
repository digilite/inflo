
<?php
    if( have_rows("content_builder") ):
        // loop through the rows of data
        while ( have_rows("content_builder") ) : the_row();
            switch (get_row_layout()) :
              case "homepage_header":
                get_template_part("templates/content/homepage-header");
                break;
              case "two_column_content_with_icon":
                get_template_part("templates/content/two-column-content");
                break;
              case "video_cta":
                get_template_part("templates/content/video-cta");
                break;
              case "cta_block":
                get_template_part("templates/content/general-cta");
                break;
              case "show_plans":
                get_template_part("templates/content/show-plans");
                break;
              case "product_header":
                get_template_part("templates/content/product-header");
                break;
              case "dg_testimonials_block":
                get_template_part("templates/content/testimonials");
                break;
              case "image_title_description":
                get_template_part("templates/content/image-description-block");
                break;

              case "dg_background_image_section_with_cta":
                get_template_part("templates/content/background-section");
                break;
              case "numbered_column_with_titles":
                get_template_part("templates/content/numbered-column");
                break;
              case "portfolio_slider":
                get_template_part("templates/content/show-portfolios");
                break;
              case "showcase_portfolios":
                get_template_part("templates/content/showcase-portfolios");
                break;
              case "text_over_image_and_mixed_content_option":
                get_template_part("templates/content/text-over-image-mixed-content");
                break;
              case "show_blog_posts":
                get_template_part("templates/content/blog-posts");
                break;
              case "logos_section":
                get_template_part("templates/content/logos-section");
                break;
              case "product_hero":
                get_template_part("templates/content/product-hero");
                break;
              case "logo_list":
                get_template_part("templates/content/logo-list");
                break;
              case "product_tabs":
                get_template_part("templates/content/product-tabs");
                break;
              case "about_us":
                get_template_part("templates/content/about-us");
                break;
              case "faq":
                get_template_part("templates/content/faq");
                break;
              case "related_products":
                get_template_part("templates/content/related-products");
                break;
              case "related_products":
                get_template_part("templates/content/related-products");
                break;
              case "latest_posts":
                get_template_part("templates/content/latest-posts");
                break;
              case "search_field":
                get_template_part("templates/content/search-field");
                break;
              case "gradient_buttons":
                get_template_part("templates/content/gradient-buttons");
                break;
              case "story":
                get_template_part("templates/content/story");
                break;
              case "briefing_papers":
                get_template_part("templates/content/briefing-papers");
                break;
            endswitch;
            //$block_counter++;
        endwhile;
    endif;
?>
