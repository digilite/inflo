<?php
// Add functionality for ACF syncing across development environments

add_filter('acf/settings/save_json', 'my_acf_json_save_point');

function my_acf_json_save_point($path)
{
    $path = get_stylesheet_directory() . '/library/acf-sync';
    return $path;
}

add_filter('acf/settings/load_json', 'my_acf_json_load_point');

function my_acf_json_load_point($paths)
{
    unset($paths[0]);
    $paths[] = get_stylesheet_directory() . '/library/acf-sync';
    return $paths;
}
