<?php get_header(); ?>

<div class="container">
	<div class="search-results-content">
		<h2 class="h2 text-center">Search Results</h2>
		<div class="row">
			<?php
			if (have_posts()) :
				while (have_posts()) :
					the_post(); ?>
					<div class="col-md-4 mt-5">
						<div class="post-item">
							<div class="post-g-info">
							<div class="lt-post-thumb">
								<?php the_post_thumbnail('medium'); ?> 
							</div>
							<div class="post-info">
								<p class="author-name"><?php the_author(); ?></p>
								<p class="post-date"><?php the_date(); ?></p>
							</div>
							<div class="post-title-block">
								<h4 class="post-title"><?php the_title(); ?></h4>
							</div>
							</div>
							<?php
							$bt_link = get_permalink();?>
							<div class="post-url">
							<?php echo_button('Read More', $bt_link,"button button-icon", "show"); ?>
							</div>
						</div>
					</div>
					<?php
				endwhile;
			endif; ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>