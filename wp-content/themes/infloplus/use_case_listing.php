<?php /* Template Name: Inflo Use Case Listing */ ?>

<?php get_header(); ?>

<?php $chosen_post = get_field('use_case_listing_chosen_post');
//$post_tag = get_the_tags( $chosen_post->ID )[0];
//$tag_link = get_tag_link($post_tag->term_id);
$post_tags = get_the_terms($chosen_post->ID, 'product_tax');
?>
    <section class="detail-intro-section bg-light border-bottom-light">
        <div class="container">
            <div class="detail-intro-row reverse">
                <div class="col text-col">
                    <h1 class="h2"><?= $chosen_post->post_title ?></h1>
                    <?php if ($post_tags) { ?>
                        <ul class="topic-list">
                            <?php foreach ($post_tags as $post_tag) {
                                $tag_link = get_term_link($post_tag->term_id, 'product_tax');//get_tag_link($post_tag->term_id);
                                ?>
                                <li>
                                    <a href="<?= $tag_link ?>">
                                        <img loading="lazy"
                                             src="<?= get_field('image_for_this_taxonomy', $post_tag)['sizes']['thumbnail'] ?>"
                                             class="icon"> <?= $post_tag->name ?>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                    <a href="<?= get_permalink($chosen_post->ID) ?>" class="btn">Read this story</a>
                </div>
                <div class="col img-col"
                     style="background-image: url(<?= get_the_post_thumbnail_url($chosen_post->ID, 'medium') ?>);"></div>
            </div>
        </div>
    </section>


<?php
$latest_or_selected = get_field('list_case_studies_by');
$list_case_studies = get_field('list_case_studies');

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$args = array(
    'post_type' => 'use_cases',
    'posts_per_page' => 12,
    'paged' => $paged,
);
if ($latest_or_selected == 'Free') {
    $args['post__in'] = $list_case_studies;
    $args['orderby'] = 'post__in';
}
$loop = new WP_Query($args);
if ($loop->have_posts()): ?>
    <section class="default-section bg-grey">
        <div class="container">
            <div class="post-slider post-light">
                <?php while ($loop->have_posts()): $loop->the_post(); ?>
                    <div class="post-slide">
                    <div class="use-case-preview-sm">
                        <div class="descr-block">
                            <?php the_title('<div class="title-holder"><h4 class="title"><a href="' . get_permalink() . '">', '</a></h4></div>'); ?>
                            <ul class="topic-list">
                                <?php
                                $this_post_tags = get_the_terms(get_the_ID(), 'product_tax');
                                foreach ($this_post_tags as $this_post_tag):
                                    $this_tag_link = get_term_link($this_post_tag->term_id, 'product_tax'); ?>
                                    <li>
                                        <a href="<?php echo $this_tag_link; ?>">
                                            <img loading="lazy"
                                                 src="<?php echo get_field('image_for_this_taxonomy', $this_post_tag)['sizes']['thumbnail']; ?>"
                                                 class="icon">
                                            <?php echo $this_post_tag->name; ?>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                            <a href="<?php the_permalink(); ?>"
                               class="btn btn-outline"><?php _e('Read story', 'hello-elementor-child'); ?></a>
                        </div>
                    </div>
                    </div><?php
                endwhile; ?>
            </div>
            <div class="pagination">
                <?php
                $big = 999999999;
                echo paginate_links(array(
                    'base' => str_replace($big, '%#%', get_pagenum_link($big)),
                    'format' => '?paged=%#%',
                    'current' => max(1, $paged),
                    'total' => $loop->max_num_pages,
                    'prev_text' => '&laquo;',
                    'next_text' => '&raquo;',
                    'type' => 'list',
                ));
                ?>
            </div>
            <?php wp_reset_postdata(); ?>
            <?php if (get_field('display_bottom_cta') != 'No') { ?>
                <div class="cta-block">
                    <p><?= the_field('bottom_cta_description') ?></p>
                    <div class="btn-center-holder">
                        <a href="<?= the_field('bottom_cta_destination') ?>"
                           class="btn"><?= the_field('bottom_cta_text') ?></a>
                    </div>
                </div>
            <?php } ?>
        </div>
    </section>
<?php endif; ?>

<?php get_footer();
