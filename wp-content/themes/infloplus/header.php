<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo wp_get_document_title(); ?></title>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link href="https://fonts.googleapis.com/css2?family=Baloo+2:wght@400;700&family=Lato:ital,wght@0,300;0,400;0,700;0,900;1,300;1,400;1,700;1,900&display=swap" rel="stylesheet">
	<!-- Google Tag Manager
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-TND8VFH');</script>
	End Google Tag Manager -->


	<!-- Generate favicon here http://www.favicon-generator.org/ -->
	<?php wp_head(); ?>
</head>
<?php
  if (get_field("dark_header")){
    $class = "dark-header";
  } elseif ($img_url = get_field("main_background","option")) {
    $background = "style = 'background: url(". $img_url .") no-repeat left top / contain; background-size: 60% 460px;'";
  }
?>


<body <?php body_class(); echo $background; ?>>

<!-- Google Tag Manager (noscript)
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TND8VFH"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
End Google Tag Manager (noscript) -->

	<header itemscope itemtype="http://schema.org/WPHeader" class=<?= $class; ?>>
		<div class="container flex-container header-items">
      <div class="row justify-content-between">
        <div class="col-sm-9">
          <nav itemscope itemtype="http://schema.org/SiteNavigationElement">
            <?php wp_nav_menu([ 'theme_location' => 'secondary-menu', 'menu_class' => 'nav navbar-expand secondary-menu', 'container' => '',]); ?>
          </nav>
          <div class="d-flex alig-items-end">
            <div itemscope itemtype="http://schema.org/Organization" id="logo">
              <a itemprop="url" href="<?php echo bloginfo('url') ?>">
                <?php $image_id = get_field("website_logo","option"); ?>
                <?php echo wp_get_attachment_image($image_id,"medium"); ?>
              </a>
            </div>
            <nav class="d-flex" itemscope itemtype="http://schema.org/SiteNavigationElement">
              <?php wp_nav_menu([ 'theme_location' => 'primary-menu', 'menu_class' => 'nav navbar-expand main-menu', 'container' => '',]); ?>
            </nav>
          </div>
        </div>
        <div class="col-sm-3 d-flex flex-column">
          <nav itemscope itemtype="http://schema.org/SiteNavigationElement">
            <?php wp_nav_menu([ 'theme_location' => 'language-menu', 'menu_class' => 'nav navbar-expand language-menu', 'container' => '',]); ?>
          </nav>
          <a href="//info.inflosoftware.com/en-gb/get-a-demo" class="button button-orange button-small" target="_blank">Get a demo</a>
        </div>
      </div>
		</div>
	</header>
  <main class="<?php the_field("skin_color_"); ; ?>">
