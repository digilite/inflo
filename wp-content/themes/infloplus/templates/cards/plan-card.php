<div class="plans-card">
	<h3 class="plan-title h5">
    <?php
      the_title();
      if (get_field("most_popular")):
        echo "<span class='best-plan'>Most popular</span>";
      endif;
    ?>
  </h3>
  <div class="plan-content d-flex flex-column">
    <?php if($des = get_field("short_description")): ?>
      <p><?= $des; ?></p>
    <?php endif; ?>
    <a class="button" href="<?= get_page_link(1557); ?>">Request a call</a>
    <div class="plan-details">
      <?= $post->post_content; ?>
    </div>
  </div>
</div>
