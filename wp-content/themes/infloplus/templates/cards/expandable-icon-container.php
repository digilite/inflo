<div class="ex-container">
  <div class="top-part d-flex align-items-center">
    <?php the_post_thumbnail($post->ID,'thumbnail'); ?>
    <h2><?php the_title(); ?></h2>
    <svg class="flex-end" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M19 10C19 5.03125 14.9688 1 10 1C5.03125 1 1 5.03125 1 10C1 14.9688 5.03125 19 10 19C14.9688 19 19 14.9688 19 10Z" stroke="#C1C1C1" stroke-miterlimit="10"/>
      <path d="M13.75 10H6.25M10 6.25V13.75V6.25Z" stroke="#C1C1C1" stroke-linecap="round" stroke-linejoin="round"/>
    </svg>
  </div>
</div>
<div class="expand-content">
  <?= $post->post_content; ?>
</div>
