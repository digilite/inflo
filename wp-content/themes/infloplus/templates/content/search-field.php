<?php
$title = get_sub_field('title');
?>
<section class="search-field">
  <div class="container">
    <?php if($title) { ?>
      <h2 class="h2 text-center"><?= $title; ?></h2>
    <?php } ?>
    <div class="row justify-content-center">
      <div class="col-md-5">
        <div class="cs-search">
          <?php get_search_form(); ?>
        </div>
      </div>
    </div>
  </div>
</section>
