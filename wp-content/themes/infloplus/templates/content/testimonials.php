<?php
  global $block_counter;

  $counter = 1;
  $title = get_sub_field('dg_testimonials_title');

  $args = array(
    'post_type'=> 'testimonials',
    'areas'    => 'testimonial',
    'order'    => 'ASC',
    'meta_query' => array(
      array(
          'key'   => 'show_testimonial_in_block',
          'value' => '1',
      )
    )
  );
?>
<section id="block-<?= $block_counter; ?>" class="testimonials">
   <div class="container">
     <div id="testimonials-slider" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        <?php
          $the_query = new WP_Query( $args );
          if($the_query->have_posts() ) :  while ( $the_query->have_posts() ) : $the_query->the_post();
        ?>
          <div class="carousel-item">
            <div class="row">
              <div class="col-lg-10 col-md-9 testimonial-padd testimonial-content">
                <h2 class="client-say"><?= $title; ?></h2>
                <?php the_content(); ?>
                <a class="icon-btn-container" href="<?= $explorebtnurl; ?>">
                  <span class="icon-btn">
                    <svg class="eye-icon"width="17.5" height="13.886" viewBox="0 0 17.5 13.886"><use xlink:href="#eye-icon"></use></svg>
                  </span>
                  All testimonials
                </a>
              </div>
              <div class="col-lg-2 col-md-3 testimonial-padd">
                  <p class="testimonial-date"><?= $counter; ?></p>
                  <p class="testimonial-project-name"><span>Reviewed by</span><br><?php $testtitle = get_field( 'testimonials_title' ); echo $testtitle ?></p>
              </div>
            </div>
          </div>
        <?php $counter ++; endwhile; wp_reset_postdata(); endif; ?>
      </div>
      <div class="controls">
        <a class="carousel-control-prev icon-btn" href="#testimonials-slider" role="button" data-slide="prev">
          <svg class="arrow-left" width="12" height="10" viewBox="0 0 12 10"><use xlink:href="#arrow-left"></use></svg>
        </a>
        <a class="carousel-control-next icon-btn" href="#testimonials-slider" role="button" data-slide="next">
          <svg class="arrow-right" width="12" height="10" viewBox="0 0 12 10"><use xlink:href="#arrow-right"></use></svg>
        </a>
      </div>
    </div>
  </div>
</section>
