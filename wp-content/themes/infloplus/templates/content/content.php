<?php
  global $block_counter;
  $title_size = get_sub_field("title_size");
  $col = get_sub_field("column_width");
  $col_offset = get_sub_field("column_offset");
  $contnet = get_sub_field("text");
  //echo "<pre>"; var_dump($col["value"]); echo "</pre>"; die;
  $class = $col["value"];
  if ( (int)$col["label"] < 9 ) {
    $class .= " " . $col_offset;
  }
?>
<section id="block-<?= $block_counter; ?>"  class="content-block">
  <div class="container">
    <div class="row">
      <div class="<?= $class; ?>">
        <?php
          if ($title = get_sub_field("content_title")):
           echo "<h2 class='". $title_size ."'>". $title ."</h2>";
          endif;
          echo $contnet;
        ?>
      </div>
    </div>
  </div>
</section>
