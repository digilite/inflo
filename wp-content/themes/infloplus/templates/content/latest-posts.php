<?php
$title = get_sub_field('title');
$button = get_sub_field('button_url');
$latest_posts = get_sub_field('choose_category');
?>
<section class="latest-posts">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h1 class="h2"><?= $title; ?></h1>
      </div>
      <div class="col-md-6">
        <div class="more-button-block">
          <?php echo_button($button['title'], $button['url'],"button"); ?>
        </div>
      </div>
    </div>
    <div class="post-items">
      <div class="row">
        <?php
        $args = array(
          'posts_per_page' => 3,
          'cat' => $latest_posts
        );    
        $query = new WP_Query( $args );
        if ( $query->have_posts() ) {
          while ( $query->have_posts() ) {
            $query->the_post();?>
            <div class="col-md-4">
              <div class="post-item">
                <div class="post-g-info">
                  <div class="lt-post-thumb">
                    <?php the_post_thumbnail('medium'); ?> 
                  </div>
                  <div class="post-info">
                    <p class="author-name"><?php the_author(); ?></p>
                    <p class="post-date"><?php the_date(); ?></p>
                  </div>
                  <div class="post-title-block">
                    <h4 class="post-title"><?php the_title(); ?></h4>
                  </div>
                </div>
                <?php
                $bt_link = get_permalink();?>
                <div class="post-url">
                  <?php echo_button('Read More', $bt_link,"button button-icon", "show"); ?>
                </div>
              </div>
            </div>
          <?php }
        } else {
          // Постов не найдено
        }
        wp_reset_postdata(); ?>
      </div>
    </div>
  </div>
</section>
