<?php

?>
<section class="product-tabs">
    <div class="container">
      <?php if ($title = get_sub_field('title')): ?>
        <h3 class="h3 text-center logo-list-title"><?= $title; ?></h3>
      <?php endif; ?>
      <div class="tab">
        <div class="row justify-content-between">
          <?php
            if( have_rows('tabs') ):
            $count = 1; ?>
              <?php while( have_rows('tabs') ) : the_row();
              $tab_title = get_sub_field('tab_title'); ?>
              <button class="tablinks col <?php if($count == 1): echo 'active'; endif; ?>" onclick="openCity(event, 'tab<?= $count; ?>')"><?= $tab_title; ?></button>
              <?php
              $count ++;
              endwhile; ?>
            <?php endif; ?>
        </div>
      </div>
      <div class="tab-contents">
          <?php
          if( have_rows('tabs') ):
          $count = 1; ?>
          <?php while( have_rows('tabs') ) : the_row();
          $tab_desc = get_sub_field('tab_description');
          $tab_image = get_sub_field('tab_image');
          $features_title = get_sub_field('features_title'); ?>
          <div class="tabcontent" id="tab<?= $count; ?>">
            <div class="row">
              <div class="col">
                <p class="tab-description"><?= $tab_desc; ?></p>
              </div>
              <div class="col-md-9">
                <?php echo wp_get_attachment_image($tab_image,"full"); ?>
                <div class="features">
                  <p class="features-title"><?= $features_title; ?></p>
                  <div class="row">
                  <?php
                      if( have_rows('features_list') ):
                          while( have_rows('features_list') ) : the_row();
                            $ft_title = get_sub_field('feature_title');
                            $ft_desc = get_sub_field('feature_description'); ?>
                            <div class="col-md-4 feature-item">
                              <p class="feature-title">
                                <svg width="14" height="16" viewBox="0 0 14 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                  <path d="M14 8L0.5 15.7942L0.5 0.205771L14 8Z" fill="#39B54A"/>
                                </svg>
                                <?= $ft_title; ?>
                              </p>
                              <p class="feature-description"><?= $ft_desc; ?></p>
                            </div>
                        <?php endwhile;
                    else :
                    endif; ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php
        $count ++;
        endwhile; ?>
        <?php endif; ?>
      </div>
    </div>
</section>
