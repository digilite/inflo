<?php
  $button_text_1 = get_sub_field("primary_button_text");
  $button_link_1 = get_sub_field("primary_button_link");
  $button_text_2 = get_sub_field("secondary_button_text");
  $button_link_2 = get_sub_field("secondary_button_link");
  $image_id = get_sub_field("right_side_image");
  $services = get_sub_field("select_services");
?>
<section class="homepage-header">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-md-6">
        <h1 class="h2"><?php the_sub_field("title"); ?></h1>
        <p><?php the_sub_field("content"); ?></p>
        <div class="button-container d-flex">
          <?php
            echo_button($button_text_1, $button_link_1,"button");
            echo_button($button_text_2, $button_link_2,"button button-icon", "show");
          ?>
        </div>
        <a href=""></a>
      </div>
      <div class="col-md-6">
        <div class="img-container">
          <?= wp_get_attachment_image($image_id,"full"); ?>
            <div class="product-blocks-container">
              <?php foreach($services as $post): ?>
              <a href="<?php the_permalink(); ?>">
                <div class="related-item">
                    <div class="related-thumb">
                      <?php the_post_thumbnail('medium'); ?>
                    </div>
                    <p class="related-title"><?php the_title(); ?></p>
                </div>
                </a>
              <?php endforeach;  wp_reset_query(); ?>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>
