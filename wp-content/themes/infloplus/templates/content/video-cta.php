<?php
  global $block_counter;

  $video_cover = get_sub_field("video_cover");
  $logo = get_sub_field("logo");
  $video_text = get_sub_field("video_text");
  $cta_text = get_sub_field("cta_text");
  $cta_button_text = get_sub_field("cta_button_text");
  $cta_button_link = get_sub_field("cta_button_link");
?>
<section class="video-section">
  <div class="video-container d-flex justify-content-center" style ="background: url('<?= $video_cover; ?>') no-repeat center / cover;">
    <div class="container d-flex">
      <div class="video-cta-content d-flex flex-column align-items-center m-auto">
        <?php
          $rand_number =  rand(1,500);
          echo wp_get_attachment_image($logo,'medium');
          if ($video_text = get_sub_field("video_text")) {
            echo "<p>". $video_text ."</p>";
          }
        ?>

        <svg type="button" data-toggle="modal" data-target="#vide-popup-<?= $rand_number; ?>" width="80" height="80" viewBox="0 0 80 80" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" clip-rule="evenodd" d="M73.3334 40C73.3334 58.4095 58.4095 73.3333 40 73.3333C21.5905 73.3333 6.66668 58.4095 6.66668 40C6.66668 21.5905 21.5905 6.66663 40 6.66663C58.4095 6.66663 73.3334 21.5905 73.3334 40ZM53.3333 40L33.3333 26.6666V53.3333L53.3333 40Z" fill="white" fill-opacity="0.6"/>
          <path d="M40 77C60.4345 77 77 60.4345 77 40C77 19.5655 60.4345 3 40 3C19.5655 3 3 19.5655 3 40C3 60.4345 19.5655 77 40 77Z" stroke="white" stroke-opacity="0.6" stroke-linecap="round" stroke-linejoin="round" stroke-dasharray="2 2"/>
        </svg>
      </div>
    </div>
  </div>
  <div id="vide-popup-<?= $rand_number; ?>" class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div class="featured-video-container embed-responsive embed-responsive-16by9">
            <?php the_sub_field("youtube_video_link"); ?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="video-cta">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-sm-10 d-flex justify-content-between align-items-center">
          <p><?= $cta_text ?></p>
          <?php echo_button($cta_button_text, $cta_button_link,"button"); ?>
        </div>
      </div>
    </div>
  </div>
</section>



