<?php
  global $block_counter;

  $temp = get_sub_field("background_color");
  if ($temp  != NULL ) {
    $bk_color = $temp;
  } else {
    $bk_color = "no-background";
  }
  $title = get_sub_field("title");
  if ( $title == "" ) {
    $class = "no-title";
  }
  $galleries = get_sub_field("galleries");
?>

<section id="block-<?= $block_counter; ?>" class="icon-galleries <?= $bk_color . " " .$class; ?>">
  <div class="container">
    <?php if($title = get_sub_field("title")): ?>
      <h5 class="main-title h5"><?= $title; ?></h5>
    <?php
      endif;
      while ( have_rows("galleries") ) : the_row();
    ?>
      <div class="icon-gallery-container">
        <?php
          if($sub_title = get_sub_field("gallery_title")):
            echo "<span class='sub-title medium-text'>". $sub_title ."</span>";
          endif;
          $logo_gallery = get_sub_field("icons_gallery");
        ?>
        <div class="gallery-container d-flex flex-wrap">
          <?php foreach( $logo_gallery as $logo_url ): ?>
            <div class="logo-container">
              <?php // echo file_get_contents($logo_url); ?>
              <div class="logo-image lazy-load-image" bg-image-url="<?= $logo_url; ?>"></div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    <?php endwhile;?>
    </div>
</section>
