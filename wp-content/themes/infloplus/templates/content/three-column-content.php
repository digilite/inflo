<?php
  global $block_counter;

  $style = get_sub_field("style");

  $class= "columns";
  if ($style == "uneven-columns") {
    $class .= " col-md-4";
  }

  $first_column = get_sub_field("first_column");
  $second_column = get_sub_field("second_column");
  $third_column = get_sub_field("third_column");
?>
<section id="block-<?= $block_counter; ?>" class="three-column-content <?= $style; ?>">
  <div class="container">
    <div class="row">
      <div class="<?= $class; ?> first-column"><?= $first_column; ?></div>
      <div class="<?= $class; ?> second-column"><?= $second_column; ?></div>
      <div class="<?= $class; ?> third-column"><?= $third_column; ?></div>
    </div>
  </div>
</section>
