<?php
  $button_text_1 = get_sub_field("primary_button_text");
  $button_link_1 = get_sub_field("primary_button_link");
  $button_text_2 = get_sub_field("secondary_button_text");
  $button_link_2 = get_sub_field("secondary_button_link");
  $image_id = get_sub_field("left_side_image");
  $why_choose_us = get_sub_field('why_choose_us');
?>
<section class="homepage-header <?php if(!empty($why_choose_us)) { echo "bubble-bk"; } ?>">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-md-6">
        <?php echo wp_get_attachment_image($image_id,"full"); ?>
      </div>
      <div class="col-md-5">
        <h1 class="h2"><?php the_sub_field("title"); ?></h1>
        <div class="button-container d-flex align-items-center">
          <?php
            echo_button($button_text_1, $button_link_1,"button");
            echo_button($button_text_2, $button_link_2,"button button-icon", "show");
          ?>
        </div>
            <?php
              if( have_rows('accordion') ): ?>
              <div class="product-accordion">
              <?php while( have_rows('accordion') ) : the_row();
              $acc_title = get_sub_field('acc_title');
              $acc_description = get_sub_field('acc_description'); ?>
                    <button class="accordion">
                       <?= $acc_title; ?>
                       <svg class="circle-plus" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M21 12C21 7.03125 16.9688 3 12 3C7.03125 3 3 7.03125 3 12C3 16.9688 7.03125 21 12 21C16.9688 21 21 16.9688 21 12Z" stroke="white" stroke-miterlimit="10"/>
                        <path d="M15.75 12H8.25M12 8.25V15.75V8.25Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
                      </svg>
                      <svg class="circle-minus" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M21 12C21 7.03125 16.9688 3 12 3C7.03125 3 3 7.03125 3 12C3 16.9688 7.03125 21 12 21C16.9688 21 21 16.9688 21 12Z" stroke="white" stroke-miterlimit="10"/>
                        <path d="M15.75 12H8.25" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
                      </svg>
                    </button>
                    <div class="panel">
                      <p><?= $acc_description; ?></p>
                    </div>
              <?php
              endwhile; ?>
              </div>
              <?php endif; ?>
      </div>
    </div>
    <?php if($why_choose_us): ?>
    <div class="why-choose-us">
      <div class="d-flex justify-content-around">
        <?php
        if( have_rows('why_choose_us') ):
            while( have_rows('why_choose_us') ) : the_row();
                $wch_title = get_sub_field('wcu_title'); ?>
                <div class="wcu-bg">
                    <div class="wcu-item">
                      <p class="wcu-title text-center"><?= $wch_title; ?></p>
                    </div>
                </div>
            <?php endwhile;
        else :
        endif; ?>
      </div>
    </div>
    <?php endif; ?>
  </div>
</section>
