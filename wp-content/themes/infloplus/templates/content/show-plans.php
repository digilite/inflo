<?php
  global $block_counter;

  $title = get_sub_field("title");

  $cta_button_text = get_sub_field("cta_button_text");
  $cta_button_link = get_sub_field("cta_button_link");
  $plans_ob = get_sub_field("select_plans");
?>
<section class="plans-container">
  <div class="container">
    <div class="row justify-content-md-center">
      <div class="plans-top col-md-8 text-md-center">
        <h2 class="h2"><?= $title; ?></h2>
        <?php if ($content = get_sub_field("content")): ?>
          <p><?= $content; ?></p>
        <?php endif; ?>
        <?php echo_button($cta_button_text, $cta_button_link,"button"); ?>
      </div>
    </div>
    <div class="row justify-content-center">
      <?php
        foreach($plans_ob as $post):
          echo "<div class='col-md-3 plans-card-container'>";
            get_template_part("templates/cards/plan-card");
          echo "</div>";
        endforeach; wp_reset_query();
      ?>
    </div>
  </div>
</section>
