<?php
  $cta_link = get_sub_field('cta_link');

  $img_url = get_sub_field('background');
  $modules_ob = get_sub_field('select_modules');
  $background = "style = 'background: url(". $img_url .") no-repeat left top / cover;'";
?>
<section class="product-header" <?= $background; ?>>
  <div class="container">
    <?php if ($title = get_sub_field('title')): ?>
      <h1 class="h2 text-md-center"><?= $title; ?></h1>
    <?php endif; ?>
    <div class="row justify-content-center">
      <?php
        foreach($modules_ob as $post):
          echo "<div class='col-md-3 expand-wrapper'>";
            get_template_part("templates/cards/expandable-icon-container");
          echo "</div>";
        endforeach; wp_reset_query();
      ?>
    </div>
  </div>
</section>


