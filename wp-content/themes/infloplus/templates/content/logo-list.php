<?php
$title = get_sub_field('title');
$top_title = get_sub_field('title_in_top_background');
?>
<section class="logo-list relative-parent <?php if($top_title): echo 'absolute-title-block'; endif; ?>">
  <h3 class="h3 text-center logo-list-title <?php if($top_title): echo 'absolute-title'; endif; ?>"><?= $title; ?></h3>
  <div class="logo-list-bg">
    <div class="container">
      <div class="row justify-content-between">
        <?php
          if( have_rows('logo_list_items') ): ?>
          <?php while( have_rows('logo_list_items') ) : the_row(); 
          $logo_item = get_sub_field('partner_logo'); ?>
          <div class="logo-item">
              <?php echo wp_get_attachment_image($logo_item,"full"); ?>
          </div>
          <?php
          endwhile; ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>
