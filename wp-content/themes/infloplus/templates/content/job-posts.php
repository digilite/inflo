<?php
  global $block_counter;

  $title = get_sub_field("section_title");
  $icon = get_sub_field("icon");
  $obj = get_queried_object();


  $args = array(
    "post_type" => "job-posts",
    "meta_query" => array(
      array(
          "key" => "active",
          "value" => "1",
          "compare" => "=="
      )
    )
  );

  if ($obj->post_type == "job-posts" ) {
    $args["post__not_in"] = array($obj->ID);
  }

  $jobs = new WP_Query( $args );
  if ( $jobs->have_posts()):
?>
<section id="block-<?= $block_counter; ?>" class="job-block">
  <div class="container">
    <h2 class="section-title h1"><?= $title; ?></h2>
    <div class="row">
      <div class="col-md-3 icon-container">
        <img src="<?= $icon; ?>" class="img-fluid" alt="job post icon">
      </div>
      <div class="col-md-9">
        <?php
          while ($jobs->have_posts()) : $jobs->the_post();
          $location = get_field("location");
        ?>
          <div class="single-job-link">
            <a class="absolute" href="<?php the_permalink(); ?>"></a>
            <span class="small-text text-uppercase"><?= $location; ?></span>
            <h3 class="job-title h5 d-flex justify-content-between"><?php the_title(); ?><span class="icon-btn"><svg class="plus" width="8" height="8" viewBox="0 0 8 8"><use xlink:href="#plus"></use></svg></span></h3>
          </div>
        <?php endwhile; endif; wp_reset_postdata(); ?>
      </div>
    </div>
  </div>
</section>
