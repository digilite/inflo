<?php
  $cta_link = get_sub_field('cta_link');
  $title = get_sub_field('title');
  $image = get_sub_field('image');
  if (get_sub_field("gradient_title")) {
    $title_class="grad-heading";
  }
?>
<section class="cta-block">
  <div class="container">
    <?php if (!empty($image)):?>
      <div class="row align-items-center two-column-cta">
        <div class="col-md-6 right-image">
          <?= wp_get_attachment_image($image,'full'); ?>
        </div>
        <div class="col-md-6">
          <h2 class="cta-title h2 <?= $title_class; ?>"><?= $title; ?></h2>
          <?php if ($content = get_sub_field("content")): ?>
            <p><?= $content; ?></p>
          <?php endif; ?>
          <?php echo_button($cta_link["title"],$cta_link["url"],"button","",$cta_link["target"] ); ?>
        </div>
      </div>
      <?php else: ?>
        <div class="row justify-content-center">
          <div class="col-lg-10 col-md-11 text-md-center">
            <h2 class="cta-title h2 <?= $title_class; ?>"><?= $title; ?></h2>
            <?php if ($content = get_sub_field("content")): ?>
              <p><?= $content; ?></p>
            <?php endif; ?>
            <?php echo_button($cta_link["title"],$cta_link["url"],"button","",$cta_link["target"] ); ?>
          </div>
        </div>
      <?php endif; ?>
  </div>
</section>
