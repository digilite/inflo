<?php
  global $block_counter;

  $title = get_sub_field("title");
  $portfolios = get_sub_field("select_portfolios");
  //echo "<pre>"; var_dump($portfolios); echo "</pre>"; die;
?>
<section id="block-<?= $block_counter; ?>" class="showcase-portfolios">
  <div class="container">
    <div class="row justify-content-lg-between">
      <div class="col-lg-4 first-port">
        <div class="port-card">
          <a href="<?= get_permalink($portfolios[0]->ID); ?>"><span class="port-image lazy-load-cover" bg-image-url="<?= get_the_post_thumbnail_url($portfolios[0]->ID); ?>"></span><span class="port-title"><?= $portfolios[0]->post_title; ?></span></a>
        </div>
      </div>
      <div class="col-lg-3 second-port">
        <h2 class="h1"><?= $title; ?></h2>
        <?php if ($sub_title = get_sub_field("sub-title")): ?>
          <p class="h6"><?= $sub_title; ?></p>
        <?php endif; ?>
        <a class="icon-btn-container" href="<?= get_page_link(1749); ?>">
            <span class="icon-btn">
                <svg class="eye-icon" width="17.5" height="13.886" viewBox="0 0 17.5 13.886"><use xlink:href="#eye-icon"></use></svg>
            </span>
            Watch Portfolio
        </a>
        <div class="port-card">
          <a href="<?= get_permalink($portfolios[1]->ID);; ?>"><span class="port-image lazy-load-cover" bg-image-url="<?= get_the_post_thumbnail_url($portfolios[1]->ID); ?>"></span><span class="port-title"><?= $portfolios[1]->post_title; ?></span></a>
        </div>
      </div>
      <div class="col-lg-3 third-port">
        <div class="port-card">
          <a href="<?= get_permalink($portfolios[2]->ID);; ?>"><span class="port-image lazy-load-cover" bg-image-url="<?= get_the_post_thumbnail_url($portfolios[2]->ID); ?>"></span><span class="port-title"><?= $portfolios[2]->post_title; ?></span></a>
        </div>
      </div>
    </div>
  </div>
</section>
