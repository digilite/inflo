<?php
  $title = get_sub_field('title');
  $description = get_sub_field('description');
  $button = get_sub_field("button");
  $image_id = get_sub_field("client_image");
  $client_name = get_sub_field('client_name');
  $client_position = get_sub_field('client_position');
  $story = get_sub_field('client_story');
?>
<section class="story">
  <div class="container">
    <div class="story-section-general">
      <h3 class="h3 text-center"><?= $title; ?></h3>
      <p class="text-center description"><?= $description; ?></p>
    </div>
    <div class="row justify-content-center">
      <div class="col-md-5">  
        <div class="button-container d-flex align-items-center">
            <?php
              if( have_rows('accordion') ): ?>
              <div class="product-accordion">
              <?php while( have_rows('accordion') ) : the_row(); 
              $acc_title = get_sub_field('acc_title');
              $acc_description = get_sub_field('acc_description'); ?>
                    <button class="accordion">
                       <?= $acc_title; ?>
                       <svg class="circle-plus" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M21 12C21 7.03125 16.9688 3 12 3C7.03125 3 3 7.03125 3 12C3 16.9688 7.03125 21 12 21C16.9688 21 21 16.9688 21 12Z" stroke="white" stroke-miterlimit="10"/>
                        <path d="M15.75 12H8.25M12 8.25V15.75V8.25Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
                      </svg>
                      <svg class="circle-minus" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M21 12C21 7.03125 16.9688 3 12 3C7.03125 3 3 7.03125 3 12C3 16.9688 7.03125 21 12 21C16.9688 21 21 16.9688 21 12Z" stroke="white" stroke-miterlimit="10"/>
                        <path d="M15.75 12H8.25" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
                      </svg>
                    </button>
                    <div class="panel">
                      <p><?= $acc_description; ?></p>
                    </div>
              <?php
              endwhile; ?>
              </div>
              <?php endif; ?>
          </div>
      </div>
      <div class="col-md-6">
        <div class="client-story">
          <div class="client-info d-flex">
            <div class="client-image">
              <?php echo wp_get_attachment_image($image_id,"thumb"); ?>
            </div>
            <div class="client-meta">
              <p class="client-name"><?= $client_name; ?></p>
              <p class="client-position"><?= $client_position; ?></p>
            </div>
          </div>
          <div class="cl-story-text">
            <p><?= $story; ?></p>
          </div>
          <?php echo_button($button['title'], $button['url'],"button button-icon", "show"); ?>
        </div>
      </div>
    </div>
  </div>
</section>
