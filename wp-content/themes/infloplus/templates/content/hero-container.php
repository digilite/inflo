<?php
  if( have_rows('dg_page_builder') ):
    while ( have_rows('dg_page_builder') ) : the_row();
    if( get_row_layout() == 'dg_hero_container' ):

      $herostyle = get_sub_field('dg_hero_container_style');

      $styledtext1 = get_sub_field('dg_first_line');
      $styledtext2 = get_sub_field('dg_second_line');
      $styledtext3 = get_sub_field('dg_third_line');
      $twocolumntext1 = get_sub_field('dg_first_block_text');
      $title = get_sub_field('dg_second_block_text');

      $overlay = get_sub_field("overlay_option");

      if(empty($title)) {
        $title = get_the_title();
      }

      $icon = get_sub_field('dg_header_icon');
      $alt = "";
      if ( empty($icon["alt"]) ) {
        $alt = $icon["title"];
      }

      $homeherotext1 = $styledtext1;
      $homeherotext2 = $styledtext2;
      $homeherotext3 = get_sub_field('dg_home_hero_paragraph');
      $hero_bg = get_sub_field('dg_home_hero_background_image');
      $button_url = get_sub_field('dg_button_url');

?>
<section class="hero-block <?= $herostyle; ?>">
  <?php if($herostyle == 'dg_homepage_hero'): ?>
    <div class="hero-image banner" style="background: url('<?= $hero_bg; ?>') center center / cover no-repeat;"></div>
  <?php endif; ?>

  <div class="container">
  <?php if($herostyle == 'dg_simple_title'): ?>
    <h1 class="h2"><?= $title; ?></h1>
  <?php elseif($herostyle == 'dg_styled_text_hero'): ?>
    <h1 class="hero-text huge-title text-uppercase">
      <span><?= $styledtext1;?></span>
      <span><?= $styledtext2;?></span>
      <span class="offset-md-1"><?= $styledtext3;?></span>
    </h1>
  <?php elseif($herostyle == 'dg_two_column_text_hero'): ?>
      <div class="row two_col_block">
          <div class="col-md-4 hero-left">
            <?= $twocolumntext1; ?>
          </div>
          <div class="col-md-8 hero-right">
              <h1 class="huge-title text-uppercase"><?= $title; ?></h1>
          </div>
      </div>
  <?php elseif($herostyle == 'dg_two_column_with_icon'): ?>
    <div class="row two_col_block">
        <div class="col-md-4 hero-left">
          <img src="<?= $icon["url"]; ?>" alt="<?= $alt; ?>">
        </div>
        <div class="col-md-8 hero-right">
            <h1 class="huge-title text-uppercase"><?= $title; ?></h1>
        </div>
    </div>
  <?php elseif($herostyle == 'dg_single_hero_with_title'): ?>
    <div class="hero-image" style="background-image: url(<?= $hero_bg; ?>);">
      <span class="absolute overlay <?= $overlay; ?>"></span>
      <div class="hero-text">
        <h1 class="h1 text-uppercase"><?= $title; ?></h1>
        <?php if ($small_text = get_sub_field("small_text")): ?>
          <span class="small-text small-caps-text"><?= $small_text; ?></span>
        <?php endif; ?>
      </div>
    </div>
  <?php elseif($herostyle == 'dg_homepage_hero'): ?>
      <div class="hero-image-content">
          <h2 class="content__title big-title"><?= $homeherotext1; ?> <span class="indented-text"><?= $homeherotext2; ?></span></h2>
          <div class="content__text indented-text">
            <p><?= $homeherotext3; ?></p>
            <?php if(!empty($button_url)): ?>
            <a class="icon-btn-container" href="<?= $button_url; ?>">
                <span class="icon-btn">
                    <svg class="eye-icon"width="17.5" height="13.886" viewBox="0 0 17.5 13.886"><use xlink:href="#eye-icon"></use></svg>
                </span>
                Explore
            </a>
            <?php endif; ?>
          </div>

          <?php if( have_rows('dg_socialicons', 'option') ): ?>
          <div class="social-icons-hero">
            <div class="flex-container">
            <?php while( have_rows('dg_socialicons', 'option') ): the_row(); ?>
              <a href="<?php the_sub_field('dg_socialurl'); ?>"><?php the_sub_field('dg_socialicon'); ?></a>
            <?php endwhile; ?>
          </div>
          <?php endif; ?>

          </div>
      </div>
  <?php endif; ?>
  </div>
</section>
<?php endif; endwhile; endif; ?>
