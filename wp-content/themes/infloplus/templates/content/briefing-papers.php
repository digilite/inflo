<?php
$title = get_sub_field('section_title');
?>
<section class="about-us briefing-papers" style="background: url('<?= $bg1; ?>') left center / 50% no-repeat;">
  <div class="about-us-block">
    <div class="container">
      <h2 class="h2 text-center"><?= $title; ?></h2>
      <?php
      $count = 1;
      if( have_rows('add_briefing_papers') ):
          while( have_rows('add_briefing_papers') ) : the_row();
            $bg1 = get_sub_field('main_background');
            $bg2 = get_sub_field('second_background');
            $title = get_sub_field('title');
            $description = get_sub_field('description');
            $button = get_sub_field('button');
            $subtitle = get_sub_field('subtitle'); ?>
          <div class="brif-item">
            <div class="row <?php if($count % 2 === 0): echo 'flex-row-reverse left-image'; else: echo 'right-image'; endif; ?> align-items-center">
              <div class="col-md-5">
                <div class="about-info">
                  <h3 class="h3 grad-heading"><?= $title; ?></h3>
                  <h4 class="h4"><?= $subtitle; ?></h4>
                  <p class="about-description"><?= $description; ?></p>
                  <?php echo_button($button['title'], $button['url'],"button"); ?>
                </div>
              </div>
              <div class="col-md-6">
                <div class="abs-image">
                  <?php echo wp_get_attachment_image( $bg2 , 'full' ); ?>
                </div>
              </div>
            </div>
          </div>
          <?php
          $count ++;
          endwhile;
      else :
      endif; ?>
    </div>
  </div>
</section>
