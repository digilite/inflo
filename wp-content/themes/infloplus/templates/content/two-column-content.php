<?php
  global $block_counter;

  $type = get_sub_field("column_type");
  $cols = get_sub_field("column_sizes");
  $l_col_s = $cols["left_column_size"];
  $r_col_s = $cols["right_column_size"];

  if ( ($l_col_s + $r_col_s) > 12 ) {
    $l_col_s = 6;
    $r_col_s = 6;
  }

  $text_size = get_sub_field("text_size");
  $left_title_size = get_sub_field("left_title_size");

  $icon = get_sub_field("icon");
  $alt = "";
  if ( empty($icon["alt"]) ) {
    $alt = $icon["title"];
  }

  $left_title = get_sub_field("left_title");
  $right_title = get_sub_field("right_title");
  $left_content = get_sub_field("left_content");
  $right_content = get_sub_field("right_content");

?>

<?php if ($type == "left-heading-right-icon"): ?>
<section id="block-<?= $block_counter; ?>" class="two-column-content <?= $text_size; ?>">
  <div class="container">
    <div class="row justify-content-lg-between">
      <h2 class="<?= "col-lg-{$l_col_s} {$left_title_size}"; ?>"><?= $left_title; ?></h2>
      <div class="user-content <?= "col-lg-". $r_col_s; ?>"><?= $right_content; ?></div>
    </div>
  </div>
</section>
<?php endif; ?>

<?php if ($type == "columns-with-icon"): ?>
<section class="two-column-content">
  <div class="container">
    <div class="row justify-content-lg-between">
      <div class="left-content <?= "col-lg-". $l_col_s; ?>">
        <img src="<?= $icon["url"]; ?>" alt="<?= $alt; ?>">
        <h2 class="h6"><?= $left_title; ?></h2>
        <div class="user-content <?= $text_size; ?>"><?= $left_content; ?></div>
      </div>
      <div class="right-content <?= "col-lg-". $r_col_s; ?>">
        <div class="user-content <?= $text_size; ?>">
          <h2 class="h6"><?= $right_title; ?></h2>
          <?= $right_content; ?>
        </div>
      </div>
    </div>
  </div>
</section>
<?php endif; ?>

<?php if ($type == "columns-with-titles"): ?>
<section class="two-column-content">
  <div class="container">
    <div class="row justify-content-lg-between">
      <div class="left-content <?= "col-lg-". $l_col_s; ?>">
        <h2 class="h6"><?= $left_title; ?></h2>
        <div class="user-content <?= $text_size; ?>"><?= $left_content; ?></div>
      </div>
      <div class="right-content <?= "col-lg-". $r_col_s; ?>">
        <h2 class="h6"><?= $right_title; ?></h2>
        <div class="user-content <?= $text_size; ?>">
          <?= $right_content; ?>
        </div>
      </div>
    </div>
  </div>
</section>
<?php endif; ?>
