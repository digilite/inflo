<?php
$title = get_sub_field('title');
$description = get_sub_field('description');
$featured_posts = get_sub_field('choose_posts');
?>
<section class="related-products">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="related-top text-center">
            <h2 class="h2 logo-list-title "><?= $title; ?></h2>
            <p class=""><?= $description; ?></p>
          </div>
          <div class="posts-list">
            <div class="row">
              <?php
              if( $featured_posts ): ?>
                  <?php foreach( $featured_posts as $post ):

                      // Setup this post for WP functions (variable must be named $post).
                      setup_postdata($post); ?>
                      <div class="col-md-4">
                      <a href="<?php the_permalink(); ?>">
                        <div class="related-item">

                            <div class="related-thumb">
                              <?php the_post_thumbnail('medium'); ?>
                            </div>
                            <p class="related-title"><?php the_title(); ?></p>

                        </div>
                        </a>
                      </div>
                  <?php endforeach; ?>
                  <?php
                  // Reset the global post object so that the rest of the page works correctly.
                  wp_reset_postdata(); ?>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
