<?php
  global $block_counter;
  $num_posts = get_sub_field("number_of_posts_to_show");
?>

<section id="block-<?= $block_counter; ?>" class="related-post dark-mode">
  <div class="container">
    <?php if ($title = get_sub_field("title")): ?>
      <h2 class="related-block-title h1"><?= $title; ?></h2>
    <?php endif; ?>
    <a class="icon-btn-container" href="<?= get_page_link(46); ?>">
      <span class="icon-btn ">
        <svg xmlns="http://www.w3.org/2000/svg" width="9.244" height="8.75" viewBox="0 0 9.244 8.75"><g transform="translate(0.494 0.75)"><path d="M0,0H8V8" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="1.5"/><path d="M8,0,0,7" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="1.5"/></g></svg>
      </span>Blog
    </a>
  </div>
</section>
