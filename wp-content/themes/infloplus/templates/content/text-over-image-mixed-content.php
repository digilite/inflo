<?php
  global $block_counter;

  $section_title = get_sub_field("section_title");
  $bg_option = get_sub_field("background_option");

  $block_style = get_sub_field("block_style");

  if ($block_style == "container") {
    $col_classes = "col-sm-4";
    $block_icon_calss = "column-layout-3";
  }else {
    $col_classes = "col-sm-6";
    $block_icon_calss = "column-layout-2";
  }

  $img_obj = get_sub_field("first_image");
  $first_image = $img_obj["sizes"]["large"];

  $img_obj = get_sub_field("second_image");
  $second_image = $img_obj["sizes"]["large"];

  $content = get_sub_field("content");
  $icons_title = get_sub_field("icons_title")
?>
<section id="block-<?= $block_counter; ?>" class="text-over-image-mixed-content <?= $bg_option[0]; ?>">
  <h2 class="huge-title main-title"><?= $section_title ?></h2>
  <div class="<?= $block_style; ?>">
    <div class="cover-image over-text-image" style="background-image: url(<?= $first_image; ?>);">
    </div>
    <div class="user-content">
      <?= $content; ?>
    </div>

    <?php
      if (!empty($icons_title)) {
        echo "<h4 class='icons-title h6'>" . $icons_title . "</h4>";
      }
    ?>
    <?php if( have_rows("icon_blocks") ): ?>
      <div class="icon-block-container d-flex flex-md-wrap <?= $block_icon_calss; ?>">
        <?php
          while ( have_rows("icon_blocks") ) : the_row();
          $icon = get_sub_field('icon_block_image');
          $alt = "";
          if ( empty($icon["alt"]) ) {
            $alt = $icon["title"];
          }
        ?>
          <div class="<?= $col_classes; ?> col-icon d-md-flex flex-md-column">
            <img class="icon" src="<?= $icon["url"]; ?>" alt="<?= $alt; ?>">
            <p class="description"><?php the_sub_field("main_title"); ?></p>
          </div>
        <?php endwhile; ?>
      </div>
    <?php endif; ?>
  </div>
</section>
