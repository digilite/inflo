<?php
  global $block_counter;
  $bk_image = get_sub_field("backgorund_image");
  $block_height = get_sub_field("block_height");
  $section_type = get_sub_field("section_type");
  $overlay_option = get_sub_field("overlay_option");
  $icon = get_sub_field("icon");
  $alt = "";
  if ( empty($icon["alt"]) ) {
    $alt = $icon["title"];
  }

  $small_title = get_sub_field("small_title");
  $big_title = get_sub_field("big_title");
  $b_t_size = get_sub_field("big_title_size_optoins");
  $b_t_styles = implode(get_sub_field("big_title_styles"), " ");
  if ($b_t_size == "" ) {
    $b_t_size = "h1";
  }
  $title_classes = $b_t_size . " " . $b_t_styles;
  $medium_title = get_sub_field("medium_title");

?>
<section id="block-<?= $block_counter; ?>" class="backgorund-image-section <?php echo $section_type . " " . $block_height; ?>" style="background-image: url(<?= $bk_image; ?>);">
  <span class="absolute overlay <?= $overlay_option; ?>"></span>
  <?php
    if ($section_type == "three-titled-bg"):
      echo "<div class='container'>";
      echo "<div class='row justify-content-sm-end'>";
      echo "<div class='col-md-8 bg-content'>";
      echo "<span class='small-text small-caps-text'>". $small_title ."</span>";
      echo "<h2 class='". $title_classes ."'>". $big_title ."</h2>";
      echo "<p>". $medium_title ."</p>";
      echo '<img src="'. $icon["url"] .'" alt="">';
      echo "</div>";
      echo "</div>";
      echo "</div>";
    endif;

    if ($section_type == "quote-bg" && !empty($medium_title)):
      echo "<div class='container'>";
      echo "<div class='row justify-content-sm-center'>";
      echo "<div class='col-md-8 bg-content'>";
      echo "<blockquote class='h3'>". $medium_title ."</blockquote>";
      echo "</div>";
      echo "</div>";
      echo "</div>";
    endif;
    if ($section_type == "centered-logo" && !empty($icon)):
      echo '<img src="'. $icon["url"] .'" alt="">';
    endif;

    ?>
  <div class=""></div>
</section>

