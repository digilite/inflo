<?php
$bg1 = get_sub_field('main_background');
$bg2 = get_sub_field('second_background');
$title = get_sub_field('title');
$description = get_sub_field('description');
$button = get_sub_field('button');
$button_url = get_sub_field('button_url');
?>
<section class="about-us" style="background: url('<?= $bg1; ?>') left center / 50% no-repeat;">
  <div class="about-us-block">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="about-info">
            <h2 class="h2"><?= $title; ?></h2>
            <p class="about-description"><?= $description; ?></p>
            <?php echo_button($button, $button_url,"button button-icon", "show"); ?>
          </div>
          <div class="stats">
            <div class="row">
            <?php
            if( have_rows('stats_rp') ):
                while( have_rows('stats_rp') ) : the_row();
                    $number = get_sub_field('number');
                    $description = get_sub_field('stats_description'); ?>
                    <div class="col-md-4">
                      <div class="stats-item">
                        <p class="stats-number h2 grad-heading"><?= $number; ?></p>
                        <p class="stats-description"><?= $description; ?></p>
                      </div>
                    </div>
            <?php endwhile;
            else :
            endif; ?>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="abs-image">
            <?php echo wp_get_attachment_image( $bg2 , 'full' ); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>