<?php
  global $block_counter;

  $title = get_sub_field("title");
  $num_ports = get_sub_field("number_portfolios");
  $port_cat = get_sub_field("select_category");

  if (empty($title)) {
    $title = $port_cat->name;
  }
  $args = array(
    "post_type" => "portfolio",
    //"category_name" => $port_cat->slug,
    "posts_per_page" => $num_ports,
  );
  $portfolios = new WP_Query( $args );
  //echo "<pre>"; var_dump($portfolios); echo "</pre>"; die;
?>
<div id="block-<?= $block_counter; ?>" class="portfolio-slider-section">
  <div class="container">
    <div class="d-flex justify-content-between align-items-center">
      <h2 class="section-title h3"><?= $title; ?></h2>
      <div class="slider-nav-container"></div>
    </div>
    <div class="portfolio-slider">
      <?php while ($portfolios->have_posts()) : $portfolios->the_post(); ?>
        <?php get_template_part("templates/cards/portfolio-card"); ?>
      <?php endwhile;  wp_reset_postdata(); ?>
    </div>
  </div>
</div>


