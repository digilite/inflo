<?php
  global $block_counter;
?>

<section id="block-<?= $block_counter; ?>" class="numbered-column">
  <div class="container">
    <?php
      if($small_title = get_sub_field("small_description")):
        echo "<p class='sub-title small-text small-caps-text'>". $small_title ."</p>";
      endif;
      if($main_title = get_sub_field("main_title")):
        echo "<h2 class='main-title h1'>". $main_title ."</h2>";
      endif;
    ?>
    <div class="row">
      <?php $count = 1; while ( have_rows("numbered_columns") ) : the_row(); ?>
        <div class="col-md-3 numbered-box">
          <span class="number"><?php echo sprintf('%02s', $count); ?></span>
          <h3 class="h6"><?php the_sub_field("title"); ?></h3>
          <p><?php the_sub_field("desciption"); ?></p>
        </div>
      <?php $count++; endwhile;?>
    </div>
  </div>
</section>
