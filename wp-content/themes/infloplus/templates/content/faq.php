<?php
$title = get_sub_field('section_title');
?>
<section class="faq relative-parent <?php if($top_title): echo 'absolute-title-block'; endif; ?>">
  <div class="container">
  <h2 class="h2 text-center logo-list-title "><?= $title; ?></h2>
    <div class="row justify-content-center">
      <div class="col-md-8">
        <?php
        if( have_rows('faq_rp','option') ): ?>
          <div class="product-accordion">
              <?php while( have_rows('faq_rp','option') ) : the_row();
              $acc_title = get_sub_field('title','option');
              $acc_description = get_sub_field('description','option'); ?>
              <button class="accordion">
                <span class="grad-circle acc-circle"></span>
                <?= $acc_title; ?>
                <svg width="18" height="10" viewBox="0 0 18 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M17 1L9 9L1 1" stroke="#C1C1C1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
              </button>
              <div class="panel">
                <p><?= $acc_description; ?></p>
              </div>
              <?php
              endwhile; ?>
           </div>
          <?php endif; ?>
      </div>
    </div>
  </div>
</section>
