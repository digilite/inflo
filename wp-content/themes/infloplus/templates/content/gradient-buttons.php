<?php
$title = get_sub_field('title');
$description = get_sub_field('description');
$first_button = get_sub_field('first_button');
$second_button = get_sub_field('second_button');
$first_subtitle = get_sub_field('first_button_subtitle');
$second_subtitle = get_sub_field('second_button_subtitle');
?>
<section class="gradient-buttons">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <h2 class="h2 text-center text-white"><?= $title; ?></h3>
        <p class="text-white text-center gr-description"><?= $description; ?></p>
        <div class="row">
          <div class="col-md-6">
            <a href="<?= $first_button['url']; ?>">
              <div class="gradient-button with-desc">
                <div class="button-info">
                    <p><?= $first_button['title']; ?></p>
                    <p class="button-subtitle"><?= $first_subtitle; ?></p>
                </div>
                <div class="button-icon">
                  <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M28 16C28 9.375 22.625 4 16 4C9.375 4 4 9.375 4 16C4 22.625 9.375 28 16 28C22.625 28 28 22.625 28 16Z" stroke="white" stroke-width="2" stroke-miterlimit="10"/>
                    <path d="M13 10L19 16L13 22" stroke="white" stroke-width="2" stroke-linecap="round"/>
                  </svg>
                </div>
              </div>
            </a>
          </div>
          <div class="col-md-6">
            <a href="<?= $first_button['url']; ?>">
              <div class="gradient-button with-desc">
                  <div class="button-info">
                    <p><?= $second_button['title']; ?></p>
                    <p class="button-subtitle"><?= $second_subtitle; ?></p>
                  </div>
                  <div class="button-icon">
                    <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M28 16C28 9.375 22.625 4 16 4C9.375 4 4 9.375 4 16C4 22.625 9.375 28 16 28C22.625 28 28 22.625 28 16Z" stroke="white" stroke-width="2" stroke-miterlimit="10"/>
                      <path d="M13 10L19 16L13 22" stroke="white" stroke-width="2" stroke-linecap="round"/>
                    </svg>
                  </div>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
