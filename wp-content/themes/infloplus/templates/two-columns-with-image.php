<?php 
    $title = get_sub_field("section_title");
    if($title != null): echo "<section class='content-sections'>"; endif;
?>
    <div class="container">
        <div class="two-column-image">
        <?php if($title != null): echo "<h2>{$title}</h2>"; endif; ?>
        <?php if(have_rows("column_row")): $counter = 1; while(have_rows("column_row")) : the_row(); ?>
            <div class="row two-column-container">
                <?php 
                    if(get_sub_field("text_column") && $image_obj = get_sub_field("image_box")): 
                        $alignment = get_sub_field("image_alignment");
                        if ($alignment == "Left" || $alignment == null):
                ?>
                    <div class="col-md-6">
                        <?php if($image_obj != null): ?>
                            <div class="full-image-container" style="background-image: url(<?= $image_obj["sizes"]["medium_large"]; ?>);"></div>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-6">
                        <?php the_sub_field("text_column"); ?>
                    </div>
                <?php else: ?>
                    <div class="col-md-6">
                        <?php the_sub_field("text_column"); ?>
                    </div>
                    <div class="col-md-6">
                        <?php if($image_obj != null): ?>
                            <div class="full-image-container" style="background-image: url(<?= $image_obj["sizes"]["medium_large"]; ?>);"></div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <?php endif; ?>
            </div><!-- End of row two columns with image -->
        <?php $counter++; endwhile; endif; ?>
        </div>
        <?php 
            if(get_sub_field("add_video")): 
            $video_cover = get_sub_field("cover_image");
        ?>
            <div class="featured-video-container embed-responsive embed-responsive-16by9">
                <div class="featured-poster" style="background-image: url(<?= $video_cover["sizes"]["large"] ?>)"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
                <?php the_sub_field("featured_video"); ?>		
            </div>
        <?php endif; ?>
    </div>
<?php if($title != null): echo "</section>"; endif; ?>