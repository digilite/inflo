<div id="<?php the_sub_field("popup_id"); ?>" class="modal-frame">
    <div class="modal">
        <div class="modal-inset">
            <div class="button close close-popup"><i class="fa fa-times" aria-hidden="true"></i></div>
            <?php $popup_image = get_sub_field("popup_image","option"); ?>
            <div class="modal-image" style="background-image: url(<?php echo $popup_image["sizes"]["popup"]; ?>);"></div>
            <div class="modal-body">
                <h2><?php the_sub_field("popup_title","option"); ?></h2>
                <?php the_sub_field("popup_content","option"); ?>
                <div class="button-container">
                    <?php while(have_rows("links","option")) : the_row(); 
                            if(get_sub_field("button_text")):
                                $link = (get_sub_field("link_type","option") == "Internal" ) ? get_sub_field("internal_link","option") : get_sub_field("external_link","option"); 
                                echo '<a href="' . $link .'" class="button">'. get_sub_field("button_text","option"). '</a>';
                            endif; ?>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
</div>