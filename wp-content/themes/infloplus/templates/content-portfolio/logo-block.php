<?php
  $logo_bg = get_sub_field("logo_block_background");
  $color = get_sub_field("select_color");
  $logo = get_sub_field("logo");
  $logo_desc = get_sub_field("logo_description");
  $prod_image_ob = get_sub_field("product_screenshot");
  $prod_desc = get_sub_field("product_description");
  $website_url = get_sub_field("website_link");

  $bg_color = "";
  if ($logo_bg != "select-bg") {
    $bg_color = $logo_bg;
  } else {
    $logo_bg = "style='background-color:  ". $color ."'";
  }
?>
<section class="logo-block">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="logo-container square-box" <?= $logo_bg; ?>>
          <img class="img-fluid" src="<?= $logo; ?>" alt="<?php the_title(); ?> logo">
        </div>
        <?php
          if (!empty($logo_desc)) {
            echo "<h4 class='description'>". $logo_desc ."</h4>";
          }
        ?>
      </div>
      <div class="col-md-6">
        <div class="square-box cover" style="background-image: url(<?= $prod_image_ob["sizes"]["blog"]; ?>);"></div>
        <?php
          if (!empty($prod_desc)) {
            echo "<h4 class='description'>". $prod_desc ."</h4>";
          }
        ?>
        <?php if(!empty($website_url)): ?>
        <a class="icon-btn-container" href="<?= $website_url; ?>" target="_blank">
            <span class="icon-btn">
                <svg class="eye-icon"width="17.5" height="13.886" viewBox="0 0 17.5 13.886"><use xlink:href="#eye-icon"></use></svg>
            </span>
            Visit Website
        </a>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>
