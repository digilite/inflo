<?php
  $numb = count(get_sub_field("portfolio_blocks"));
  if( have_rows("portfolio_blocks") ):
    $counter = 1;
?>
  <section class="image-title-blocks blocks-<?= $numb; ?>">
    <div class="container">
      <div class="row">
        <?php
          while ( have_rows("portfolio_blocks") ) : the_row();
          $image_ob = get_sub_field("image");
          $title = get_sub_field("title");
          $description = get_sub_field("description");

          $bg_imge = $image_ob["sizes"]["full-slider"];
          $class = 12 / $numb;
        ?>
        <div class="col-md-<?= $class; ?>">
          <div class="portfolio-card cover" style="background-image: url(<?= $bg_imge; ?>)"></div>
          <?php
            if (!empty($title)) {
              echo "<h5 class='title'>". $title ."</h5>";
            }
            if (!empty($description)) {
              echo "<p class='description'>". $description ."</p>";
            }
          ?>
        </div>
        <?php $counter++; endwhile; ?>
      </div>
    </div>
  </section>

<?php endif; ?>
