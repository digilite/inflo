<?php
  $bg_style = get_sub_field("background_style");
  $bg_image = get_sub_field("background_image");
  $height = get_sub_field("block_heigh");
  $image = $bg_image["sizes"]["full-slider"];

  if ( $bg_style == "full-width" ) {
    $featured_image = "<div class='featured-image cover {$height} {$bg_style}' style='background-image: url(". $image .");'></div>";
  } else {
    $featured_image = "<div class='container'><div class='featured-image cover {$height} {$bg_style}' style='background-image: url(". $image .");'></div></div>";

  }

?>
<section class="background-block">
  <?= $featured_image; ?>
</section>
