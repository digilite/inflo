<section class="quotation-block">
  <div class="container">
    <div class="row justify-content-md-end">
      <div class="col-md-8">
        <blockquote><?php the_sub_field("quotation_text"); ?></blockquote>
      </div>
    </div>
  </div>
</section>
