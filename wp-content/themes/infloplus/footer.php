    <!--<div class="custom-popup" id="digital_audit_popup">
    <div class="popup-inner">
      <a href="#" class="close close-popup"></a>
      <div class="_white-form-holder digital-audit-form-holder">
        <h4 class="text-center">Register now to see the Digital Audit in action</h4>
        </!--[if lte IE 8]>
        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
        <![endif]--/>
        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
        <script>
          hbspt.forms.create({
          portalId: "7936749",
          formId: "ac7654fe-584e-4007-b8c1-5230bcc8d2f6"
        });
        </script>
      </div>
    </div>
  </div>-->
  </main>
    <footer itemscope itemtype="http://schema.org/WPFooter">
			<div class="container">
				<div class="row justify-content-between column-revers">
					<div class="col-md-5">
						<!-- <div class="email-link-holder">
							<a href="mailto:<?= esc_attr( get_option('gb_email'.$gf_prefix) ) ?>"><?= esc_attr( get_option('gb_email'.$gf_prefix) ) ?></a>
						</div> -->
						<div id="footer_hub_form" style="display: none;">
						  <?php echo do_shortcode('[hubspot type=form portal=7936749 id=f83a3ac6-3b5f-4081-a02b-bc0d7f9d9a1c]'); ?>
						</div>
						<form action="#" class="subscribe-form" id="hub_sub">
							<p class="h6">Sign up for Inflo product updates and content</p>
							<div class="single-form-row d-flex">
								<input type="email" class="form-control" placeholder="Your e-mail">
								<input type="submit" value="Subscribe" class="button button-small">
							</div>
						</form>
            <div class="social-pages">
              <?php get_template_part("social"); ?>
            </div>
            <?php wp_nav_menu( array('menu' => 'footer3','container' => '','menu_class' => 'nav navbar-expand privacy-menu','depth' => '0',) ); ?>
            <span class="copyright">&copy; <?php echo date("Y") . " " . get_bloginfo("name"); ?>. All rights are reserved.</span>
					</div>

					<div class="col-md-4 d-flex justify-content-between second-column">
							<?php wp_nav_menu( array('menu' => 'footer1','menu_class' => 'nav flex-column product-menu' ,'container' => '','depth' => '0',) ); ?>
              <?php wp_nav_menu( array('menu' => 'footer2','menu_class' => 'nav flex-column' ,'container' => '','depth' => '0',) ); ?>
					</div>
				</div>
			</div>

    </footer>
		<?php wp_footer(); ?>
	</body>
</html>
