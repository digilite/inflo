<?php

function use_cases_posts() {
  register_post_type('use_cases',
    array('labels'=> array('name'=> __('Use Cases'),
        'singular_name'=> __('Use Case'),
        'all_items'=> __('All Use Cases'),
        'add_new'=> __('Add New'),
        'add_new_item'=> __('Add New'),
        'edit'=> __('Edit'),
        'edit_item'=> __('Edit Use Case'),
        'new_item'=> __('New Use Case'),
        'view_item'=> __('View Use Case'),
        'search_items'=> __('Search Use Case'),
        'not_found'=> __('Nothing found in the Database.'),
        'not_found_in_trash'=> __('Nothing found in Trash'),
        'parent_item_colon'=> ''
      ),
      /* end of arrays */
      'description'=> __('This is the Use Case post type'),
      'public'=> true,
      'publicly_queryable'=> true,
      'exclude_from_search'=> true,
      'show_ui'=> true,
      'query_var'=> true,
      'has_archive'=> false,
      //'show_in_menu'       => false,
      //'show_in_nav_menus'  => false,
      'menu_position'=> 5,
      'menu_icon'=> 'dashicons-welcome-write-blog',
      'rewrite'=> array('slug'=> 'customer-success', 'with_front'=> false),
      'has_archive'=> false,
      'capability_type'=> 'post',
      'supports'=> array('title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'revisions', 'comments', 'page-attributes'),
      'taxonomies'=> array('product_tax'))
    /* end of options */
  );
}

add_action('init', 'use_cases_posts');

function service_plans () {
  register_post_type('cpt_plans',
    array('labels'=> array('name'=> __('Plans'),
        'singular_name'=> __('Plan'),
        'all_items'=> __('All Plans'),
        'add_new'=> __('Add New'),
        'add_new_item'=> __('Add New'),
        'edit'=> __('Edit'),
        'edit_item'=> __('Edit Plan'),
        'new_item'=> __('New Plan'),
        'view_item'=> __('View Plan'),
        'search_items'=> __('Search Plan'),
        'not_found'=> __('Nothing found in the Database.'),
        'not_found_in_trash'=> __('Nothing found in Trash'),
        'parent_item_colon'=> ''
      ),
      /* end of arrays */
      'description'=> __('This is the Plan post type'),
      'public'=> false,
      'publicly_queryable'=> false,
      'exclude_from_search'=> true,
      'show_ui'=> true,
      'query_var'=> true,
      'has_archive'=> false,
      //'show_in_menu'       => false,
      //'show_in_nav_menus'  => false,
      'menu_position'=> 4,
      'menu_icon'=> 'dashicons-welcome-write-blog',
      //'rewrite'=> array('slug'=> 'customer-success', 'with_front'=> false),
      'has_archive'=> false,
      'capability_type'=> 'post',
      'supports'=> array('title', 'editor', 'custom-fields', 'revisions'))
    /* end of options */
  );
}

add_action('init', 'service_plans');

// Fix for "customer-success" pagination
add_action('init', function () {
    add_rewrite_rule('customer-success/page/([0-9]+)/?$',
      'index.php?pagename=customer-success&paged=$matches[1]',
      'top'
    );
  }

);

function product_posts() {

  register_post_type('products',
    array('labels'=> array('name'=> __('Products'),
        'singular_name'=> __('Product'),
        'all_items'=> __('All Products'),
        'add_new'=> __('Add Product'),
        'add_new_item'=> __('Add Product'),
        'edit'=> __('Edit'),
        'edit_item'=> __('Edit Product'),
        'new_item'=> __('New Product'),
        'view_item'=> __('View Product'),
        'search_items'=> __('Search Product'),
        'not_found'=> __('Nothing found in the Database.'),
        'not_found_in_trash'=> __('Nothing found in Trash'),
        'parent_item_colon'=> ''
      ),
      /* end of arrays */
      'description'=> __('This is the Product post type'),
      'public'=> true,
      'publicly_queryable'=> true,
      'exclude_from_search'=> true,
      'show_ui'=> true,
      'query_var'=> true,
      'hierarchical'=> false,
      //'show_in_menu'       => false,
      //'show_in_nav_menus'  => false,
      'menu_position'=> 5,
      'menu_icon'=> 'dashicons-welcome-write-blog',
      'rewrite'=> array('slug'=> 'products', 'with_front'=> false),
      'has_archive'=> false,
      'capability_type'=> 'post',
      'supports'=> array('title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'revisions', 'comments', 'page-attributes'),
      'taxonomies'=> array())
    /* end of options */
  );
}

add_action('init', 'product_posts');

add_action('add_meta_boxes', function () {
    add_meta_box('parent_product', 'Parent Product', 'modules_parent_meta_box', 'modules', 'side', 'default');
  }

);

function modules_parent_meta_box($post) {
  /* $pages = wp_dropdown_pages(array('post_type' => 'products', 'name' => 'parent_product', 'show_option_none' => __('(no parent)'), 'sort_column'=> 'post_title', 'echo' => 0));
        print_r($pages);
        if ( ! empty($pages) ) {
            echo $pages;
        } // end empty pages check*/
  $current_parent_product=get_post_meta($post->ID, 'parent_product')[0];
  echo '<select id="parent_product" name="parent_product">';
  $prod_posts=get_posts(['numberposts'=> -1, 'post_type'=> 'products']);

  //print_r($prod_posts);
  foreach ($prod_posts as $ppost) {
    $selected='';

    if ($current_parent_product==$ppost->post_name) {
      $selected='selected';
    }

    echo '<option value="'. $ppost->post_name . '" '. $selected . '>'. $ppost->post_title . '</option>';
  }

  echo '</select>';
}

add_action('save_post', 'so_save_metabox');

function so_save_metabox() {
  global $post;

  if (get_post_type($post->ID)=='modules') {

    //print_r($_POST); print_r(get_post_meta($post->ID)); die;
    if (isset($_POST["parent_product"])) {
      //UPDATE:
      $meta_element=$_POST['parent_product'];
      //END OF UPDATE

      update_post_meta($post->ID, 'parent_product', $meta_element);
    }
  }
}

function module_post_link($post_link, $post) {

  //$post = get_post($id);
  if (is_object($post)) {

    if (get_post_type($post->ID)=='modules') {
      $parent_product=get_post_meta($post->ID, 'parent_product')[0];

      //$parent_product_post = get_post($parent_product);
      return str_replace('%parent_product%', $parent_product, $post_link);
    }
  }

  return $post_link;
}

add_filter('post_type_link', 'module_post_link', 1, 3);

function module_posts() {

  register_post_type('modules',
    array('labels'=> array('name'=> __('Modules'),
        'singular_name'=> __('Module'),
        'all_items'=> __('Modules'),
        'add_new'=> __('Add New'),
        'add_new_item'=> __('Add New'),
        'edit'=> __('Edit'),
        'edit_item'=> __('Edit Module'),
        'new_item'=> __('New Module'),
        'view_item'=> __('View Module'),
        'search_items'=> __('Search Module'),
        'not_found'=> __('Nothing found in the Database.'),
        'not_found_in_trash'=> __('Nothing found in Trash'),
        'parent_item_colon'=> ''
      ),
      /* end of arrays */
      'description'=> __('This is the Module post type'),
      'public'=> true,
      'publicly_queryable'=> true,
      'exclude_from_search'=> true,
      'show_ui'=> true,
      'query_var'=> true,
      'hierarchical'=> false,
      'show_in_menu'=> 'edit.php?post_type=products',
      //'show_in_nav_menus'  => false,
      //'menu_position' => 6,
      'menu_icon'=> 'dashicons-welcome-write-blog',
      'rewrite'=> array('slug'=> '%parent_product%/modules', 'with_front'=> false),
      'has_archive'=> false,
      'capability_type'=> 'post',
      'supports'=> array('title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'revisions', 'comments', 'page-attributes'),
      'taxonomies'=> array())
    /* end of options */
  );
}

add_action('init', 'module_posts');

add_action('init', 'do_rewrite');

function do_rewrite() {
  add_rewrite_rule('^([^/]*)/modules/([^/]*)/?', 'index.php?post_type=modules&name=$matches[2]', 'top');
}
