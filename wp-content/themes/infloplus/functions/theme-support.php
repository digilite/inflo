<?php
function blank_widgets_init() {
	// Area 1, located at the top of the sidebar.
	register_sidebar([
		'name' => 'Sidebar',
		'id' => 'sidebar',
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	]);
}
add_action("widgets_init", "blank_widgets_init");

/* Add Thumbnail Support */
add_theme_support("post-thumbnails");
// add_image_size( 'featured-small', 640, 200, true ); // name, width, height, crop
// add_image_size( 'featured-medium', 1280, 400, true );
// add_image_size( 'featured-large', 1440, 400, true );
// add_image_size( 'featured-xlarge', 1920, 400, true );

/* custom read more and excerpt heigh.
 * usage: echo my_excerpts(20, $post) or echo my_excerpts(20)
 * or use wp_trim_words() @ https://developer.wordpress.org/reference/functions/wp_trim_words/
 */
function custom_excerpt($excerpt_length, $content = false) {
	global $post;
	$mycontent = $post->post_excerpt;
	$link = $post->guid;

	$mycontent = __($post->post_content);
	$mycontent = strip_shortcodes($mycontent);
	$mycontent = str_replace("]]>", "]]&gt;", $mycontent);
	$mycontent = strip_tags($mycontent);
	$words = explode(" ", $mycontent, $excerpt_length + 1);
	if(count($words) > $excerpt_length) :
	array_pop($words);
	array_push($words, "...");
	$mycontent = implode(" ", $words);
	endif;

	// Make sure to return the content
	return $mycontent;
}

/* change e-mail name */
function res_fromemail($email) {
	$wpfrom = get_option("admin_email");
	return $wpfrom;
}
add_filter("wp_mail_from", "res_fromemail");

function res_fromname($email) {
	$wpfrom = get_option("blogname");
	return $wpfrom;
}
add_filter("wp_mail_from_name", "res_fromname");


// This function is to output buttons
function echo_button ( $text, $link, $class = "button", $show_icon = "hide", $target = "_self"){
  if (!empty($text) && !empty($link)){
    if ($show_icon == "hide" || $show_icon == "") {
      echo "<a class='". $class ."' href='". $link ."' target=". $target .">". $text ."</a>";
    } else {
      echo "<a class='". $class ."' href='". $link ."' target=". $target .">". $text ." <svg width='20' height='20' viewBox='0 0 20 20' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M10.6237 13.75L14.0312 10L10.6237 6.25M13.5142 10H5.96875' stroke='#EC6325' stroke-linecap='round' stroke-linejoin='round'/>
      <path d='M10 19C14.9688 19 19 14.9688 19 10C19 5.03125 14.9688 1 10 1C5.03125 1 1 5.03125 1 10C1 14.9688 5.03125 19 10 19Z' stroke='#EC6325' stroke-miterlimit='10'/>
      </svg></a>";
    }
  }
}

