<?php /* Template Name: Inflo Career */ ?>

<?php get_header(); ?>

<section class="intro-section career-intro bg-anim border-bottom-white">
	<div class="container">
		<div class="text-center">
			<h1 class="h2"><?= get_field('careers_top_title') ?></h1>
			<div class="career-intro-inner">
				<div class="img-holder">
					<!--<img src="<?= get_field('careers_top_image') ?>" alt="Inflo">-->
					<div class="in-holder">
						<div class="in-slider">
							<div>
								<div class="in-slide" style="background-image: url('<?= get_field('careers_top_image_1')['sizes']['medium_large'] ?>');"></div>
							</div>
							<div>
								<div class="in-slide" style="background-image: url('<?= get_field('careers_top_image_2')['sizes']['medium_large'] ?>');"></div>
							</div>
							<div>
								<div class="in-slide" style="background-image: url('<?= get_field('careers_top_image_3')['sizes']['medium_large'] ?>');"></div>
							</div>
						</div>

						<div class="in-slider in-slider-over">
							<div>
								<div class="in-slide" style="background-image: url('<?= get_field('careers_top_image_1')['sizes']['medium_large'] ?>');"></div>
							</div>
							<div>
								<div class="in-slide" style="background-image: url('<?= get_field('careers_top_image_2')['sizes']['medium_large'] ?>');"></div>
							</div>
							<div>
								<div class="in-slide" style="background-image: url('<?= get_field('careers_top_image_3')['sizes']['medium_large'] ?>');"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="text-holder">
					<?= get_field('careers_top_text') ?>
					<?php if(get_field('careers_top_cta_text') != null && get_field('careers_top_cta_text') != '') { ?>
					<a href="<?= get_field('careers_top_cta_destination') ?>" class="btn"><?= get_field('careers_top_cta_text') ?></a>
					<?php } ?>
				</div>
			</div>
		</div>

	</div>
</section>

<?php $stat_cols = acf_get_fields_by_id(1735); if(get_field($stat_cols[0]['name'])['stat_title'] != null && get_field($stat_cols[0]['name'])['stat_title'] != '' ) { ?>
<section class="section-collapse stats-career">
	<div class="container">
		<div class="balls-set">

			<?php
			foreach($stat_cols as $col) { $the_col = get_field($col['name']); ?>
			<div class="ball bg-anim">
				<div class="title-price-block">
					<strong class="price-title"><?= $the_col['stat_title'] ?></strong>
					<span class="price-subtitle"><?= $the_col['stat_subtitle'] ?></span>
				</div>
			</div>
			<?php }  ?>

		</div>
	</div>
</section>
<?php } ?>

<section class="career-offer-section">
	<div class="container">
		<div class="text-holder">
			<div class="text-center">
				<h2 class="h3"><?= get_field('careers_offer_title') ?></h2>
			</div>
			<?= get_field('careers_offer_text') ?>
		</div>

		<div class="offers-slider">
			<?php
			for($i=1; $i <= 4; $i++){
			$slide = get_field('careers_offer_block_'.$i);
			?>
			<div class="offer-slide">
				<div class="offer-item">
					<div class="img-block" style="background-image: url(<?= $slide['offer_block_image']['sizes']['medium'] ?>);"></div>
					<div class="text-block">
						<h3 class="offer-title"><?= $slide['offer_block_title'] ?></h3>
						<p><?= $slide['offer_block_description'] ?></p>
					</div>
				</div>
			</div>
			<?php } ?>

		</div>
	</div>
</section>

<section class="core-value-section bg-grey border-top-white border-bottom-white">
	<div class="container">
		<h2 class="h3"><?= get_field('core_values_title') ?></h2>
		<div class="float-columns">
			<div class="col">
				<div class="core-values-holder">
					<?php
					for($i=1; $i <= 9; $i++){
					$value = get_field('core_values_block_'.$i);
					if($value && $value['value_text'] != ''){
					?>
					<div class="core-value">
						<div class="img-holder">
							<img loading="lazy" src="<?= $value['value_image']['sizes']['medium'] ?>">
						</div>
						<p><?= $value['value_text'] ?></p>
					</div>
					<?php }} ?>
				</div>
			</div>
			<div class="col first-col">
				<p><?= get_field('core_values_text') ?></p>
			</div>
			<div class="col">
				<?php $use_case_id = get_field('testimonial_from_use_case'); ?>
				<div class="testimonial-item">
					<div class="text-block">
						<div class="author-block">
							<div class="author-name-holder">
								<div class="author-img" style="background-image: url('<?= get_field('testimonial_avatar', $use_case_id)['sizes']['thumbnail'] ?>');"></div>
								<div class="author-descr">
									<h5 class="author-name"><?= get_field('testimonial_name', $use_case_id) ?></h5>
									<p><?= get_field('testimonial_job_title', $use_case_id) ?></p>
								</div>
							</div>
							<img loading="lazy" src="https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/icon-50.png" class="quote-img">
							<p><?= get_field('testimonial_quote', $use_case_id) ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="testimonials-section bg-white ">
	<div class="container">
		<h2><?= get_field('testimonial_slider_title') ?></h2>
	</div>
	<div class="testimonials-slider">

		<?php $testimonial_slides = acf_get_fields_by_id(1415);
			foreach($testimonial_slides as $item) { $the_item = get_field($item['name']);
				if($the_item && $item['name'] != 'testimonial_slider_title' && $the_item['testimonial_author_name'] != ''){
if($the_item['show_testimonial_slide'] != 'No') {
			?>

		<div class="testimonial-slide">
			<div class="testimonial-item">
				<?php if($the_item['testimonial_slide_video'] && trim($the_item['testimonial_slide_video']) != '') {


						$additional_str_start = '?';
						if(strrpos($the_item['testimonial_slide_video'], '?') !== false){
							$additional_str_start = '&';
						}
					?>
					<div class="img-block" style="background: #000;">
						<a data-fancybox href="<?= $the_item['testimonial_slide_video'] ?>" class="video-popup-opener"><iframe loading="lazy" src="<?= $the_item['testimonial_slide_video'] . $additional_str_start ?>title=0&byline=0&portrait=0&sidedock=0&controls=0" width="100%" height="100%" frameborder="0"></iframe></a>
					</div>
					<?php } else { ?>
					<div class="img-block" style="background-image: url(<?= $the_item['testimonial_slide_image']['sizes']['medium_large'] ?>)">
						<a class="video-popup-opener"></a>
					</div>
					<?php } ?>
				<div class="text-block">
					<div class="author-block">
						<div class="author-name-holder">
							<div class="author-img" style="background-image: url('<?= $the_item['testimonial_author_image']['sizes']['thumbnail'] ?>');"></div>
							<div class="author-descr">
								<h5 class="author-name"><?= $the_item['testimonial_author_name'] ?></h5>
								<p><?= $the_item['testimonial_author_position'] ?></p>
							</div>
						</div>
						<img loading="lazy" src="https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/icon-50.png" class="quote-img">
						<p><?= $the_item['testimonial_text'] ?></p>
					</div>
				</div>
			</div>
		</div>

		<?php } }} ?>
	</div>
</section>

<section class="hiring-section bg-light border-top-white">
	<div class="container">
		<h2 id="vacancies" class="gradient-underline _h3 _text-center"><?= get_field('hiring_title') ?></h2>
		<div class="two-col-holder">
			<?php $av_jobs = acf_get_fields_by_id(4206);
			foreach($av_jobs as $job) { if($job['name'] != 'hiring_title') { ?>
			<?php $the_job = get_field($job['name']); if($the_job['job_title'] != null && trim($the_job['job_title']) != '') { ?>
			<div class="col">
				<div class="position-item">
					<h3 class="position-title"><?= $the_job['job_title'] ?></h3>
					<ul class="position-details-list">
						<li>
							<img loading="lazy" src="https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/icon-47.png">
							<span class="detail-irem"><?= $the_job['job_location'] ?></span>
						</li>
						<li>
							<img loading="lazy" src="https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/icon-25.png">
							<span class="detail-irem">Deadline: <?= $the_job['job_deadline'] ?></span>
						</li>
					</ul>
					<a href="<?= $the_job['job_url'] ?>" class="btn _btn-outline">Position details</a>
				</div>
			</div>
			<?php } } } ?>

		</div>
		<div class="two-col-holder justify-end">
			<div class="col">
				<a href="<?= get_field('gdpr_url') ?>" class="gdpr-note-block">
					<div class="icon-holder">
						<img loading="lazy" src="<?= get_field('gdpr_image')['sizes']['medium'] ?>" alt="gdpr">
					</div>
					<div class="text-holder">
						<?= get_field('gdpr_text') ?>
					</div>
				</a>
			</div>
		</div>
	</div>
</section>

<section class="faq-section border-top-light-secondary">
	<div class="container">

		<?php if(get_field('display_bottom_cta') != 'No'){ ?>
		<div class="container">
			<div class="cta-block">
				<p><?= get_field('bottom_cta_description') ?></p>
				<div class="btn-center-holder">
					<a href="<?= get_field('bottom_cta_destination') ?>" class="btn"><?= get_field('bottom_cta_text') ?></a>
				</div>
			</div>
		</div>
		<?php } ?>

		<?php if(get_field('faq_section_title') != null && get_field('faq_section_title') != '') { ?>
		<h3 class="h4"><?= get_field('faq_section_title') ?></h3>
		<?php } ?>
		<div class="faq-accordion accordion">
			<?php $contact_faq = acf_get_fields_by_id(1582);
			foreach($contact_faq as $qa) { if($qa['name'] != 'faq_section_title') {
			$the_qa = get_field($qa['name']); ?>

			<?php if($the_qa['question'] != '') { ?>
			<div class="accordion-item">
				<h4 class="accordion-item-title">
					<a href="#" class="item-opener"><?= $the_qa['question'] ?></a>
				</h4>
				<div class="accordion-item-slide">
					<div class="accordion-item-slide-content">
						<p><?= $the_qa['answer'] ?></p>
					</div>
				</div>
			</div>
			<?php }}} ?>
		</div>
	</div>

</section>

<?php get_footer(); ?>
