<?php
/* 
Template Name: Elementor Full Width Custom
Template Post Type: page, post, products, modules 
*/
?>

<?php
get_header();

global $post;

$post_cat 			   = get_the_category($post->ID)[0];
$cat_link_unfiltered   = get_category_link($post_cat->term_id);
$cat_link 			   = $cat_link_unfiltered; //str_replace('/blog', '', $cat_link_unfiltered);

$post_tag 			   = get_the_tags($post->ID)[0];
$tag_link 			   = get_tag_link($post_tag->term_id);

$this_post_id 		   = $post->ID;

$enable_background 	   = get_field('enable_background_image') ?? null;
if ($enable_background) {
	$image 		  	   = get_field('background_image') ?? null;
	if (is_array($image) && isset($image)) {
		$image_id      = $image['id'];
		$image_size    = 'header';
		$image_array   = wp_get_attachment_image_src($image_id, $image_size);
		if (is_array($image_array)) {
			$image_url = $image_array[0];
		}
	}

	$image_full_width  = get_field('full_width') ?? null;
	$clip_image 	   = get_field('clip_image') ?? null;
	if (!$image_full_width) {
		$clip_image    = false;
	}
}

$content_on_top 	   = get_field('content_on_top') ?? null;
$page_title			   = get_field('override_page_title') ?? null;
if (!$page_title) {
	$page_title		   = $post->post_title;
}
$display_breadcrumbs   = get_field('display_breadcrumbs') ?? null;
$display_author		   = get_field('display_author') ?? null;
$display_date	       = get_field('display_date') ?? null;
$display_category	   = get_field('display_category') ?? null;
$underline_content     = get_field('underline_content') ?? null;
$content_position      = get_field('content_position') ?? null;

$no_image_class	       = null;
if (!$enable_background || !$image) {
	$no_image_class	   = 'add-margin-top';
}

$order 			       = get_field('order') ?? null;

?>


<?php if ($enable_background && $image) : ?>
	<section class="detail-intro-section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="header-background-image <?php if ($clip_image) echo 'header-background-image--clip'; ?> <?php if ($image_full_width) echo 'width--full' ?>">
						<?php if (is_array($image) && isset($image)) : ?>
							<style>
								.header-background-image {
									background-image: url('<?php echo $image_url; ?>');
									background-position: <?php echo $image['left'] . '% ' . $image['top']; ?>%;
									background-size: cover;
									width: 100%;
									height: 100%;
								}
							</style>
						<?php endif; ?>
					</div>
					<?php if ($content_on_top) : ?>
						<div class="header-background-content header-background-content__absolute position--<?php echo $content_position; ?>">
							<?php if ($order == 'author-date') : ?>
								<div class="blog-detail-info">
									<?php if ($display_author) : ?>
										<div class="post-author-and-date">
											<span class="post-author"><strong><?= get_the_author_meta('display_name', $post->post_author); ?></strong></span>
											<?php if ($display_date) : ?>
												<?php
												// get date values
												$post_date_no_time = explode(' ', $post->post_date_gmt)[0];
												$post_formatted_date_arr = explode('-', $post_date_no_time);
												$dateObj = DateTime::createFromFormat('!m', $post_formatted_date_arr[1]);
												$month = $dateObj->format('F');
												$post_formatted_date = $post_formatted_date_arr[2] . ' ' . $month . ' ' . $post_formatted_date_arr[0];
												?>
												<span class="post-date"><strong>|<time datetime="<?= $post_date_no_time ?>"><?= $post_formatted_date ?></time></strong></span>
											<?php endif; ?>
										</div>
									<?php endif; ?>

									<h1 class="h2"><?= $page_title ?></h1>

									<?php if ($display_category) : ?>
										<a href="<?= $cat_link ?>" class="post-category"><?= $post_cat->name ?></a>
									<?php endif; ?>

									<?php if ($display_breadcrumbs) : ?>
										<div class="breadcrumbs-holder">
											<ul class="breadcrumbs-list">
												<li><a href="<?php echo esc_url(home_url()); ?>">Home</a></li>
												<li><a href="<?= $cat_link ?>"><?= $post_cat->name ?></a></li>
												<li><?= $post->post_title ?></li>
											</ul>
										</div>
									<?php endif; ?>

									<?php if ($underline_content) : ?>
										<div class="header-separator"></div>
									<?php endif; ?>
								</div>
							<?php elseif ($order == 'title') : ?>
								<h1 class="h2"><?= $page_title ?></h1>

								<?php if ($display_author) : ?>
									<div class="post-author-and-date">
										<span class="post-author"><strong><?= get_the_author_meta('display_name', $post->post_author); ?></strong></span>
										<?php if ($display_date) : ?>
											<?php
											// get date values
											$post_date_no_time = explode(' ', $post->post_date_gmt)[0];
											$post_formatted_date_arr = explode('-', $post_date_no_time);
											$dateObj = DateTime::createFromFormat('!m', $post_formatted_date_arr[1]);
											$month = $dateObj->format('F');
											$post_formatted_date = $post_formatted_date_arr[2] . ' ' . $month . ' ' . $post_formatted_date_arr[0];
											?>
											<span class="post-date"><strong>|<time datetime="<?= $post_date_no_time ?>"><?= $post_formatted_date ?></time></strong></span>
										<?php endif; ?>
									</div>
								<?php endif; ?>

								<?php if ($display_category) : ?>
									<a href="<?= $cat_link ?>" class="post-category"><?= $post_cat->name ?></a>
								<?php endif; ?>

								<?php if ($display_breadcrumbs) : ?>
									<div class="breadcrumbs-holder">
										<ul class="breadcrumbs-list">
											<li><a href="<?php echo esc_url(home_url()); ?>">Home</a></li>
											<li><a href="<?= $cat_link ?>"><?= $post_cat->name ?></a></li>
											<li><?= $post->post_title ?></li>
										</ul>
									</div>
								<?php endif; ?>

								<?php if ($underline_content) : ?>
									<div class="header-separator"></div>
								<?php endif; ?>
							<?php elseif ($order == 'breadcrumbs') : ?>
								<?php if ($display_breadcrumbs) : ?>
									<div class="breadcrumbs-holder">
										<ul class="breadcrumbs-list">
											<li><a href="<?php echo esc_url(home_url()); ?>">Home</a></li>
											<li><a href="<?= $cat_link ?>"><?= $post_cat->name ?></a></li>
											<li><?= $post->post_title ?></li>
										</ul>
									</div>
								<?php endif; ?>

								<h1 class="h2"><?= $page_title ?></h1>

								<?php if ($display_author) : ?>
									<div class="post-author-and-date">
										<span class="post-author"><strong><?= get_the_author_meta('display_name', $post->post_author); ?></strong></span>
										<?php if ($display_date) : ?>
											<?php
											// get date values
											$post_date_no_time = explode(' ', $post->post_date_gmt)[0];
											$post_formatted_date_arr = explode('-', $post_date_no_time);
											$dateObj = DateTime::createFromFormat('!m', $post_formatted_date_arr[1]);
											$month = $dateObj->format('F');
											$post_formatted_date = $post_formatted_date_arr[2] . ' ' . $month . ' ' . $post_formatted_date_arr[0];
											?>
											<span class="post-date"><strong>|<time datetime="<?= $post_date_no_time ?>"><?= $post_formatted_date ?></time></strong></span>
										<?php endif; ?>
									</div>
								<?php endif; ?>

								<?php if ($display_category) : ?>
									<a href="<?= $cat_link ?>" class="post-category"><?= $post_cat->name ?></a>
								<?php endif; ?>

								<?php if ($underline_content) : ?>
									<div class="header-separator"></div>
								<?php endif; ?>
							<?php endif; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>

<?php if (!$content_on_top || !$enable_background || !$image) : ?>
	<section class="header-content <?php echo $no_image_class ?>">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="header-background-content position--<?php echo $content_position; ?>">

						<?php if ($order == 'author-date') : ?>
							<div class="blog-detail-info">
								<?php if ($display_author) : ?>
									<div class="post-author-and-date">
										<span class="post-author"><strong><?= get_the_author_meta('display_name', $post->post_author); ?></strong></span>
										<?php if ($display_date) : ?>
											<?php
											// get date values
											$post_date_no_time = explode(' ', $post->post_date_gmt)[0];
											$post_formatted_date_arr = explode('-', $post_date_no_time);
											$dateObj = DateTime::createFromFormat('!m', $post_formatted_date_arr[1]);
											$month = $dateObj->format('F');
											$post_formatted_date = $post_formatted_date_arr[2] . ' ' . $month . ' ' . $post_formatted_date_arr[0];
											?>
											<span class="post-date"><strong>|<time datetime="<?= $post_date_no_time ?>"><?= $post_formatted_date ?></time></strong></span>
										<?php endif; ?>
									</div>
								<?php endif; ?>

								<h1 class="h2"><?= $page_title ?></h1>

								<?php if ($display_category) : ?>
									<a href="<?= $cat_link ?>" class="post-category"><?= $post_cat->name ?></a>
								<?php endif; ?>

								<?php if ($display_breadcrumbs) : ?>
									<div class="breadcrumbs-holder">
										<ul class="breadcrumbs-list">
											<li><a href="<?php echo esc_url(home_url()); ?>">Home</a></li>
											<li><a href="<?= $cat_link ?>"><?= $post_cat->name ?></a></li>
											<li><?= $post->post_title ?></li>
										</ul>
									</div>
								<?php endif; ?>

								<?php if ($underline_content) : ?>
									<div class="header-separator"></div>
								<?php endif; ?>
							</div>
						<?php elseif ($order == 'title') : ?>
							<h1 class="h2"><?= $page_title ?></h1>

							<?php if ($display_author) : ?>
								<div class="post-author-and-date">
									<span class="post-author"><strong><?= get_the_author_meta('display_name', $post->post_author); ?></strong></span>
									<?php if ($display_date) : ?>
										<?php
										// get date values
										$post_date_no_time = explode(' ', $post->post_date_gmt)[0];
										$post_formatted_date_arr = explode('-', $post_date_no_time);
										$dateObj = DateTime::createFromFormat('!m', $post_formatted_date_arr[1]);
										$month = $dateObj->format('F');
										$post_formatted_date = $post_formatted_date_arr[2] . ' ' . $month . ' ' . $post_formatted_date_arr[0];
										?>
										<span class="post-date"><strong>|<time datetime="<?= $post_date_no_time ?>"><?= $post_formatted_date ?></time></strong></span>
									<?php endif; ?>
								</div>
							<?php endif; ?>

							<?php if ($display_category) : ?>
								<a href="<?= $cat_link ?>" class="post-category"><?= $post_cat->name ?></a>
							<?php endif; ?>

							<?php if ($display_breadcrumbs) : ?>
								<div class="breadcrumbs-holder">
									<ul class="breadcrumbs-list">
										<li><a href="<?php echo esc_url(home_url()); ?>">Home</a></li>
										<li><a href="<?= $cat_link ?>"><?= $post_cat->name ?></a></li>
										<li><?= $post->post_title ?></li>
									</ul>
								</div>
							<?php endif; ?>

							<?php if ($underline_content) : ?>
								<div class="header-separator"></div>
							<?php endif; ?>
						<?php elseif ($order == 'breadcrumbs') : ?>
							<?php if ($display_breadcrumbs) : ?>
								<div class="breadcrumbs-holder">
									<ul class="breadcrumbs-list">
										<li><a href="<?php echo esc_url(home_url()); ?>">Home</a></li>
										<li><a href="<?= $cat_link ?>"><?= $post_cat->name ?></a></li>
										<li><?= $post->post_title ?></li>
									</ul>
								</div>
							<?php endif; ?>

							<h1 class="h2"><?= $page_title ?></h1>

							<?php if ($display_author) : ?>
								<div class="post-author-and-date">
									<span class="post-author"><strong><?= get_the_author_meta('display_name', $post->post_author); ?></strong></span>
									<?php if ($display_date) : ?>
										<?php
										// get date values
										$post_date_no_time = explode(' ', $post->post_date_gmt)[0];
										$post_formatted_date_arr = explode('-', $post_date_no_time);
										$dateObj = DateTime::createFromFormat('!m', $post_formatted_date_arr[1]);
										$month = $dateObj->format('F');
										$post_formatted_date = $post_formatted_date_arr[2] . ' ' . $month . ' ' . $post_formatted_date_arr[0];
										?>
										<span class="post-date"><strong>|<time datetime="<?= $post_date_no_time ?>"><?= $post_formatted_date ?></time></strong></span>
									<?php endif; ?>
								</div>
							<?php endif; ?>

							<?php if ($display_category) : ?>
								<a href="<?= $cat_link ?>" class="post-category"><?= $post_cat->name ?></a>
							<?php endif; ?>

							<?php if ($underline_content) : ?>
								<div class="header-separator"></div>
							<?php endif; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
	</section>
<?php endif; ?>

<section class="article-content-section">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</section>

<?php // if (get_field('display_bottom_cta') == "Yes" || get_field('enable_read_more') || get_field('display_newsletter')) : 
?>
<section class="more-studies-section bg-light border-top-white">
	<div class="container">
		<div class="cta-testimonial-holder">
			<?php include 'template-parts/blocks/sign-up-to-newsletter.php'; ?>
			<?php if (get_field('display_bottom_cta') != 'No') { ?>
				<div class="cta-block">
					<p><?= get_field('bottom_cta_description') ?></p>
					<div class="btn-center-holder">
						<a href="<?= get_field('bottom_cta_destination') ?>" class="btn"><?= get_field('bottom_cta_text') ?></a>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</section>
<?php // endif; 
?>

<?php get_footer(); ?>