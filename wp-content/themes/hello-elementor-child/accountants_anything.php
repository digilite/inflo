<?php /* Template Name: Inflo Accountants */ ?>
<?php get_header();
$which_acc = explode('/',$wp->request)[1];
?>
<section class="info-section accountant-section bg-light <?php if($which_acc == 'accountants-in-business'){ ?> bg-skew-line <?php } ?> border-bottom-white border-bottom-decor-section">
	<div class="container">
		<?php if($which_acc == 'accountants-in-practice') { ?>
		<div class="section-title">
			<h1 class="h2 gradient-underline"><?= get_field('accountants_header_title') ?></h1>
			<?= get_field('accountants_header_text') ?>
			<a href="<?= get_field('accountants_header_cta_destination') ?>" class="btn"><?= get_field('accountants_header_cta_text') ?></a>
			<?php if(get_field('accountants_header_image') && !empty(get_field('accountants_header_image')) ) { ?>
			<img style="margin-top: 40px" loading="lazy" src="<?= get_field('accountants_header_image')['sizes']['medium_large'] ?>">
			<?php } ?>
		</div>
		<?php } else { ?>
		<div class="two-col-holder">
			<div class="col">
				<h1 class="h2"><?= get_field('accountants_header_title') ?></h1>
				<?= get_field('accountants_header_text') ?>
				<a href="<?= get_field('accountants_header_cta_destination') ?>" class="btn"><?= get_field('accountants_header_cta_text') ?></a>
			</div>
			<div class="col">
				<img loading="lazy" src="<?= get_field('accountants_header_image')['sizes']['medium_large'] ?>" class="animate-right-to-left">
			</div>
		</div>
		<?php } ?>


		<?php
		if(get_field('solution_cards_show') != 'No') {

		$solution_card_2 = get_field('solution_card_1');
		$solution_card_3 = get_field('solution_card_2');
		?>

		<div class="solutions-card-holder">
			<div class="solutions-card">
				<div class="solutions-card-inner">
					<h3 class="h4"><?= $solution_card_2['solution_card_title'] ?></h3>
					<div class="subtitle-text">
						<p><?= $solution_card_2['solution_card_subtitle'] ?></p>
					</div>
					<?= $solution_card_2['solution_card_list'] ?>
					<?php if($solution_card_2['solution_card_cta_text'] != null && $solution_card_2['solution_card_cta_text'] != '') { ?>
					<a href="<?= $solution_card_2['solution_card_cta_destination'] ?>" class="btn card-btn"><?= $solution_card_2['solution_card_cta_text'] ?></a>
					<?php } ?>
				</div>
			</div>
			<div class="solutions-card">
				<div class="solutions-card-inner">
					<h3 class="h4"><?= $solution_card_3['solution_card_title'] ?></h3>
					<div class="subtitle-text">
						<p><?= $solution_card_3['solution_card_subtitle'] ?></p>
					</div>
					<?= $solution_card_3['solution_card_list'] ?>
					<?php if($solution_card_3['solution_card_cta_text'] != null && $solution_card_3['solution_card_cta_text'] != '') { ?>
					<a href="<?= $solution_card_3['solution_card_cta_destination'] ?>" class="btn card-btn"><?= $solution_card_3['solution_card_cta_text'] ?></a>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php } ?>

	</div>
	<span class="border-decor"></span>
</section>
<section class="default-section accountant-page">
	<div class="container">

		<?php if($which_acc == 'accountants-in-business') {  ?>
		<div class="diagram-holder animation-box">
			<div class="diagram-block diagram-block-1">
				<div class="acc-link-holder animate-elem elem-2">
					<a href="<?= get_field('diagram_left_image_link', 1661) ?>" class="acc-link">
						<img loading="lazy" src="<?= get_field('diagram_left_image', 1661)['sizes']['medium'] ?>" alt="city">
						<span><?= get_field('diagram_left_image_title', 1661) ?></span>
					</a>
				</div>
				<div class="note-block note-block-top animate-elem elem-6"><?= get_field('diagram_left_top_arrow', 1661) ?></div>
				<div class="note-block note-block-bottom animate-elem elem-9"><?= get_field('diagram_left_bottom_arrow', 1661) ?></div>
			</div>
			<div class="diagram-block diagram-block-2">
				<ul class="diagram-link-list animate-elem elem-3">
					<li>
						<a href="<?= get_field('diagram_top_middle_1_link', 1661) ?>" class="link-with-icon">
							<img loading="lazy" src="<?= get_field('diagram_top_middle_1_image', 1661)['sizes']['medium'] ?>">
							<span><?= get_field('diagram_top_middle_1_title', 1661) ?></span>
						</a>
					</li>
					<li>
						<a href="<?= get_field('diagram_top_middle_2_link', 1661) ?>" class="link-with-icon">
							<img loading="lazy" src="<?= get_field('diagram_top_middle_2_image', 1661)['sizes']['medium'] ?>">
							<span><?= get_field('diagram_top_middle_2_title', 1661) ?></span>
						</a>
					</li>
				</ul>
				<span class="logo-block animate-elem elem-1">
					<img loading="lazy" src="<?= get_field('diagram_middle_image', 1661)['sizes']['medium'] ?>" alt="Inflo">
				</span>
				<ul class="diagram-link-list animate-elem elem-5">
					<li>
						<a href="<?= get_field('diagram_bottom_middle_link', 1661) ?>" class="link-with-icon">
							<img loading="lazy" src="<?= get_field('diagram_bottom_middle_image', 1661)['sizes']['medium'] ?>">
							<span><?= get_field('diagram_bottom_middle_title', 1661) ?></span>
						</a>
					</li>
				</ul>
			</div>
			<div class="diagram-block diagram-block-3">
				<div class="acc-link-holder">
					<a href="<?= get_field('diagram_right_image_link', 1661) ?>" class="acc-link">
						<span class="animate-elem elem-10">
							<img loading="lazy" src="<?= get_field('diagram_right_image', 1661)['sizes']['medium'] ?>" alt="collaboration">
							<span><?= get_field('diagram_right_image_title', 1661) ?></span>
						</span>
					</a>
					<a href="<?= get_field('diagram_right_image_secondary_link', 1661) ?>" class="inflo-hi-label">
						<span class="animate-elem elem-4">
							<img loading="lazy" src="<?= get_field('diagram_right_image_secondary', 1661)['sizes']['medium'] ?>" alt="Hi">
							<span class="text"><?= get_field('diagram_right_image_secondary_title', 1661) ?></span>
						</span>
					</a>
				</div>
				<div class="note-block note-block-top animate-elem elem-7"><?= get_field('diagram_right_top_arrow', 1661) ?></div>
				<div class="note-block note-block-bottom animate-elem elem-8"><?= get_field('diagram_right_bottom_arrow', 1661) ?></div>
			</div>
			<div class="diagram-block diagram-block-4">
				<div class="diagram-circle-block animate-elem elem-11">
					<ul class="diagram-img-circle">
						<li><img loading="lazy" src="<?= get_field('diagram_circle_icon_1', 1661)['sizes']['medium'] ?>"></li>
						<li><img loading="lazy" src="<?= get_field('diagram_circle_icon_2', 1661)['sizes']['medium'] ?>"></li>
						<li><img loading="lazy" src="<?= get_field('diagram_circle_icon_3', 1661)['sizes']['medium'] ?>"></li>
						<li><img loading="lazy" src="<?= get_field('diagram_circle_icon_4', 1661)['sizes']['medium'] ?>"></li>
						<li><img loading="lazy" src="<?= get_field('diagram_circle_icon_5', 1661)['sizes']['medium'] ?>"></li>
						<li><img loading="lazy" src="<?= get_field('diagram_circle_icon_6', 1661)['sizes']['medium'] ?>"></li>
					</ul>
					<a href="<?= get_field('diagram_circle_top_link', 1661) ?>" class="link-with-icon diagram-circle-link">
						<span><?= get_field('diagram_circle_top_title', 1661) ?></span>
					</a>
					<a href="<?= get_field('diagram_circle_bottom_link', 1661) ?>" class="link-with-icon">
						<img loading="lazy" src="<?= get_field('diagram_circle_bottom_image', 1661)['sizes']['medium'] ?>">
						<span><?= get_field('diagram_circle_bottom_title', 1661) ?></span>
					</a>
				</div>
				<div class="note-block note-block-top animate-elem elem-12"><?= get_field('diagram_circle_text_above', 1661) ?></div>
			</div>
		</div>
		<?php } ?>

		<div class="features-block">
			<div class="show-more-block">
				<div class="features-holder">

					<?php $features_items = acf_get_fields_by_id(1347); //print_r($features_items);
					 foreach($features_items as $item) { $the_item = get_field($item['name']);
					 if($the_item['title'] != null && $the_item['title'] != '') { ?>
					<div class="features-item show-more-item">
						<img loading="lazy" src="<?= $the_item['icon']['sizes']['medium_large'] ?>" class="features-item-icon">
						<h3 class="h4"><?= $the_item['title'] ?></h3>
						<?= $the_item['description'] ?>
						<?php if($the_item['cta_text'] != null && $the_item['cta_text'] != '') { ?>
						<div class="text-right">
							<a href="<?= $the_item['cta_destination'] ?>" class="btn btn-outline"><?= $the_item['cta_text'] ?></a>
						</div>
						<?php } ?>
					</div>
					<?php }} ?>

				</div>
			</div>
		</div>
	</div>
</section>
<section class="default-section accountant-page bg-light border-top-white">
	<div class="container">
		<div class="two-col-holder">
			<div class="col">
				<div class="video-popup-holder">
					<?php if(get_field('accountants_cover_image') != null && get_field('accountants_cover_image') != '') { ?>
					<a data-fancybox href="<?= get_field('accountants_video') ?>" class="video-popup-opener">
						<img loading="lazy" src="<?= get_field('accountants_cover_image')['sizes']['medium_large'] ?>">
					</a>
					<?php } else { ?>

						<?php
							$additional_str_start = '?';
							if(strrpos(get_field('accountants_video'), '?') !== false){
								$additional_str_start = '&';
							}
						?>

						<a data-fancybox href="<?= get_field('accountants_video') ?>" class="video-popup-opener iframe-holder">
							<iframe loading="lazy" src="<?= get_field('accountants_video') . $additional_str_start ?>title=0&byline=0&portrait=0&sidedock=0&controls=0" width="100%" height="auto" frameborder="0"></iframe>
						</a>

					<?php } ?>
				</div>
				<div class="video-note-row">
					<div class="video-note">
						<p><?= get_field('accountants_video_note') ?></p>
					</div>
					<?php if(get_field('accountants_video_cta_text') != null && get_field('accountants_video_cta_text') != '') { ?>
					<div class="video-note-btn">
						<a href="<?= get_field('accountants_video_cta_destination') ?>" class="btn"><?= get_field('accountants_video_cta_text') ?></a>
					</div>
					<?php } ?>
				</div>
			</div>
			<div class="col">
				<?= get_field('accountants_video_side_text') ?>
			</div>
		</div>
	</div>
</section>
<section class="testimonial-two-col-section bg-orange border-top-light-secondary border-bottom-white">
	<div class="container">
		<div class="two-col-holder">
			<div class="col">

				<?php if(get_field('simple_rich_text_section_show') == 'Yes') { ?>

					<?= get_field('simple_rich_text_section_content') ?>

				<?php } ?>

			</div>
			<div class="col">
				<?php if(get_field('testimonial_from_use_case') != null && get_field('testimonial_from_use_case') != '') { ?>
					<?php $use_case_id = get_field('testimonial_from_use_case'); ?>
				<div class="testimonial-item">
					<div class="text-block">
						<div class="author-block">
							<div class="author-name-holder">
								<div class="author-img" style="background-image: url('<?= get_field('testimonial_avatar', $use_case_id)['sizes']['thumbnail'] ?>');"></div>
								<div class="author-descr">
									<h5 class="author-name"><?= get_field('testimonial_name', $use_case_id) ?></h5>
									<p><?= get_field('testimonial_job_title', $use_case_id) ?></p>
								</div>
							</div>
							<img loading="lazy" src="https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/icon-50.png" class="quote-img">
							<p><?= get_field('testimonial_quote', $use_case_id) ?></p>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>
<section class="partners-section accountant-page border-bottom-light-secondary">
	<div class="container">

		<h2 class="h4"><?= get_field("partners_text", 1312) ?></h2>
		<ul class="partners-list">
			<?php $partner_icons = acf_get_fields_by_id(1308);
			foreach($partner_icons as $icon) { if($icon['name'] != 'partners_text') { ?>
			<?php $the_image = get_field($icon['name'], 1312);
				$partner_link = $the_image['partner_link'];
				if(trim($partner_link) == '') {
					$partner_link = '/'.ICL_LANGUAGE_CODE.'/customer-success';
				}
			?>
			<li>
				<a href="<?= $partner_link ?>">
					<img loading="lazy" src="<?= $the_image['partner_image']['sizes']['medium'] ?>">
				</a>
			</li>
			<?php }} ?>
		</ul>

	</div>
</section>
<?php if(get_field('display_bottom_cta') != 'No'){ ?>
<section class="default-section bg-light">
	<div class="container">
		<div class="cta-block">
			<p><?= get_field('bottom_cta_description') ?></p>
			<div class="btn-center-holder">
				<a href="<?= get_field('bottom_cta_destination') ?>" class="btn"><?= get_field('bottom_cta_text') ?></a>
			</div>
		</div>
	</div>
</section>
<?php } ?>
<?php get_footer(); ?>
