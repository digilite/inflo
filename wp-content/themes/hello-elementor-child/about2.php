<?php /* Template Name: Inflo About New */ ?>

<?php get_header(); ?>
    <section class="intro-section secondary-page bg-anim border-bottom-white">
        <div class="container">
            <div class="text-block full-width">
                <h1 class="h2"><?= get_field('header_text') ?></h1>
            </div>
            <div class="three-columns">
                <?php $stat_cols = acf_get_fields_by_id(1468);
                foreach ($stat_cols as $col) {
                    if ($col['name'] != 'header_text') {
                        $the_col = get_field($col['name']); ?>
                        <div class="col">
                            <div class="title-price-block">
                                <strong class="price-title anim-num"><?= $the_col['stat_text'] ?></strong>
                                <span class="price-subtitle"><?= $the_col['stat_description'] ?></span>
                            </div>
                        </div>
                    <?php }
                } ?>
            </div>
        </div>
    </section>
    <section class="simple-about-section">
        <div class="container">
            <div class="two-col-holder">
                <div class="col">
                    <img loading="lazy" src="<?= get_field('about_image')['sizes']['medium_large'] ?>"
                         class="content-img animate-left-to-right">
                </div>
                <div class="col">
                    <?= get_field('about_half-section_description') ?>
                    <?php if (get_field('about_cta_link') != null && get_field('about_cta_link') != '') { ?>
                        <a href="<?= get_field('about_cta_link') ?>" class="btn"><?= the_field('about_cta_text') ?></a>
                    <?php } ?>
                </div>
            </div>
            <div class="simple-about-descr">
                <?= get_field('about_wide_description') ?>
            </div>
        </div>
    </section>
    <section class="member-section bg-dark-grey border-top-white border-bottom-light">
        <div class="container">
            <div class="map-row map-row-wide">
                <div class="text-block">
                    <p><?= get_field('text_near_map') ?></p>
                </div>
                <div class="map-block">
                    <div class="map-holder">
                        <div class="map-box">
                            <img loading="lazy" src="https://dev.digilabs.ca/inflo/wp-content/uploads/2021/06/map-1.svg" alt="Map" width="1099">
                            <ul class="dot-list">
                                <li>
                                    <span class="line line-1"></span>
                                </li>
                                <li class="dot-lg">
                                    <span class="line line-1"></span>
                                    <span class="line line-2"></span>
                                </li>
                                <li>
                                    <span class="line line-1"></span>
                                </li>
                                <li>
                                    <span class="line line-1"></span>
                                </li>
                                <li class="dot-lg">
                                    <span class="line line-1"></span>
                                </li>
                                <li>
                                    <span class="line line-1"></span>
                                </li>
                                <li class="dot-lg">
                                    <span class="line line-1"></span>
                                    <span class="line line-2"></span>
                                    <span class="line line-3"></span>
                                    <span class="line line-4"></span>
                                </li>
                                <li class="dot-lg">
                                    <span class="line line-1"></span>
                                </li>
                                <li></li>
                                <li>
                                    <span class="line line-1"></span>
                                </li>
                                <li class="dot-lg">
                                    <span class="line line-1"></span>
                                </li>
                                <li class="dot-lg">
                                    <span class="line line-1"></span>
                                    <span class="line line-2"></span>
                                </li>
                                <li class="dot-lg">
                                    <span class="line line-1"></span>
                                </li>
                                <li></li>
                                <li class="dot-lg">
                                    <span class="line line-1"></span>
                                    <span class="line line-2"></span>
                                    <span class="line line-3"></span>
                                    <span class="line line-4"></span>
                                    <span class="line line-5"></span>
                                    <span class="line line-6"></span>
                                </li>
                                <li></li>
                                <li></li>
                                <li class="dot-lg">
                                    <span class="line line-1"></span>
                                </li>
                                <li>
                                    <span class="line line-1"></span>
                                </li>
                                <li class="dot-lg">
                                    <span class="line line-1"></span>
                                </li>
                                <li>
                                    <span class="line line-1"></span>
                                </li>
                                <li>
                                    <span class="line line-1"></span>
                                </li>
                                <li class="dot-lg">
                                    <span class="line line-1"></span>
                                </li>
                                <li class="dot-lg">
                                    <span class="line line-1"></span>
                                    <span class="line line-2"></span>
                                    <span class="line line-3"></span>
                                </li>
                                <li class="dot-lg">
                                    <span class="line line-1"></span>
                                </li>
                                <li class="dot-lg">
                                    <span class="line line-1"></span>
                                </li>
                                <li class="dot-lg">
                                    <span class="line line-1"></span>
                                </li>
                                <li>
                                    <span class="line line-1"></span>
                                </li>
                                <li>
                                    <span class="line line-1"></span>
                                </li>
                                <li>
                                    <span class="line line-1"></span>
                                </li>
                                <li class="dot-lg">
                                    <span class="line line-1"></span>
                                </li>
                                <li>
                                    <span class="line line-1"></span>
                                </li>
                                <li>
                                    <span class="line line-1"></span>
                                    <span class="line line-2"></span>
                                </li>
                                <li>
                                    <span class="line line-1"></span>
                                </li>
                                <li>
                                    <span class="line line-1"></span>
                                </li>
                                <li class="dot-lg">
                                    <span class="line line-1"></span>
                                    <span class="line line-2"></span>
                                </li>
                                <li>
                                    <span class="line line-1"></span>
                                </li>
                                <li class="dot-lg">
                                    <span class="line line-1"></span>
                                </li>
                                <li class="dot-lg"></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="default-section bg-grey">
        <div class="container">
            <div class="section-title">
                <?= the_field('text_before_products') ?>
            </div>
            <!--<div class="feature-card-holder">
			<div class="featured-card-list five-cols">
				<?php $how_products = acf_get_fields_by_id(4652);
            foreach ($how_products as $item) {
                if ($item['name'] != 'text_before_products' && $item['name'] != 'graphic_element_after_products' && $item['name'] != 'text_after_products') {
                    $the_item = get_field($item['name']); ?>

				<div class="featured-card" style="" aria-hidden="true" tabindex="0">
					<div class="main-block">
						<img loading="lazy" src="<?= get_the_post_thumbnail_url($the_item->ID, 'medium') ?>">
						<span><?= $the_item->post_title ?></span>
					</div>
					<div class="info-block">
						<p><?= $the_item->post_excerpt ?></p>
						<div class="text-center">
							<a href="<?= get_permalink($the_item->ID) ?>" tabindex="-1">Read more <i class="fas fa-angle-right"></i>
							</a></div>
					</div>
				</div>

		<?php }
            } ?>

			</div>
		</div>-->

            <div class="diagram-holder animation-box">
                <div class="diagram-block diagram-block-1">
                    <div class="acc-link-holder animate-elem elem-2">
                        <a href="<?= get_field('diagram_left_image_link', 1661) ?>" class="acc-link">
                            <img loading="lazy" src="<?= get_field('diagram_left_image', 1661)['sizes']['medium'] ?>"
                                 alt="city">
                            <span><?= get_field('diagram_left_image_title', 1661) ?></span>
                        </a>
                    </div>
                    <div class="note-block note-block-top animate-elem elem-6"><?= get_field('diagram_left_top_arrow', 1661) ?></div>
                    <div class="note-block note-block-bottom animate-elem elem-9"><?= get_field('diagram_left_bottom_arrow', 1661) ?></div>
                </div>
                <div class="diagram-block diagram-block-2">
                    <ul class="diagram-link-list animate-elem elem-3">
                        <li>
                            <a href="<?= get_field('diagram_top_middle_1_link', 1661) ?>" class="link-with-icon">
                                <img loading="lazy"
                                     src="<?= get_field('diagram_top_middle_1_image', 1661)['sizes']['medium'] ?>">
                                <span><?= get_field('diagram_top_middle_1_title', 1661) ?></span>
                            </a>
                        </li>
                        <li>
                            <a href="<?= get_field('diagram_top_middle_2_link', 1661) ?>" class="link-with-icon">
                                <img loading="lazy"
                                     src="<?= get_field('diagram_top_middle_2_image', 1661)['sizes']['medium'] ?>">
                                <span><?= get_field('diagram_top_middle_2_title', 1661) ?></span>
                            </a>
                        </li>
                    </ul>
                    <span class="logo-block animate-elem elem-1">
					<img loading="lazy" src="<?= get_field('diagram_middle_image', 1661)['sizes']['medium'] ?>"
                         alt="Inflo">
				</span>
                    <ul class="diagram-link-list animate-elem elem-5">
                        <li>
                            <a href="<?= get_field('diagram_bottom_middle_link', 1661) ?>" class="link-with-icon">
                                <img loading="lazy"
                                     src="<?= get_field('diagram_bottom_middle_image', 1661)['sizes']['medium'] ?>">
                                <span><?= get_field('diagram_bottom_middle_title', 1661) ?></span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="diagram-block diagram-block-3">
                    <div class="acc-link-holder">
                        <a href="<?= get_field('diagram_right_image_link', 1661) ?>" class="acc-link">
						<span class="animate-elem elem-10">
							<img loading="lazy" src="<?= get_field('diagram_right_image', 1661)['sizes']['medium'] ?>"
                                 alt="collaboration">
							<span><?= get_field('diagram_right_image_title', 1661) ?></span>
						</span>
                        </a>
                        <a href="<?= get_field('diagram_right_image_secondary_link', 1661) ?>" class="inflo-hi-label">
						<span class="animate-elem elem-4">
							<img loading="lazy"
                                 src="<?= get_field('diagram_right_image_secondary', 1661)['sizes']['medium'] ?>"
                                 alt="Hi">
							<span class="text"><?= get_field('diagram_right_image_secondary_title', 1661) ?></span>
						</span>
                        </a>
                    </div>
                    <div class="note-block note-block-top animate-elem elem-7"><?= the_field('diagram_right_top_arrow', 1661) ?></div>
                    <div class="note-block note-block-bottom animate-elem elem-8"><?= the_field('diagram_right_bottom_arrow', 1661) ?></div>
                </div>
                <div class="diagram-block diagram-block-4">
                    <div class="diagram-circle-block animate-elem elem-11">
                        <ul class="diagram-img-circle">
                            <li><img loading="lazy"
                                     src="<?= get_field('diagram_circle_icon_1', 1661)['sizes']['medium'] ?>"></li>
                            <li><img loading="lazy"
                                     src="<?= get_field('diagram_circle_icon_2', 1661)['sizes']['medium'] ?>"></li>
                            <li><img loading="lazy"
                                     src="<?= get_field('diagram_circle_icon_3', 1661)['sizes']['medium'] ?>"></li>
                            <li><img loading="lazy"
                                     src="<?= get_field('diagram_circle_icon_4', 1661)['sizes']['medium'] ?>"></li>
                            <li><img loading="lazy"
                                     src="<?= get_field('diagram_circle_icon_5', 1661)['sizes']['medium'] ?>"></li>
                            <li><img loading="lazy"
                                     src="<?= get_field('diagram_circle_icon_6', 1661)['sizes']['medium'] ?>"></li>
                        </ul>
                        <a href="<?= get_field('diagram_circle_top_link', 1661) ?>"
                           class="link-with-icon diagram-circle-link">
                            <span><?= get_field('diagram_circle_top_title', 1661) ?></span>
                        </a>
                        <a href="<?= get_field('diagram_circle_bottom_link', 1661) ?>" class="link-with-icon">
                            <img loading="lazy"
                                 src="<?= get_field('diagram_circle_bottom_image', 1661)['sizes']['medium'] ?>">
                            <span><?= get_field('diagram_circle_bottom_title', 1661) ?></span>
                        </a>
                    </div>
                    <div class="note-block note-block-top animate-elem elem-12"><?= the_field('diagram_circle_text_above', 1661) ?></div>
                </div>
            </div>

            <h4 class="text-center"><?= get_field('inflo_different_title') ?></h4>
            <div class="two-col-holder text-img-row">
                <div class="col">
                    <?= get_field('inflo_different_text_1') ?>
                </div>
                <div class="col">
                    <img loading="lazy" src="<?= get_field('inflo_different_image_1')['sizes']['medium_large'] ?>"
                         class="animate-right-to-left">
                </div>
            </div>
            <div class="two-col-holder text-img-row">
                <div class="col">
                    <img loading="lazy" src="<?= get_field('inflo_different_image_2')['sizes']['medium_large'] ?>"
                         class="animate-left-to-right">
                </div>
                <div class="col">
                    <?= get_field('inflo_different_text_2') ?>
                </div>
            </div>

            <div class="default-section">
                <div class="container container-sm">
                    <?= get_field('world_class_security_text') ?>
                </div>
            </div>
        </div>
    </section>
    <section class="member-section bg-dark-grey border-top-light border-bottom-light">
        <div class="container">
            <div class="member-holder">
                <h2 class="h3">Leadership team</h2>
                <div class="member-list">

                    <?php $leadership_members = acf_get_fields_by_id(1500);
                    if ($leadership_members) {
                        foreach ($leadership_members as $member) {
                            $the_member = get_field($member['name']); ?>

                            <div class="member-item">
                                <a href="#<?= $member['name'] ?>_popup" class="member-inner popup-opener">
                                    <div class="member-img"
                                         style="background-image: url(<?= $the_member['leadership_team_member_photo']['sizes']['medium'] ?>);"></div>
                                    <div class="member-info">
                                        <h4 class="member-name"><?= $the_member['leadership_team_member_name'] ?></h4>
                                        <span class="member-position"><?= $the_member['leadership_team_member_position'] ?></span>
                                        <span class="bio-link">Read BIO</span>
                                    </div>
                                </a>
                            </div>

                            <div class="custom-popup" id="<?= $member['name'] ?>_popup">
                                <div class="popup-inner">
                                    <a href="#" class="close close-popup"></a>
                                    <div class="member-popup">
                                        <div class="memebr-top-block">
                                            <div class="member-img"
                                                 style="background-image: url(<?= $the_member['leadership_team_member_photo']['sizes']['medium'] ?>);"></div>
                                            <div class="member-title-info">
                                                <h3 class="member-name"><?= $the_member['leadership_team_member_name'] ?></h3>
                                                <span class="member-position"><?= $the_member['leadership_team_member_position'] ?></span>
                                                <ul class="network-list">
                                                    <?php if ($the_member['leadership_team_member_linkedin'] != null && $the_member['leadership_team_member_linkedin'] != '') { ?>
                                                        <li>
                                                            <a href="<?= $the_member['leadership_team_member_linkedin'] ?>">
                                                                <i class="fab fa-linkedin-in"></i>
                                                            </a>
                                                        </li>
                                                    <?php } else { ?>
                                                        <li style="margin-right:0">
                                                            <a style="visibility: hidden; width: 1px; height: 26px;"
                                                               href="#">

                                                            </a>
                                                        </li>
                                                    <?php } ?>
                                                    <?php if ($the_member['leadership_team_member_email_address'] != null && $the_member['leadership_team_member_email_address'] != '') { ?>
                                                        <li>
                                                            <a href="mailto:<?= $the_member['leadership_team_member_email_address'] ?>">
                                                                <i class="far fa-envelope"></i>
                                                            </a>
                                                        </li>
                                                    <?php } else { ?>
                                                        <li style="margin-right:0">
                                                            <a style="visibility: hidden; width: 1px; height: 26px;"
                                                               href="#">

                                                            </a>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="member-info-block">
                                            <?= $the_member['leadership_team_member_bio'] ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }
                    } ?>
                </div>
            </div>
        </div>
    </section>
    <section class="career-preview-section bg-grey border-bottom-white">
        <div class="container">
            <div class="section-title text-center">
                <h2 class="h4"><?= get_field('about_career_section_heading') ?></h2>
            </div>
            <div class="career-block-holder">
                <div class="testimonial-item career-block">
                    <div class="img-block bg-orange img-holder">
                        <div class="in-holder">
                            <div class="in-slider">
                                <div>
                                    <div class="in-slide"
                                         style="background-image: url('<?= get_field('about_career_image')['sizes']['medium_large'] ?>');"></div>
                                </div>
                                <div>
                                    <div class="in-slide"
                                         style="background-image: url('<?= get_field('about_career_image_2')['sizes']['medium_large'] ?>');"></div>
                                </div>
                                <div>
                                    <div class="in-slide"
                                         style="background-image: url('<?= get_field('about_career_image_3')['sizes']['medium_large'] ?>');"></div>
                                </div>
                            </div>

                            <div class="in-slider in-slider-over">
                                <div>
                                    <div class="in-slide"
                                         style="background-image: url('<?= get_field('about_career_image')['sizes']['medium_large'] ?>');"></div>
                                </div>
                                <div>
                                    <div class="in-slide"
                                         style="background-image: url('<?= get_field('about_career_image_2')['sizes']['medium_large'] ?>');"></div>
                                </div>
                                <div>
                                    <div class="in-slide"
                                         style="background-image: url('<?= get_field('about_career_image_3')['sizes']['medium_large'] ?>');"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="text-block">
                        <h3><?= get_field('about_career_heading') ?></h3>
                        <?= get_field('about_career_description') ?>
                        <div class="secondary-text-holder">
                            <p><?= get_field('about_career_gray_text') ?></p>
                        </div>
                        <?php if (get_field('about_career_cta_text') != null && get_field('about_career_cta_text') != '') { ?>
                            <a href="<?= get_field('about_career_cta_destination') ?>"
                               class="btn"><?= get_field('about_career_cta_text') ?></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section
            class="partners-section <?php if (get_field('testimonial_from_use_case') != null && get_field('testimonial_from_use_case') != '') { ?>border-bottom-decor-section border-bottom-light<?php } ?>">
        <div class="container">
            <h2 class="h4"><?= get_field("partners_text", 1312) ?></h2>
            <ul class="partners-list">
                <?php $partner_icons = acf_get_fields_by_id(1308);
                foreach ($partner_icons as $icon) {
                    if ($icon['name'] != 'partners_text') { ?>
                        <?php $the_image = get_field($icon['name'], 1312);
                        $partner_link = $the_image['partner_link'];
                        if (trim($partner_link) == '') {
                            $partner_link = '/' . ICL_LANGUAGE_CODE . '/customer-success';
                        }
                        ?>
                        <li>
                            <a href="<?= $partner_link ?>">
                                <img loading="lazy" src="<?= $the_image['partner_image']['sizes']['medium'] ?>">
                            </a>
                        </li>
                    <?php }
                } ?>
            </ul>
        </div>
        <?php if (get_field('testimonial_from_use_case') != null && get_field('testimonial_from_use_case') != '') { ?>
            <span class="border-decor"></span>
        <?php } ?>
    </section>
<?php if (get_field('testimonial_from_use_case') != null && get_field('testimonial_from_use_case') != '') { ?>
    <section class="single-testimonial-section bg-grey">
        <?php $use_case_id = get_field('testimonial_from_use_case'); ?>
        <div class="container container-sm">
            <div class="single-testimonial-block">
                <?php if (get_field('simple_rich_text_section_show') == 'Yes') { ?>

                    <?= get_field('simple_rich_text_section_content') ?>

                <?php } ?>
                <div class="testimonial-item">
                    <div class="text-block">
                        <div class="author-block">
                            <div class="author-name-holder">
                                <div class="author-img"
                                     style="background-image: url('<?= get_field('testimonial_avatar', $use_case_id)['sizes']['thumbnail'] ?>');"></div>
                                <div class="author-descr">
                                    <h5 class="author-name"><?= get_field('testimonial_name', $use_case_id) ?></h5>
                                    <p><?= get_field('testimonial_job_title', $use_case_id) ?></p>
                                </div>
                            </div>
                            <img loading="lazy" src="https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/icon-50.png" class="quote-img">
                            <p><?= get_field('testimonial_quote', $use_case_id) ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
<?php } ?>
<?php get_footer(); ?>
