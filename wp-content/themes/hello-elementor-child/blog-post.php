<?php /* Template Name: Inflo Blog Post
Template Post Type: post */ ?>

<?php
get_header();
global $post;
$post_cat = get_the_category($post->ID)[0];
$cat_link_unfiltered = get_category_link($post_cat->term_id);
$cat_link = $cat_link_unfiltered; //str_replace('/blog', '', $cat_link_unfiltered);

$post_tag = get_the_tags($post->ID)[0];
$tag_link = get_tag_link($post_tag->term_id);

$this_post_id = $post->ID;

?>

<section class="detail-intro-section border-bottom-white bg-grey">
	<div class="container">
		<div class="breadcrumbs-holder">
			<ul class="breadcrumbs-list">
				<li><a href="<?php echo esc_url(home_url()); ?>">Home</a></li>
				<li><a href="<?= $cat_link ?>"><?= $post_cat->name ?></a></li>
				<li><?= $post->post_title ?></li>
			</ul>
		</div>
		<div class="detail-intro-row">
			<div class="col img-col" style="background-image: url(<?= get_the_post_thumbnail_url($post->ID, 'medium_large') ?>);"></div>
			<div class="col text-col">
				<h1 class="h2"><?= $post->post_title ?></h1>
				<div class="blog-detail-info">
					<strong class="post-author"><?= get_the_author_meta('display_name', $post->post_author); ?></strong>
					<?php $post_date_no_time = explode(' ', $post->post_date_gmt)[0];
					$post_formatted_date_arr = explode('-', $post_date_no_time);
					$dateObj = DateTime::createFromFormat('!m', $post_formatted_date_arr[1]);
					$month = $dateObj->format('F');
					$post_formatted_date = $post_formatted_date_arr[2] . ' ' . $month . ' ' . $post_formatted_date_arr[0]; ?>
					<span class="post-date">Posted <time datetime="<?= $post_date_no_time ?>"><?= $post_formatted_date ?></time></span>
					<a href="<?= $cat_link ?>" class="post-category"><?= $post_cat->name ?></a>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="article-content-section">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-md-10">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</section>

<?php // if (get_field('display_bottom_cta') == "Yes" || get_field('enable_read_more') || get_field('display_newsletter')) : ?>
	<section class="more-studies-section bg-light border-top-white">
		<div class="container">
			<?php include 'template-parts/blocks/more-to-read.php'; ?>
			<div class="cta-testimonial-holder">
				<?php include 'template-parts/blocks/sign-up-to-newsletter.php'; ?>
				<?php if (get_field('display_bottom_cta') != 'No') { ?>
					<div class="cta-block">
						<p><?= get_field('bottom_cta_description') ?></p>
						<div class="btn-center-holder">
							<a href="<?= get_field('bottom_cta_destination') ?>" class="btn"><?= get_field('bottom_cta_text') ?></a>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</section>
<?php // endif; ?>

<?php get_footer(); ?>