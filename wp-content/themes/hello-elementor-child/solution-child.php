<?php /* Template Name: Inflo Solution */ ?>

<?php get_header(); ?>

    <section class="intro-section secondary-page bg-anim border-bottom-white">
        <div class="container">
            <div class="text-block full-width">
                <h1 class="h2"><?= get_field('solution_title') ?></h1>
                <p class="intro-note"><?= get_field('solution_subtitle') ?></p>
            </div>
            <div class="solutions-features-card-holder">
                <?php
                $solution_top_blocks = acf_get_fields_by_id(2100);
                foreach ($solution_top_blocks as $item) {
                    $the_item = get_field($item['name']);
                    if ($the_item != null && $the_item != '' && $the_item['solution_top_block_title'] != '') {
                        ?>
                        <div class="solutions-features-card">
                            <h3 class="card-title"><?= $the_item['solution_top_block_title'] ?></h3>
                            <?= $the_item['solution_top_block_text'] ?>
                        </div>
                    <?php }
                } ?>

            </div>
        </div>
    </section>

    <section class="default-section">
        <div class="container">
            <div class="two-third-col-holder">
                <div class="col-sm">
                    <?= get_field('top_paragraph_left') ?>
                </div>
                <div class="col-lg">
                    <?= get_field('top_paragraph_right') ?>
                </div>
            </div>
        </div>
    </section>
    <section class="use-case-preview-section anim-bg-line bg-grey border-top-white">
        <div class="container">
            <div class="use-case-preview-section-inner">
                <div class="text-block">
                    <?= get_field('wave_paragraph') ?>
                </div>
                <?php $first_use_case = get_field('wave_use_case_select');
                //$first_use_case_tag = get_the_tags( $first_use_case->ID )[0];
                //$first_use_case_tag_link = get_tag_link($first_use_case_tag->term_id);
                $first_use_case_tags = get_the_terms($first_use_case->ID, 'product_tax'); ?>
                <div class="use-case-block">
                    <div class="use-case-preview-sm">
                        <div class="descr-block">
                            <div class="title-holder">
                                <h4 class="title"><a
                                            href="<?= get_permalink($first_use_case->ID) ?>"><?= $first_use_case->post_title ?></a>
                                </h4>
                            </div>
                            <ul class="topic-list">
                                <?php foreach ($first_use_case_tags as $post_tag) {
                                    $tag_link = get_term_link($post_tag->term_id, 'product_tax');//get_tag_link($post_tag->term_id);
                                    ?>
                                    <li>
                                        <a href="<?= $tag_link ?>">
                                            <img loading="lazy"
                                                 src="<?= get_field('image_for_this_taxonomy', $post_tag)['sizes']['thumbnail'] ?>"
                                                 class="icon"> <?= $post_tag->name ?>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                            <a href="<?= get_permalink($first_use_case->ID) ?>" class="btn btn-outline">Read story</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="anim-line"></div>
    </section>

<?php if (get_field('video_link') != null && get_field('video_link') != '') { ?>
    <section class="border-top-light">
        <div class="container">
            <div class="video-popup-holder">
                <?php if (get_field('title_above') != null && get_field('title_above') != '') { ?>
                    <h2><?= get_field('title_above') ?></h2>
                <?php } ?>

                <?php if (get_field('cover_image') != null && get_field('cover_image') != '') { ?>
                    <a data-fancybox href="<?= get_field('video_link') ?>" class="video-popup-opener">
                        <img loading="lazy" src="<?= get_field('cover_image')['sizes']['medium_large'] ?>">
                    </a>
                <?php } else { ?>

                    <?php
                    $additional_str_start = '?';
                    if (strrpos(get_field('video_link'), '?') !== false) {
                        $additional_str_start = '&';
                    }
                    ?>

                    <a data-fancybox href="<?= get_field('video_link') ?>" class="video-popup-opener iframe-holder">
                        <iframe loading="lazy"
                                src="<?= the_field('video_link') . $additional_str_start ?>title=0&byline=0&portrait=0&sidedock=0&controls=0"
                                width="100%" height="auto" frameborder="0"></iframe>
                    </a>

                <?php } ?>

            </div>
            <div class="video-note-row">
                <div class="video-note">
                    <h5><?= get_field('title_under') ?></h5>
                    <p><?= get_field('text_under') ?></p>
                </div>
                <?php if (get_field('cta_tel_text') != null && get_field('cta_tel_text') != '') { ?>
                    <div class="video-note-btn">
                        <a href="<?= get_field('cta_tel') ?>" class="btn"><?= get_field('cta_tel_text') ?></a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
<?php } ?>

    <section
            class="bg-img-section border-bottom-white <?php if (get_field('video_link') == null || get_field('video_link') == '') {
                echo 'border-top-light';
            } else {
                echo 'border-top-white';
            } ?>">
        <div class="container">
            <div class="text-block">
                <?= get_field('image_section_text') ?>
                <?php if (get_field('image_section_cta_text') != null && get_field('image_section_cta_text') != '') { ?>
                    <a href="<?= get_field('image_section_cta_destination') ?>"
                       class="btn"><?= get_field('image_section_cta_text') ?></a>
                <?php } ?>
            </div>
        </div>
        <div class="bg-img"
             style="background-image: url(<?= get_field('image_of_this_section')['sizes']['medium_large'] ?>);"></div>
    </section>
    <section class="default-section">
        <div class="container">
            <div class="colored-title-block">
                <p><?= get_field('color_blocks_title') ?></p>
            </div>
            <div class="colored-blocks-holder">
                <div class="colored-block">
                    <p><?= get_field('color_block_left') ?></p>
                </div>
                <div class="colored-block">
                    <p><?= get_field('color_block_middle') ?></p>
                </div>
                <div class="colored-block">
                    <p><?= get_field('color_block_right') ?></p>
                </div>
            </div>
        </div>
    </section>
    <section class="use-case-preview-section bg-grey border-top-white border-bottom-white">
        <div class="container">
            <div class="use-case-preview-section-inner">
                <?php $second_use_case = get_field('bottom_block_use_case_select');
                //$second_use_case_tag = get_the_tags( $second_use_case->ID )[0];
                //$second_use_case_tag_link = get_tag_link($second_use_case_tag->term_id);
                $second_use_case_tags = get_the_terms($second_use_case->ID, 'product_tax'); ?>
                <div class="use-case-block">
                    <div class="use-case-preview-sm">
                        <div class="descr-block">
                            <div class="title-holder">
                                <h4 class="title"><a
                                            href="<?= get_permalink($second_use_case->ID) ?>"><?= $second_use_case->post_title ?></a>
                                </h4>
                            </div>
                            <?php if ($second_use_case_tags) { ?>
                                <ul class="topic-list">
                                    <?php foreach ($second_use_case_tags as $post_tag) {
                                        $tag_link = get_term_link($post_tag->term_id, 'product_tax');//get_tag_link($post_tag->term_id);
                                        ?>
                                        <li>
                                            <a href="<?= $tag_link ?>">
                                                <img loading="lazy"
                                                     src="<?= get_field('image_for_this_taxonomy', $post_tag)['sizes']['thumbnail'] ?>"
                                                     class="icon"> <?= $post_tag->name ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                            <a href="<?= get_permalink($second_use_case->ID) ?>" class="btn btn-outline">Read story</a>
                        </div>
                    </div>
                </div>
                <div class="text-block">
                    <?= get_field('bottom_block_text_paragraph') ?>
                    <?php if (get_field('bottom_block_cta_text') != null && get_field('bottom_block_cta_text') != '') { ?>
                        <a href="<?= get_field('bottom_block_cta_destination') ?>"
                           class="btn"><?= get_field('bottom_block_cta_text') ?></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
<?php if (get_field('display_bottom_cta') != 'No') { ?>
    <section class="default-section">
        <div class="container">
            <div class="cta-block">
                <p><?= get_field('bottom_cta_description') ?></p>
                <div class="btn-center-holder">
                    <a href="<?= get_field('bottom_cta_destination') ?>"
                       class="btn"><?= get_field('bottom_cta_text') ?></a>
                </div>
            </div>
        </div>
    </section>
<?php } ?>

<?php get_footer(); ?>
