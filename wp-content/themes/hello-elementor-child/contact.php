<?php /* Template Name: Inflo Contact */ ?>

<?php get_header(); ?>

<section class="contact-us-section bg-skew-line bg-light border-bottom-white border-bottom-decor-section">
	<div class="container">
		<div class="section-title">
			<h1 class="h2"><?= get_field('contact_page_title') ?></h1>
		</div>
		<div class="two-col-holder contact-columns">
			<div class="col">
				<div class="white-form-holder">
					<div class="contact-form-holder">
						<!--[if lte IE 8]>
						<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
						<![endif]-->
						<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
						<script>
						  hbspt.forms.create({
							portalId: "7936749",
							formId: "e7dfc505-20dd-45d7-8715-12bb269ec9e1"
						});
						</script>
					</div>
				</div>
			</div>
			<div class="col first-col">
				<div class="contact-support-block text-center">
					<?= get_field('orange_block_content') ?>
				</div>
			</div>
			<div class="col">
				<div class="contact-accordion-holder">
					<div class="text-center">
						<h4><?= get_field('contact_locations_title') ?></h4>
					</div>
					<div class="faq-accordion accordion">

						<?php $contact_locations = acf_get_fields_by_id(1563); 
						foreach($contact_locations as $location) { if($location['name'] != 'contact_locations_title') { 
						$the_location = get_field($location['name']); ?>

						<div class="accordion-item <?php if($the_location['is_current_location']) {echo 'active-item';} ?>">
							<h4 class="accordion-item-title">
								<a href="#" class="item-opener"><?= $the_location['location_title'] ?></a>
							</h4>
							<div class="accordion-item-slide">
								<div class="accordion-item-slide-content">
									<a href="<?= $the_location['location_map'] ?>" target="_blank" class="location-block">
										<div class="img-holder" style="background-image: url(<?= $the_location['location_image']['sizes']['medium'] ?>);">
											<!-- <img src="images/img-32.png" alt="city"> -->
										</div>
										<div class="address-holder">
											<strong class="location-title"><?= $the_location['location_title'] ?></strong>
											<address><?= $the_location['location_address'] ?></address>
										</div>
									</a>
								</div>
							</div>
						</div>

						<?php }} ?>
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<span class="border-decor"></span>
</section>

<section class="faq-section">
	<div class="container">
		<h3 class="h4"><?= get_field('faq_section_title') ?></h3>
		<div class="faq-accordion accordion">

			<?php $contact_faq = acf_get_fields_by_id(1582); 
			foreach($contact_faq as $qa) { if($qa['name'] != 'faq_section_title') { 
			$the_qa = get_field($qa['name']); ?>

			<?php if($the_qa['question'] != '') { ?>
			<div class="accordion-item">
				<h4 class="accordion-item-title">
					<a href="#" class="item-opener"><?= $the_qa['question'] ?></a>
				</h4>
				<div class="accordion-item-slide">
					<div class="accordion-item-slide-content">
						<p><?= $the_qa['answer'] ?></p>
					</div>
				</div>
			</div>
			<?php }}} ?>
		</div>
	</div>
	<?php if(get_field('display_bottom_cta') != 'No'){ ?>
	<div class="container">
		<div class="cta-block">
			<p><?= get_field('bottom_cta_description') ?></p>
			<div class="btn-center-holder">
				<a href="<?= get_field('bottom_cta_destination') ?>" class="btn"><?= get_field('bottom_cta_text') ?></a>
			</div>
		</div>
	</div>
	<?php } ?>
</section>

<div class="custom-popup" id="support_popup">
	<div class="popup-inner">
		<a href="#" class="close close-popup"></a>
		<div class="_white-form-holder digital-audit-form-holder">
			<h4 class="text-center">Support</h4>
			<!--[if lte IE 8]>
			<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
			<![endif]-->
			<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
			<script>
			  hbspt.forms.create({
				portalId: "7936749",
				formId: "0da2c44c-2ec0-4ec4-8f10-8b681a5a546f"
			});
			</script>
			</script>
		</div>
	</div>
</div>

<?php get_footer(); ?>
