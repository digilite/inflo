<?php /**
 * The template for displaying archive pages
 * Template Name: Inflo Product Tag */ ?>

<?php get_header(); ?>

<?php
$use_cases_page = 1822;
$chosen_post = get_field('use_case_listing_chosen_post', $use_cases_page);
//$post_tag = get_the_tags( $chosen_post->ID )[0];
//$tag_link = get_tag_link($post_tag->term_id);
$post_tags = get_the_terms($chosen_post->ID, 'product_tax');
$this_tag_id = get_queried_object()->term_id;
?>
    <section class="detail-intro-section bg-light border-bottom-light">
        <div class="container">
            <div class="detail-intro-row reverse">
                <div class="col text-col">
                    <h1 class="h2"><?= $chosen_post->post_title ?></h1>
                    <ul class="topic-list">
                        <?php foreach ($post_tags as $post_tag) {
                            $tag_link = get_term_link($post_tag->term_id, 'product_tax');
                            ?>
                            <li>
                                <a href="<?= $tag_link ?>">
                                    <img loading="lazy"
                                         src="<?= get_field('image_for_this_taxonomy', $post_tag)['sizes']['thumbnail'] ?>"
                                         class="icon"> <?= $post_tag->name ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                    <a href="<?= get_permalink($chosen_post->ID) ?>" class="btn">Read this story</a>
                </div>
                <div class="col img-col"
                     style="background-image: url(<?= get_the_post_thumbnail_url($chosen_post->ID, 'medium') ?>);"></div>
            </div>
        </div>
    </section>

    <section class="default-section bg-grey">
        <div class="container">
            <div class="post-slider post-light">

                <?php


                //$got_posts = get_posts( ['numberposts' => 12, 'tag' => sanitize_title( $wp->request )] );
                $got_posts = get_posts(['numberposts' => 12, 'post_type' => 'use_cases',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'product_tax',
                            'field' => 'term_id',
                            'terms' => $this_tag_id,
                        )
                    ),]);


                foreach ($got_posts as $post) : setup_postdata($post);

                    ?>

                    <div class="post-slide">
                        <div class="use-case-preview-sm">
                            <div class="descr-block">
                                <div class="title-holder">
                                    <h4 class="title"><a
                                                href="<?= get_permalink($post->ID) ?>"><?= $post->post_title ?></a></h4>
                                </div>
                                <ul class="topic-list">
                                    <?php
                                    $this_post_tags = get_the_terms($post->ID, 'product_tax');
                                    foreach ($this_post_tags as $this_post_tag) {
                                        $this_tag_link = get_term_link($this_post_tag->term_id, 'product_tax');
                                        ?>
                                        <li>
                                            <?php //$this_post_tag = get_the_tags( $post->ID )[0];
                                            //$this_tag_link = get_tag_link($this_post_tag->term_id);
                                            ?>
                                            <a href="<?= $this_tag_link ?>">
                                                <img loading="lazy"
                                                     src="<?= get_field('image_for_this_taxonomy', $this_post_tag)['sizes']['thumbnail'] ?>"
                                                     class="icon"> <?= $this_post_tag->name ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                                <a href="<?= get_permalink($post->ID) ?>" class="btn btn-outline">Read story</a>
                            </div>
                        </div>
                    </div>

                <?php endforeach;
                wp_reset_postdata(); ?>
            </div>
            <?php if (get_field('display_bottom_cta', $use_cases_page) != 'No') { ?>
                <div class="cta-block">
                    <p><?= get_field('bottom_cta_description', $use_cases_page) ?></p>
                    <div class="btn-center-holder">
                        <a href="<?= get_field('bottom_cta_destination', $use_cases_page) ?>"
                           class="btn"><?= get_field('bottom_cta_text', $use_cases_page) ?></a>
                    </div>
                </div>
            <?php } ?>
        </div>
    </section>

<?php get_footer(); ?>
