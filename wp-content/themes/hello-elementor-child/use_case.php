<?php /* Template Name: Inflo Use Case
Template Post Type: post, use_cases */ ?>

<?php get_header();
global $post;
//$post_cat = get_the_category( $post->ID )[0];
//$cat_link = get_category_link( $post_cat->term_id );
$post_tags = get_the_terms($post->ID, 'product_tax');//get_the_tags( $post->ID )[0];
//$tag_link = get_tag_link($post_tag->term_id);

$this_post_id = $post->ID;
?>

<?php

$first_section_class = "border-bottom-light-secondary";
$third_section_class_one = "border-bottom-decor-section";
$third_section_class_two = "border-bottom-light";
$fifth_section_class = "border-top-light";
$sixth_section_class = "border-top-white";

$stat_cols = acf_get_fields_by_id(1735);
if (get_field($stat_cols[0]['name'])['stat_title'] == null || get_field($stat_cols[0]['name'])['stat_title'] == '') {
    $first_section_class = 'border-bottom-white';
}

if (get_field('testimonial_name') == null || get_field('testimonial_name') == '') {
    $third_section_class_one = '';
    $third_section_class_two = '';
    $fifth_section_class = "border-top-white";
}

if ((get_field('text_area_after_testimonial') == null && get_field('text_area_after_testimonial') == '') && (get_field('testimonial_name') != null && get_field('testimonial_name') != '')) {
    $sixth_section_class = "border-top-light";
}

?>


    <section class="detail-intro-section bg-grey <?= $first_section_class ?>">
        <div class="container">
            <div class="breadcrumbs-holder">
                <ul class="breadcrumbs-list" style="display: none !important;">
                    <li><a href="<?php echo esc_url(home_url()); ?>">Home</a></li>
                    <li><a href="<?= esc_url(home_url('customer-success')) ?>">Customer-Success</a></li>
                    <li><?= $post->post_title ?></li>
                </ul>
            </div>
            <div class="detail-intro-row reverse">
                <div class="col text-col">
                    <h1 class="h2"><?= $post->post_title ?></h1>
                    <?php if ($post_tags) { ?>
                        <ul class="topic-list">
                            <?php foreach ($post_tags as $post_tag) {
                                $tag_link = get_term_link($post_tag->term_id, 'product_tax');//get_tag_link($post_tag->term_id);
                                ?>
                                <li>
                                    <a href="<?= $tag_link ?>">
                                        <img loading="lazy"
                                             src="<?= get_field('image_for_this_taxonomy', $post_tag)['sizes']['thumbnail'] ?>"
                                             class="icon"> <?= $post_tag->name ?>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </div>
                <div class="col img-col"
                     style="background-image: url(<?= get_the_post_thumbnail_url($post->ID, 'medium') ?>);"></div>
            </div>
        </div>
    </section>

<?php if (get_field($stat_cols[0]['name'])['stat_title'] != null && get_field($stat_cols[0]['name'])['stat_title'] != '') { ?>
    <section class="section-collapse bg-light border-bottom-white">
        <div class="container">
            <div class="balls-set">

                <?php
                foreach ($stat_cols as $col) {
                    $the_col = get_field($col['name']); ?>
                    <div class="ball bg-anim">
                        <div class="title-price-block">
                            <strong class="price-title"><?= $the_col['stat_title'] ?></strong>
                            <span class="price-subtitle"><?= $the_col['stat_subtitle'] ?></span>
                        </div>
                    </div>
                <?php } ?>

            </div>
        </div>
    </section>
<?php } ?>

<?php if (get_field('testimonial_name') != null && get_field('testimonial_name') != '') { ?>
    <section class="section-collapse bg-white  <?= $third_section_class_one ?> <?= $third_section_class_two ?>">
        <div class="container">
            <?php the_content(); ?>
        </div>
        <span class="border-decor"></span>
    </section>
<?php } else { ?>
    <section class="section-collapse bg-white <?= $third_section_class_one ?>">
        <?php the_content(); ?>
        <style>
            .border-decor::before {
                top: calc(100% + 100px) !important;
            }

            .border-decor::after {
                top: calc(100% + 167px) !important;
            }
        </style>
        <span class="border-decor"></span>
    </section>
<?php } ?>

<?php if (get_field('testimonial_name') != null && get_field('testimonial_name') != '') { ?>
    <section class="single-testimonial-section bg-grey">
        <div class="container container-sm">
            <div class="single-testimonial-block">
                <div class="testimonial-item">
                    <div class="text-block">
                        <div class="author-block">
                            <div class="author-name-holder">
                                <div class="author-img"
                                     style="background-image: url('<?= get_field('testimonial_avatar')['sizes']['thumbnail'] ?>');"></div>
                                <div class="author-descr">
                                    <h5 class="author-name"><?= the_field('testimonial_name') ?></h5>
                                    <p><?= the_field('testimonial_job_title') ?></p>
                                </div>
                            </div>
                            <img loading="lazy" src="https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/icon-50.png" class="quote-img">
                            <p><?= the_field('testimonial_quote') ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>

<?php if (get_field('text_area_after_testimonial') != null && get_field('text_area_after_testimonial') != '') { ?>
    <section class="section-collapse <?= $fifth_section_class ?>">
        <div class="container container-sm">
            <?= the_field('text_area_after_testimonial') ?>
        </div>
    </section>
<?php } ?>

    <section class="more-studies-section bg-light <?= $sixth_section_class ?>">
        <div class="container">
            <h2>More CUSTOMER SUCCESS STORIES</h2>
            <div class="post-slider post-light">
                <?php if (get_field('more_case_studies_by_tag') == 'Yes') {
                    $got_posts = get_posts(['numberposts' => 3,
                        'post_type' => 'use_cases',
                        //'tag' => sanitize_title( $post_tag->name ),
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'product_tax',
                                'field' => 'term_id',
                                'terms' => get_field('by_which_tag') ? get_field('by_which_tag') : $post_tags[0]->term_id,
                            )
                        ),
                        'exclude' => [$this_post_id]]);
                    /*$got_posts = get_posts( ['numberposts' => 3,
                                            'tax_query'      => array(
                                                array(
                                                    'taxonomy'  => 'post_tag',
                                                    //'field'     => 'slug',
                                                    'terms'     => sanitize_title( $post_tag->name )
                                                )
                                            ), 'exclude' => [$this_post_id] ] ); */

                    foreach ($got_posts as $post) : setup_postdata($post);
                        if ($post->ID != $this_post_id) {
                            ?>

                            <div class="post-slide">
                                <div class="use-case-preview-sm">
                                    <div class="descr-block">
                                        <div class="title-holder">
                                            <h4 class="title"><a
                                                        href="<?= get_permalink($post->ID) ?>"><?= $post->post_title ?></a>
                                            </h4>
                                        </div>
                                        <ul class="topic-list">
                                            <?php
                                            $use_case_tags = get_the_terms($post->ID, 'product_tax');
                                            foreach ($use_case_tags as $the_tag) {
                                                $tag_link = get_term_link($the_tag->term_id, 'product_tax');
                                                ?>
                                                <li>
                                                    <a href="<?= $tag_link ?>">
                                                        <img loading="lazy"
                                                             src="<?= get_field('image_for_this_taxonomy', $the_tag)['sizes']['thumbnail'] ?>"
                                                             class="icon"> <?= $the_tag->name ?>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                        <a href="<?= get_permalink($post->ID) ?>" class="btn btn-outline">Read Story</a>
                                    </div>
                                </div>
                            </div>

                        <?php } endforeach;
                    wp_reset_postdata();
                } else {

                    for ($i = 1; $i < 4; $i++) {
                        $post = get_field('more_case_studies_post_' . $i);
                        if ($post && $post != null) {
                            $loop_post_tags = get_the_terms($post->ID, 'product_tax');//= get_the_tags( $post->ID )[0];
                            //$loop_tag_link = get_tag_link($loop_post_tag->term_id);
                            ?>

                            <div class="post-slide">
                                <div class="use-case-preview-sm">
                                    <div class="descr-block">
                                        <div class="title-holder">
                                            <h4 class="title"><a
                                                        href="<?= get_permalink($post->ID) ?>"><?= $post->post_title ?></a>
                                            </h4>
                                        </div>
                                        <ul class="topic-list">
                                            <?php
                                            foreach ($loop_post_tags as $the_tag) {
                                                $loop_tag_link = get_term_link($the_tag->term_id, 'product_tax');
                                                ?>
                                                <li>
                                                    <a href="<?= $loop_tag_link ?>">
                                                        <img loading="lazy"
                                                             src="<?= get_field('image_for_this_taxonomy', $the_tag)['sizes']['thumbnail'] ?>"
                                                             class="icon"> <?= $the_tag->name ?>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                        <a href="<?= get_permalink($post->ID) ?>" class="btn btn-outline">Read Story</a>
                                    </div>
                                </div>
                            </div>

                            <?php wp_reset_postdata();
                        }
                    }
                } ?>
            </div>
            <div class="cta-testimonial-holder">

            <?php include 'template-parts/blocks/sign-up-to-newsletter.php'; ?>

                <?php if (get_field('display_bottom_cta') != 'No') { ?>
                    <div class="cta-block">
                        <p><?= the_field('bottom_cta_description') ?></p>
                        <div class="btn-center-holder">
                            <a href="<?= the_field('bottom_cta_destination') ?>"
                               class="btn"><?= the_field('bottom_cta_text') ?></a>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>

<?php get_footer(); ?>
