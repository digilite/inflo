<?php /* Template Name: Inflo Solutions Parent */ ?>

<?php get_header(); ?>

    <section class="solutions-section bg-grey">
        <div class="container">
            <div class="section-title">
                <h1 class="h2 gradient-underline"><?= the_field('solutions_title') ?></h1>
                <p><?= the_field('solutions_subtitle') ?></p>
            </div>

            <?php
            $solution_card_1 = get_field('solution_card_1');
            $solution_card_2 = get_field('solution_card_2');
            $solution_card_3 = get_field('solution_card_3');
            ?>

            <div class="solutions-card-holder">
                <div class="solutions-card with-img">
                    <div class="solutions-card-inner">
                        <h3 class="h4"><?= $solution_card_1['solution_card_title'] ?></h3>
                        <div class="subtitle-text">
                            <p><?= $solution_card_1['solution_card_subtitle'] ?></p>
                        </div>
                        <?= $solution_card_1['solution_card_list'] ?>
                        <?php if ($solution_card_1['solution_card_cta_text'] != null && $solution_card_1['solution_card_cta_text'] != '') { ?>
                            <a href="<?= $solution_card_1['solution_card_cta_destination'] ?>"
                               class="btn card-btn"><?= $solution_card_1['solution_card_cta_text'] ?></a>
                        <?php } ?>
                    </div>
                    <div class="solutions-card-img"
                         style="background-image: url(<?= get_field('solutions_side_image')['sizes']['medium_large'] ?>"></div>
                </div>
                <div class="solutions-card">
                    <div class="solutions-card-inner">
                        <h3 class="h4"><?= $solution_card_2['solution_card_title'] ?></h3>
                        <div class="subtitle-text">
                            <p><?= $solution_card_2['solution_card_subtitle'] ?></p>
                        </div>
                        <?= $solution_card_2['solution_card_list'] ?>
                        <?php if ($solution_card_2['solution_card_cta_text'] != null && $solution_card_2['solution_card_cta_text'] != '') { ?>
                            <a href="<?= $solution_card_2['solution_card_cta_destination'] ?>"
                               class="btn card-btn"><?= $solution_card_2['solution_card_cta_text'] ?></a>
                        <?php } ?>
                    </div>
                </div>
                <div class="solutions-card">
                    <div class="solutions-card-inner">
                        <h3 class="h4"><?= $solution_card_3['solution_card_title'] ?></h3>
                        <div class="subtitle-text">
                            <p><?= $solution_card_3['solution_card_subtitle'] ?></p>
                        </div>
                        <?= $solution_card_3['solution_card_list'] ?>
                        <?php if ($solution_card_3['solution_card_cta_text'] != null && $solution_card_3['solution_card_cta_text'] != '') { ?>
                            <a href="<?= $solution_card_3['solution_card_cta_destination'] ?>"
                               class="btn card-btn"><?= $solution_card_3['solution_card_cta_text'] ?></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="info-modules-section bg-orange border-bottom-light border-top-light">
        <div class="container">
            <h2>Inflo modules</h2>
            <div class="tab-holder honeycomb-holder">
                <div class="honeycomb-wrapper">
                    <div class="honeycomb-block tab-nav">
                        <?php $modules_cards = acf_get_fields_by_id(2044);
                        if ($modules_cards) {
                            foreach ($modules_cards as $key => $item) {
                                $the_item = get_field($item['name'], 5677);
                                if ($the_item != null && $the_item != '') { ?>

                                    <a href="#tab<?= $key + 1 ?>" class="<?php if ($key == 0) {
                                        echo 'active-tab';
                                    } ?> tab-opener honeycomb-item">
                                        <div class="honeycomb-inner"
                                             style="background-image: url(https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/icon-51.png);">
                                            <div class="img-holder">
                                                <img loading="lazy"
                                                     src="<?= $the_item['cell_icon']['sizes']['medium'] ?>">
                                            </div>
                                            <span><?= $the_item['cell_title'] ?></span>
                                        </div>
                                    </a>

                                <?php }
                            }
                        } ?>
                    </div>
                </div>
                <div class="main-tab-content">
                    <?php foreach ($modules_cards as $key => $item) {
                        $the_item = get_field($item['name'], 5677);
                        if ($the_item != null && $the_item != '') { ?>
                            <div id="tab<?= $key + 1 ?>" class="tab">
                                <h3 class="h4"><?= $the_item['cell_title'] ?></h3>
                                <?= $the_item['cell_description'] ?>
                                <?php if ($the_item['cell_cta_destination'] && $the_item['cell_cta_destination'] != null && $the_item['cell_cta_destination'] != '') { ?>
                                    <div class="btn-center-holder">
                                        <a href="<?= $the_item['cell_cta_destination'] ?>"
                                           class="btn"><?= $the_item['cell_cta_text'] ?></a>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php }
                    } ?>

                </div>
            </div>
        </div>
    </section>

    <section class="cta-testimonial-section bg-grey">
        <?php $use_case_id = get_field('testimonial_from_use_case'); ?>
        <div class="container">
            <div class="cta-testimonial-holder">
                <div class="testimonial-block">
                    <div class="testimonial-item">
                        <div class="text-block">
                            <div class="author-block">
                                <div class="author-name-holder">
                                    <div class="author-img"
                                         style="background-image: url('<?= get_field('testimonial_avatar', $use_case_id)['sizes']['thumbnail'] ?>');"></div>
                                    <div class="author-descr">
                                        <h5 class="author-name"><?= the_field('testimonial_name', $use_case_id) ?></h5>
                                        <p><?= the_field('testimonial_job_title', $use_case_id) ?></p>
                                    </div>
                                </div>
                                <img loading="lazy" src="https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/icon-50.png" class="quote-img">
                                <p><?= the_field('testimonial_quote', $use_case_id) ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if (get_field('display_bottom_cta') != 'No') { ?>
                    <div class="cta-block">
                        <p><?= the_field('bottom_cta_description') ?></p>
                        <div class="btn-center-holder">
                            <a href="<?= the_field('bottom_cta_destination') ?>"
                               class="btn"><?= the_field('bottom_cta_text') ?></a>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>

<?php get_footer(); ?>
