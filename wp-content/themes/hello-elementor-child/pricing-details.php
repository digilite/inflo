<?php /* Template Name: Inflo Pricing Details */ ?>
<?php
get_header();
$this_is = 'starter';
if(strpos(strtolower(get_field('pricing_plan_description_title')), 'pro') !== false){
	$this_is = 'pro';
}
if(strpos(strtolower(get_field('pricing_plan_description_title')), 'free') !== false){
	$this_is = 'free';
}
if(strpos(strtolower(get_field('pricing_plan_description_title')), 'enterprise') !== false){
	$this_is = 'enterprise';
}

?>

<section class="intro-section secondary-page price-detail intro-<?= $this_is ?> border-bottom-light-secondary">
	<div class="container">
		<div class="price-intro-inner">
			<div class="text-holder">
				<h1><?= get_field('pricing_title') ?></h1>
				<p><?= get_field('pricing_subtitle') ?></p>
			</div>
			<div class="img-holder">
				<div class="img-tales">
					<div class="img-tile">
						<img loading="lazy" src="<?= get_field('pricing_heading_image_1')['sizes']['medium_large'] ?>">
					</div>
					<div class="img-tile">
						<img loading="lazy" src="<?= get_field('pricing_heading_image_2')['sizes']['medium_large'] ?>">
					</div>
					<div class="img-tile">
						<img loading="lazy" src="<?= get_field('pricing_heading_image_3')['sizes']['medium_large'] ?>">
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="pricing-detail-section bg-light">
	<div class="container">
		<div class="pricing-detail-section-inner">
			<div class="pricing-inner-sidebar">
				<div class="plan-item <?= $this_is ?>">
					<div class="title-block">
						<h3 class="title"><?= get_field('pricing_plan_description_title') ?></h3>
						<?php if(get_field('pricing_plan_label') != null && get_field('pricing_plan_label') != '') { ?>
						<span class="label"><?= get_field('pricing_plan_label') ?></span>
						<?php } ?>
					</div>
					<div class="content-block">
						<div class="price-holder">
							<h3 class="price"><?= get_field('pricing_plan_cost') ?> <span class="price-sm">/<?= get_field('pricing_plan_basis') ?></span></h3>
							<span class="price-note"><?= get_field('pricing_plan_condition') ?></span>
						</div>
						<p style="height: 108px;"><?= get_field('pricing_plan_description') ?></p>
						<?php if(get_field('pricing_plan_cta_text') != null && get_field('pricing_plan_cta_text') != '') { ?>
						<div class="btn-holder">
							<a href="<?= get_field('pricing_plan_cta_destination') ?>" class="btn" tabindex="0"><?= get_field('pricing_plan_cta_text') ?></a>
						</div>
						<?php } ?>
						<h4><?= get_field('pricing_plan_included_title') ?></h4>
						<?= get_field('pricing_plan_included') ?>
						<?php if(get_field('pricing_plan_add-ons_title') != null && get_field('pricing_plan_add-ons_title') != '') { ?>
						<div class="skew-bg-list">
							<p class="gradient-underline" style="color: #858a94; margin-bottom: 10px; font-size: 0.9em;"><b><?= get_field('pricing_plan_add-ons_title') ?></b></p>
							<?= get_field('pricing_plan_add-ons') ?>
						</div>
						<?php } ?>
						<!-- <p><a class="btn-link" href="http://staging-inflo.kinsta.cloud/en-gb/pricing/pricing-pro-package/" tabindex="0">Learn about the pro plan</a></p> -->
					</div>
				</div>
			</div>
			<div class="pricing-inner-wide">
				<div class="video-popup-holder">
					<?php if(get_field('title_above') != null && get_field('title_above') != '') { ?>
					<h2><?= get_field('title_above') ?></h2>
					<?php } ?>

					<?php if(get_field('cover_image') != null && get_field('cover_image') != '') { ?>
					<a data-fancybox href="<?= get_field('video_link') ?>" class="video-popup-opener">
						<img loading="lazy" src="<?= get_field('cover_image')['sizes']['medium_large'] ?>">
					</a>
					<?php } else { ?>

						<?php
							$additional_str_start = '?';
							if(strrpos(get_field('video_link'), '?') !== false){
								$additional_str_start = '&';
							}
						?>

						<a data-fancybox href="<?= get_field('video_link') ?>" class="video-popup-opener iframe-holder">
							<iframe loading="lazy" src="<?= get_field('video_link') . $additional_str_start ?>title=0&byline=0&portrait=0&sidedock=0&controls=0" width="100%" height="auto" frameborder="0"></iframe>
						</a>

					<?php } ?>

				</div>
				<div class="video-note-row">
					<div class="video-note">
						<h5><?= get_field('title_under') ?></h5>
						<p><?= get_field('text_under') ?></p>
					</div>
					<?php if(get_field('cta_tel_text') != null && get_field('cta_tel_text') != '') { ?>
					<div class="video-note-btn">
						<a href="<?= get_field('cta_tel') ?>" class="btn"><?= get_field('cta_tel_text') ?></a>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="container container-sm">
			<?php if(get_field('simple_rich_text_section_show') == 'Yes') { ?>

					<?= get_field('simple_rich_text_section_content') ?>

			<?php } ?>
		</div>
	</div>

	<?php
	$use_case = get_field('choose_product_use_case');
	if($use_case && $use_case != null && $use_case != '') {

	$use_case_solution = get_field('use_case_solution', $use_case->ID);
	$use_case_impact = get_field('use_case_impact', $use_case->ID);
	//$use_case_tags = get_the_tags( $use_case->ID );
	$use_case_tags = get_the_terms( $use_case->ID, 'product_tax' );
	?>

	<div class="skew-bg-end">
		<div class="container">
			<div class="use-case-preview-holder">
				<div class="use-case-preview">
					<div class="descr-block">
						<div class="title-holder">
							<strong class="subtitle">CUSTOMER SUCCESS STORY</strong>
							<h4 class="title"><?= $use_case->post_title ?></h4>
						</div>
						<h5>Background</h5>
						<p><?= $use_case_solution ?></p>
						<h5>Impact</h5>
						<p><?= $use_case_impact ?></p>
						<ul class="topic-list">
							<?php foreach($use_case_tags as $the_tag){
								$tag_link =  get_term_link( $the_tag->term_id, 'product_tax' );
							?>
							<li>
								<a href="<?= $tag_link/*str_replace('/tag', '', get_tag_link($use_case_tags[$i]->term_id))*/ ?>">
									<img loading="lazy" src="<?= get_field('image_for_this_taxonomy', $the_tag)['sizes']['thumbnail'] ?>" class="icon"> <?= $the_tag->name ?>
								</a>
							</li>
							<?php } ?>

						</ul>
					</div>
					<div class="testimonial-block">
						<div class="testimonial-item">
							<div class="text-block">
								<div class="author-block">
									<div class="author-name-holder">
										<div class="author-img" style="background-image: url('<?= get_field('testimonial_avatar', $use_case->ID)['sizes']['thumbnail'] ?>');"></div>
										<div class="author-descr">
											<h5 class="author-name"><?= get_field('testimonial_name', $use_case->ID) ?></h5>
											<p><?= get_field('testimonial_job_title', $use_case->ID) ?></p>
										</div>
									</div>
									<img loading="lazy" src="https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/icon-50.png" class="quote-img">
									<p><?= get_field('testimonial_quote', $use_case->ID) ?></p>
									<a href="<?= get_permalink($use_case->ID) ?>" class="btn btn-outline">Read Story</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
</section>
<section class="partners-section border-bottom-light-secondary border-bottom-decor-section">
	<div class="container">

		<h2 class="h4"><?= get_field("partners_text", 1312) ?></h2>
		<ul class="partners-list">
			<?php $partner_icons = acf_get_fields_by_id(1308);
			foreach($partner_icons as $icon) { if($icon['name'] != 'partners_text') { ?>
			<?php $the_image = get_field($icon['name'], 1312);
				$partner_link = $the_image['partner_link'];
				if(trim($partner_link) == '') {
					$partner_link = '/'.ICL_LANGUAGE_CODE.'/customer-success';
				}
			?>
			<li>
				<a href="<?= $partner_link ?>">
					<img loading="lazy" src="<?= $the_image['partner_image']['sizes']['medium'] ?>">
				</a>
			</li>
			<?php }} ?>
		</ul>

	</div>
	<span class="border-decor"></span>
</section>
<section class="faq-section bg-light">
	<div class="container">

		<h3 class="h4"><?= get_field('faq_section_title') ?></h3>
		<div class="faq-accordion accordion">
			<?php $contact_faq = acf_get_fields_by_id(1582);
			foreach($contact_faq as $qa) { if($qa['name'] != 'faq_section_title') {
			$the_qa = get_field($qa['name']); ?>

			<?php if($the_qa['question'] != '') { ?>
			<div class="accordion-item">
				<h4 class="accordion-item-title">
					<a href="#" class="item-opener"><?= $the_qa['question'] ?></a>
				</h4>
				<div class="accordion-item-slide">
					<div class="accordion-item-slide-content">
						<p><?= $the_qa['answer'] ?></p>
					</div>
				</div>
			</div>
			<?php }}} ?>
		</div>

		<div class="pricing-detail-cta-block">
			<h4><?= get_field('pricing_cta_description') ?></h4>
			<div class="btn-center-holder">
				<?php if(get_field('pricing_cta_left_text') != null && get_field('pricing_cta_left_text') != '') { ?>
				<span class="btn-with-note">
					<a href="<?= get_field('pricing_cta_left_destination') ?>" class="btn"><?= get_field('pricing_cta_left_text') ?></a>
					<span class="btn-note"><?= get_field('pricing_cta_left_note') ?></span>
				</span>
				<?php } ?>
				<?php if(get_field('pricing_cta_right_text') != null && get_field('pricing_cta_right_text') != '') { ?>
				<a href="<?= get_field('pricing_cta_right_destination') ?>" class="btn btn-white"><?= get_field('pricing_cta_right_text') ?></a>
				<?php } ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>
