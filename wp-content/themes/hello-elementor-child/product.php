<?php /* Template Name: Inflo Product Listing */ ?>

<?php get_header(); ?>

<section class="product-section bg-skew-line bg-light" style="padding-bottom: 0">
    <div class="container removed-sections">
        <div class="section-title">
            <h1 class="h2 gradient-underline"><?= get_field('product_listing_title') ?></h1>
            <p><?= get_field('product_listing_subtitle') ?></p>
        </div>
        <div class="diagram-holder animation-box">
            <div class="diagram-block diagram-block-1">
                <div class="acc-link-holder  animate-elem elem-2">
                    <a href="<?= get_field('diagram_left_image_link') ?>" class="acc-link">
                        <img loading="lazy" src="<?= get_field('diagram_left_image')['sizes']['medium'] ?>" alt="city">
                        <span><?= get_field('diagram_left_image_title') ?></span>
                    </a>
                </div>
                <div class="note-block note-block-top animate-elem elem-6"><?= get_field('diagram_left_top_arrow') ?></div>
                <div class="note-block note-block-bottom animate-elem elem-9"><?= get_field('diagram_left_bottom_arrow') ?></div>
            </div>
            <div class="diagram-block diagram-block-2">
                <ul class="diagram-link-list animate-elem elem-3">
                    <li>
                        <a href="<?= get_field('diagram_top_middle_1_link') ?>" class="link-with-icon">
                            <img loading="lazy" src="<?= get_field('diagram_top_middle_1_image')['sizes']['medium'] ?>">
                            <span><?= get_field('diagram_top_middle_1_title') ?></span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= get_field('diagram_top_middle_2_link') ?>" class="link-with-icon">
                            <img loading="lazy" src="<?= get_field('diagram_top_middle_2_image')['sizes']['medium'] ?>">
                            <span><?= get_field('diagram_top_middle_2_title') ?></span>
                        </a>
                    </li>
                </ul>
                <span class="logo-block animate-elem elem-1">
                    <img loading="lazy" src="<?= get_field('diagram_middle_image')['sizes']['medium'] ?>" alt="Inflo">
                </span>
                <ul class="diagram-link-list animate-elem elem-5">
                    <li>
                        <a href="<?= get_field('diagram_bottom_middle_link') ?>" class="link-with-icon">
                            <img loading="lazy" src="<?= get_field('diagram_bottom_middle_image')['sizes']['medium'] ?>">
                            <span><?= get_field('diagram_bottom_middle_title') ?></span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="diagram-block diagram-block-3">
                <div class="acc-link-holder">
                    <a href="<?= get_field('diagram_right_image_link') ?>" class="acc-link">
                        <span class="animate-elem elem-10">
                            <img loading="lazy" src="<?= get_field('diagram_right_image')['sizes']['medium'] ?>" alt="collaboration">
                            <span><?= get_field('diagram_right_image_title') ?></span>
                        </span>
                    </a>
                    <a href="<?= get_field('diagram_right_image_secondary_link') ?>" class="inflo-hi-label">
                        <span class="animate-elem elem-4">
                            <img loading="lazy" src="<?= get_field('diagram_right_image_secondary')['sizes']['medium'] ?>" alt="Hi">
                            <span class="text"><?= get_field('diagram_right_image_secondary_title') ?></span>
                        </span>
                    </a>
                </div>
                <div class="note-block note-block-top animate-elem elem-7"><?= get_field('diagram_right_top_arrow') ?></div>
                <div class="note-block note-block-bottom animate-elem elem-8"><?= get_field('diagram_right_bottom_arrow') ?></div>
            </div>
            <div class="diagram-block diagram-block-4">
                <div class="diagram-circle-block animate-elem elem-11">
                    <ul class="diagram-img-circle">
                        <li><img loading="lazy" src="<?= get_field('diagram_circle_icon_1')['sizes']['medium'] ?>">
                        </li>
                        <li><img loading="lazy" src="<?= get_field('diagram_circle_icon_2')['sizes']['medium'] ?>">
                        </li>
                        <li><img loading="lazy" src="<?= get_field('diagram_circle_icon_3')['sizes']['medium'] ?>">
                        </li>
                        <li><img loading="lazy" src="<?= get_field('diagram_circle_icon_4')['sizes']['medium'] ?>">
                        </li>
                        <li><img loading="lazy" src="<?= get_field('diagram_circle_icon_5')['sizes']['medium'] ?>">
                        </li>
                        <li><img loading="lazy" src="<?= get_field('diagram_circle_icon_6')['sizes']['medium'] ?>">
                        </li>
                    </ul>
                    <a href="<?= get_field('diagram_circle_top_link') ?>" class="link-with-icon diagram-circle-link">
                        <span><?= get_field('diagram_circle_top_title') ?></span>
                    </a>
                    <a href="<?= get_field('diagram_circle_bottom_link') ?>" class="link-with-icon">
                        <img loading="lazy" src="<?= get_field('diagram_circle_bottom_image')['sizes']['medium'] ?>">
                        <span><?= get_field('diagram_circle_bottom_title') ?></span>
                    </a>
                </div>
                <div class="note-block note-block-top animate-elem elem-12"><?= get_field('diagram_circle_text_above') ?></div>
            </div>
        </div>
        <div class="feature-card-holder">
            <!-- <br> -->
            <h3 class="h4 mb-0 text-center"><?= get_field('solutions_discover_title') ?></h3>
            <div class="subtitle-text text-center">
                <p><?= get_field('solutions_discover_text') ?></p>
            </div>
            <div class="featured-card-list five-cols">

                <?php $product_cards = acf_get_fields_by_id(1980);
                if ($product_cards) {
                    foreach ($product_cards as $item) {
                        $the_item = get_field($item['name']);
                        if ($the_item != null && $the_item != '') { ?>
                            <div class="featured-card">
                                <div class="main-block">
                                    <img loading="lazy" src="<?= get_the_post_thumbnail_url($the_item->ID, 'medium') ?>">
                                    <span><?= $the_item->post_title ?></span>
                                </div>
                                <div class="info-block">
                                    <p><?= $the_item->post_excerpt ?></p>
                                    <div class="text-center">
                                        <a href="<?= get_permalink($the_item->ID) ?>">Read more <i class="fas fa-angle-right"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                <?php }
                    }
                } ?>
            </div>
        </div>

        <!--<div class="benefits-block">
			<div class="benefits-text-block-holder">
				<div class="benefits-text-block">
					<strong class="h6 subtitle"><?= get_field('why_title') ?></strong>
					<h2><?= get_field('why_section_title') ?></h2>
					<p><?= get_field('why_section_text') ?></p>
				</div>
				<?php if (get_field('why_image') != null && get_field('why_image') != '') { ?>
				<div class="benefits-img-block">
					<img loading="lazy" src="<?= get_field('why_image')['sizes']['medium_large'] ?>" class="content-img transparent-img animate-right-to-left">
				</div>
				<?php } ?>
			</div>
			 <?php $benefit1 = get_field('why_section_benefit_1');
                $benefit2 = get_field('why_section_benefit_2');
                $benefit3 = get_field('why_section_benefit_3'); ?>
			<div class="benefits-holder">
				<div class="features-item">
					<img loading="lazy" src="<?= $benefit1['why_benefit_icon_1']['sizes']['medium_large'] ?>" class="features-item-icon">
					<h3 class="h2"><?= $benefit1['why_benefit_1_title'] ?></h3>
					<p><?= $benefit1['why_benefit_1_description'] ?></p>
					<?php if ($benefit1['why_benefit_1_cta'] != null && $benefit1['why_benefit_1_cta'] != '') { ?>
					<a href="<?= $benefit1['why_benefit_1_cta_url'] ?>" class="btn btn-outline"><?= $benefit1['why_benefit_1_cta'] ?></a>
					<?php } ?>
				</div>
				<div class="features-item">
					<img loading="lazy" src="<?= $benefit2['why_benefit_icon_2']['sizes']['medium_large'] ?>" class="features-item-icon">
					<h3 class="h2"><?= $benefit2['why_benefit_2_title'] ?></h3>
					<p><?= $benefit2['why_benefit_2_description'] ?></p>
					<?php if ($benefit2['why_benefit_2_cta'] != null && $benefit2['why_benefit_2_cta'] != '') { ?>
					<a href="<?= $benefit2['why_benefit_2_cta_url'] ?>" class="btn btn-outline"><?= $benefit2['why_benefit_2_cta'] ?></a>
					<?php } ?>
				</div>
				<div class="features-item">
					<img loading="lazy" src="<?= $benefit3['why_benefit_icon_3']['sizes']['medium_large'] ?>" class="features-item-icon">
					<h3 class="h2"><?= $benefit3['why_benefit_3_title'] ?></h3>
					<p><?= $benefit3['why_benefit_3_description'] ?></p>
					<?php if ($benefit3['why_benefit_3_cta'] != null && $benefit3['why_benefit_3_cta'] != '') { ?>
					<a href="<?= $benefit3['why_benefit_3_cta_url'] ?>" class="btn btn-outline"><?= $benefit3['why_benefit_3_cta'] ?></a>
					<?php } ?>
				</div>
			</div>
			<?php if (get_field('why_section_cta_text') != null && get_field('why_section_cta_text') != '') { ?>
			<div class="btn-center-holder">
				<a href="<?= the_field('why_section_cta_destination') ?>" class="btn btn-lg"><?= the_field('why_section_cta_text') ?></a>
			</div>
			<?php } ?>
		</div>-->
    </div>
    <!--<section class="partners-section white-section border-top-light-secondary">
		<div class="container">
			<h2 class="h4"><?= the_field("partners_text", 1312) ?></h2>
			<ul class="partners-list">
				<?php $partner_icons = acf_get_fields_by_id(1308);
                if ($partner_icons) {
                    foreach ($partner_icons as $icon) {
                        if ($icon['name'] != 'partners_text') { ?>
				<?php $the_image = get_field($icon['name'], 1312);
                            $partner_link = $the_image['partner_link'];
                            if (trim($partner_link) == '') {
                                $partner_link = '/' . ICL_LANGUAGE_CODE . '/customer-success';
                            }
                ?>
				<li>
					<a href="<?= $partner_link ?>">
						<img loading="lazy" src="<?= $the_image['partner_image']['sizes']['medium'] ?>">
					</a>
				</li>
				<?php }
                    }
                } ?>
			</ul>
		</div>
	</section>-->
    <span class="border-decor"></span>
</section>

<section class="info-modules-section bg-orange border-top-light-secondary border-bottom-light-secondary">
    <!--border-top-white-->
    <div class="container">
        <h2>Inflo modules</h2>
        <div class="tab-holder honeycomb-holder">
            <div class="honeycomb-wrapper">
                <div class="honeycomb-block honeycomb-block__wrapper tab-nav">
                    <?php
                    $modules_cards = acf_get_fields_by_id(2044);
                    $count = 0;
                    ?>
                    <?php if ($modules_cards) : ?>
                        <?php foreach ($modules_cards as $key => $item) : ?>
                            <?php
                            if ($count == 0) {
                                echo '<div class="honeycomb-block 4-columns tab-nav">';
                            } elseif ($count % 7 == 4 && $count > 1) {
                                echo '</div>';
                                echo '<div class="honeycomb-block 3-columns tab-nav">';
                            } elseif ($count % 7 == 0 && $count > 1) {
                                echo '</div>';
                                echo '<div class="honeycomb-block 4-columns tab-nav">';
                            }
                            $the_item = get_field($item['name']);
                            if ($the_item != null && $the_item != '') : ?>
                                <a href="#tab<?= $key + 1 ?>" class="<?php if ($key == 0) echo 'active-tab'; ?> tab-opener honeycomb-item">
                                    <div class="honeycomb-inner" style="background-image: url(https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/icon-51.png);">
                                        <div class="img-holder">
                                            <?php
                                            $img_src = $the_item['cell_icon']['sizes']['medium'];
                                            if (!(isset($img_src) && $img_src)) {
                                                $img_src = $the_item['cell_icon'];
                                            }
                                            ?>
                                            <img loading="lazy" src="<?= $img_src ?>">
                                        </div>
                                        <span><?= $the_item['cell_title'] ?></span>
                                    </div>
                                </a>
                                <?php $count++; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="main-tab-content">
                <?php
                if ($modules_cards) {
                    foreach ($modules_cards as $key => $item) {
                        $the_item = get_field($item['name'], 5677);
                        if ($the_item != null && $the_item != '') { ?>
                            <div id="tab<?= $key + 1 ?>" class="tab">
                                <h3 class="h4"><?= $the_item['cell_title'] ?></h3>
                                <?= $the_item['cell_description'] ?>
                                <?php if ($the_item['cell_cta_destination'] && $the_item['cell_cta_destination'] != null && $the_item['cell_cta_destination'] != '') { ?>
                                    <div class="btn-center-holder">
                                        <a href="<?= $the_item['cell_cta_destination'] ?>" class="btn"><?= $the_item['cell_cta_text'] ?></a>
                                    </div>
                                <?php } ?>
                            </div>
                <?php }
                    }
                } ?>

            </div>
        </div>
    </div>
</section>

<section class="default-section bg-light">

    <!--<div class="container">
				<h2>
	<?= get_field('testimonial_slider_title') ?>
</h2>
			</div>

	<div class="testimonials-slider">

		<?php $testimonial_slides = acf_get_fields_by_id(1415);
        foreach ($testimonial_slides as $item) {
            $the_item = get_field($item['name']);
            if ($item['name'] != 'testimonial_slider_title' && $the_item['show_testimonial_slide'] != 'No') { ?>

		<div class="testimonial-slide">
			<div class="testimonial-item">

					<?php if ($the_item['testimonial_slide_video'] && trim($the_item['testimonial_slide_video']) != '') {
                        $additional_str_start = '?';
                        if (strrpos($the_item['testimonial_slide_video'], '?') !== false) {
                            $additional_str_start = '&';
                        }
                    ?>
					<div class="img-block" style="background: #000;">
						<a data-fancybox href="<?= $the_item['testimonial_slide_video'] ?>" class="video-popup-opener"><iframe loading="lazy" src="<?= $the_item['testimonial_slide_video'] . $additional_str_start ?>title=0&byline=0&portrait=0&sidedock=0&controls=0" width="100%" height="100%" frameborder="0"></iframe></a>
					</div>
					<?php } else { ?>
					<div class="img-block" style="background-image: url(<?= $the_item['testimonial_slide_image']['sizes']['medium_large'] ?>)">
						<a class="video-popup-opener"></a>
					</div>
					<?php } ?>


				<div class="text-block">
					<div class="author-block">
						<div class="author-name-holder">
							<div class="author-img" style="background-image: url('<?= $the_item['testimonial_author_image']['sizes']['thumbnail'] ?>');"></div>
							<div class="author-descr">
								<h5 class="author-name"><?= $the_item['testimonial_author_name'] ?></h5>
								<p><?= $the_item['testimonial_author_position'] ?></p>
							</div>
						</div>
						<img loading="lazy" src="https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/icon-50.png" class="quote-img">
						<p><?= $the_item['testimonial_text'] ?></p>
					</div>
				</div>
			</div>
		</div>

		<?php }
        } ?>
	</div>-->

    <div class="container">

        <?php if (get_field('display_bottom_cta') != 'No') { ?>
            <div class="cta-block">
                <p><?= get_field('bottom_cta_description') ?></p>
                <div class="btn-center-holder">
                    <a href="<?= get_field('bottom_cta_destination') ?>" class="btn"><?= get_field('bottom_cta_text') ?></a>
                </div>
            </div>
        <?php } ?>
    </div>
</section>

<?php get_footer(); ?>
