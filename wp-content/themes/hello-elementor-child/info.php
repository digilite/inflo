<?php /* Template Name: Inflo Info */ ?>

<?php get_header(); ?>

<section class="info-section bg-skew-line border-bottom-light border-bottom-decor-section">
	<div class="container container-sm">
		<?php the_content(); ?>		
	</div>
	<span class="border-decor"></span>
</section>

<section class="faq-section bg-grey border-bottom-white">
	<div class="container">
		<h3 class="h4"><?= get_field('faq_section_title') ?></h3>
		<div class="faq-accordion accordion">
			<?php $contact_faq = acf_get_fields_by_id(1582); 
			foreach($contact_faq as $qa) { if($qa['name'] != 'faq_section_title') { 
			$the_qa = get_field($qa['name']); ?>

			<?php if($the_qa['question'] != '') { ?>
			<div class="accordion-item">
				<h4 class="accordion-item-title">
					<a href="#" class="item-opener"><?= $the_qa['question'] ?></a>
				</h4>
				<div class="accordion-item-slide">
					<div class="accordion-item-slide-content">
						<p><?= $the_qa['answer'] ?></p>
					</div>
				</div>
			</div>
			<?php }}} ?>
		</div>
	</div>
</section>
<?php if(get_field('display_bottom_cta') != 'No'){ ?>
<section class="default-section">
	<div class="container">
		<div class="cta-block">
			<p><?= get_field('bottom_cta_description') ?></p>
			<div class="btn-center-holder">
				<a href="<?= get_field('bottom_cta_destination') ?>" class="btn"><?= get_field('bottom_cta_text') ?></a>
			</div>
		</div>
	</div>
</section>
<?php } ?>

<?php get_footer(); ?>
