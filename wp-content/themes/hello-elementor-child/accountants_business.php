<?php /* Template Name: Inflo Accountants Business */ ?>
<?php get_header(); ?>
<section class="intro-section secondary-page bg-anim border-bottom-white accountants-business-section">
	<div class="container">
		<div class="text-block full-width">
			<h1 class="h2"><?= get_field('solution_title') ?></h1>
			
		</div>
		<div class="solutions-features-card-holder">
			<?php 
			$solution_top_blocks = acf_get_fields_by_id(2100); 
			foreach($solution_top_blocks as $item) { $the_item = get_field($item['name']); if($the_item != null && $the_item != '' && $the_item['solution_top_block_title'] != '') { 
			?>
			<div class="solutions-features-card">
				<h3 class="card-title"><?= $the_item['solution_top_block_title'] ?></h3>
				<?= $the_item['solution_top_block_text'] ?>
			</div>
			<?php } } ?>
			
		</div>

	</div>
</section>

<?php if(get_field('simple_rich_text_section_show') == 'Yes') { ?>
<section class="default-section">
	<div class="container">
		<?= get_field('simple_rich_text_section_content') ?>
	</div>
</section>
<?php } ?>

<section class="default-section accountants-default">
	<div class="container">
		<div class="benefits-block">
			<div class="benefits-text-block">
				<strong class="h6 subtitle"><?= get_field('why_title') ?></strong>
				<h2><?= get_field('why_section_title') ?></h2>
				<p><?= get_field('why_section_text') ?></p>
			</div> <?php $benefit1 = get_field('why_section_benefit_1');
			$benefit2 = get_field('why_section_benefit_2');
			$benefit3 = get_field('why_section_benefit_3'); ?>
			<div class="benefits-holder">
				<div class="features-item">
					<?php if($benefit1['why_benefit_icon_1']){ ?>
					<img src="<?= $benefit1['why_benefit_icon_1'] ?>" class="features-item-icon">
					<?php } ?>
					<h3 class="h2"><?= $benefit1['why_benefit_1_title'] ?></h3>
					<p><?= $benefit1['why_benefit_1_description'] ?></p>
					<?php if($benefit1['why_benefit_1_cta_url']) {?>
					<a href="<?= $benefit1['why_benefit_1_cta_url'] ?>" class="btn btn-outline"><?= $benefit1['why_benefit_1_cta'] ?></a>
					<?php } ?>
				</div>
				<div class="features-item">
					<?php if($benefit2['why_benefit_icon_2']){ ?>
					<img src="<?= $benefit2['why_benefit_icon_2'] ?>" class="features-item-icon">
					<?php } ?>
					<h3 class="h2"><?= $benefit2['why_benefit_2_title'] ?></h3>					
					<p><?= $benefit2['why_benefit_2_description'] ?></p>
					<?php if($benefit2['why_benefit_2_cta_url']) {?>
					<a href="<?= $benefit2['why_benefit_2_cta_url'] ?>" class="btn btn-outline"><?= $benefit2['why_benefit_2_cta'] ?></a>
					<?php } ?>
				</div>
				<div class="features-item">
					<?php if($benefit3['why_benefit_icon_3']){ ?>
					<img src="<?= $benefit3['why_benefit_icon_3'] ?>" class="features-item-icon">
					<?php } ?>
					<h3 class="h2"><?= $benefit3['why_benefit_3_title'] ?></h3>
					<p><?= $benefit3['why_benefit_3_description'] ?></p>
					<?php if($benefit3['why_benefit_3_cta_url']) {?>
					<a href="<?= $benefit3['why_benefit_3_cta_url'] ?>" class="btn btn-outline"><?= $benefit3['why_benefit_3_cta'] ?></a>
					<?php } ?>
				</div>
			</div>			
			<div class="btn-center-holder">
				<a href="<?= get_field('why_section_cta_destination') ?>" class="btn btn-lg"><?= get_field('why_section_cta_text') ?></a>
			</div>
		</div>
	</div>
</section>

<section class="bg-light accountants-business-features">
	<div class="container">
		<div class="feature-card-holder">
			<h3 class="mb-0">Useful products adding value to your business reporting</h3>
			<div class="subtitle-text">
				<!-- <p>People interested in this feature were also interested in:</p> -->
			</div>
			<div class="featured-card-list">
				<?php $product_cards = acf_get_fields_by_id(1974); 
				foreach($product_cards as $item) { $the_item = get_field($item['name']); if($the_item != null && $the_item != '') { ?>
				<div class="featured-card">
					<div class="main-block">
						<span class="img-wrapper">
							<img src="<?= get_the_post_thumbnail_url($the_item->ID, 'full') ?>">
						</span>
						<span><?= $the_item->post_title ?></span>
					</div>
					<div class="info-block">
						<p><?= $the_item->post_excerpt ?></p>
						<div class="text-center">
							<a href="<?= get_permalink($the_item->ID) ?>">Read more <i class="fas fa-angle-right"></i>
							</a></div>
					</div>
				</div>		
				<?php } } ?>				
			</div>
		</div>
	</div>
</section>

<?php if(get_field('text_area_after_testimonial') != null && get_field('text_area_after_testimonial') != '') { ?>
<section class="section-collapse accountants-business-pretestimonial bg-white border-bottom-light-secondary border-top-light-secondary">
	<div class="container container-sm">
		<?= get_field('text_area_after_testimonial') ?>
	</div>
</section>
<?php } ?>

<?php if(get_field('solutions_testimonial_name') != null && get_field('solutions_testimonial_name') != '') { ?>
<section class="single-testimonial-section bg-light">
	<div class="container container-sm">
		<div class="single-testimonial-block">
			<div class="testimonial-item">
				<div class="text-block">
					<div class="author-block">
						<div class="author-name-holder">
							<div class="author-img" style="background-image: url('<?= the_field('solutions_testimonial_avatar') ?>');"></div>
							<div class="author-descr">
								<h5 class="author-name"><?= get_field('solutions_testimonial_name') ?></h5>
								<p><?= get_field('solutions_testimonial_title') ?></p>
							</div>
						</div>
						<img src="http://kwrdev.itcraftlab.com/wp-content/uploads/2021/02/icon-50.png" class="quote-img">
						<p><?= the_field('solutions_testimonial_quote') ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php if(get_field('display_bottom_cta') != 'No'){ ?>
	<div class="container">
		<div class="cta-block">
			<p><?= get_field('bottom_cta_description') ?></p>
			<div class="btn-center-holder">
				<a href="<?= get_field('bottom_cta_destination') ?>" class="btn"><?= get_field('bottom_cta_text') ?></a>
			</div>
		</div>
	</div>
	<?php } ?>
</section>
<?php } ?>


<?php get_footer(); ?>
