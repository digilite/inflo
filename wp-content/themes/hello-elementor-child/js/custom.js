jQuery(document).ready(function($){

	$('#hub_sub').submit(function(e){
		e.preventDefault();
		var emsub = $('#hub_sub').find('input[type="email"]').val(); 		
		$('#footer_hub_form form input[type="email"]').val(emsub);		
		$('#footer_hub_form form').submit();
	});

	$('#footer_hub_form').on('DOMNodeInserted', '.submitted-message', function () {
	      var message = $('#footer_hub_form .submitted-message').html();
			$('#hub_sub .form-text').append('<p style="color:#39b54a; font-weight:700;">'+message+'</p>');
	});

	$('body').click(function(e){
		
		if ($('#wpml_opener').has(e.target).length === 0 && $('#header_lang_switcher .wpml-language-nav').has(e.target).length === 0) {
	        $('#header_lang_switcher .wpml-language-nav .wpml-language-nav-sub-wrapper').slideUp();
	    }	

	    if ($('#wpml_opener_footer').has(e.target).length === 0 && $('#footer_lang_switcher .wpml-language-nav').has(e.target).length === 0) {
	        $('#footer_lang_switcher .wpml-language-nav .wpml-language-nav-sub-wrapper').slideUp();
	    }	
		
	});

	$('#wpml_opener').click(function(e){
		e.preventDefault();

		if(!$('#header_lang_switcher .wpml-language-nav .wpml-language-nav-sub-wrapper').is(':visible')){
			$('#header_lang_switcher .wpml-language-nav .wpml-language-nav-sub-wrapper').slideDown();
		} else {
			$('#header_lang_switcher .wpml-language-nav .wpml-language-nav-sub-wrapper').slideUp();
		}
	});

	$('#wpml_opener_footer').click(function(e){
		e.preventDefault();

		if(!$('#footer_lang_switcher .wpml-language-nav .wpml-language-nav-sub-wrapper').is(':visible')){
			$('#footer_lang_switcher .wpml-language-nav .wpml-language-nav-sub-wrapper').slideDown();
		} else {
			$('#footer_lang_switcher .wpml-language-nav .wpml-language-nav-sub-wrapper').slideUp();
		}
	});

	$('.featured-card .info-block a').click(function(){
		window.location.href = $(this).attr('href');
	});

	$('.honeycomb-item').mouseenter(function(){			
		if($('.honeycomb-item.clicked').length < 1){
			$(this).click();
		}
	});

	$('.honeycomb-item').mousedown(function(){
		if(!$(this).hasClass('clicked')){
			$('.honeycomb-item').removeClass('clicked');
			$(this).addClass('clicked');
		} else {
			$('.honeycomb-item').removeClass('clicked');
		}
	});

	if(
		$('.intro-section').hasClass('bg-anim')
		|| $('.intro-section').hasClass('intro-free')
		|| $('.intro-section').hasClass('intro-starter')
		|| $('.intro-section').hasClass('intro-pro')
		|| $('.intro-section').hasClass('intro-enterprise')
		){
		$('header').removeClass('header-light');
	}
	
	$(function() {
        $('.lazy-bg').Lazy();
    });

});
