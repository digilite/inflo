'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

jQuery(document).ready(function () {
	initBurgerMenu();
	initNavMenu();
	initSlickSlider();
	initShowMore();
	initAccordion();
	initTabs();
	initPopups();
	// initFormValidation();
});

jQuery(window).on('load', function () {
	initHeaderFixed();
	initParallax();
	initFeaturesCardList();
	jQuery('.anim-bg-line').append('<div class="anim-line"></div>');
	// animate bg line
	animateSvgBg();
});

function initHeaderFixed() {
	var activeClass = 'page-scrolled';
	var win = jQuery(window);
	var body = jQuery('body');

	var header = jQuery('#header');
	var headerHeight = header.height();
	var headerBg = header.css('backgroundColor');

	if (win.scrollTop() > headerHeight / 2) {
		body.addClass(activeClass);
	} else {
		body.removeClass(activeClass);
	}

	win.scroll(function () {
		var scroll = win.scrollTop();

		if (scroll > headerHeight / 2) {
			body.addClass(activeClass);
		} else {
			body.removeClass(activeClass);
		}
	});

	var headerPlaceholder = jQuery('#header_placeholder');

	win.resize(function () {
		headerHeight = header.outerHeight();
		headerPlaceholder.css('height', headerHeight);
	});

	setTimeout(function () {
		win.trigger('resize');
		headerPlaceholder.css('height', headerHeight);
	}, 10);
};

function initBurgerMenu() {
	jQuery('.burger-opener').each(function () {
		var opener = jQuery(this);
		var holder = jQuery('body');
		var activeClass = 'menu-active';
		var menu = jQuery('#header .menu');

		var win = jQuery(window);
		var header = jQuery('#header');
		var headerHeight = header.height();

		opener.on('click', function (e) {
			e.preventDefault();
			e.stopPropagation();

			holder.toggleClass(activeClass);

			headerHeight = header.height();
			// menu.css('height', `calc(100vh - ${headerHeight}px)`);
			// menu.css('height', `100vh`);
		});

		win.resize(function () {
			if (holder.hasClass(activeClass)) {
				// menu.css('height', `calc(100vh - ${headerHeight}px)`);
				// menu.css('height', `100vh`);
			}
		});

		jQuery(document).on('mouseup', function (e) {
			if (!menu.is(e.target) && menu.has(e.target).length === 0 && !opener.is(e.target)) {
				holder.removeClass(activeClass);
			}
		});
	});
};

function initNavMenu() {
	jQuery('.nav-holder').each(function () {
		var holder = jQuery(this);
		var submenuList = holder.find('.submenu');
		var submenuOpenerList = submenuList.siblings('a');
		var menuOpenClass = 'submenu-open';

		submenuList.hide();

		submenuOpenerList.each(function () {
			var opener = jQuery(this);
			var submenu = opener.siblings('.submenu');

			opener.on('click', function (e) {
				if (!opener.parent().hasClass(menuOpenClass)) {
					e.preventDefault();
				}

				submenu.fadeToggle();
				opener.parent().toggleClass(menuOpenClass);

				opener.parent().siblings().find('.submenu').fadeOut();
				opener.parent().siblings().removeClass(menuOpenClass);
			});
		});

		jQuery(document).on('mouseup', function (e) {
			if (!holder.is(e.target) && holder.has(e.target).length === 0 && !submenuOpenerList.is(e.target)) {
				submenuList.fadeOut();
				submenuOpenerList.parent().removeClass(menuOpenClass);
			}
		});
	});
};

function initParallax() {
	jQuery(".intro-section .parallax-img-block").paroller({
		factor: 0.2, // multiplier for scrolling speed and offset
		factorXs: 0, // multiplier for scrolling speed and offset if window width is <576px
		type: 'foreground', // background, foreground
		direction: 'vertical', // vertical, horizontal
		transition: 'transform 0.4s linear' // CSS transition
	});
};

function initSlickSlider() {
	jQuery('.features-slider > div').slick({
		infinite: true,
		autoplay: true,
		autoplaySpeed: 5000,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		dots: true,
		fade: true
	});

	jQuery('.testimonials-slider > div').slick({
		infinite: true,
		autoplay: false,
		autoplaySpeed: 5000,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		dots: false,
		centerMode: false,
		focusOnSelect: true,
		responsive: [{
			breakpoint: 1024,
			settings: {
				centerMode: false
			}
		}, {
			breakpoint: 768,
			settings: {
				centerMode: true,
				centerPadding: '25px'
			}
		}]
	});

	jQuery('.post-slider:not(.post-light)').slick({
		infinite: true,
		autoplay: true,
		autoplaySpeed: 5000,
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: false,
		dots: false,
		centerMode: false,
		focusOnSelect: true,
		responsive: [{
			breakpoint: 1024,
			settings: {
				slidesToShow: 3
			}
		}, {
			breakpoint: 768,
			settings: {
				slidesToShow: 1,
				centerMode: true,
				centerPadding: '20px'
			}
		}]
	});

	/*jQuery('.post-slider.post-light').slick({
		infinite: true,
		autoplay: true,
		autoplaySpeed: 5000,
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: false,
		dots: false,
		centerMode: false,
		focusOnSelect: true,
		responsive: [{
			breakpoint: 9999,
			settings: "unslick"
		}, {
			breakpoint: 1024,
			settings: {
				slidesToShow: 3
			}
		}, {
			breakpoint: 768,
			settings: {
				slidesToShow: 1,
				centerMode: true,
				centerPadding: '20px'
			}
		}]
	});*/

	jQuery('.plan-slider').slick({
		infinite: true,
		autoplay: true,
		autoplaySpeed: 5000,
		slidesToShow: 4,
		slidesToScroll: 1,
		arrows: false,
		dots: false,
		centerMode: false,
		focusOnSelect: true,
		responsive: [{
			breakpoint: 1024,
			settings: {
				slidesToShow: 2
			}
		}, {
			breakpoint: 768,
			settings: {
				slidesToShow: 1,
				centerMode: true,
				centerPadding: '40px'
			}
		}]
	});

	jQuery('.featured-card-list').slick({
		infinite: true,
		autoplay: true,
		autoplaySpeed: 5000,
		slidesToShow: 4,
		slidesToScroll: 1,
		arrows: false,
		dots: false,
		centerMode: false,
		focusOnSelect: true,
		responsive: [{
			breakpoint: 9999,
			settings: "unslick"
		}, {
			breakpoint: 1024,
			settings: {
				slidesToShow: 2
			}
		}, {
			breakpoint: 768,
			settings: {
				slidesToShow: 1,
				centerMode: true,
				centerPadding: '30px'
			}
		}]
	});

	jQuery('.member-list > div').slick({
		infinite: true,
		autoplay: true,
		autoplaySpeed: 5000,
		slidesToShow: 4,
		slidesToScroll: 1,
		arrows: false,
		dots: false,
		centerMode: false,
		focusOnSelect: true,
		responsive: [{
			breakpoint: 9999,
			settings: "unslick"
		}, {
			breakpoint: 1024,
			settings: "unslick"
			/*settings: {
				slidesToShow: 2
			}*/
		}, {
			breakpoint: 768,
			settings: {
				slidesToShow: 1,
				centerMode: true,
				centerPadding: '30px'
			}
		}]
	});

	jQuery('.offers-slider').slick({
		infinite: true,
		autoplay: true,
		autoplaySpeed: 5000,
		slidesToShow: 4,
		slidesToScroll: 1,
		arrows: false,
		dots: false,
		centerMode: false,
		focusOnSelect: true,
		responsive: [{
			breakpoint: 1024,
			settings: {
				slidesToShow: 3
			}
		}, {
			breakpoint: 768,
			settings: {
				slidesToShow: 2
			}
		}, {
			breakpoint: 480,
			settings: {
				slidesToShow: 1,
				centerMode: true,
				centerPadding: '20px'
			}
		}]
	});
};

function animateSvgBg() {
	var Tool = function () {
		function Tool() {
			_classCallCheck(this, Tool);
		}

		_createClass(Tool, null, [{
			key: 'randomNumber',

			// random number.
			value: function randomNumber(min, max) {
				return Math.floor(Math.random() * (max - min + 1) + min);
			}
			// random color rgb.

		}, {
			key: 'randomColorRGB',
			value: function randomColorRGB() {
				return "rgb(" + this.randomNumber(0, 255) + ", " + this.randomNumber(0, 255) + ", " + this.randomNumber(0, 255) + ")";
			}
			// random color hsl.

		}, {
			key: 'randomColorHSL',
			value: function randomColorHSL(hue, saturation, lightness) {
				return "hsl(" + hue + ", " + saturation + "%, " + lightness + "%)";
			}
			// gradient color.

		}, {
			key: 'gradientColor',
			value: function gradientColor(ctx, cr, cg, cb, ca, x, y, r) {
				var col = cr + "," + cg + "," + cb;
				var g = ctx.createRadialGradient(x, y, 0, x, y, r);
				g.addColorStop(0, "rgba(" + col + ", " + ca * 1 + ")");
				g.addColorStop(0.5, "rgba(" + col + ", " + ca * 0.5 + ")");
				g.addColorStop(1, "rgba(" + col + ", " + ca * 0 + ")");
				return g;
			}
		}]);

		return Tool;
	}();

	var Angle = function () {
		function Angle(angle) {
			_classCallCheck(this, Angle);

			this.a = angle;
			this.rad = this.a * Math.PI / 180;
		}

		_createClass(Angle, [{
			key: 'incDec',
			value: function incDec(num) {
				this.a += num;
				this.rad = this.a * Math.PI / 180;
				return this.rad;
			}
		}]);

		return Angle;
	}();

	var Controller = function () {
		function Controller(id) {
			_classCallCheck(this, Controller);

			this.id = document.getElementById(id);
		}

		_createClass(Controller, [{
			key: 'getVal',
			value: function getVal() {
				return this.id.value;
			}
		}]);

		return Controller;
	}();

	var Time = function () {
		function Time(time) {
			_classCallCheck(this, Time);

			this.startTime = time;
			this.lastTime;
			this.elapsedTime;
		}

		_createClass(Time, [{
			key: 'getElapsedTime',
			value: function getElapsedTime() {
				this.lastTime = Date.now();
				this.elapsedTime = (this.startTime - this.lastTime) * -1;
				return this.elapsedTime;
			}
		}]);

		return Time;
	}();

	var holder = jQuery('.anim-bg-line .anim-line');

	if (holder.length < 1) return;

	var canvas = void 0;
	var simplex = new SimplexNoise('different seed');

	var Canvas = function () {
		function Canvas(bool) {
			_classCallCheck(this, Canvas);

			// create canvas.
			this.canvas = document.createElement("canvas");
			// if on screen.
			if (bool === true) {
				this.canvas.style.display = 'block';
				this.canvas.style.top = 0;
				this.canvas.style.left = 0;
				holder.append(this.canvas);
			}
			this.ctx = this.canvas.getContext("2d");
			// this.width = this.canvas.width = window.innerWidth;
			// this.height = this.canvas.height = window.innerHeight;
			this.width = this.canvas.width = holder.width();
			this.height = this.canvas.height = holder.height();
			// mouse infomation.
			this.mouseX = null;
			// this.mouseY = this.height / 2;
			this.mouseY = this.height / 1.5;
			// this.mouseY = 300;
			// shape
			this.shapeNum = 20;
			this.shapes = [];
			// time
			this.time = new Time(Date.now());
		}

		// init, render, resize


		_createClass(Canvas, [{
			key: 'init',
			value: function init() {
				this.shapes.length = 0;
				var width = 20;
				for (var i = 0; i < this.shapeNum; i++) {
					var s = new Shape(this.ctx, 0, this.height / 2 - i * 2, width, i / this.shapeNum / 3, i);
					this.shapes.push(s);
				}
			}
		}, {
			key: 'render',
			value: function render() {
				this.ctx.clearRect(0, 0, canvas.width, canvas.height);
				for (var i = 0; i < this.shapes.length; i++) {
					this.shapes[i].render(i);
				}
			}
		}, {
			key: 'resize',
			value: function resize() {
				this.width = this.canvas.width = holder.width();
				this.height = this.canvas.height = holder.height();
				this.init();
			}
		}]);

		return Canvas;
	}();

	var Shape = function () {
		function Shape(ctx, x, y, width, alpha, i) {
			_classCallCheck(this, Shape);

			this.ctx = ctx;
			this.width = width;
			this.alpha = alpha;
			this.i = i;
			this.init(x, y);
		}

		_createClass(Shape, [{
			key: 'init',
			value: function init(x, y) {
				this.x = x;
				this.y = y;
				this.a = new Angle(~~(360 / this.i / 2));
				this.step = canvas.width < 360 ? 1 : canvas.width / 360;
			}
		}, {
			key: 'draw',
			value: function draw(x, y) {
				var ctx = this.ctx;
				ctx.save();
				ctx.lineJoin = 'bevel';
				ctx.lineWidth = this.width;
				ctx.beginPath();
				ctx.moveTo(this.x, this.y);

				ctx.strokeStyle = 'rgba(249, 184, 79, ' + this.alpha + ')';
				ctx.shadowColor = 'rgba(249, 184, 79, ' + this.alpha + ')';
				ctx.shadowBlur = 25;
				ctx.shadowOffsetY = this.width;

				for (var _x = 0; _x < canvas.width; _x += this.step) {
					// const noise = simplex.noise3D(0 , 0 , 0) * (canvas.height - canvas.mouseY) / 2;
					var noise = simplex.noise3D(_x / (canvas.width / 6), this.y / canvas.height, this.a.rad) * (canvas.height - 70) / 2;
					var _y = Math.cos(_x * 2 * Math.PI / 180 / 2) * noise + this.y;
					ctx.lineTo(_x * this.step, _y);
				}

				ctx.stroke();
				ctx.restore();
			}
		}, {
			key: 'updateParams',
			value: function updateParams() {
				this.a.incDec(1);
			}
		}, {
			key: 'render',
			value: function render() {
				this.draw();
				this.updateParams();
			}
		}]);

		return Shape;
	}();

	canvas = new Canvas(true);
	canvas.init();

	var Animation = function () {
		function Animation() {
			_classCallCheck(this, Animation);
		}

		_createClass(Animation, null, [{
			key: 'startRender',
			value: function startRender() {
				this._render();
			}
		}, {
			key: '_render',
			value: function _render() {
				var _this = this;

				window.requestAnimationFrame(function () {
					canvas.render();
					_this._render();
				});
			}
		}]);

		return Animation;
	}();

	Animation.startRender();

	// event
	window.addEventListener("resize", function () {
		canvas.resize();
	}, false);
};

function initShowMore() {
	jQuery('.show-more-block').each(function () {
		var holder = jQuery(this);
		var showMoreItems = holder.find('.show-more-item');
		var showMoreBtn = holder.find('.show-more-btn');
		var hiddenClass = 'hidden-item';
		var visible = 4;

		showMoreItems.hide();
		showMoreItems.addClass(hiddenClass);
		showMoreItems.slice(0, visible).show().removeClass(hiddenClass);

		showMoreBtn.on('click', function (e) {
			e.preventDefault();
			holder.find('.show-more-item.hidden-item').slice(0, visible).removeClass(hiddenClass).slideDown();
			if (holder.find('.show-more-item.hidden-item').length == 0) {
				showMoreBtn.fadeOut('slow');
			}
		});
	});
};

function initFeaturesCardList() {
	jQuery('.featured-card-list').each(function () {
		var holder = jQuery(this);
		var cardList = holder.find('.featured-card');
		var activeClass = 'active-item';

		cardList.on('click', function (e) {
			e.preventDefault();
			var card = jQuery(this);
			var cardInfoBlock = card.find('.info-block');

			if (cardInfoBlock.is(e.target) || cardInfoBlock.has(e.target).length > 0) {
				return false;
			}

			cardList.not(card).removeClass(activeClass);
			card.toggleClass(activeClass);
		});

		jQuery(document).on('mouseup', function (e) {
			if (!cardList.is(e.target) && cardList.has(e.target).length === 0) {
				cardList.removeClass(activeClass);
			}
		});
	});
};

function initAccordion() {
	jQuery('.accordion').each(function () {
		var holder = jQuery(this);
		var itemList = holder.find('.accordion-item');
		var itemDropSlides = holder.find('.accordion-item-slide');
		var activeClass = 'active-item';

		itemList.each(function () {
			var item = jQuery(this);
			var currentSlide = item.find('.accordion-item-slide');
			var currentOpener = item.find('.item-opener');

			if (item.hasClass(activeClass)) {
				currentSlide.show();
			} else {
				currentSlide.hide();
			}

			currentOpener.on('click', function (e) {
				e.preventDefault();

				item.siblings('.accordion-item').removeClass(activeClass);
				item.toggleClass(activeClass);

				item.siblings('.accordion-item').find('.accordion-item-slide').slideUp();
				currentSlide.slideToggle();
			});
		});
	});
};

function initTabs() {
	jQuery('.tab-holder').each(function () {
		var holder = jQuery(this);
		var navList = holder.find('.tab-nav a');
		var tabList = holder.find('.main-tab-content .tab');
		var activeClass = 'active-tab';

		tabList.hide();

		navList.each(function () {
			var link = jQuery(this);

			if (link.hasClass(activeClass)) {
				jQuery(link.attr('href')).addClass(activeClass);
				jQuery(link.attr('href')).show();
			}
		});

		navList.on('click', function (e) {
			e.preventDefault();

			var link = jQuery(this);
			var tab = jQuery(link.attr('href'));

			navList.not(link).removeClass(activeClass);
			tabList.not(tab).removeClass(activeClass);
			tabList.not(tab).hide();

			link.addClass(activeClass);
			tab.addClass(activeClass);
			tab.show();
		});
	});
};

function initPopups() {
	jQuery('.popup-opener').each(function () {
		var opener = jQuery(this);
		var popup = jQuery(opener.attr('href'));
		var popupInner = popup.find('.popup-inner');
		var activeClass = 'popup-open';

		if (!popup) return false;

		opener.on('click', function (e) {
			e.preventDefault();

			popup.toggleClass(activeClass);
		});
	});

	jQuery('.custom-popup').each(function (e) {
		var popup = jQuery(this);
		var popupInner = popup.find('.popup-inner');
		var closePopup = popup.find('.close-popup');
		var activeClass = 'popup-open';

		closePopup.on('click', function (e) {
			e.preventDefault();

			popup.removeClass(activeClass);
		});

		jQuery(document).on('mouseup', function (e) {
			if (!popupInner.is(e.target) && popupInner.has(e.target).length === 0) {
				popup.removeClass(activeClass);
			}
		});
	});
};

function initFormValidation() {
	jQuery('.validate-form').each(function () {
		var form = jQuery(this);
		var formInputs = form.find('input, textarea, select');
		var errorClass = 'error';
		var successClass = 'success';
		var constraints = {
			email: {
				presence: true,
				email: true
				// message: "*Please enter correct email"
			},
			'first-name': {
				presence: true,
				format: {
					pattern: "[a-z]+",
					flags: "i"
					// message: "*Please enter correct first name"
				}
			},
			'last-name': {
				presence: true,
				format: {
					pattern: "[a-z]+",
					flags: "i"
					// message: "*Please enter correct last name"
				}
			},
			message: {
				presence: true
			}
		};

		form.on('submit', function (e) {
			var errors = validate(form, constraints);

			if (errors) {
				e.preventDefault();
			}

			drawErrors(errors);
		});

		function drawErrors(errors) {
			var prevErrors = form.find('.' + errorClass);

			prevErrors.removeClass(errorClass);
			prevErrors.addClass(successClass);
			prevErrors.siblings('.error-text').remove();

			for (name in errors) {
				var errorInp = form.find('[name="' + name + '"]');
				var message = '*Please enter correct data.';

				errorInp.removeClass(successClass);

				switch (name) {
					case 'email':
						message = '*Please enter correct email';
						break;
					case 'first-name':
						message = '*Please enter first name';
						break;
					case 'last-name':
						message = '*Please enter last name';
						break;
					case 'message':
						message = '*Please enter your message';
						break;
				}

				errorInp.after(createError(message));
				errorInp.addClass(errorClass);
			}
		}

		function createError(message) {
			var errorMessage = jQuery('<span class="error-text">' + message + '</span>');

			return errorMessage;
		}
	});
};