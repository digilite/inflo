<?php /* Template Name: Inflo Learn */ ?>

<?php get_header(); ?>

<section class="sources-intro-section bg-light">
    <div class="container">
        <div class="section-title">
            <h1 class="h2"><?= get_field('learn_title_text') ?></h1>
            <?php get_search_form(); ?>
        </div>
        <div class="post-panel">
            <h2 class="h3"><?= get_field('learn_custom_title') ?></h2>
            <div class="post-slider post-light">

                <?php // Check learn custom cards rows exists.
            if( have_rows('learn_custom_cards') ): 
                // Loop through rows.
                while( have_rows('learn_custom_cards') ) : the_row();

                    // Load learn custom card sub field values.
                    $title = get_sub_field('learn_custom_card_title');
                    $body = get_sub_field('learn_custom_card_body');
                    $image_id      = get_sub_field('learn_custom_card_image');
                    $image_size    = 'full';
                    $image_array   = wp_get_attachment_image_src($image_id, $image_size);
                    $image_url = $image_array[0];

                    $url = get_sub_field('learn_custom_card_url');
                    ?>
                <div class="post-slide">

                    <div class="post-item">
                        <a href="<?= $url; ?>" class="img-block"
                            style="background-image: url('<?= $image_url; ?>')"></a>
                        <div class="post-txt">
                            <a href="<?= $url; ?>" class="txt">
                                <h4 class="post-title" style="min-height: 0;"><?= $title; ?></h4>
                                <p style="color:black; font-size:1rem"><?= $body; ?></p>
                                <a href="<?= $url; ?>" class="btn btn-outline" style="align-self:flex-start;">View</a>
                            </a>
                        </div>
                    </div>
                </div>


                <?php 
                // End loop.
                endwhile; 
             endif;

            ?>

            </div>
            <div class="btn-position-top">
                <?php  if( get_field('learn_custom_more_link_url') ):  ?>
                <a href="<?= the_field('learn_custom_more_link_url'); ?>"
                    class="btn"><?= the_field('learn_custom_more_link_text'); ?></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>


<section class="sources-section bg-grey border-top-light-secondary border-bottom-light-secondary">
    <div class="container">
        <div class="post-panel">
            <h2 class="h3"><?= get_field('learn_use_case_title') ?></h2>
            <div class="post-slider post-light">

                <?php
                    $use_case_load_more_link = '';

                    $more_link = get_field('learn_use_case_more_link');//get_field('learn_use_case_more_link');

                    if (get_field('learn_use_case_show_most_recent') == 'Yes') {

                        //$learn_use_case_cat_obj = get_field('learn_use_case_category');

                        //$learn_use_case_cat = $learn_use_case_cat_obj->term_id;

                        $got_posts = get_posts(['numberposts' => 3, 'post_type' => 'use_cases', 'suppress_filters' => 0]);//get_posts( ['numberposts' => 3, 'category' => $learn_use_case_cat] );

                        if ($got_posts) {
                            foreach ($got_posts as $post) : setup_postdata($post);

                                //$use_case_link = get_category_link( $learn_use_case_cat );

                                //$post_tags = get_the_tags( $post->ID );
                                //$tag_link = get_tag_link($post_tag->term_id);
                                $post_tags = get_the_terms($post->ID, 'product_tax');
                                ?>
                <div class="post-slide">

                    <div class="use-case-preview-sm">
                        <div class="descr-block">
                            <div class="title-holder">
                                <h4 class="title"><a href="<?= get_permalink($post->ID) ?>"><?= $post->post_title ?></a>
                                </h4>
                            </div>
                            <ul class="topic-list">
                                <?php foreach ($post_tags as $post_tag) {
                                                    $tag_link = get_term_link($post_tag->term_id, 'product_tax');//get_tag_link($post_tag->term_id);
                                                    ?>
                                <li>
                                    <a href="<?= $tag_link ?>">
                                        <img loading="lazy"
                                            src="<?= get_field('image_for_this_taxonomy', $post_tag)['sizes']['thumbnail'] ?>"
                                            class="icon"> <?= $post_tag->name ?>
                                    </a>
                                </li>
                                <?php } ?>
                            </ul>
                            <a href="<?= get_permalink($post->ID) ?>" class="btn btn-outline">Read
                                story</a>
                        </div>
                    </div>
                </div>
                <?php endforeach;
                            wp_reset_postdata();
                        }
                    } else {

                        for ($i = 1; $i < 4; $i++) {
                            $post = get_field('learn_use_case_post_' . $i);
                            if ($post && $post != null) {

                                $post_cat = get_the_category($post->ID)[0];
                                //$use_case_link = get_category_link( $post_cat->term_id );
                                //$post_tag = get_the_tags( $post->ID )[0];
                                //$tag_link = get_tag_link($post_tag->term_id);
                                $post_tags = get_the_terms($post->ID, 'product_tax');
                                ?>

                <div class="post-slide">

                    <div class="use-case-preview-sm">
                        <div class="descr-block">
                            <div class="title-holder">
                                <h4 class="title"><a href="<?= get_permalink($post->ID) ?>"><?= $post->post_title ?></a>
                                </h4>
                            </div>
                            <ul class="topic-list">
                                <?php foreach ($post_tags as $post_tag) {
                                                    $tag_link = get_term_link($post_tag->term_id, 'product_tax');//get_tag_link($post_tag->term_id);
                                                    ?>
                                <li>
                                    <a href="<?= $tag_link ?>">
                                        <img loading="lazy"
                                            src="<?= get_field('image_for_this_taxonomy', $post_tag)['sizes']['thumbnail'] ?>"
                                            class="icon"> <?= $post_tag->name ?>
                                    </a>
                                </li>
                                <?php } ?>
                            </ul>
                            <a href="<?= get_permalink($post->ID) ?>" class="btn btn-outline">Read
                                story</a>
                        </div>
                    </div>

                </div>

                <?php wp_reset_postdata();
                            }
                        }
                    } ?>

            </div>
            <div class="btn-position-top">
                <a href="<?= $more_link ?>" class="btn">Load more stories</a>
            </div>
        </div>
    </div>
</section>

<section class="sources-section bg-light border-bottom-light">
    <div class="container">
        <div class="post-panel">
            <h2 class="h3"><?= get_field('learn_guides_title'); ?></h2>
            <div class="post-slider post-light">
                <?php

                    $more_link = get_category_link(get_field('learn_guides_more_link'));//get_field('learn_guides_more_link');

                    if (get_field('learn_guides_show_most_recent') == 'Yes') {

                        $learn_guides_cat_obj = get_field('learn_guides_category');

                        $learn_guides_cat = $learn_guides_cat_obj->term_id;

                        $got_posts = get_posts(['numberposts' => 3, 'category' => $learn_guides_cat]);

                        foreach ($got_posts as $post) : setup_postdata($post);
                            ?>
                <div class="post-slide">
                    <div class="post-item">
                        <a href="<?= get_permalink($post->ID) ?>" class="img-block"
                            style="background-image: url('<?= get_the_post_thumbnail_url($post->ID, 'medium') ?>');"></a>
                        <div class="post-txt">
                            <a href="<?= get_permalink($post->ID) ?>" class="txt">
                                <h4 class="post-title"><?= $post->post_title ?>
                                </h4>
                                <span
                                    class="post-author"><?= get_the_author_meta('display_name', $post->post_author); ?></span>

                                <?php $post_date_no_time = explode(' ', $post->post_date_gmt)[0];
                                            $post_formatted_date_arr = explode('-', $post_date_no_time);
                                            $dateObj = DateTime::createFromFormat('!m', $post_formatted_date_arr[1]);
                                            $month = $dateObj->format('F');
                                            $post_formatted_date = $post_formatted_date_arr[2] . ' ' . $month . ' ' . $post_formatted_date_arr[0]; ?>

                                <span class="post-date">Posted <time
                                        datetime="<?= $post_date_no_time ?>"><?= $post_formatted_date ?></time></span>
                            </a>
                            <?php $guides_link = get_category_link($learn_guides_cat); ?>
                            <a href="<?= $guides_link ?>"
                                class="post-category xs-mobile"><?= $learn_guides_cat_obj->name ?></a>
                        </div>
                    </div>
                </div>
                <?php endforeach;
                        wp_reset_postdata();
                    } else {

                        for ($i = 1; $i < 4; $i++) {
                            $post = get_field('learn_guides_post_' . $i);
                            if ($post && $post != null) {
                                ?>

                <div class="post-slide">
                    <div class="post-item">
                        <a href="<?= get_permalink($post->ID) ?>" class="img-block"
                            style="background-image: url('<?= get_the_post_thumbnail_url($post->ID, 'medium') ?>');"></a>
                        <div class="post-txt">
                            <a href="<?= get_permalink($post->ID) ?>" class="txt">
                                <h4 class="post-title"><?= $post->post_title ?>
                                </h4>
                                <span
                                    class="post-author"><?= get_the_author_meta('display_name', $post->post_author); ?></span>

                                <?php $post_date_no_time = explode(' ', $post->post_date_gmt)[0];
                                                $post_formatted_date_arr = explode('-', $post_date_no_time);
                                                $dateObj = DateTime::createFromFormat('!m', $post_formatted_date_arr[1]);
                                                $month = $dateObj->format('F');
                                                $post_formatted_date = $post_formatted_date_arr[2] . ' ' . $month . ' ' . $post_formatted_date_arr[0]; ?>

                                <span class="post-date">Posted <time
                                        datetime="<?= $post_date_no_time ?>"><?= $post_formatted_date ?></time></span>
                            </a>
                            <?php $post_cat = get_the_category($post->ID)[0]; ?>
                            <?php $guides_link = get_category_link($post_cat->term_id); ?>
                            <a href="<?= $guides_link ?>" class="post-category xs-mobile"><?= $post_cat->name ?></a>
                        </div>
                    </div>
                </div>

                <?php wp_reset_postdata();
                            }
                        }
                    } ?>
            </div>
            <div class="btn-position-top">
                <?php if (get_field('learn_guides_show_most_recent') == 'Yes') { ?>
                <a href="<?= $guides_link ?>" class="btn">Load more product insights</a>
                <?php } else { ?>
                <a href="<?= $more_link ?>" class="btn">Load more product insights</a>
                <?php } ?>
            </div>
        </div>
    </div>
</section>

<section class="sources-section bg-grey border-bottom-dark">
    <div class="container">
        <div class="post-panel">
            <h2 class="h3"><?= get_field('learn_blog_title') ?></h2>
            <div class="post-slider post-light">
                <?php
                    $blog_load_more_link = '';

                    $more_link = get_category_link(get_field('learn_blog_more_link'));

                    if (get_field('learn_blog_show_most_recent') == 'Yes') {

                        $learn_blog_cat_obj = get_field('learn_blog_category');

                        $learn_blog_cat = $learn_blog_cat_obj->term_id;

                        $got_posts = get_posts(['numberposts' => 3, 'category' => $learn_blog_cat]);

                        if ($got_posts) {
                            foreach ($got_posts as $post) : setup_postdata($post);
                                ?>
                <div class="post-slide">
                    <div class="post-item">
                        <a href="<?= get_permalink($post->ID) ?>" class="img-block"
                            style="background-image: url('<?= get_the_post_thumbnail_url($post->ID, 'medium') ?>');"></a>
                        <div class="post-txt">
                            <a href="<?= get_permalink($post->ID) ?>" class="txt">
                                <h4 class="post-title"><?= $post->post_title ?>
                                </h4>
                                <span
                                    class="post-author"><?= get_the_author_meta('display_name', $post->post_author); ?></span>

                                <?php $post_date_no_time = explode(' ', $post->post_date_gmt)[0];
                                                $post_formatted_date_arr = explode('-', $post_date_no_time);
                                                $dateObj = DateTime::createFromFormat('!m', $post_formatted_date_arr[1]);
                                                $month = $dateObj->format('F');
                                                $post_formatted_date = $post_formatted_date_arr[2] . ' ' . $month . ' ' . $post_formatted_date_arr[0]; ?>

                                <span class="post-date">Posted <time
                                        datetime="<?= $post_date_no_time ?>"><?= $post_formatted_date ?></time></span>
                            </a>
                            <?php $blog_load_more_link = get_category_link($learn_blog_cat); ?>
                            <a href="<?= $blog_load_more_link ?>"
                                class="post-category xs-mobile"><?= $learn_blog_cat_obj->name ?></a>
                        </div>
                    </div>
                </div>
                <?php endforeach;
                            wp_reset_postdata();
                        }
                    } else {

                        for ($i = 1; $i < 4; $i++) {
                            $post = get_field('learn_blog_post_' . $i);
                            if ($post && $post != null) {
                                ?>

                <div class="post-slide">
                    <div class="post-item">
                        <a href="<?= get_permalink($post->ID) ?>" class="img-block"
                            style="background-image: url('<?= get_the_post_thumbnail_url($post->ID, 'medium') ?>');"></a>
                        <div class="post-txt">
                            <a href="<?= get_permalink($post->ID) ?>" class="txt">
                                <h4 class="post-title"><?= $post->post_title ?>
                                </h4>
                                <span
                                    class="post-author"><?= get_the_author_meta('display_name', $post->post_author); ?></span>

                                <?php $post_date_no_time = explode(' ', $post->post_date_gmt)[0];
                                                $post_formatted_date_arr = explode('-', $post_date_no_time);
                                                $dateObj = DateTime::createFromFormat('!m', $post_formatted_date_arr[1]);
                                                $month = $dateObj->format('F');
                                                $post_formatted_date = $post_formatted_date_arr[2] . ' ' . $month . ' ' . $post_formatted_date_arr[0]; ?>

                                <span class="post-date">Posted <time
                                        datetime="<?= $post_date_no_time ?>"><?= $post_formatted_date ?></time></span>
                            </a>
                            <?php $post_cat = get_the_category($post->ID)[0]; ?>
                            <?php $blog_load_more_link = get_category_link($post_cat->term_id); ?>
                            <a href="<?= $blog_load_more_link ?>"
                                class="post-category xs-mobile"><?= $post_cat->name ?></a>
                        </div>
                    </div>
                </div>

                <?php wp_reset_postdata();
                            }
                        }
                    } ?>
            </div>
            <div class="btn-position-top">
                <?php if (get_field('learn_blog_show_most_recent') == 'Yes') { ?>
                <a href="<?= $blog_load_more_link ?>" class="btn">Load more articles</a>
                <?php } else {
                        if ($more_link && trim($more_link) != '') { ?>
                <a href="<?= $more_link ?>" class="btn">Load more articles</a>
                <?php }
                    } ?>
            </div>
        </div>
    </div>
</section>

<section class="guidance-section bg-dark">
    <div class="container">
        <div class="section-title">
            <h3><?= get_field('cards_section_title') ?></h3>
            <p><?= get_field('cards_section_subtitle') ?></p>
        </div>
        <ul class="guidance-cards">
            <li>
                <a href="<?= get_field('left_card_link') ?>" class="guidance-cont bg-orange">
                    <h3 class="guidance-title"><?= get_field('left_card_title') ?></h3>
                    <p class="guidance-txt"><?= get_field('left_card_subtitle') ?></p>
                </a>
            </li>
            <li>
                <a href="<?= get_field('right_card_link') ?>" class="guidance-cont bg-green">
                    <h3 class="guidance-title"><?= get_field('right_card_title') ?></h3>
                    <p class="guidance-txt"><?= get_field('right_card_subtitle') ?></p>
                </a>
            </li>
        </ul>
    </div>
</section>

<?php if (get_field('display_bottom_cta') != 'No') { ?>
<section class="cta-section border-top-dark bg-grey">
    <div class="container">
        <div class="cta-block">
            <p><?= get_field('bottom_cta_description') ?></p>
            <div class="btn-center-holder">
                <a href="<?= get_field('bottom_cta_destination') ?>" class="btn"><?= get_field('bottom_cta_text') ?></a>
            </div>
        </div>
    </div>
</section>
<?php } ?>

<?php get_footer(); ?>