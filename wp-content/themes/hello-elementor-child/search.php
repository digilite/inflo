<?php
get_header();
global $wp_query;
?>

<section class="search-page-section">
  <div class="container">
      <h1 class="search-title"> <?php echo $wp_query->found_posts; ?>
        <?php _e( 'Search Results Found For', 'locale' ); ?>: "<?php the_search_query(); ?>" </h1>

        <?php if ( have_posts() ) { ?>

            <ul>

            <?php while ( have_posts() ) { the_post(); ?>

               <li>
                 <h3><a href="<?php echo get_permalink(); ?>">
                   <?php the_title();  ?>
                 </a></h3>
                 <div class="img-link-holder">
                   <a class="img-link" href="<?php echo get_permalink(); ?>">
                    <?php  the_post_thumbnail('medium') ?>
                   </a>
                 </div>
                 <?php echo substr(get_the_excerpt(), 0,200); ?>
                 <div class="h-readmore"> <a href="<?php the_permalink(); ?>">Read More</a></div>
               </li>

            <?php } ?>

            </ul>

           <?php echo paginate_links(); ?>

        <?php } ?>
    
  </div>
</section>
<?php get_footer(); ?>