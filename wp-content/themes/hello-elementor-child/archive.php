<?php /**
 * The template for displaying archive pages
 * Template Name: Inflo Blog */ ?>

<?php get_header(); 
global $wp; 
$category = get_queried_object();
//print_r($category);
$this_cat_link_unchanged = get_category_link( $category->term_id );
$this_cat_link = $this_cat_link_unchanged;//str_replace('/blog', '', $this_cat_link_unchanged);

$hero_post = get_field('hero_post_of_this_category', $category);
?>

<section class="blog-listing-intro-section bg-light">
	<div class="container">
		<div class="section-title">
			<h1 class="h2"><?= $category->name ?></h1>
			<?= get_the_archive_description() ?>
			<?php get_search_form(); ?>
		</div>
	</div>
</section>
<section class="blog-listing-section bg-grey border-bottom-dark">
	<div class="filter-holder">
		<div class="container">
			<ul class="filter">
				<?php $blog_categories = get_categories(
				    array( /*'parent' => 28,*/ 'hide_empty' => true )
				);
				
				foreach($blog_categories as $cat) { 
					if(get_field('show_category_on_blog', $cat) == 'Yes'){
				?>
				
				<li <?php if(strpos($wp->request, $cat->slug) > 0){ ?>class="active"<?php } ?>><a href="<?= /*str_replace('/blog', '',*/ get_category_link( $cat->term_id )//) ?>"><?= $cat->name ?></a></li>
				
				<?php } } ?>
			</ul>
		</div>
	</div>

	<?php
	$paged = $_GET['page'] ? $_GET['page'] : 1;
	$args = array(
	    'posts_per_page' => 8,
	    'orderby'   => array(
	      'date' =>'DESC'	      
	     ),
	    'paged'=>$paged,
	    'post_type' => 'post',
	    'cat' => $category->term_id,
	    //'post__not_in' => array(278)
	    );
	if($hero_post != null && $hero_post != ''){
		$args['post__not_in'] = [$hero_post->ID];
	} else {
		$hero_post = get_posts( ['numberposts' => 1, 'category' => $category->term_id] )[0];
		$args['post__not_in'] = [$hero_post->ID];
	}
	$res = new WP_Query($args);
	$posts = $res->posts;
	if(count($posts) == 0){ ?>

		<?php if($hero_post){ ?>
			<div class="container">
				<div class="post-list post-light">
					<div class="post-grid">
						<div class="col col-lg">
							<?php if($hero_post) {  ?>
							<div class="post-item">
								<a href="<?= get_permalink($hero_post->ID) ?>" class="img-block"
									style="background-image: url('<?= get_the_post_thumbnail_url($hero_post->ID, 'medium_large') ?>');"></a>
								<div class="post-txt">
									<a href="<?= get_permalink($hero_post->ID) ?>" class="txt">
										<h4 class="post-title"><?= $hero_post->post_title ?>
										</h4>
										<span class="post-author"><?= get_the_author_meta('display_name', $hero_post->post_author); ?></span>
										<?php $post_date_no_time = explode(' ', $hero_post->post_date_gmt)[0]; 
										$post_formatted_date_arr = explode('-', $post_date_no_time);
										$dateObj = DateTime::createFromFormat('!m', $post_formatted_date_arr[1]);
										$month = $dateObj->format('F');
										$post_formatted_date = $post_formatted_date_arr[2].' '.$month.' '.$post_formatted_date_arr[0]; ?>
										<span class="post-date">Posted <time datetime="<?= $post_date_no_time ?>"><?= $post_formatted_date ?></time></span>
									</a>
									<a href="<?= $this_cat_link ?>" class="post-category xs-mobile"><?= $category->name ?></a>
								</div>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		<?php } else { ?>
		<div class="container" style="text-align: center;">
			No posts in this category
		</div>


	<?php }} else {

	?>

	<div class="container">
		<div class="post-list post-light">
			<div class="post-grid">
				<div class="col col-lg">
					<?php if($hero_post) {  ?>
					<div class="post-item">
						<a href="<?= get_permalink($hero_post->ID) ?>" class="img-block"
							style="background-image: url('<?= get_the_post_thumbnail_url($hero_post->ID, 'medium_large') ?>');"></a>
						<div class="post-txt">
							<a href="<?= get_permalink($hero_post->ID) ?>" class="txt">
								<h4 class="post-title"><?= $hero_post->post_title ?>
								</h4>
								<span class="post-author"><?= get_the_author_meta('display_name', $hero_post->post_author); ?></span>
								<?php $post_date_no_time = explode(' ', $hero_post->post_date_gmt)[0]; 
								$post_formatted_date_arr = explode('-', $post_date_no_time);
								$dateObj = DateTime::createFromFormat('!m', $post_formatted_date_arr[1]);
								$month = $dateObj->format('F');
								$post_formatted_date = $post_formatted_date_arr[2].' '.$month.' '.$post_formatted_date_arr[0]; ?>
								<span class="post-date">Posted <time datetime="<?= $post_date_no_time ?>"><?= $post_formatted_date ?></time></span>
							</a>
							<a href="<?= $this_cat_link ?>" class="post-category xs-mobile"><?= $category->name ?></a>
						</div>
					</div>
					<?php } ?>
				</div>
				<div class="col">
					<?php if(isset($posts[0])) {  ?>
					<div class="post-item">
						<a href="<?= get_permalink($posts[0]->ID) ?>" class="img-block"
							style="background-image: url('<?= get_the_post_thumbnail_url($posts[0]->ID, 'medium') ?>');"></a>
						<div class="post-txt">
							<a href="<?= get_permalink($posts[0]->ID) ?>" class="txt">
								<h4 class="post-title"><?= $posts[0]->post_title ?>
								</h4>
								<span class="post-author"><?= get_the_author_meta('display_name', $posts[0]->post_author); ?></span>
								<?php $post_date_no_time = explode(' ', $posts[0]->post_date_gmt)[0]; 
								$post_formatted_date_arr = explode('-', $post_date_no_time);
								$dateObj = DateTime::createFromFormat('!m', $post_formatted_date_arr[1]);
								$month = $dateObj->format('F');
								$post_formatted_date = $post_formatted_date_arr[2].' '.$month.' '.$post_formatted_date_arr[0]; ?>
								<span class="post-date">Posted <time datetime="<?= $post_date_no_time ?>"><?= $post_formatted_date ?></time></span>
							</a>
							<a href="<?= $this_cat_link ?>" class="post-category xs-mobile"><?= $category->name ?></a>
						</div>
					</div>
					<?php } ?>
					<?php if(isset($posts[1])) { ?>
					<div class="post-item">
						<a href="<?= get_permalink($posts[1]->ID) ?>" class="img-block"
							style="background-image: url('<?= get_the_post_thumbnail_url($posts[1]->ID, 'medium') ?>');"></a>
						<div class="post-txt">
							<a href="<?= get_permalink($posts[1]->ID) ?>" class="txt">
								<h4 class="post-title"><?= $posts[1]->post_title ?>
								</h4>
								<span class="post-author"><?= get_the_author_meta('display_name', $posts[1]->post_author); ?></span>
								<?php $post_date_no_time = explode(' ', $posts[1]->post_date_gmt)[0]; 
								$post_formatted_date_arr = explode('-', $post_date_no_time);
								$dateObj = DateTime::createFromFormat('!m', $post_formatted_date_arr[1]);
								$month = $dateObj->format('F');
								$post_formatted_date = $post_formatted_date_arr[2].' '.$month.' '.$post_formatted_date_arr[0]; ?>
								<span class="post-date">Posted <time datetime="<?= $post_date_no_time ?>"><?= $post_formatted_date ?></time></span>
							</a>
							<a href="<?= $this_cat_link ?>" class="post-category xs-mobile"><?= $category->name ?></a>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>

			<?php
			$post_number = 0;
			if(count($posts) > 2) {
			foreach($posts as $key=>$post){
				if($key != 0 && $key != 1){					
			?>
			<div class="col">
				<div class="post-item">
					<a href="<?= get_permalink($post->ID) ?>" class="img-block" style="background-image: url('<?= get_the_post_thumbnail_url($post->ID, 'medium') ?>');"></a>
					<div class="post-txt">
						<a href="<?= get_permalink($post->ID) ?>" class="txt">
							<h4 class="post-title"><?= $post->post_title ?>
							</h4>
							<span class="post-author"><?= get_the_author_meta('display_name', $post->post_author); ?></span>
							<?php $post_date_no_time = explode(' ', $post->post_date_gmt)[0]; 
							$post_formatted_date_arr = explode('-', $post_date_no_time);
							$dateObj = DateTime::createFromFormat('!m', $post_formatted_date_arr[1]);
							$month = $dateObj->format('F');
							$post_formatted_date = $post_formatted_date_arr[2].' '.$month.' '.$post_formatted_date_arr[0]; ?>
							<span class="post-date">Posted <time datetime="<?= $post_date_no_time ?>"><?= $post_formatted_date ?></time></span>
						</a>
						<a href="<?= $this_cat_link ?>" class="post-category xs-mobile"><?= $category->name ?></a>
					</div>
				</div>
			</div>
			<?php }
				} wp_reset_postdata();
			} ?>

		</div>

		<?php the_posts_pagination(); ?>

		<div class="pagination">
			<ul>
				<?php $amount_of_pages = ceil($category->category_count / 8);
				if($amount_of_pages < 1){
					$amount_of_pages = 1;
				}
				$prev_page = $paged - 1; 
				if($prev_page < 1){
					$prev_page = 1;
				}
				$next_page = $paged + 1;
				if($next_page > $amount_of_pages){
					$next_page = $amount_of_pages;
				} ?>

				<li class="pagination-previous">
					<a href="<?= $this_cat_link ?>?page=<?= $prev_page ?>">«</a>
				</li>
				
				<?php for($i=1; $i <= $amount_of_pages; $i++) { ?>
				<li <?php if($paged == $i) {echo 'class="active"';} ?>>
					<a href="<?= $this_cat_link ?>?page=<?= $i ?>"><?= $i ?></a>
				</li>
				<?php } ?>

				<li class="pagination-next">
					<a href="<?= $this_cat_link ?>?page=<?= $next_page ?>">»</a>
				</li>
								
			</ul>
		</div>
	</div>
	<?php } ?>
</section>

<section class="guidance-section bg-dark">
	<div class="container">
		<div class="section-title">
			<h3><?= the_field('cards_section_title', $category) ?></h3>
			<p><?= the_field('cards_section_subtitle', $category) ?></p>
		</div>
		<ul class="guidance-cards">
			<li>
				<a href="<?= get_field('left_card_link', $category) /*? get_field('left_card_link', $category) : '/audit/10-best-practices-for-valuable-audit-planning/'*/ ?>" class="guidance-cont bg-orange">
					<h3 class="guidance-title"><?= the_field('left_card_title', $category) ?></h3>
					<p class="guidance-txt"><?= the_field('left_card_subtitle', $category) ?></p>
				</a>
			</li>
			<li>
				<a href="<?= get_field('right_card_link', $category) /*? get_field('right_card_link', $category) : '/audit/top-tips-for-effective-audit-execution/'*/ ?>" class="guidance-cont bg-green">
					<h3 class="guidance-title"><?= the_field('right_card_title', $category) ?></h3>
					<p class="guidance-txt"><?= the_field('right_card_subtitle', $category) ?></p>
				</a>
			</li>
		</ul>
	</div>
</section>

<?php if(get_field('display_bottom_cta') != 'No'){ ?>
<section class="cta-section border-top-dark bg-grey">
	<div class="container">
		<div class="cta-block">
			<p><?= the_field('bottom_cta_description', $category) ?></p>
			<div class="btn-center-holder">
				<a href="<?= the_field('bottom_cta_destination', $category) ?>" class="btn"><?= the_field('bottom_cta_text', $category) ?></a>
			</div>
		</div>
	</div>
</section>
<?php } ?>

<?php get_footer(); ?>