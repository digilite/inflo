<?php /* Template Name: Inflo Thank You */ ?>

<?php get_header(); ?>

<style>
	footer {
		display: none !important;
	}
</style>

<div class="custom-popup popup-open" id="popup1">
	<div class="popup-inner">
		<a href="<?php echo esc_url( home_url() ); ?>" class="close close-popup"></a>
		<div class="thank-you-popup text-center">
			<img src="<?= get_field('thank_you_image')['sizes']['medium'] ?>" alt="Thank you!">
			<h2 class="h3"><?= get_field('thank_you_title') ?></h2>
			<p><?= get_field('thank_you_text') ?></p>
			<div class="btn-block">
				<a href="<?php echo esc_url( home_url() ); ?>" class="btn close-popup">Close</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	jQuery('body').click(function(e){
		e.preventDefault();
		e.stopPropagation();
		window.location.href = '<?php echo esc_url( home_url() ); ?>';
	});
</script>

<?php get_footer(); ?>
