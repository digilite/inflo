<?php /* Template Name: Inflo About */ ?>

<?php get_header(); ?>
    <section class="intro-section secondary-page bg-anim border-bottom-white">
        <div class="container">
            <div class="text-block full-width">
                <h1 class="h2"><?= get_field('header_text') ?></h1>
            </div>
            <div class="three-columns">
                <?php $stat_cols = acf_get_fields_by_id(1468);
                foreach ($stat_cols as $col) {
                    if ($col['name'] != 'header_text') {
                        $the_col = get_field($col['name']); ?>
                        <div class="col">
                            <div class="title-price-block">
                                <strong class="price-title"><?= $the_col['stat_text'] ?></strong>
                                <span class="price-subtitle"><?= $the_col['stat_description'] ?></span>
                            </div>
                        </div>
                    <?php }
                } ?>
            </div>
        </div>
    </section>

    <section class="story-section">
        <div class="container">
            <div class="story-block">
                <div class="story-row reversed-lg">
                    <div class="text-block">
                        <?= get_field('person_layout_text_1') ?>
                    </div>
                    <div class="members-block">
                        <div class="member-item-photo"
                             style="background-image: url(<?= get_field('person_1_photo') ?>);">
                            <div class="hover-block">
                                <h4 class="title"><?= get_field('person_1_name') ?></h4>
                                <span class="subtitle"><?= get_field('person_1_title') ?></span>
                            </div>
                        </div>
                        <div class="member-item-photo"
                             style="background-image: url(<?= the_field('person_2_photo') ?>);">
                            <div class="hover-block">
                                <h4 class="title"><?= get_field('person_2_name') ?></h4>
                                <span class="subtitle"><?= get_field('person_2_title') ?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="colored-title-block">
                    <p><?= get_field('person_layout_green_text') ?></p>
                </div>
                <div class="story-row">
                    <div class="text-block">
                        <?= get_field('person_layout_text_2') ?>
                    </div>
                    <div class="members-block">
                        <div class="member-item-photo"
                             style="background-image: url(<?= get_field('person_3_photo') ?>);">
                            <div class="hover-block">
                                <h4 class="title"><?= get_field('person_3_name') ?></h4>
                                <span class="subtitle"><?= get_field('person_3_title') ?></span>
                            </div>
                        </div>
                        <div class="member-item-photo"
                             style="background-image: url(<?= get_field('person_4_photo') ?>);">
                            <div class="hover-block">
                                <h4 class="title"><?= get_field('person_4_name') ?></h4>
                                <span class="subtitle"><?= get_field('person_4_title') ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="story-section-title">
                <strong class="logo">
                    <img src="https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/logo-lg.png" alt="Inflo" width="245">
                </strong>
            </div>
        </div>
    </section>

    <section class="how-works-section bg-grey border-top-white">
        <div class="container">
            <?= get_field('text_before_products') ?>
            <div class="feature-card-holder">
                <div class="featured-card-list">
                    <?php $how_products = acf_get_fields_by_id(4652);
                    foreach ($how_products as $item) {
                        if ($item['name'] != 'text_before_products' && $item['name'] != 'graphic_element_after_products' && $item['name'] != 'text_after_products') {
                            $the_item = get_field($item['name']); ?>

                            <div class="featured-card">
                                <div class="main-block">
                                    <img src="<?= get_the_post_thumbnail_url($the_item->ID, 'full') ?>">
                                    <span><?= $the_item->post_title ?></span>
                                </div>
                                <div class="info-block">
                                    <p><?= $the_item->post_excerpt ?></p>
                                    <div class="text-center">
                                        <a href="<?= get_permalink($the_item->ID) ?>">Read more <i
                                                    class="fas fa-angle-right"></i>
                                        </a></div>
                                </div>
                            </div>

                        <?php }
                    } ?>
                </div>
            </div>
            <div class="graphic-holder">
                <img src="<?= get_field('graphic_element_after_products') ?>">
            </div>
            <?= get_field('text_after_products') ?>
        </div>
    </section>

    <section class="member-section bg-dark-grey border-top-light border-bottom-light">
        <div class="container">
            <div class="map-row">
                <div class="text-block">
                    <p><?= get_field('text_near_map') ?></p>
                </div>
                <div class="map-block">
                    <div class="map-holder">
                        <img src="https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/img-01.png" alt="Map">
                        <ul class="pin-list">
                            <li style="top: 39%; left: 18%">
                                <span class="pin-title">Inflo USA</span>
                            </li>
                            <li style="top: 33%; left: 45%">
                                <span class="pin-title">Inflo Europe</span>
                            </li>
                            <li style="top: 79%; left: 83%">
                                <span class="pin-title">Inflo Australia</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="member-holder">
                <h2 class="h3">Leadership team</h2>
                <div class="member-list">

                    <?php $leadership_members = acf_get_fields_by_id(1500);
                    if ($leadership_members) {
                        foreach ($leadership_members as $member) {
                            $the_member = get_field($member['name']); ?>

                            <div class="member-item">
                                <a href="#<?= $member['name'] ?>_popup" class="member-inner popup-opener">
                                    <div class="member-img"
                                         style="background-image: url(<?= $the_member['leadership_team_member_photo'] ?>);"></div>
                                    <div class="member-info">
                                        <h4 class="member-name"><?= $the_member['leadership_team_member_name'] ?></h4>
                                        <span class="member-position"><?= $the_member['leadership_team_member_position'] ?></span>
                                        <span class="bio-link">Read BIO</span>
                                    </div>
                                </a>
                            </div>

                            <div class="custom-popup" id="<?= $member['name'] ?>_popup">
                                <div class="popup-inner">
                                    <a href="#" class="close close-popup"></a>
                                    <div class="member-popup">
                                        <div class="memebr-top-block">
                                            <div class="member-img"
                                                 style="background-image: url(<?= $the_member['leadership_team_member_photo'] ?>);"></div>
                                            <div class="member-title-info">
                                                <h3 class="member-name"><?= $the_member['leadership_team_member_name'] ?></h3>
                                                <span class="member-position"><?= $the_member['leadership_team_member_position'] ?></span>
                                                <ul class="network-list">
                                                    <li>
                                                        <a href="<?= $the_member['leadership_team_member_linkedin'] ?>">
                                                            <i class="fab fa-linkedin-in"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="member-info-block">
                                            <?= $the_member['leadership_team_member_bio'] ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }
                    } ?>
                </div>
            </div>
        </div>
    </section>

    <section class="career-preview-section bg-grey border-bottom-white">
        <div class="container">
            <div class="section-title text-center">
                <h2 class="h4"><?= get_field('about_career_section_heading') ?></h2>
            </div>
            <div class="career-block-holder">
                <div class="testimonial-item career-block">
                    <div class="img-block bg-orange">
                        <img src="<?= get_field('about_career_image') ?>" alt="Inflo">
                    </div>
                    <div class="text-block">
                        <h3><?= get_field('about_career_heading') ?></h3>
                        <?= get_field('about_career_description') ?>
                        <div class="secondary-text-holder">
                            <p><?= get_field('about_career_gray_text') ?></p>
                        </div>
                        <a href="<?= get_field('about_career_cta_destination') ?>"
                           class="btn"><?= get_field('about_career_cta_text') ?></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php if (get_field('text_area_after_testimonial') != null && get_field('text_area_after_testimonial') != '') { ?>
    <section class="section-collapse accountants-business-pretestimonial bg-white border-bottom-light-secondary">
        <div class="container container-sm">
            <?= get_field('text_area_after_testimonial') ?>
        </div>
    </section>
<?php } ?>

<?php if (get_field('solutions_testimonial_name') != null && get_field('solutions_testimonial_name') != '') { ?>
    <section class="single-testimonial-section bg-light border-bottom-white">
        <div class="container container-sm">
            <div class="single-testimonial-block">
                <div class="testimonial-item">
                    <div class="text-block">
                        <div class="author-block">
                            <div class="author-name-holder">
                                <div class="author-img"
                                     style="background-image: url('<?= the_field('solutions_testimonial_avatar') ?>');"></div>
                                <div class="author-descr">
                                    <h5 class="author-name"><?= get_field('solutions_testimonial_name') ?></h5>
                                    <p><?= get_field('solutions_testimonial_title') ?></p>
                                </div>
                            </div>
                            <img src="https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/icon-50.png" class="quote-img">
                            <p><?= get_field('solutions_testimonial_quote') ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
<?php } ?>

    <section class="partners-section">
        <div class="container">
            <h2 class="h4"><?= get_field("partners_text") ?></h2>
            <ul class="partners-list">
                <?php $partner_icons = acf_get_fields_by_id(1308);
                foreach ($partner_icons as $icon) {
                    if ($icon['name'] != 'partners_text') { ?>
                        <?php $the_image = get_field($icon['name']);
                        $partner_link = $the_image['partner_link'];
                        if (trim($partner_link) == '') {
                            $partner_link = '/customer-success';
                        }
                        ?>
                        <li>
                            <a href="<?= $partner_link ?>">
                                <img src="<?= $the_image['partner_image'] ?>">
                            </a>
                        </li>
                    <?php }
                } ?>
            </ul>
        </div>
    </section>

<?php if (get_field('display_bottom_cta') != 'No') { ?>
    <div class="container">
        <div class="cta-block">
            <p><?= get_field('bottom_cta_description') ?></p>
            <div class="btn-center-holder">
                <a href="<?= get_field('bottom_cta_destination') ?>" class="btn"><?= get_field('bottom_cta_text') ?></a>
            </div>
        </div>
    </div>
<?php } ?>

<?php get_footer(); ?>
