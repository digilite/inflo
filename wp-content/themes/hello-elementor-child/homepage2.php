<?php /* Template Name: Inflo Home ACF */ ?>

<?php get_header(); ?>

<section class="intro-section bg-anim border-bottom-white">
	<?php $which_header = get_field( "choose_header" );
	if($which_header == "title_1") { ?>
	<div class="container">
		<div class="text-block">
			<h1><?= get_field("title_1") ?></h1>
			<?php if(trim(get_field('subtitle_1')) != '') { ?>
			<p class="intro-note"><?= get_field("subtitle_1") ?></p>
			<?php } ?>
			<?php if(trim(get_field('paragraph_1')) != '') { ?>
			<p><?= get_field("paragraph_1") ?></p>
			<?php } ?>
			<div class="intro-btn-holder">
				<?php if(get_field('primary_cta_text_1') != null && get_field('primary_cta_text_1') != '') { ?>
				<a href="<?= get_field("primary_cta_destination_1") ?>" class="btn"><?= get_field("primary_cta_text_1") ?></a>
				<?php } ?>
				<?php if(get_field('secondary_cta_text_1') != null && get_field('secondary_cta_text_1') != '') { ?>
				<?php if(get_field('secondary_cta_destination_1') != null && get_field('secondary_cta_destination_1') != '') { ?>
				<a href="<?= get_field('secondary_cta_destination_1') ?>" class="btn btn-white"><?= get_field("secondary_cta_text_1") ?></a>
				<?php } else { ?>
				<a href="#digital_audit_popup" class="btn btn-white popup-opener"><?= get_field("secondary_cta_text_1") ?></a>
				<?php } } ?>
			</div>
		</div>
	</div>
	<div class="parallax-img-block double-parallax">
		<div class="base-img">
			<img src="<?= get_field("hero_image_1")['sizes']['large'] ?>">
		</div>
		<div class="over-img">
			<img  src="<?= get_field("hero_image_secondary_1")['sizes']['large'] ?>">
		</div>
	</div>
	<?php } else if($which_header == "title_2") { ?>
	<div class="container">
		<div class="text-block">
			<h1><?= get_field("title_2") ?></h1>
			<?php if(trim(get_field('subtitle_2')) != '') { ?>
			<p class="intro-note"><?= get_field("subtitle_2") ?></p>
			<?php } ?>
			<?php if(trim(get_field('paragraph_2')) != '') { ?>
			<p><?= get_field("paragraph_2") ?></p>
			<?php } ?>
			<div class="intro-btn-holder">
				<?php if(get_field('primary_cta_text_2') != null && get_field('primary_cta_text_2') != '') { ?>
				<a href="<?= get_field("primary_cta_destination_2") ?>" class="btn"><?= get_field("primary_cta_text_2") ?></a>
				<?php } ?>
				<?php if(get_field('secondary_cta_text_2') != null && get_field('secondary_cta_text_2') != '') { ?>
				<?php if(get_field('secondary_cta_destination_2') != null && get_field('secondary_cta_destination_2') != '') { ?>
				<a href="<?= get_field('secondary_cta_destination_2') ?>" class="btn btn-white"><?= get_field("secondary_cta_text_2") ?></a>
				<?php } else { ?>
				<a href="#digital_audit_popup" class="btn btn-white popup-opener"><?= get_field("secondary_cta_text_2") ?></a>
				<?php } } ?>
			</div>
		</div>
	</div>
	<div class="parallax-img-block">
		<img  src="<?= get_field("hero_image_2")['sizes']['large'] ?>">
	</div>
	<?php } else if($which_header == "title_3") { ?>
	<div class="container">
		<div class="text-block">
			<h1><?= get_field("title_3") ?></h1>
			<?php if(trim(get_field('subtitle_3')) != '') { ?>
			<p class="intro-note"><?= get_field("subtitle_3") ?></p>
			<?php } ?>
			<?php if(trim(get_field('paragraph_3')) != '') { ?>
			<p><?= get_field("paragraph_3") ?></p>
			<?php } ?>
			<div class="intro-btn-holder">
				<?php if(get_field('primary_cta_text_3') != null && get_field('primary_cta_text_3') != '') { ?>
				<a href="<?= get_field("primary_cta_destination_3") ?>" class="btn"><?= get_field("primary_cta_text_3") ?></a>
				<?php } ?>
				<?php if(get_field('secondary_cta_text_3') != null && get_field('secondary_cta_text_3') != '') { ?>
				<?php if(get_field('secondary_cta_destination_3') != null && get_field('secondary_cta_destination_3') != '') { ?>
				<a href="<?= get_field('secondary_cta_destination_3') ?>" class="btn btn-white"><?= get_field("secondary_cta_text_3") ?></a>
				<?php } else { ?>
				<a href="#digital_audit_popup" class="btn btn-white popup-opener"><?= get_field("secondary_cta_text_3") ?></a>
				<?php } } ?>
			</div>
		</div>
	</div>
	<div class="parallax-img-block">
		<img  src="<?= get_field("hero_image_3")['sizes']['large'] ?>">
	</div>
	<?php } ?>
</section>

<section class="default-section">
	<div class="container">
		<!--<ul class="guidance-cards guidance-cards-wide">
			<li>
				<a href="<?= get_field('left_card_link') ?>" class="guidance-cont bg-orange">
					<h3 class="guidance-title guidance-title-img-holder">
						<img loading="lazy" src="<?= get_field('left_card_icon')['sizes']['medium_large'] ?>" alt="city" class="guidance-title-img">
						<span class="guidance-title-img-text"><?= get_field('left_card_title') ?></span>
					</h3>
				</a>
			</li>
			<li>
				<a href="<?= get_field('right_card_link') ?>" class="guidance-cont bg-green">
					<h3 class="guidance-title guidance-title-img-holder">
						<img loading="lazy" src="<?= get_field('right_card_icon')['sizes']['medium_large'] ?>" alt="collaboration" class="guidance-title-img">
						<span class="guidance-title-img-text"><?= get_field('right_card_title') ?></span>
					</h3>
				</a>
			</li>
		</ul>-->
		<div class="three-columns homepage-numbers">
			<?php $stat_cols = acf_get_fields_by_id(1468);
			foreach($stat_cols as $col) { if($col['name'] != 'header_text') { $the_col = get_field($col['name']); ?>
			<div class="col">
				<div class="title-price-block">
					<strong class="price-title anim-num"><?= $the_col['stat_text'] ?></strong>
					<span class="price-subtitle"><?= $the_col['stat_description'] ?></span>
				</div>
			</div>
			<?php } else { if(get_field('header_text') != null && get_field('header_text') != '') { ?>
				<div class="text-block full-width">
					<h1 class="h2"><?= get_field('header_text') ?></h1>
				</div>
			<?php }}} ?>
		</div>
	</div>
</section>

<section class="partners-section">
	<div class="container">
		<h2 class="h4"><?= get_field("partners_text") ?></h2>
		<ul class="partners-list">
			<?php $partner_icons = acf_get_fields_by_id(1308);
			foreach($partner_icons as $icon) { if($icon['name'] != 'partners_text') { ?>
			<?php $the_image = get_field($icon['name']);
				$partner_link = $the_image['partner_link'];
				if(trim($partner_link) == '') {
					$partner_link = '/'.ICL_LANGUAGE_CODE.'/customer-success';
				}
			?>
			<li>
				<a href="<?= $partner_link ?>">
					<img loading="lazy" src="<?= $the_image['partner_image']['sizes']['medium'] ?>">
				</a>
			</li>
			<?php }} ?>
		</ul>
	</div>
</section>

<section class="features-section bg-grey border-top-white border-bottom-white border-top-img-section border-bottom-decor-section">
	<div class="anim-bg-lin"> <!--animat-->
		<div class="container">
			<div class="features-slider">
				<?php $features_slider = acf_get_fields_by_id(1323);
				foreach($features_slider as $slide) {
				$the_slide = get_field($slide['name']); ?>

				<div class="features-slide">
					<div class="features-item">
						<div class="slide-text-block">
							<img loading="lazy" src="<?= $the_slide['icon']['sizes']['medium_large'] ?>" class="features-item-icon">
							<h3 class="h2"><?= $the_slide['title'] ?></h3>
							<p><?= $the_slide['description'] ?></p>
							<?php if($the_slide['cta_text'] != null && $the_slide['cta_text'] != '') { ?>
							<a href="<?= $the_slide['cta_destination'] ?>" class="btn"><?= $the_slide['cta_text'] ?></a>
							<?php } ?>
						</div>
					</div>
					<div class="slide-image-block">
						<img loading="lazy" src="<?= $the_slide['slide_image']['sizes']['medium_large'] ?>">
					</div>
				</div>

				<?php } ?>
			</div>
		</div>
		<div class="anim-lin"></div><!--animat-->
	</div>
	<div class="container">
		<div class="features-block">
			<h3 class="h4">And so much more...</h3>
			<div class="show-more-block">
				<div class="features-holder">
					<?php $features_items = acf_get_fields_by_id(1347); //print_r($features_items);
					 foreach($features_items as $item) { $the_item = get_field($item['name']);
					 if($the_item['title'] != null && $the_item['title'] != '') { ?>
					<div class="features-item show-more-item">
						<img loading="lazy" src="<?= $the_item['icon']['sizes']['medium_large'] ?>" class="features-item-icon">
						<h3 class="h2"><?= $the_item['title'] ?></h3>
						<p><?= $the_item['description'] ?></p>
						<?php if($the_item['cta_text'] != null && $the_item['cta_text'] != '') { ?>
						<a href="<?= $the_item['cta_destination'] ?>" class="btn btn-outline"><?= $the_item['cta_text'] ?></a>
						<?php } ?>
					</div>
					<?php }} ?>
				</div>
				<div class="btn-center-holder">
					<a href="#" class="btn show-more-btn">Discover more features</a>
				</div>
			</div>
		</div>
	</div>
	<!-- <img src="https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/logo-grey-lg.png" alt="Inflo" class="border-img"> -->
	<span class="border-decor"></span>
</section>

<section class="simple-about-section">
	<div class="container">
		<div class="section-title">
			<h3><?= get_field('about_title') ?></h3>
		</div>
		<div class="two-col-holder">
			<div class="col">
				<img loading="lazy" src="<?= get_field('about_image')['sizes']['medium_large'] ?>" class="content-img animate-left-to-right">
			</div>
			<div class="col">
				<?= get_field('about_half-section_description') ?>
				<?php if(get_field('about_cta_text') != null && get_field('about_cta_text') != '') { ?>
				<a href="<?= get_field('about_cta_link') ?>" class="btn"><?= get_field('about_cta_text') ?></a>
				<?php } ?>
			</div>
		</div>
		<div class="simple-about-descr">
			<?= get_field('about_wide_description') ?>
		</div>
	</div>
</section>

<section class="why-section">
	<div class="container">
		<div class="video-popup-block">
			<h2><?= get_field('title_above') ?></h2>
			<div class="video-popup-holder">
				<?php if(get_field('cover_image') != null && get_field('cover_image') != '') { ?>
					<a data-fancybox href="<?= get_field('video_link') ?>" class="video-popup-opener">
						<img loading="lazy" src="<?= get_field('cover_image')['sizes']['medium_large'] ?>">
					</a>
					<?php } else { ?>

						<?php
							$additional_str_start = '?';
							if(strrpos(get_field('video_link'), '?') !== false){
								$additional_str_start = '&';
							}
						?>

						<a data-fancybox href="<?= get_field('video_link') ?>" class="video-popup-opener iframe-holder">
							<iframe loading="lazy" src="<?= the_field('video_link') . $additional_str_start ?>title=0&byline=0&portrait=0&sidedock=0&controls=0" width="100%" height="auto" frameborder="0"></iframe>
						</a>

					<?php } ?>

			</div>
			<div class="video-note-row">
				<div class="video-note">
					<h5><?= get_field('title_under') ?></h5>
					<p><?= get_field('text_under') ?></p>
				</div>
				<?php if(get_field('cta_tel') != null && get_field('cta_tel') != '') { ?>
				<div class="video-note-btn">
					<a href="<?= get_field('cta_tel') ?>" class="btn"><?= get_field('cta_tel_text') ?></a>
				</div>
				<?php } ?>
			</div>
		</div>
		<div class="benefits-block">
			<div class="benefits-text-block-holder">
				<div class="benefits-text-block">
					<strong class="h6 subtitle"><?= get_field('why_title') ?></strong>
					<h2><?= get_field('why_section_title') ?></h2>
					<p><?= get_field('why_section_text') ?></p>
				</div>
				<?php if(get_field('why_image') != null && get_field('why_image') != '') { ?>
				<div class="benefits-img-block">
					<img loading="lazy" src="<?= get_field('why_image')['sizes']['medium_large'] ?>" class="content-img transparent-img animate-right-to-left">
				</div>
				<?php } ?>
			</div>
			 <?php $benefit1 = get_field('why_section_benefit_1');
			$benefit2 = get_field('why_section_benefit_2');
			$benefit3 = get_field('why_section_benefit_3'); ?>
			<div class="benefits-holder">
				<div class="features-item">
					<img loading="lazy" src="<?= $benefit1['why_benefit_icon_1']['sizes']['medium_large'] ?>" class="features-item-icon">
					<h3 class="h2"><?= $benefit1['why_benefit_1_title'] ?></h3>
					<p><?= $benefit1['why_benefit_1_description'] ?></p>
					<?php if($benefit1['why_benefit_1_cta'] != null && $benefit1['why_benefit_1_cta'] != '') { ?>
					<a href="<?= $benefit1['why_benefit_1_cta_url'] ?>" class="btn btn-outline"><?= $benefit1['why_benefit_1_cta'] ?></a>
					<?php } ?>
				</div>
				<div class="features-item">
					<img loading="lazy" src="<?= $benefit2['why_benefit_icon_2']['sizes']['medium_large'] ?>" class="features-item-icon">
					<h3 class="h2"><?= $benefit2['why_benefit_2_title'] ?></h3>
					<p><?= $benefit2['why_benefit_2_description'] ?></p>
					<?php if($benefit2['why_benefit_2_cta'] != null && $benefit2['why_benefit_2_cta'] != '') { ?>
					<a href="<?= $benefit2['why_benefit_2_cta_url'] ?>" class="btn btn-outline"><?= $benefit2['why_benefit_2_cta'] ?></a>
					<?php } ?>
				</div>
				<div class="features-item">
					<img loading="lazy" src="<?= $benefit3['why_benefit_icon_3']['sizes']['medium_large'] ?>" class="features-item-icon">
					<h3 class="h2"><?= $benefit3['why_benefit_3_title'] ?></h3>
					<p><?= $benefit3['why_benefit_3_description'] ?></p>
					<?php if($benefit3['why_benefit_3_cta'] != null && $benefit3['why_benefit_3_cta'] != '') { ?>
					<a href="<?= $benefit3['why_benefit_3_cta_url'] ?>" class="btn btn-outline"><?= $benefit3['why_benefit_3_cta'] ?></a>
					<?php } ?>
				</div>
			</div>
			<?php if(get_field('why_section_cta_text') != null && get_field('why_section_cta_text') != '') { ?>
			<div class="btn-center-holder">
				<a href="<?= get_field('why_section_cta_destination') ?>" class="btn btn-lg"><?= get_field('why_section_cta_text') ?></a>
			</div>
			<?php } ?>
		</div>
	</div>
</section>

<section class="testimonials-section bg-grey border-top-white border-bottom-dark">
	<?php $use_case_id = get_field('testimonial_from_use_case'); ?>
	<div class="container">
		<div class="single-testimonial-block">
			<div class="testimonial-item">
				<div class="text-block">
					<div class="author-block">
						<div class="author-name-holder">
							<div class="author-img lazy-bg" data-src="<?= get_field('testimonial_avatar', $use_case_id)['sizes']['thumbnail'] ?>" ></div>
							<div class="author-descr">
								<h5 class="author-name"><?= get_field('testimonial_name', $use_case_id) ?></h5>
								<p><?= get_field('testimonial_job_title', $use_case_id) ?></p>
							</div>
						</div>
						<img loading="lazy" src="https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/icon-50.png" class="quote-img">
						<p><?= get_field('testimonial_quote', $use_case_id) ?></p>
					</div>
				</div>
				<div class="img-block lazy-bg" data-src="<?= get_field('testimonial_side_image')['sizes']['medium'] ?>" >
				</div>
			</div>
		</div>
		<h2><?= get_field('testimonial_slider_title') ?></h2>
	</div>
	<div class="testimonials-slider">

		<?php $testimonial_slides = acf_get_fields_by_id(1415);
			foreach($testimonial_slides as $item) { $the_item = get_field($item['name']);
			if($item['name'] != 'testimonial_slider_title' && $the_item['show_testimonial_slide'] != 'No') {

			?>

		<div class="testimonial-slide">
			<div class="testimonial-item">

					<?php if($the_item['testimonial_slide_video'] && trim($the_item['testimonial_slide_video']) != '') {
						$additional_str_start = '?';
						if(strrpos($the_item['testimonial_slide_video'], '?') !== false){
							$additional_str_start = '&';
						}
					?>
					<div class="img-block" style="background: #000;">
						<a data-fancybox href="<?= $the_item['testimonial_slide_video'] ?>" class="video-popup-opener"><iframe loading="lazy" src="<?= $the_item['testimonial_slide_video'] . $additional_str_start ?>title=0&byline=0&portrait=0&sidedock=0&controls=0" width="100%" height="100%" frameborder="0"></iframe></a>
					</div>
					<?php } else { ?>
					<div class="img-block lazy-bg" data-src="<?= $the_item['testimonial_slide_image']['sizes']['medium_large'] ?>" >
						<a class="video-popup-opener"></a>
					</div>
					<?php } ?>


				<div class="text-block">
					<div class="author-block">
						<div class="author-name-holder">
							<div class="author-img lazy-bg" data-src="<?= $the_item['testimonial_author_image']['sizes']['thumbnail'] ?>" ></div>
							<div class="author-descr">
								<h5 class="author-name"><?= $the_item['testimonial_author_name'] ?></h5>
								<p><?= $the_item['testimonial_author_position'] ?></p>
							</div>
						</div>
						<img loading="lazy" src="https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/icon-50.png" class="quote-img">
						<p><?= $the_item['testimonial_text'] ?></p>
					</div>
				</div>
			</div>
		</div>

		<?php } } ?>
	</div>

	<?php if(get_field('display_bottom_cta') != 'No'){ ?>
	<div class="container">
		<div class="cta-block">
			<p><?= get_field('bottom_cta_description') ?></p>
			<div class="btn-center-holder">
				<a href="<?= get_field('bottom_cta_destination') ?>" class="btn"><?= get_field('bottom_cta_text') ?></a>
			</div>
		</div>
	</div>
	<?php } ?>
</section>

<section class="information-section bg-dark">
<div class="container">
		<div class="post-panel">
			<h2 class="h3"><?= get_field('learn_blog_title') ?></h2>
			<div class="post-slider">
				<?php
				$blog_load_more_link = '';

				$more_link = get_category_link(get_field('learn_blog_more_link'));

				if(get_field('learn_blog_show_most_recent') == 'Yes') {

				$learn_blog_cat_obj = get_field('learn_blog_category');

				$learn_blog_cat = $learn_blog_cat_obj->term_id;

				$got_posts = get_posts( ['numberposts' => 3, 'category' => $learn_blog_cat] );

				foreach($got_posts as $post) : setup_postdata( $post );
				?>
				<div class="post-slide">
					<div class="post-item">
						<a href="<?= get_permalink($post->ID) ?>" class="img-block lazy-bg" data-src="<?= get_the_post_thumbnail_url($post->ID, 'medium') ?>" ></a>
						<div class="post-txt">
							<a href="<?= get_permalink($post->ID) ?>" class="txt">
								<h4 class="post-title"><?= $post->post_title ?></h4>
								<span class="post-author"><?= get_the_author_meta('display_name', $post->post_author); ?></span>
								<?php $post_date_no_time = explode(' ', $post->post_date_gmt)[0];
								$post_formatted_date_arr = explode('-', $post_date_no_time);
								$dateObj = DateTime::createFromFormat('!m', $post_formatted_date_arr[1]);
								$month = $dateObj->format('F');
								$post_formatted_date = $post_formatted_date_arr[2].' '.$month.' '.$post_formatted_date_arr[0]; ?>
								<span class="post-date">Posted <time datetime="<?= $post_date_no_time ?>"><?= $post_formatted_date ?></time></span>
							</a>
							<a href="<?= get_category_link( $learn_blog_cat ) ?>" class="post-category xs-mobile"><?= $learn_blog_cat_obj->name ?></a>
						</div>
					</div>

				</div>
				<?php endforeach; wp_reset_postdata(); } else {

					for($i=1; $i < 4; $i++) {
					$post = get_field('learn_blog_post_'.$i);
					if($post && $post != null) {
				?>

					<div class="post-slide">
						<div class="post-item">
							<a href="<?= get_permalink($post->ID) ?>" class="img-block lazy-bg" data-src="<?= get_the_post_thumbnail_url($post->ID, 'medium') ?>" ></a>
							<div class="post-txt">
								<a href="<?= get_permalink($post->ID) ?>" class="txt">
									<h4 class="post-title"><?= $post->post_title ?></h4>
									<span class="post-author"><?= get_the_author_meta('display_name', $post->post_author); ?></span>
									<?php $post_date_no_time = explode(' ', $post->post_date_gmt)[0];
									$post_formatted_date_arr = explode('-', $post_date_no_time);
									$dateObj = DateTime::createFromFormat('!m', $post_formatted_date_arr[1]);
									$month = $dateObj->format('F');
									$post_formatted_date = $post_formatted_date_arr[2].' '.$month.' '.$post_formatted_date_arr[0]; ?>
									<span class="post-date">Posted <time datetime="<?= $post_date_no_time ?>"><?= $post_formatted_date ?></time></span>
								</a>
								<?php $post_cat = get_the_category( $post->ID )[0];?>
								<?php $blog_load_more_link = get_category_link( $post_cat->term_id ); ?>
								<a href="<?= $blog_load_more_link ?>" class="post-category xs-mobile"><?= $post_cat->name ?></a>
							</div>
						</div>
					</div>

				<?php wp_reset_postdata(); } } } ?>
			</div>
			<?php if($more_link && trim($more_link) != '') { ?>
			<div class="btn-center-holder">
				<a href="<?= $more_link ?>" class="btn btn-lg">Explore more articles</a>
			</div>
			<?php } ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>
