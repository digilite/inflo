<?php /* Template Name: Other Solution Child */ ?>

<?php get_header(); ?>

    <section class="product-detail-section bg-skew-line  product-detail-page" style="padding-bottom: 0">
        <div class="container">

            <h1 class="h2"><?= $post->post_title ?></h1>
            <?php the_content(); ?>
    </section>

    <section class="partners-section white-section ">
        <div class="container">
            <h2 class="h4"><?= get_field("partners_text", 1312) ?></h2>
            <ul class="partners-list">
                <?php $partner_icons = acf_get_fields_by_id(1308);
                if ($partner_icons) {
                    foreach ($partner_icons as $icon) {
                        if ($icon['name'] != 'partners_text') { ?>
                            <?php $the_image = get_field($icon['name'], 1312);
                            $partner_link = $the_image['partner_link'];
                            if (trim($partner_link) == '') {
                                $partner_link = '/' . ICL_LANGUAGE_CODE . '/customer-success';
                            }
                            ?>
                            <li>
                                <a href="<?= $partner_link ?>">
                                    <img loading="lazy" src="<?= $the_image['partner_image']['sizes']['medium'] ?>">
                                </a>
                            </li>
                        <?php }
                    }
                } ?>
            </ul>
        </div>
    </section>

    <section class="testimonials-section border-top-white bg-light">

        <div class="container">
            <h2><?= get_field('testimonial_slider_title') ?></h2>
        </div>

        <div class="testimonials-slider">

            <?php $testimonial_slides = acf_get_fields_by_id(1415);
            if ($testimonial_slides) {
                foreach ($testimonial_slides as $item) {
                    $the_item = get_field($item['name']);
                    if ($item['name'] != 'testimonial_slider_title' && $the_item['show_testimonial_slide'] != 'No') { ?>

                        <div class="testimonial-slide">
                            <div class="testimonial-item">

                                <?php if ($the_item['testimonial_slide_video'] && trim($the_item['testimonial_slide_video']) != '') {
                                    $additional_str_start = '?';
                                    if (strrpos($the_item['testimonial_slide_video'], '?') !== false) {
                                        $additional_str_start = '&';
                                    }
                                    ?>
                                    <div class="img-block" style="background: #000;">
                                        <a data-fancybox href="<?= $the_item['testimonial_slide_video'] ?>"
                                           class="video-popup-opener">
                                            <iframe loading="lazy"
                                                    src="<?= $the_item['testimonial_slide_video'] . $additional_str_start ?>title=0&byline=0&portrait=0&sidedock=0&controls=0"
                                                    width="100%" height="100%" frameborder="0"></iframe>
                                        </a>
                                    </div>
                                <?php } else { ?>
                                    <div class="img-block"
                                         style="background-image: url(<?= $the_item['testimonial_slide_image']['sizes']['medium_large'] ?>)">
                                        <a class="video-popup-opener"></a>
                                    </div>
                                <?php } ?>


                                <div class="text-block">
                                    <div class="author-block">
                                        <div class="author-name-holder">
                                            <div class="author-img"
                                                 style="background-image: url('<?= $the_item['testimonial_author_image']['sizes']['thumbnail'] ?>');"></div>
                                            <div class="author-descr">
                                                <h5 class="author-name"><?= $the_item['testimonial_author_name'] ?></h5>
                                                <p><?= $the_item['testimonial_author_position'] ?></p>
                                            </div>
                                        </div>
                                        <img loading="lazy" src="https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/icon-50.png"
                                             class="quote-img">
                                        <p><?= $the_item['testimonial_text'] ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php }
                }
            } ?>
        </div>

        <div class="container">
            <div class="feature-card-holder">
                <h3 class="mb-0">Discover what else Inflo has to offer</h3>
                <div class="subtitle-text">
                    <!-- <p>People interested in this feature were also interested in:</p> -->
                </div>

                <div class="featured-card-list five-cols">
                    <?php $product_cards = acf_get_fields_by_id(1980);
                    if ($product_cards) {
                        foreach ($product_cards as $item) {
                            $the_item = get_field($item['name']);
                            if ($the_item != null && $the_item != '') { ?>
                                <div class="featured-card">
                                    <div class="main-block">
						<span class="img-wrapper">
							<img loading="lazy" src="<?= get_the_post_thumbnail_url($the_item->ID, 'medium') ?>">
						</span>
                                        <span><?= $the_item->post_title ?></span>
                                    </div>
                                    <div class="info-block">
                                        <p><?= $the_item->post_excerpt ?></p>
                                        <div class="text-center">
                                            <a href="<?= get_permalink($the_item->ID) ?>">Read more <i
                                                        class="fas fa-angle-right"></i>
                                            </a></div>
                                    </div>
                                </div>
                            <?php }
                        }
                    } ?>
                </div>
            </div>
        </div>

    </section>


    <section class="more-studies-section border-top-light-secondary bg-grey border-bottom-white">
        <div class="container">
            <div class="post-panel">
                <h2 class="h3"><?= get_field('learn_blog_title') ?></h2>
                <div class="post-slider post-light">
                    <?php
                    $blog_load_more_link = '';

                    $more_link = get_category_link(get_field('learn_blog_more_link'));

                    if (get_field('learn_blog_show_most_recent') == 'Yes') {

                        $learn_blog_cat_obj = get_field('learn_blog_category');

                        $learn_blog_cat = $learn_blog_cat_obj->term_id;

                        $got_posts = get_posts(['numberposts' => 3, 'category' => $learn_blog_cat]);

                        if ($got_posts) {
                            foreach ($got_posts as $post) : setup_postdata($post);
                                ?>
                                <div class="post-slide">
                                    <div class="post-item">
                                        <a href="<?= get_permalink($post->ID) ?>" class="img-block"
                                           style="background-image: url('<?= get_the_post_thumbnail_url($post->ID, 'medium') ?>');"></a>
                                        <div class="post-txt">
                                            <a href="<?= get_permalink($post->ID) ?>" class="txt">
                                                <h4 class="post-title"><?= $post->post_title ?>
                                                </h4>
                                                <span class="post-author"><?= get_the_author_meta('display_name', $post->post_author); ?></span>

                                                <?php $post_date_no_time = explode(' ', $post->post_date_gmt)[0];
                                                $post_formatted_date_arr = explode('-', $post_date_no_time);
                                                $dateObj = DateTime::createFromFormat('!m', $post_formatted_date_arr[1]);
                                                $month = $dateObj->format('F');
                                                $post_formatted_date = $post_formatted_date_arr[2] . ' ' . $month . ' ' . $post_formatted_date_arr[0]; ?>

                                                <span class="post-date">Posted <time
                                                            datetime="<?= $post_date_no_time ?>"><?= $post_formatted_date ?></time></span>
                                            </a>
                                            <?php $blog_load_more_link = get_category_link($learn_blog_cat); ?>
                                            <a href="<?= $blog_load_more_link ?>"
                                               class="post-category xs-mobile"><?= $learn_blog_cat_obj->name ?></a>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach;
                            wp_reset_postdata();
                        }
                    } else {

                        for ($i = 1; $i < 4; $i++) {
                            $post = get_field('learn_blog_post_' . $i);
                            if ($post && $post != null) {
                                ?>

                                <div class="post-slide">
                                    <div class="post-item">
                                        <a href="<?= get_permalink($post->ID) ?>" class="img-block"
                                           style="background-image: url('<?= get_the_post_thumbnail_url($post->ID, 'medium') ?>');"></a>
                                        <div class="post-txt">
                                            <a href="<?= get_permalink($post->ID) ?>" class="txt">
                                                <h4 class="post-title"><?= $post->post_title ?>
                                                </h4>
                                                <span class="post-author"><?= get_the_author_meta('display_name', $post->post_author); ?></span>

                                                <?php $post_date_no_time = explode(' ', $post->post_date_gmt)[0];
                                                $post_formatted_date_arr = explode('-', $post_date_no_time);
                                                $dateObj = DateTime::createFromFormat('!m', $post_formatted_date_arr[1]);
                                                $month = $dateObj->format('F');
                                                $post_formatted_date = $post_formatted_date_arr[2] . ' ' . $month . ' ' . $post_formatted_date_arr[0]; ?>

                                                <span class="post-date">Posted <time
                                                            datetime="<?= $post_date_no_time ?>"><?= $post_formatted_date ?></time></span>
                                            </a>
                                            <?php $post_cat = get_the_category($post->ID)[0]; ?>
                                            <?php $blog_load_more_link = get_category_link($post_cat->term_id); ?>
                                            <a href="<?= $blog_load_more_link ?>"
                                               class="post-category xs-mobile"><?= $post_cat->name ?></a>
                                        </div>
                                    </div>
                                </div>

                                <?php wp_reset_postdata();
                            }
                        }
                    } ?>
                </div>
                <div class="btn-position-top">
                    <?php if (get_field('learn_blog_show_most_recent') == 'Yes') {
                        if ($blog_load_more_link && trim($blog_load_more_link) != '') { ?>
                            <a href="<?= $blog_load_more_link ?>" class="btn">Load more articles</a>
                        <?php }
                    } else {
                        if ($more_link && trim($more_link) != '') { ?>
                            <a href="<?= $more_link ?>" class="btn">Load more articles</a>
                        <?php }
                    } ?>
                </div>
            </div>
        </div>
    </section>

<?php if (get_field('display_bottom_cta') != 'No') { ?>
    <section class="default-section">
        <div class="container">
            <div class="cta-block">
                <p><?= get_field('bottom_cta_description') ?></p>
                <div class="btn-center-holder">
                    <a href="<?= get_field('bottom_cta_destination') ?>"
                       class="btn"><?= get_field('bottom_cta_text') ?></a>
                </div>
            </div>
        </div>
    </section>
<?php } ?>


<?php get_footer(); ?>
