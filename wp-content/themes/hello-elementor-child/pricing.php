<?php /* Template Name: Inflo Pricing */ ?>

<?php get_header(); ?>

    <section class="pricing-section bg-skew-line bg-light border-bottom-white border-bottom-decor-section">
        <div class="container">
            <div class="section-title">
                <h1 class="h2"><?= get_field('pricing_title') ?></h1>
                <?= get_field('pricing_subtitle') ?>
            </div>
            <div class="plan-slider">

                <?php $pricing_plans = acf_get_fields_by_id(1829);
                if ($pricing_plans) {
                    foreach ($pricing_plans as $plan) {
                        $the_plan = get_field($plan['name']); ?>

                        <div class="plan-slide">
                            <div class="plan-item <?= strtolower($the_plan['pricing_plan_name']) ?>">
                                <div class="title-block">
                                    <h3 class="title"><?= $the_plan['pricing_plan_name'] ?></h3>
                                    <?php if ($the_plan['pricing_plan_label'] != null && $the_plan['pricing_plan_label'] != '') { ?>
                                        <span class="label"><?= $the_plan['pricing_plan_label'] ?></span>
                                    <?php } ?>
                                </div>
                                <div class="content-block">
                                    <div class="price-holder">
                                        <?php if ($the_plan['pricing_plan_image'] == null || $the_plan['pricing_plan_image'] == '') { ?>
                                            <h3 class="price"><?= $the_plan['pricing_plan_cost'] ?> <span
                                                        class="price-sm"><?= $the_plan['pricing_plan_basis'] ?></span>
                                            </h3>
                                            <span class="price-note"><?= $the_plan['pricing_plan_condition'] ?></span>
                                        <?php } else { ?>
                                            <h3 class="price"><img loading="lazy"
                                                                   src="<?= $the_plan['pricing_plan_image']['sizes']['medium'] ?>">
                                            </h3>
                                        <?php } ?>
                                    </div>
                                    <p><?= $the_plan['pricing_plan_description'] ?></p>
                                    <?php if ($the_plan['pricing_plan_cta_text'] != null && $the_plan['pricing_plan_cta_text'] != '') { ?>
                                        <div class="btn-holder">
                                            <a href="<?= $the_plan['pricing_plan_cta_destination'] ?>"
                                               class="btn"><?= $the_plan['pricing_plan_cta_text'] ?></a>
                                        </div>
                                    <?php } ?>
                                    <h4><?= $the_plan['pricing_plan_list_title'] ?></h4>
                                    <?= $the_plan['pricing_plan_list'] ?>
                                </div>
                            </div>
                        </div>

                    <?php }
                } ?>

            </div>

            <?php if (get_field('simple_rich_text_section_show') == 'Yes') { ?>
                <section class="bg-light">
                    <div class="container">
                        <div class="section-title text-center">
                            <?= get_field('simple_rich_text_section_content') ?>
                        </div>
                    </div>
                </section>
            <?php } ?>

            <?php
            if (get_field('show_testimonial') == 'Yes') {
                $use_case_id = get_field('testimonial_from_use_case'); ?>
                <div class="single-testimonial-block">
                    <div class="testimonial-item">
                        <div class="text-block">
                            <div class="author-block">
                                <div class="author-name-holder">
                                    <div class="author-img"
                                         style="background-image: url('<?= get_field('testimonial_avatar', $use_case_id)['sizes']['thumbnail'] ?>');"></div>
                                    <div class="author-descr">
                                        <h5 class="author-name"><?= get_field('testimonial_name', $use_case_id) ?></h5>
                                        <p><?= get_field('testimonial_job_title', $use_case_id) ?></p>
                                    </div>
                                </div>
                                <img loading="lazy" src="https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/icon-50.png" class="quote-img">
                                <p><?= get_field('testimonial_quote', $use_case_id) ?></p>
                            </div>
                        </div>
                        <div class="img-block"
                             style="background-image: url('<?= get_field('image_on_testimonial_side')['sizes']['medium_large'] ?>');">
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>

        <section class="partners-section white-section border-top-light-secondary border-bottom-light-secondary">
            <div class="container">

                <?php if (get_field('destination_of_the_pricing_cta') && get_field('destination_of_the_pricing_cta') != null && get_field('destination_of_the_pricing_cta') != '') { ?>
                    <div class="cta-pricing">
                        <?= get_field('text_before_pricing_cta') ?>
                        <div class="btn-center-holder">
                            <a href="<?= get_field('destination_of_the_pricing_cta') ?>"
                               class="btn"><?= get_field('text_of_the_pricing_cta') ?></a>
                        </div>
                    </div>
                <?php } ?>

                <h2 class="h4"><?= get_field("partners_text", 1312) ?></h2>
                <ul class="partners-list">
                    <?php $partner_icons = acf_get_fields_by_id(1308);
                    foreach ($partner_icons as $icon) {
                        if ($icon['name'] != 'partners_text') { ?>
                            <?php $the_image = get_field($icon['name'], 1312);
                            $partner_link = $the_image['partner_link'];
                            if (trim($partner_link) == '') {
                                $partner_link = '/' . ICL_LANGUAGE_CODE . '/ucustomer-success';
                            }
                            ?>
                            <li>
                                <a href="<?= $partner_link ?>">
                                    <img loading="lazy" src="<?= $the_image['partner_image']['sizes']['medium'] ?>">
                                </a>
                            </li>
                        <?php }
                    } ?>
                </ul>
            </div>
        </section>

        <div class="container">

            <?php if (get_field('show_linked_boxes') == 'Yes') { ?>
                <div class="pricing-boxes-holder pricing-boxes-margin">
                    <div class="pricing-box">
                        <div class="pricing-box-inner">
                            <?= get_field('box_left') ?>
                        </div>
                    </div>
                    <div class="pricing-box">
                        <div class="pricing-box-inner">
                            <?= get_field('box_right') ?>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <div class="feature-card-holder pricing-features">
                <h3 class="h4 text-center"><?= get_field('feature_list_heading') ?></h3>
                <div class="featured-card-list">

                    <?php
                    $feature_list = acf_get_fields_by_id(6063);

                    if ($feature_list) {
                        foreach ($feature_list as $feature) {
                            if ($feature['name'] != 'feature_list_heading') {
                                $the_feature = get_field($feature['name']);
                                if ($the_feature != null && $the_feature['feature_title'] != '') { ?>
                                    <div class="featured-card">
                                        <div class="main-block">
                                            <img loading="lazy"
                                                 src="<?= $the_feature['feature_icon']['sizes']['medium_large'] ?>">
                                            <span><?= $the_feature['feature_title'] ?></span>
                                        </div>
                                        <div class="info-block">
                                            <p><?= $the_feature['feature_description'] ?></p>
                                        </div>
                                    </div>
                                <?php }
                            }
                        }
                    } ?>

                </div>
            </div>

        </div>
        <span class="border-decor"></span>
    </section>

    <section class="faq-section">
        <div class="container">
            <h3 class="h4"><?= get_field('faq_section_title') ?></h3>
            <div class="faq-accordion accordion">
                <?php $contact_faq = acf_get_fields_by_id(1582);
                if ($contact_faq) {
                    foreach ($contact_faq as $qa) {
                        if ($qa['name'] != 'faq_section_title') {
                            $the_qa = get_field($qa['name']); ?>

                            <?php if ($the_qa['question'] != '') { ?>
                                <div class="accordion-item">
                                    <h4 class="accordion-item-title">
                                        <a href="#" class="item-opener"><?= $the_qa['question'] ?></a>
                                    </h4>
                                    <div class="accordion-item-slide">
                                        <div class="accordion-item-slide-content">
                                            <p><?= $the_qa['answer'] ?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php }
                        }
                    }
                } ?>
            </div>
        </div>
        <?php if (get_field('display_bottom_cta') != 'No') { ?>
            <div class="container">
                <div class="cta-block">
                    <p><?= get_field('bottom_cta_description') ?></p>
                    <div class="btn-center-holder">
                        <a href="<?= get_field('bottom_cta_destination') ?>"
                           class="btn"><?= get_field('bottom_cta_text') ?></a>
                    </div>
                </div>
            </div>
        <?php } ?>
    </section>

<?php get_footer(); ?>
