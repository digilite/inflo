<?php /* Template Name: Inflo Home */ ?>

<?php get_header(); ?>

<section class="intro-section bg-anim border-bottom-white">
	<?php $which_header = get_field( "choose_header" );
	if($which_header == "title_1") { ?>
	<div class="container">
		<div class="text-block">
			<h1><?= get_field("title_1") ?></h1>
			<p class="intro-note"><?= get_field("subtitle_1") ?></p>
			<p><?= get_field("paragraph_1") ?></p>
			<div class="intro-btn-holder">
				<a href="<?= get_field("primary_cta_destination_1") ?>" class="btn"><?= get_field("primary_cta_text_1") ?></a>
				<a href="<?= get_field("secondary_cta_destination_1") ?>" class="btn btn-white"><?= get_field("secondary_cta_text_1") ?></a>
			</div>
		</div>
	</div>
	<div class="parallax-img-block">
		<img src="<?= get_field("hero_image_1") ?>">
	</div>
	<?php } ?>

</section>

<?php the_content(); ?>

<?php get_footer(); ?>
