<?php /* Template Name: Inflo Solutions Main */ ?>

<?php get_header(); ?>

<section class="solutions-section bg-grey">
    <div class="container">
        <div class="section-title">
            <h1 class="h2 gradient-underline"><?= the_field('solution_title') ?></h1>
            <p><?= the_field('solution_subtitle') ?></p>
        </div>
        <ul class="guidance-cards guidance-cards-wide">
            <li>
                <a href="<?= the_field('left_card_link') ?>" class="guidance-cont bg-orange">
                    <h3 class="guidance-title guidance-title-img-holder">
                        <img loading="lazy" src="<?= get_field('left_card_icon')['sizes']['medium'] ?>" alt="city" class="guidance-title-img">
                        <span class="guidance-title-img-text"><?= the_field('left_card_title') ?></span>
                    </h3>
                </a>
            </li>
            <li>
                <a href="<?= the_field('right_card_link') ?>" class="guidance-cont bg-green">
                    <h3 class="guidance-title guidance-title-img-holder">
                        <img loading="lazy" src="<?= get_field('right_card_icon')['sizes']['medium'] ?>" alt="collaboration" class="guidance-title-img">
                        <span class="guidance-title-img-text"><?= the_field('right_card_title') ?></span>
                    </h3>
                </a>
            </li>
        </ul>
        <div class="feature-card-holder">
            <h3 class="h4 mb-0 text-center"><?= the_field('solutions_discover_title') ?></h3>
            <div class="subtitle-text text-center">
                <p><?= the_field('solutions_discover_text') ?></p>
            </div>
            <div class="featured-card-list five-cols">

                <?php $product_cards = acf_get_fields_by_id(1980);
                if ($product_cards) {
                    foreach ($product_cards as $item) {
                        $the_item = get_field($item['name']);
                        if ($the_item != null && $the_item != '') { ?>
                            <div class="featured-card" style="" aria-hidden="true" tabindex="0">
                                <div class="main-block">
                                    <img loading="lazy" src="<?= get_the_post_thumbnail_url($the_item->ID, 'medium') ?>">
                                    <span><?= $the_item->post_title ?></span>
                                </div>
                                <div class="info-block">
                                    <p><?= $the_item->post_excerpt ?></p>
                                    <div class="text-center">
                                        <a href="<?= get_permalink($the_item->ID) ?>">Read more <i class="fas fa-angle-right"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                <?php }
                    }
                } ?>

            </div>
        </div>
    </div>
</section>

<section class="info-modules-section bg-orange border-bottom-light border-top-light">
    <div class="container">
        <h2>Inflo modules</h2>
        <div class="tab-holder honeycomb-holder">
            <div class="honeycomb-wrapper">
                <div class="honeycomb-block honeycomb-block__wrapper tab-nav">
                    <?php
                    $modules_cards = acf_get_fields_by_id(2044);
                    $count = 0;
                    ?>
                    <?php if ($modules_cards) : ?>
                        <?php foreach ($modules_cards as $key => $item) : ?>
                            <?php
                            if ($count == 0) {
                                echo '<div class="honeycomb-block 4-columns tab-nav">';
                            } elseif ($count % 7 == 4 && $count > 1) {
                                echo '</div>';
                                echo '<div class="honeycomb-block 3-columns tab-nav">';
                            } elseif ($count % 7 == 0 && $count > 1) {
                                echo '</div>';
                                echo '<div class="honeycomb-block 4-columns tab-nav">';
                            }
                            $the_item = get_field($item['name']);
                            if ($the_item != null && $the_item != '') : ?>
                                <a href="#tab<?= $key + 1 ?>" class="<?php if ($key == 0) echo 'active-tab'; ?> tab-opener honeycomb-item">
                                    <div class="honeycomb-inner" style="background-image: url(https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/icon-51.png);">
                                        <div class="img-holder">
                                            <?php
                                            $img_src = $the_item['cell_icon']['sizes']['medium'];
                                            if (!(isset($img_src) && $img_src)) {
                                                $img_src = $the_item['cell_icon'];
                                            }
                                            ?>
                                            <img loading="lazy" src="<?= $img_src ?>">
                                        </div>
                                        <span><?= $the_item['cell_title'] ?></span>
                                    </div>
                                </a>
                                <?php $count++; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="main-tab-content">
                <?php foreach ($modules_cards as $key => $item) {
                    $the_item = get_field($item['name']);
                    if ($the_item != null && $the_item != '') { ?>
                        <div id="tab<?= $key + 1 ?>" class="tab">
                            <h3 class="h4"><?= $the_item['cell_title'] ?></h3>
                            <?= $the_item['cell_description'] ?>
                            <?php if ($the_item['cell_cta_destination'] && $the_item['cell_cta_destination'] != null && $the_item['cell_cta_destination'] != '') { ?>
                                <div class="btn-center-holder">
                                    <a href="<?= $the_item['cell_cta_destination'] ?>" class="btn"><?= $the_item['cell_cta_text'] ?></a>
                                </div>
                            <?php } ?>
                        </div>
                <?php }
                } ?>

            </div>
        </div>
    </div>
</section>

<section class="cta-testimonial-section bg-grey">
    <?php $use_case_id = get_field('testimonial_from_use_case'); ?>
    <div class="container">
        <div class="cta-testimonial-holder">
            <div class="testimonial-block">
                <div class="testimonial-item">
                    <div class="text-block">
                        <div class="author-block">
                            <div class="author-name-holder">
                                <div class="author-img" style="background-image: url('<?= get_field('testimonial_avatar', $use_case_id)['sizes']['thumbnail'] ?>');"></div>
                                <div class="author-descr">
                                    <h5 class="author-name"><?= get_field('testimonial_name', $use_case_id) ?></h5>
                                    <p><?= get_field('testimonial_job_title', $use_case_id) ?></p>
                                </div>
                            </div>
                            <img loading="lazy" src="https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/icon-50.png" class="quote-img">
                            <p><?= get_field('testimonial_quote', $use_case_id) ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <?php if (get_field('display_bottom_cta') != 'No') { ?>
                <div class="cta-block">
                    <p><?= get_field('bottom_cta_description') ?></p>
                    <div class="btn-center-holder">
                        <a href="<?= get_field('bottom_cta_destination') ?>" class="btn"><?= get_field('bottom_cta_text') ?></a>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
