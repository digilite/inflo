<?php
// get values
$enable_read_more = get_field('enable_read_more') ?? true;
$title_read_more  = get_field('title_read_more') ?? 'More to read';
$by_category      = get_field('more_to_read_by_category') ?? 'No';
?>
<?php if ($enable_read_more) : ?>
    <h2><?php echo $title_read_more ?></h2>
    <div class="post-slider post-light">
        <?php if ( $by_category == 'Yes') {

            $got_posts = get_posts(['numberposts' => 3, 'category' => $post_cat->term_id, 'exclude' => [$this_post_id]]);

            foreach ($got_posts as $post) : setup_postdata($post);
                if ($post->ID != $this_post_id) {
        ?>
                    <div class="post-slide">
                        <div class="post-item">
                            <a href="<?= get_permalink($post->ID) ?>" class="img-block" style="background-image: url('<?= get_the_post_thumbnail_url($post->ID, 'medium') ?>');"></a>
                            <div class="post-txt">
                                <a href="<?= get_permalink($post->ID) ?>" class="txt">
                                    <h4 class="post-title"><?= $post->post_title ?></h4>
                                    <span class="post-author"><?= get_the_author_meta('display_name', $post->post_author); ?></span>
                                    <?php $post_date_no_time = explode(' ', $post->post_date_gmt)[0];
                                    $post_formatted_date_arr = explode('-', $post_date_no_time);
                                    $dateObj = DateTime::createFromFormat('!m', $post_formatted_date_arr[1]);
                                    $month = $dateObj->format('F');
                                    $post_formatted_date = $post_formatted_date_arr[2] . ' ' . $month . ' ' . $post_formatted_date_arr[0]; ?>
                                    <span class="post-date">Posted <time datetime="<?= $post_date_no_time ?>"><?= $post_formatted_date ?></time></span>
                                </a>
                                <a href="<?= $cat_link ?>" class="post-category xs-mobile"><?= $post_cat->name ?></a>
                            </div>
                        </div>
                    </div>

                <?php }
            endforeach;
            wp_reset_postdata();
        } else {
            for ($i = 1; $i < 4; $i++) {
                $post = get_field('more_to_read_post_' . $i);
                if ($post && $post != null) {
                    $loop_post_cat = get_the_category($post->ID)[0];
                    $loop_cat_link = get_category_link($post_cat->term_id); //str_replace('/blog', '', get_category_link( $post_cat->term_id ));
                ?>

                    <div class="post-slide">
                        <div class="post-item">
                            <a href="<?= get_permalink($post->ID) ?>" class="img-block" style="background-image: url('<?= get_the_post_thumbnail_url($post->ID, 'medium') ?>');"></a>
                            <div class="post-txt">
                                <a href="<?= get_permalink($post->ID) ?>" class="txt">
                                    <h4 class="post-title"><?= $post->post_title ?></h4>
                                    <span class="post-author"><?= get_the_author_meta('display_name', $post->post_author); ?></span>
                                    <?php $post_date_no_time = explode(' ', $post->post_date_gmt)[0];
                                    $post_formatted_date_arr = explode('-', $post_date_no_time);
                                    $dateObj = DateTime::createFromFormat('!m', $post_formatted_date_arr[1]);
                                    $month = $dateObj->format('F');
                                    $post_formatted_date = $post_formatted_date_arr[2] . ' ' . $month . ' ' . $post_formatted_date_arr[0]; ?>
                                    <span class="post-date">Posted <time datetime="<?= $post_date_no_time ?>"><?= $post_formatted_date ?></time></span>
                                </a>
                                <a href="<?= $cat_link ?>" class="post-category xs-mobile"><?= $post_cat->name ?></a>
                            </div>
                        </div>
                    </div>

        <?php wp_reset_postdata();
                }
            }
        } ?>
    </div>
<?php endif; ?>