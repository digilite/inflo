<?php $display_newsletter = get_field('display_newsletter') ?? true; ?>
<?php if ($display_newsletter) : ?>
    <?php $title = get_field('title') ?? 'Signup for our newsletter'; ?>
    <div class="testimonial-block">
        <div class="white-form-holder">
            <div class="text-center">
                <h3 class="h4"><?= $title ?></h3>
            </div>
            <div class="signup-form-holder">
                <!--[if lte IE 8]>
						<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
						<![endif]-->
                <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                <script>
                    hbspt.forms.create({
                        portalId: "7936749",
                        formId: "0991c4ff-ce40-4a5b-8dcc-52ce926bddf7"
                    });
                </script>
            </div>
        </div>
    </div>
<?php endif; ?>