<?php /* Template Name: Inflo Product
Template Post Type: post, products, modules */ ?>

<?php get_header(); ?>

    <section class="product-detail-section bg-skew-line bg-light product-detail-page">
        <div class="container">
            <div class="breadcrumbs-holder">
                <ul class="breadcrumbs-list" style="display: none !important;">
                    <!--<li><a href="<?= str_replace('/blog', '', $cat_link) ?>"><?= $post_cat->name ?></a></li>-->
                    <li><a href="<?= esc_url(home_url('products')) ?>">Products</a></li>
                    <?php if (get_post_type($post->ID) == 'modules') {
                        $parent_product = get_post_meta($post->ID, 'parent_product')[0];
                        if ($parent_product && $parent_product != null && $parent_product != '') {

                            $args = array(
                                'name' => $parent_product,
                                'post_type' => 'products',
                                'numberposts' => 1
                            );
                            $prod_post = get_posts($args)[0]; ?>

                            <li><a href="<?= get_permalink($prod_post->ID) ?>"><?= $prod_post->post_title ?></a></li>
                        <?php }
                    } ?>
                    <li><?= $post->post_title ?></li>
                </ul>
            </div>
            <h1 class="h2"><?= $post->post_title ?></h1>
            <?php the_content(); ?>

            <?php if (get_field('show_blocks_on_page') != 'No') { ?>
                <div class="colored-blocks-holder four-blocks">
                    <div class="colored-block">
                        <?= get_field('block_content_1') ?>

                    </div>
                    <div class="colored-block">
                        <?= get_field('block_content_2') ?>

                    </div>
                    <div class="colored-block">
                        <?= get_field('block_content_3') ?>

                    </div>
                    <div class="colored-block">
                        <?= the_field('block_content_4') ?>

                    </div>
                </div>
            <?php } ?>
        </div>

        <div class="skew-bg-end">
            <div class="container">
                <?php
                $use_case = get_field('choose_product_use_case');
                if ($use_case && $use_case != null && $use_case != '') {

                    $use_case_solution = get_field('use_case_solution', $use_case->ID);
                    $use_case_impact = get_field('use_case_impact', $use_case->ID);
                    $use_case_widget_title = get_field('use_case_widget_title', $use_case->ID);
                    //$use_case_tags = get_the_tags( $use_case->ID );
                    $use_case_tags = get_the_terms($use_case->ID, 'product_tax');
                    ?>
                    <div class="use-case-preview-holder">
                        <div class="use-case-preview">
                            <div class="descr-block">
                                <div class="title-holder">
                                    <strong class="subtitle">CUSTOMER SUCCESS STORY</strong>
                                    <h4 class="title"><?php if ($use_case_widget_title && $use_case_widget_title != null && trim($use_case_widget_title) != '') {
                                            echo $use_case_widget_title;
                                        } else {
                                            echo $use_case->post_title;
                                        } ?></h4>
                                </div>
                                <h5>Background</h5>
                                <p><?= $use_case_solution ?></p>
                                <h5>Impact</h5>
                                <p><?= $use_case_impact ?></p>
                                <ul class="topic-list">
                                    <?php foreach ($use_case_tags as $the_tag) {
                                        $tag_link = get_term_link($the_tag->term_id, 'product_tax');
                                        ?>
                                        <li>
                                            <a href="<?= $tag_link/*str_replace('/tag', '', get_tag_link($use_case_tags[$i]->term_id))*/ ?>">
                                                <img loading="lazy"
                                                     src="<?= get_field('image_for_this_taxonomy', $the_tag)['sizes']['thumbnail'] ?>"
                                                     class="icon"> <?= $the_tag->name ?>
                                            </a>
                                        </li>
                                    <?php } ?>

                                </ul>
                            </div>
                            <div class="testimonial-block">
                                <div class="testimonial-item">
                                    <div class="text-block">
                                        <div class="author-block">
                                            <div class="author-name-holder">
                                                <div class="author-img"
                                                     style="background-image: url('<?= get_field('testimonial_avatar', $use_case->ID)['sizes']['thumbnail'] ?>');"></div>
                                                <div class="author-descr">
                                                    <h5 class="author-name"><?= get_field('testimonial_name', $use_case->ID) ?></h5>
                                                    <p><?= get_field('testimonial_job_title', $use_case->ID) ?></p>
                                                </div>
                                            </div>
                                            <img loading="lazy" src="https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/icon-50.png"
                                                 class="quote-img">
                                            <p><?= get_field('testimonial_quote', $use_case->ID) ?></p>
                                            <a href="<?= get_permalink($use_case->ID) ?>" class="btn btn-outline">Read
                                                Story</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <div class="feature-card-holder " <?php if (!$use_case || $use_case == null || $use_case == '') {
                    echo 'style="padding-top:200px"';
                } ?>>
                    <h3 class="mb-0">Discover what else Inflo has to offer</h3>
                    <div class="subtitle-text">
                        <!-- <p>People interested in this feature were also interested in:</p> -->
                    </div>
                    <div class="featured-card-list">
                        <?php $product_cards = acf_get_fields_by_id(1974);
                        if ($product_cards) {
                            foreach ($product_cards as $item) {
                                $the_item = get_field($item['name']);
                                if ($the_item != null && $the_item != '') { ?>
                                    <div class="featured-card">
                                        <div class="main-block">
						<span class="img-wrapper">
							<img loading="lazy" src="<?= get_the_post_thumbnail_url($the_item->ID, 'medium') ?>">
						</span>
                                            <span><?= $the_item->post_title ?></span>
                                        </div>
                                        <div class="info-block">
                                            <p><?= $the_item->post_excerpt ?></p>
                                            <div class="text-center">
                                                <a href="<?= get_permalink($the_item->ID) ?>">Read more <i
                                                            class="fas fa-angle-right"></i>
                                                </a></div>
                                        </div>
                                    </div>
                                <?php }
                            }
                        } ?>
                    </div>
                </div>
            </div>
        </div>

        <?php if (get_field('display_bottom_cta') != 'No') { ?>
            <div class="container">
                <div class="cta-block">
                    <p><?= get_field('bottom_cta_description') ?></p>
                    <div class="btn-center-holder">
                        <a href="<?= get_field('bottom_cta_destination') ?>"
                           class="btn"><?= get_field('bottom_cta_text') ?></a>
                    </div>
                </div>
            </div>
        <?php } ?>
        <span class="border-decor"></span>

        <section class="more-studies-section bg-light">
            <div class="container">
                <div class="post-panel">
                    <h2 class="h3"><?= get_field('learn_blog_title') ?></h2>
                    <div class="post-slider post-light">
                        <?php
                        $blog_load_more_link = '';

                        $more_link = get_category_link(get_field('learn_blog_more_link'));

                        if (get_field('learn_blog_show_most_recent') == 'Yes') {

                            $learn_blog_cat_obj = get_field('learn_blog_category');

                            $learn_blog_cat = $learn_blog_cat_obj->term_id;

                            $got_posts = get_posts(['numberposts' => 3, 'category' => $learn_blog_cat]);

                            if ($got_posts) {
                                foreach ($got_posts as $post) : setup_postdata($post);
                                    ?>
                                    <div class="post-slide">
                                        <div class="post-item">
                                            <a href="<?= get_permalink($post->ID) ?>" class="img-block"
                                               style="background-image: url('<?= get_the_post_thumbnail_url($post->ID, 'medium') ?>');"></a>
                                            <div class="post-txt">
                                                <a href="<?= get_permalink($post->ID) ?>" class="txt">
                                                    <h4 class="post-title"><?= $post->post_title ?>
                                                    </h4>
                                                    <span class="post-author"><?= get_the_author_meta('display_name', $post->post_author); ?></span>

                                                    <?php $post_date_no_time = explode(' ', $post->post_date_gmt)[0];
                                                    $post_formatted_date_arr = explode('-', $post_date_no_time);
                                                    $dateObj = DateTime::createFromFormat('!m', $post_formatted_date_arr[1]);
                                                    $month = $dateObj->format('F');
                                                    $post_formatted_date = $post_formatted_date_arr[2] . ' ' . $month . ' ' . $post_formatted_date_arr[0]; ?>

                                                    <span class="post-date">Posted <time
                                                                datetime="<?= $post_date_no_time ?>"><?= $post_formatted_date ?></time></span>
                                                </a>
                                                <?php $blog_load_more_link = get_category_link($learn_blog_cat); ?>
                                                <a href="<?= $blog_load_more_link ?>"
                                                   class="post-category xs-mobile"><?= $learn_blog_cat_obj->name ?></a>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach;
                                wp_reset_postdata();
                            } else {

                                for ($i = 1; $i < 4; $i++) {
                                    $post = get_field('learn_blog_post_' . $i);
                                    if ($post && $post != null) {
                                        ?>

                                        <div class="post-slide">
                                            <div class="post-item">
                                                <a href="<?= get_permalink($post->ID) ?>" class="img-block"
                                                   style="background-image: url('<?= get_the_post_thumbnail_url($post->ID, 'medium') ?>');"></a>
                                                <div class="post-txt">
                                                    <a href="<?= get_permalink($post->ID) ?>" class="txt">
                                                        <h4 class="post-title"><?= $post->post_title ?>
                                                        </h4>
                                                        <span class="post-author"><?= get_the_author_meta('display_name', $post->post_author); ?></span>

                                                        <?php $post_date_no_time = explode(' ', $post->post_date_gmt)[0];
                                                        $post_formatted_date_arr = explode('-', $post_date_no_time);
                                                        $dateObj = DateTime::createFromFormat('!m', $post_formatted_date_arr[1]);
                                                        $month = $dateObj->format('F');
                                                        $post_formatted_date = $post_formatted_date_arr[2] . ' ' . $month . ' ' . $post_formatted_date_arr[0]; ?>

                                                        <span class="post-date">Posted <time
                                                                    datetime="<?= $post_date_no_time ?>"><?= $post_formatted_date ?></time></span>
                                                    </a>
                                                    <?php $post_cat = get_the_category($post->ID)[0]; ?>
                                                    <?php $blog_load_more_link = get_category_link($post_cat->term_id); ?>
                                                    <a href="<?= $blog_load_more_link ?>"
                                                       class="post-category xs-mobile"><?= $post_cat->name ?></a>
                                                </div>
                                            </div>
                                        </div>

                                        <?php wp_reset_postdata();
                                    }
                                }
                            }
                        } ?>
                    </div>
                    <div class="btn-position-top">
                        <?php if (get_field('learn_blog_show_most_recent') == 'Yes') {
                            if ($blog_load_more_link && trim($blog_load_more_link) != '') { ?>
                                <a href="<?= $blog_load_more_link ?>" class="btn">Load more articles</a>
                            <?php }
                        } else {
                            if ($more_link && trim($more_link) != '') { ?>
                                <a href="<?= $more_link ?>" class="btn">Load more articles</a>
                            <?php }
                        } ?>
                    </div>
                </div>
            </div>
        </section>
    </section>


<?php get_footer(); ?>
