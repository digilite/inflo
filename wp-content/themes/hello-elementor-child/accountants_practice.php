<?php /* Template Name: Inflo Accountants Practice */ ?>
<?php get_header(); ?>
<section class="intro-section secondary-page bg-anim border-bottom-white">
	<div class="container">
		<div class="text-block full-width">
			<h1 class="h2"><?= get_field('solution_title') ?></h1>
			
		</div>
		<?php if(get_field('simple_rich_text_section_show') == 'Yes') { ?>
		<div class="intro-note">
			<?= get_field('simple_rich_text_section_content') ?>
		</div>
		<?php } ?>

	</div>
</section>

<section class="default-section accountants-default">
	<div class="container">
		<div class="benefits-block">
			<div class="benefits-text-block">
				<strong class="h6 subtitle"><?= get_field('why_title') ?></strong>
				<h2><?= get_field('why_section_title') ?></h2>
				<p><?= get_field('why_section_text') ?></p>
			</div> <?php $benefit1 = get_field('why_section_benefit_1');
			$benefit2 = get_field('why_section_benefit_2');
			$benefit3 = get_field('why_section_benefit_3'); ?>
			<div class="benefits-holder">
				<div class="features-item">
					<?php if($benefit1['why_benefit_icon_1']){ ?>
					<img src="<?= $benefit1['why_benefit_icon_1'] ?>" class="features-item-icon">
					<?php } ?>
					<h3 class="h2"><?= $benefit1['why_benefit_1_title'] ?></h3>
					<p><?= $benefit1['why_benefit_1_description'] ?></p>
					<?php if($benefit1['why_benefit_1_cta_url']) {?>
					<a href="<?= $benefit1['why_benefit_1_cta_url'] ?>" class="btn btn-outline"><?= $benefit1['why_benefit_1_cta'] ?></a>
					<?php } ?>
				</div>
				<div class="features-item">
					<?php if($benefit2['why_benefit_icon_2']){ ?>
					<img src="<?= $benefit2['why_benefit_icon_2'] ?>" class="features-item-icon">
					<?php } ?>
					<h3 class="h2"><?= $benefit2['why_benefit_2_title'] ?></h3>					
					<p><?= $benefit2['why_benefit_2_description'] ?></p>
					<?php if($benefit2['why_benefit_2_cta_url']) {?>
					<a href="<?= $benefit2['why_benefit_2_cta_url'] ?>" class="btn btn-outline"><?= $benefit2['why_benefit_2_cta'] ?></a>
					<?php } ?>
				</div>
				<div class="features-item">
					<?php if($benefit3['why_benefit_icon_3']){ ?>
					<img src="<?= $benefit3['why_benefit_icon_3'] ?>" class="features-item-icon">
					<?php } ?>
					<h3 class="h2"><?= $benefit3['why_benefit_3_title'] ?></h3>
					<p><?= $benefit3['why_benefit_3_description'] ?></p>
					<?php if($benefit3['why_benefit_3_cta_url']) {?>
					<a href="<?= $benefit3['why_benefit_3_cta_url'] ?>" class="btn btn-outline"><?= $benefit3['why_benefit_3_cta'] ?></a>
					<?php } ?>
				</div>
			</div>			
			<div class="btn-center-holder">
				<a href="<?= get_field('why_section_cta_destination') ?>" class="btn btn-lg"><?= get_field('why_section_cta_text') ?></a>
			</div>
		</div>
	</div>
</section>

<section class="default-section accountants-colored">
	<div class="container">
		<div class="colored-title-block">
			<h4><?= get_field('color_blocks_title') ?></h4>
		</div>
		<div class="colored-blocks-holder">
			<div class="colored-block">
				<p><?= get_field('color_block_left') ?></p>
			</div>
			<div class="colored-block">
				<p><?= get_field('color_block_center') ?></p>
			</div>
			<div class="colored-block">
				<p><?= get_field('color_block_right') ?></p>
			</div>
		</div>
	</div>
</section>

<section class="partners-section">
	<div class="container">
		<h2 class="h4"><?= get_field("partners_text", 1312) ?></h2>
		<ul class="partners-list">
			<?php $partner_icons = acf_get_fields_by_id(1308); 			
			foreach($partner_icons as $icon) { if($icon['name'] != 'partners_text') { ?>
			<?php $the_image = get_field($icon['name'], 1312); 
				$partner_link = $the_image['partner_link'];
				if(trim($partner_link) == '') {
					$partner_link = '/customer-success';
				}
			?>
			<li>
				<a href="<?= $partner_link ?>">
					<img src="<?= $the_image['partner_image'] ?>">
				</a>
			</li>
			<?php }} ?>			
		</ul>
	</div>
</section>

<section class="testimonials-section bg-grey border-top-white">
	<div class="container">
		<h2><?= get_field('testimonial_slider_title') ?></h2>
	</div>
	<div class="testimonials-slider">

		<?php $testimonial_slides = acf_get_fields_by_id(1415); 
			foreach($testimonial_slides as $item) { $the_item = get_field($item['name']); 
				if($the_item && $item['name'] != 'testimonial_slider_title' && $the_item['testimonial_author_name'] != ''){
					if($the_item['show_testimonial_slide'] != 'No') {
			?>

		<div class="testimonial-slide">
			<div class="testimonial-item">
				<?php if($the_item['testimonial_slide_video'] && trim($the_item['testimonial_slide_video']) != '') {
						$additional_str_start = '?';
						if(strrpos($the_item['testimonial_slide_video'], '?') !== false){
							$additional_str_start = '&';
						}
					?>
					<div class="img-block" style="background: #000;">
						<a data-fancybox href="<?= $the_item['testimonial_slide_video'] ?>" class="video-popup-opener"><iframe src="<?= $the_item['testimonial_slide_video'] . $additional_str_start ?>title=0&byline=0&portrait=0&sidedock=0&controls=0" width="100%" height="100%" frameborder="0"></iframe></a>
					</div>					
					<?php } else { ?>
					<div class="img-block" style="background-image: url(<?= $the_item['testimonial_slide_image'] ?>)">
						<a class="video-popup-opener"></a>
					</div>
					<?php } ?>
				<div class="text-block">
					<div class="author-block">
						<div class="author-name-holder">
							<div class="author-img" style="background-image: url('<?= $the_item['testimonial_author_image'] ?>');"></div>
							<div class="author-descr">
								<h5 class="author-name"><?= $the_item['testimonial_author_name'] ?></h5>
								<p><?= $the_item['testimonial_author_position'] ?></p>
							</div>
						</div>
						<img src="http://kwrdev.itcraftlab.com/wp-content/uploads/2021/02/icon-50.png" class="quote-img">
						<p><?= $the_item['testimonial_text'] ?></p>
					</div>
				</div>
			</div>
		</div>

		<?php } } } ?>
	</div>

	<?php if(get_field('display_bottom_cta') != 'No'){ ?>
	<div class="container">
		<div class="cta-block">
			<p><?= get_field('bottom_cta_description') ?></p>
			<div class="btn-center-holder">
				<a href="<?= get_field('bottom_cta_destination') ?>" class="btn"><?= get_field('bottom_cta_text') ?></a>
			</div>
		</div>
	</div>
	<?php } ?>
</section>

<?php get_footer(); ?>
