<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ) ?>" class="search-form form-light">
	<div class="input-group">
		<input name="s" id="s" type="text" class="form-control" placeholder="Search something" value="<?php echo get_search_query() ?>">
		<input type="submit" value="Search" class="btn">
	</div>
</form>