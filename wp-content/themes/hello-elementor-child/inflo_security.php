<?php /* Template Name: Inflo Security */ ?>

<?php get_header(); ?>

<section class="solutions-section bg-grey border-bottom-white">
	<div class="container">
		<div class="section-title">
			<h1 class="h2 gradient-underline"><?= get_field('page_title') ?></h1>
			<p><a href="<?= get_field('file_download_href') ?>" target="_blank"><?= get_field('file_download_text') ?></a></p>
			<p><?= get_field('page_paragraph') ?></p>
		</div>
		<div class="pricing-boxes-holder">
			<div class="pricing-box">
				<div class="pricing-box-inner">
					<h4><?= get_field('anchor_box_1') ?></h4>
				</div>
			</div>
			<div class="pricing-box">
				<div class="pricing-box-inner">
					<h4><?= get_field('anchor_box_2') ?></h4>
				</div>
			</div>
			<div class="pricing-box">
				<div class="pricing-box-inner">
					<h4><?= get_field('anchor_box_3') ?></h4>
				</div>
			</div>
			<div class="pricing-box">
				<div class="pricing-box-inner">
					<h4><?= the_field('anchor_box_4') ?></h4>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="border-bottom-light border-bottom-decor-section">
	<div class="container">
		<?php the_content(); ?>		
	</div>
	<span class="border-decor"></span>
</section>

<?php if(get_field('display_bottom_cta') != 'No'){ ?>
<section class="default-section bg-grey">
	<div class="container">
		<div class="cta-block">
			<p><?= get_field('bottom_cta_description') ?></p>
			<div class="btn-center-holder">
				<a href="<?= get_field('bottom_cta_destination') ?>" class="btn"><?= get_field('bottom_cta_text') ?></a>
			</div>
		</div>
	</div>
</section>
<?php } ?>

<?php get_footer(); ?>
