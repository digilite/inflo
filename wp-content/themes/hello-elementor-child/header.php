<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<title><?php wp_title(''); ?></title>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-TND8VFH');</script>
	<!-- End Google Tag Manager -->

	<!-- Fonts -->
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Baloo+2:wght@400;500;600;700;800&family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&display=swap" rel="stylesheet">

	<!-- Icon font -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />

	<!-- Fancybox -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />

	<?php wp_head(); ?>

	<?php
	$gf_prefix = '_' . explode('-', ICL_LANGUAGE_CODE)[1];
	if($gf_prefix == '_gb') {$gf_prefix = '';}
	?>
</head>

<body  <?php body_class(); ?> <?php if(is_search()){ ?> class="search-page" <?php } ?>>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TND8VFH"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

	<div id="wrapper">
		<header id="header" <?php global $wp; if((esc_url( home_url( $wp->request )) != esc_url( home_url() ) &&
		 esc_url( home_url( $wp->request )) != esc_url( home_url('about') ) &&
		 esc_url( home_url( $wp->request )) != esc_url( home_url('home') ) &&
		 esc_url( home_url( $wp->request )) != esc_url( home_url('careers') ) &&
		 esc_url( home_url( $wp->request )) != esc_url( home_url('inflohi') ) &&
		  !is_404() &&
		  esc_url( home_url( $wp->request )) != esc_url( home_url('other-solutions/assurance') ) &&
		  esc_url( home_url( $wp->request )) != esc_url( home_url('other-solutions/accountants-in-practice') ) &&
		  esc_url( home_url( $wp->request )) != esc_url( home_url('other-solutions/accountants-in-business')) )
		 ||
		  is_search() ) {echo  'class="header-light"';} ?>>
			<div class="container">
				<div class="header-inner">
					<strong class="logo">
						<a href="<?php echo esc_url( home_url() ); ?>">
							<?php //global $wp; if(esc_url( home_url( $wp->request )) == esc_url( home_url() ) ||
												//esc_url( home_url( $wp->request )) == esc_url( home_url('about') ) || is_404()	) {?>
							<img loading="lazy" src="https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/logo-white.png" alt="Inflo" width="84">
							<img loading="lazy" src="https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/logo.png" alt="Inflo" width="84" class="scrolled-logo">
							<?php //} else { ?>
							<!--<img src="http://kwrdev.itcraftlab.com/wp-content/uploads/2021/02/logo.png" alt="Inflo" width="84">-->
							<?php //} ?>
						</a>
					</strong>
					<div class="nav-holder">
						<a href="#" class="burger-opener">
							<span class="line"></span>
							<span class="text">Menu</span>
						</a>
						<?php include_once('includes/walker.php'); ?>
						<div class="menu">
							<ul class="nav">
							<?php wp_nav_menu( array(
		                            'menu' => 'primary',
		                            'menu_class' => 'nav',
		                            'container' => '',
		                            'depth' => '0',
		                            'walker' => new CustomWalker()
		                    ) ); ?>
		                	</ul>

							<div class="header-btn-holder">
								<!--<a href="<?= wp_login_url() ?>" class="btn btn-white-outline">Login</a>
								<a href="#" class="btn btn-white">Get Inflo free</a>-->
								<a href="<?= esc_attr( get_option('gb_header_cta'.$gf_prefix) ) ?>" class="btn">Get a demo</a>
								<a href="<?= esc_attr( get_option('gb_header_login'.$gf_prefix) ) ?>" class="btn btn-white">Login</a>
							</div>

							<div id="header_lang_switcher"> <?php
								$languages = icl_get_languages( 'skip_missing=0&orderby=custom' );
								$output    = "";
								if ( is_array( $languages ) ) {
									$curr_lang = get_locale();
									$flag = "";
									foreach ($languages as $lang) {
										if ($curr_lang === $lang['default_locale']) {
											$flag = '<img loading="lazy" src=' . $lang['country_flag_url'] . ' />';
											$ltext = esc_html( $lang['native_name'] );
										}
									}
								    $output.= '<div class="wpml-language-nav"><a href="#" id="wpml_opener">'. $flag  . /*' ' . $ltext .*/ '</a>';
								    $output.= '<div class="wpml-language-nav-sub-wrapper" style="display:none"><div class="wpml-language-nav-sub" >';
								    $output.= "<ul class='wpml-language-navigation'>";
								    foreach ( $languages as $lang ) {
								        $output.= "<li class='language_" . esc_attr( $lang['language_code'] ) . "'><a href='";

								         $output .= esc_url( $lang['url'] );

								        $output .= "'>";
								        $output.= "<span class='wpml-lang-flag'><img loading='lazy' title='" . esc_attr( $lang['native_name'] ) . "' src='" . esc_url( $lang['country_flag_url'] ) . "' /></span> ";
								        $output.= "<span class='wpml-lang-name'>" . esc_html( $lang['native_name'] ) . "</span>";
								        $output.= "</a></li>";
								    }

								    $output.= "</ul></div></div></div>";
								}

								echo $output; ?>
							</div>
						</div>



					</div>

				</div>
			</div>
		</header>
		<?php if(is_search()){ ?>
		<div id="header_placeholder"></div>
		<?php } ?>
		<main id="main">
