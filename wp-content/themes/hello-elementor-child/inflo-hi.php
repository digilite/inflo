<?php /* Template Name: Inflo HI */ ?>

<?php get_header(); ?>

<section class="intro-section secondary-page bg-anim border-bottom-white">
	<div class="container">
		<div class="text-block full-width">
			<h1 class="h2"><?= get_field('solution_title') ?></h1>
			<p class="intro-note"><?= get_field('solution_subtitle') ?></p>
		</div>
		<div class="solutions-features-card-holder">
			<?php
			$solution_top_blocks = acf_get_fields_by_id(2100);
			foreach($solution_top_blocks as $item) { $the_item = get_field($item['name']); if($the_item != null && $the_item != '' && $the_item['solution_top_block_title'] != '') {
			?>
			<div class="solutions-features-card">
				<h3 class="card-title"><?= $the_item['solution_top_block_title'] ?></h3>
				<?= $the_item['solution_top_block_text'] ?>
			</div>
			<?php } } ?>

		</div>
	</div>
</section>

<?php
$use_case = get_field('choose_product_use_case');
$simple_section_class= '';
if($use_case && $use_case != null && $use_case != '') {
	$simple_section_class= 'border-bottom-light';
} ?>

<?php if(get_field('simple_rich_text_section_show') == 'Yes') { ?>
<section class="default-section <?= $simple_section_class ?>">
	<div class="container">
		<div class="two-third-col-holder">
			<?= get_field('simple_rich_text_section_content') ?>
		</div>
	</div>
</section>
<?php } ?>

<?php

if($use_case && $use_case != null && $use_case != '') {

$use_case_solution = get_field('use_case_solution', $use_case->ID);
$use_case_impact = get_field('use_case_impact', $use_case->ID);
//$use_case_tags = get_the_tags( $use_case->ID );
$use_case_tags = get_the_terms( $use_case->ID, 'product_tax' );
?>
<section class="default-section border-bottom-white bg-grey">
	<div class="container">
<div class="use-case-preview-holder">
	<div class="use-case-preview">
		<div class="descr-block">
			<div class="title-holder">
				<strong class="subtitle">Customer Success</strong>
				<h4 class="title"><?= $use_case->post_title ?></h4>
			</div>
			<h5>Background</h5>
			<p><?= $use_case_solution ?></p>
			<h5>Impact</h5>
			<p><?= $use_case_impact ?></p>
			<ul class="topic-list">
				<?php foreach($use_case_tags as $the_tag){
					$tag_link =  get_term_link( $the_tag->term_id, 'product_tax' );
				?>
				<li>
					<a href="<?= $tag_link/*str_replace('/tag', '', get_tag_link($use_case_tags[$i]->term_id))*/ ?>">
						<img loading="lazy" src="<?= get_field('image_for_this_taxonomy', $the_tag)['sizes']['thumbnail'] ?>" class="icon"> <?= $the_tag->name ?>
					</a>
				</li>
				<?php } ?>

			</ul>
		</div>
		<div class="testimonial-block">
			<div class="testimonial-item">
				<div class="text-block">
					<div class="author-block">
						<div class="author-name-holder">
							<div class="author-img" style="background-image: url('<?= get_field('testimonial_avatar', $use_case->ID)['sizes']['thumbnail'] ?>');"></div>
							<div class="author-descr">
								<h5 class="author-name"><?= get_field('testimonial_name', $use_case->ID) ?></h5>
								<p><?= get_field('testimonial_job_title', $use_case->ID) ?></p>
							</div>
						</div>
						<img loading="lazy" src="https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/icon-50.png" class="quote-img">
						<p><?= get_field('testimonial_quote', $use_case->ID) ?></p>
						<a href="<?= get_permalink($use_case->ID) ?>" class="btn btn-outline">Read Story</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</section>
<?php } ?>


<section class="partners-section">
	<div class="container">
		<h2 class="h4"><?= get_field("partners_text", 1312) ?></h2>
		<ul class="partners-list">
			<?php $partner_icons = acf_get_fields_by_id(1308);
			foreach($partner_icons as $icon) { if($icon['name'] != 'partners_text') { ?>
			<?php $the_image = get_field($icon['name'], 1312);
				$partner_link = $the_image['partner_link'];
				if(trim($partner_link) == '') {
					$partner_link = '/'.ICL_LANGUAGE_CODE.'/customer-success';
				}
			?>
			<li>
				<a href="<?= $partner_link ?>">
					<img loading="lazy" src="<?= $the_image['partner_image']['sizes']['medium'] ?>">
				</a>
			</li>
			<?php }} ?>
		</ul>
	</div>
</section>

<?php if(get_field('display_bottom_cta') != 'No'){ ?>
<section class="default-section">
	<div class="container">
		<div class="cta-block">
			<p><?= get_field('bottom_cta_description') ?></p>
			<div class="btn-center-holder">
				<a href="<?= get_field('bottom_cta_destination') ?>" class="btn"><?= get_field('bottom_cta_text') ?></a>
			</div>
		</div>
	</div>
</section>
<?php } ?>

<?php get_footer(); ?>
