<?php

function child_enqueue__parent_scripts()
{

    wp_enqueue_style('parent', get_template_directory_uri() . '/style.css');
}

add_action('wp_enqueue_scripts', 'child_enqueue__parent_scripts');

if (!function_exists('digitalallies_asset_path')) :
    function digitalallies_asset_path($filename)
    {
        $filename_split = explode('.', $filename);
        $dir            = end($filename_split);
        $manifest_path  = dirname(__FILE__) . '/dist/assets/' . $dir . '/rev-manifest.json';

        if (file_exists($manifest_path)) {
            $manifest = json_decode(file_get_contents($manifest_path), true);
        } else {
            $manifest = array();
        }

        if (array_key_exists($filename, $manifest)) {
            return $manifest[$filename];
        }
        return $filename;
    }
endif;

function styles_and_scripts()
{
    // wp_enqueue_style('style', get_stylesheet_directory_uri().'/style.css' );
    // wp_enqueue_style('slick', get_stylesheet_directory_uri().'/css/slick.min.css' );
    // wp_enqueue_style('slick-theme', get_stylesheet_directory_uri().'/css/slick-theme.min.css' );
    // wp_enqueue_style('main', get_stylesheet_directory_uri().'/css/main.css' );

    // Enqueue the main Stylesheet.
    wp_enqueue_style('main-stylesheet', get_stylesheet_directory_uri() . '/dist/assets/css/' . digitalallies_asset_path('app.css'), array(), '2.10.4', 'all');

    //wp_dequeue_script('jquery');
    //wp_dequeue_script('jquery-migrate');

    //wp_enqueue_script('jquery', false, array(), false, true );
    //wp_enqueue_script('jquery-migrate', false, array(), false, true );
    wp_enqueue_script('paroller', get_stylesheet_directory_uri() . '/js/paroller.js', array('jquery'), false, true);
    wp_enqueue_script('slick', get_stylesheet_directory_uri() . '/js/slick.min.js', array('jquery'), false, true);
    wp_enqueue_script('fancybox', get_stylesheet_directory_uri() . '/js/fancybox.min.js', array('jquery'), false, true);
    wp_enqueue_script('matchHeight', get_stylesheet_directory_uri() . '/js/jquery.matchHeight.js', array('jquery'), false, true);
    wp_enqueue_script('main', get_stylesheet_directory_uri() . '/js/main.js', array('jquery'), false, true);
    wp_enqueue_script('custom', get_stylesheet_directory_uri() . '/js/custom.js', array('jquery'), false, true);
    wp_enqueue_script('in-viewport', get_stylesheet_directory_uri() . '/js/in-viewport.js', array('jquery'), false, true);
    wp_enqueue_script('lazy', get_stylesheet_directory_uri() . '/js/lazy.js', array('jquery'), false, true);
}
add_action('wp_enqueue_scripts', 'styles_and_scripts', 99);

function register_global_fields_settings()
{
    //register our settings
    register_setting('global-fields-settings-group', 'gb_header_cta');
    register_setting('global-fields-settings-group', 'gb_header_cta_au');
    register_setting('global-fields-settings-group', 'gb_header_cta_ca');
    register_setting('global-fields-settings-group', 'gb_header_cta_us');
    register_setting('global-fields-settings-group', 'gb_header_login');
    register_setting('global-fields-settings-group', 'gb_header_login_au');
    register_setting('global-fields-settings-group', 'gb_header_login_ca');
    register_setting('global-fields-settings-group', 'gb_header_login_us');
    register_setting('global-fields-settings-group', 'gb_twitter');
    register_setting('global-fields-settings-group', 'gb_twitter_au');
    register_setting('global-fields-settings-group', 'gb_twitter_ca');
    register_setting('global-fields-settings-group', 'gb_twitter_us');
    register_setting('global-fields-settings-group', 'gb_linkedin');
    register_setting('global-fields-settings-group', 'gb_linkedin_au');
    register_setting('global-fields-settings-group', 'gb_linkedin_ca');
    register_setting('global-fields-settings-group', 'gb_linkedin_us');
    register_setting('global-fields-settings-group', 'gb_email');
    register_setting('global-fields-settings-group', 'gb_email_au');
    register_setting('global-fields-settings-group', 'gb_email_ca');
    register_setting('global-fields-settings-group', 'gb_email_us');
    register_setting('global-fields-settings-group', 'gb_subtext');
    register_setting('global-fields-settings-group', 'gb_subtext_au');
    register_setting('global-fields-settings-group', 'gb_subtext_ca');
    register_setting('global-fields-settings-group', 'gb_subtext_us');
    register_setting('global-fields-settings-group', '404_title');
    register_setting('global-fields-settings-group', '404_text');
    register_setting('global-fields-settings-group', '404_cta1_url');
    register_setting('global-fields-settings-group', '404_cta1_text');
    register_setting('global-fields-settings-group', '404_cta2_url');
    register_setting('global-fields-settings-group', '404_cta2_text');
}
add_action('admin_init', 'register_global_fields_settings');

function create_global_fields_menu()
{
    add_menu_page('Global Fields', 'Global Fields', 'publish_posts', 'global_fields', 'global_fields_render_page', 'dashicons-text-page');
}
add_action('admin_menu', 'create_global_fields_menu');

function global_fields_render_page()
{ ?>

    <h2>Global Fields</h2>
    <form method="post" action="options.php">
        <?php settings_fields('global-fields-settings-group'); ?>
        <?php do_settings_sections('global-fields-settings-group'); ?>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">Header CTA Destination</th>
                <td>UK <input type="text" name="gb_header_cta" value="<?php echo esc_attr(get_option('gb_header_cta')); ?>" /></td>
                <td>AU <input type="text" name="gb_header_cta_au" value="<?php echo esc_attr(get_option('gb_header_cta_au')); ?>" /></td>
                <td>CA <input type="text" name="gb_header_cta_ca" value="<?php echo esc_attr(get_option('gb_header_cta_ca')); ?>" /></td>
                <td>US <input type="text" name="gb_header_cta_us" value="<?php echo esc_attr(get_option('gb_header_cta_us')); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Header Login Destination</th>
                <td>UK <input type="text" name="gb_header_login" value="<?php echo esc_attr(get_option('gb_header_login')); ?>" /></td>
                <td>AU <input type="text" name="gb_header_login_au" value="<?php echo esc_attr(get_option('gb_header_login_au')); ?>" /></td>
                <td>CA <input type="text" name="gb_header_login_ca" value="<?php echo esc_attr(get_option('gb_header_login_ca')); ?>" /></td>
                <td>US <input type="text" name="gb_header_login_us" value="<?php echo esc_attr(get_option('gb_header_login_us')); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Twitter</th>
                <td>UK <input type="text" name="gb_twitter" value="<?php echo esc_attr(get_option('gb_twitter')); ?>" /></td>
                <td>AU <input type="text" name="gb_twitter_au" value="<?php echo esc_attr(get_option('gb_twitter_au')); ?>" /></td>
                <td>CA <input type="text" name="gb_twitter_ca" value="<?php echo esc_attr(get_option('gb_twitter_ca')); ?>" /></td>
                <td>US <input type="text" name="gb_twitter_us" value="<?php echo esc_attr(get_option('gb_twitter_us')); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Linkedin</th>
                <td>UK <input type="text" name="gb_linkedin" value="<?php echo esc_attr(get_option('gb_linkedin')); ?>" /></td>
                <td>AU <input type="text" name="gb_linkedin_au" value="<?php echo esc_attr(get_option('gb_linkedin_au')); ?>" /></td>
                <td>CA <input type="text" name="gb_linkedin_ca" value="<?php echo esc_attr(get_option('gb_linkedin_ca')); ?>" /></td>
                <td>US <input type="text" name="gb_linkedin_us" value="<?php echo esc_attr(get_option('gb_linkedin_us')); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Email (for example - shown in footer)</th>
                <td>UK <input type="text" name="gb_email" value="<?php echo esc_attr(get_option('gb_email')); ?>" /></td>
                <td>AU <input type="text" name="gb_email_au" value="<?php echo esc_attr(get_option('gb_email_au')); ?>" /></td>
                <td>CA <input type="text" name="gb_email_ca" value="<?php echo esc_attr(get_option('gb_email_ca')); ?>" /></td>
                <td>US <input type="text" name="gb_email_us" value="<?php echo esc_attr(get_option('gb_email_us')); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Text In Footer Before Subscribe</th>
                <td>UK <textarea name="gb_subtext"><?php echo esc_attr(get_option('gb_subtext')); ?></textarea></td>
                <td>AU <textarea name="gb_subtext_au"><?php echo esc_attr(get_option('gb_subtext_au')); ?></textarea></td>
                <td>CA <textarea name="gb_subtext_ca"><?php echo esc_attr(get_option('gb_subtext_ca')); ?></textarea></td>
                <td>US <textarea name="gb_subtext_us"><?php echo esc_attr(get_option('gb_subtext_us')); ?></textarea></td>
            </tr>

            <tr>
                <td>
                    <h3>404:</h3>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Title</th>
                <td><input type="text" name="404_title" value="<?php echo esc_attr(get_option('404_title')); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Text</th>
                <td><textarea name="404_text"><?php echo esc_attr(get_option('404_text')); ?></textarea></td>
            </tr>

            <tr valign="top">
                <th scope="row">CTA 1 URL</th>
                <td><input type="text" name="404_cta1_url" value="<?php echo esc_attr(get_option('404_cta1_url')); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">CTA 1 Text</th>
                <td><input type="text" name="404_cta1_text" value="<?php echo esc_attr(get_option('404_cta1_text')); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">CTA 2 URL</th>
                <td><input type="text" name="404_cta2_url" value="<?php echo esc_attr(get_option('404_cta2_url')); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">CTA 2 Text</th>
                <td><input type="text" name="404_cta2_text" value="<?php echo esc_attr(get_option('404_cta2_text')); ?>" /></td>
            </tr>
        </table>

        <?php submit_button(); ?>

    </form>
<?php
}

add_filter('do_redirect_guess_404_permalink', '__return_false');

function special_product_taxonomy()
{
    register_taxonomy(
        'product_tax',  // The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'use_cases',             // post type name
        array(
            'hierarchical' => false,
            'label' => 'Product tags', // display name
            'query_var' => true,
            'has_archive' => true,
            'rewrite' => array(
                'slug' => 'used-product',    // This controls the base slug that will display before each term
                'with_front' => false  // Don't display the category base before
            ),
            'show_in_rest' => true,
            'show_admin_column' => true
        )
    );
}
add_action('init', 'special_product_taxonomy');

function use_cases_posts()
{

    register_post_type(
        'use_cases',
        array(
            'labels' => array(
                'name' => __('Use Cases'),
                'singular_name' => __('Use Case'),
                'all_items' => __('All Use Cases'),
                'add_new' => __('Add New'),
                'add_new_item' => __('Add New'),
                'edit' => __('Edit'),
                'edit_item' => __('Edit Use Case'),
                'new_item' => __('New Use Case'),
                'view_item' => __('View Use Case'),
                'search_items' => __('Search Use Case'),
                'not_found' =>  __('Nothing found in the Database.'),
                'not_found_in_trash' => __('Nothing found in Trash'),
                'parent_item_colon' => ''
            ), /* end of arrays */
            'description' => __('This is the Use Case post type'),
            'public' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => true,
            'show_ui' => true,
            'query_var' => true,
            'has_archive' => false,
            //'show_in_menu'       => false,
            //'show_in_nav_menus'  => false,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-welcome-write-blog',
            'rewrite'   => array('slug' => 'customer-success', 'with_front' => false),
            'has_archive' => false,
            'capability_type' => 'post',
            'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'revisions', 'comments', 'page-attributes'),
            'taxonomies' => array('product_tax')
        ) /* end of options */
    );
}
add_action('init', 'use_cases_posts');

// Fix for "customer-success" pagination
add_action('init', function () {
    add_rewrite_rule(
        'customer-success/page/([0-9]+)/?$',
        'index.php?pagename=customer-success&paged=$matches[1]',
        'top'
    );
});

function product_posts()
{

    register_post_type(
        'products',
        array(
            'labels' => array(
                'name' => __('Products'),
                'singular_name' => __('Product'),
                'all_items' => __('All Products'),
                'add_new' => __('Add Product'),
                'add_new_item' => __('Add Product'),
                'edit' => __('Edit'),
                'edit_item' => __('Edit Product'),
                'new_item' => __('New Product'),
                'view_item' => __('View Product'),
                'search_items' => __('Search Product'),
                'not_found' =>  __('Nothing found in the Database.'),
                'not_found_in_trash' => __('Nothing found in Trash'),
                'parent_item_colon' => ''
            ), /* end of arrays */
            'description' => __('This is the Product post type'),
            'public' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => true,
            'show_ui' => true,
            'query_var' => true,
            'hierarchical' => false,
            //'show_in_menu'       => false,
            //'show_in_nav_menus'  => false,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-welcome-write-blog',
            'rewrite'   => array('slug' => 'products', 'with_front' => false),
            'has_archive' => false,
            'capability_type' => 'post',
            'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'revisions', 'comments', 'page-attributes'),
            'taxonomies' => array()
        ) /* end of options */
    );
}
add_action('init', 'product_posts');

add_action('add_meta_boxes', function () {
    add_meta_box('parent_product', 'Parent Product', 'modules_parent_meta_box', 'modules', 'side', 'default');
});

function modules_parent_meta_box($post)
{
    /* $pages = wp_dropdown_pages(array('post_type' => 'products', 'name' => 'parent_product', 'show_option_none' => __('(no parent)'), 'sort_column'=> 'post_title', 'echo' => 0));
        print_r($pages);
        if ( ! empty($pages) ) {
            echo $pages;
        } // end empty pages check*/
    $current_parent_product = get_post_meta($post->ID, 'parent_product')[0];
    echo '<select id="parent_product" name="parent_product">';
    $prod_posts = get_posts(['numberposts' => -1, 'post_type' => 'products']);
    //print_r($prod_posts);
    foreach ($prod_posts as $ppost) {
        $selected = '';
        if ($current_parent_product == $ppost->post_name) {
            $selected = 'selected';
        }
        echo '<option value="' . $ppost->post_name . '" ' . $selected . '>' . $ppost->post_title . '</option>';
    }
    echo '</select>';
}

add_action('save_post', 'so_save_metabox');

function so_save_metabox()
{
    global $post;

    if (get_post_type($post->ID) == 'modules') { //print_r($_POST); print_r(get_post_meta($post->ID)); die;
        if (isset($_POST["parent_product"])) {
            //UPDATE:
            $meta_element = $_POST['parent_product'];
            //END OF UPDATE

            update_post_meta($post->ID, 'parent_product', $meta_element);
        }
    }
}

function module_post_link($post_link, $post)
{
    //$post = get_post($id);
    if (is_object($post)) {

        if (get_post_type($post->ID) == 'modules') {
            $parent_product = get_post_meta($post->ID, 'parent_product')[0];

            //$parent_product_post = get_post($parent_product);
            return str_replace('%parent_product%', $parent_product, $post_link);
        }
    }
    return $post_link;
}
add_filter('post_type_link', 'module_post_link', 1, 3);

function module_posts()
{

    register_post_type(
        'modules',
        array(
            'labels' => array(
                'name' => __('Modules'),
                'singular_name' => __('Module'),
                'all_items' => __('Modules'),
                'add_new' => __('Add New'),
                'add_new_item' => __('Add New'),
                'edit' => __('Edit'),
                'edit_item' => __('Edit Module'),
                'new_item' => __('New Module'),
                'view_item' => __('View Module'),
                'search_items' => __('Search Module'),
                'not_found' =>  __('Nothing found in the Database.'),
                'not_found_in_trash' => __('Nothing found in Trash'),
                'parent_item_colon' => ''
            ), /* end of arrays */
            'description' => __('This is the Module post type'),
            'public' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => true,
            'show_ui' => true,
            'query_var' => true,
            'hierarchical' => false,
            'show_in_menu' => 'edit.php?post_type=products',
            //'show_in_nav_menus'  => false,
            //'menu_position' => 6,
            'menu_icon' => 'dashicons-welcome-write-blog',
            'rewrite'   => array('slug' => '%parent_product%/modules', 'with_front' => false),
            'has_archive' => false,
            'capability_type' => 'post',
            'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'revisions', 'comments', 'page-attributes'),
            'taxonomies' => array()
        ) /* end of options */
    );
}
add_action('init', 'module_posts');

add_action('init', 'do_rewrite');
function do_rewrite()
{

    add_rewrite_rule('^([^/]*)/modules/([^/]*)/?', 'index.php?post_type=modules&name=$matches[2]', 'top');
}

/*order acf meta boxes start*/
add_filter('get_user_option_meta-box-order_page', 'metabox_order');
function metabox_order($order)
{

    $this_post_id = $_GET['post'];

    $pagetemplate = get_post_meta($this_post_id, '_wp_page_template', true);
    if (!empty($pagetemplate)) {
        if ($pagetemplate == 'homepage2.php') {
            return array(
                'normal' => join(
                    ",",
                    array(
                        'acf-group_602245e59c0e6',
                        'acf-group_6033609f183fb',
                        'acf-group_60631f1711a21',
                        'acf-group_602f68ad4d1e1',
                        'acf-group_602f70919ef18',
                        'acf-group_602f7d957bb9f',
                        'acf-group_606320ff99efd',
                        'acf-group_602f83cd50429',
                        'acf-group_602f86f12875b',
                        'acf-group_6063273526fda',
                        'acf-group_604217960b406',
                        'acf-group_602f93659dd67',
                        'acf-group_60338384b8367',
                    )
                ),
            );
        } else if ($pagetemplate == 'about2.php') {
            return array(
                'normal' => join(
                    ",",
                    array(
                        'acf-group_6033609f183fb',
                        'acf-group_606320ff99efd',
                        'acf-group_6033690f11337',
                        'acf-group_6054a57ad439c',
                        'acf-group_6065bda7a9ee6',
                        'acf-group_60af6aebee24a',
                        'acf-group_603369bf843ff',
                        'acf-group_6033718546821',
                        'acf-group_602f68ad4d1e1',
                        'acf-group_6053118639dd5',
                        'acf-group_604b5f0d96bec',
                        'acf-group_602f93659dd67',
                    )
                ),
            );
        } else if ($pagetemplate == 'accountants_anything.php') {
            return array(
                'normal' => join(
                    ",",
                    array(
                        'acf-group_6066df569b55e',
                        'acf-group_60af8b560f40e',
                        'acf-group_602f7d957bb9f',
                        'acf-group_6066e238c784b',
                        'acf-group_6053118639dd5',
                        'acf-group_604b5f0d96bec',
                        'acf-group_602f68ad4d1e1',
                        'acf-group_602f93659dd67',

                    )
                ),
            );
        } else if ($pagetemplate == 'blog-post.php') {
            return array(
                'normal' => join(
                    ",",
                    array(
                        'acf-group_6037be501bbc5',
                        'acf-group_602f93659dd67',
                    )
                ),
            );
        } else if ($pagetemplate == 'career.php') {
            return array(
                'normal' => join(
                    ",",
                    array(
                        'acf-group_604b506389255',
                        'acf-group_6033bf5ba8abe',
                        'acf-group_604b57c224b13',
                        'acf-group_604b5cc382b0c',
                        'acf-group_604b5f0d96bec',
                        'acf-group_604b677da1ecb',
                        'acf-group_604b6b5c6e8a0',
                        'acf-group_604217960b406',
                        'acf-group_60337ef56b443',
                        'acf-group_602f93659dd67',
                    )
                ),
            );
        } else if ($pagetemplate == 'contact.php') {
            return array(
                'normal' => join(
                    ",",
                    array(
                        'acf-group_603377f33ea9e',
                        'acf-group_60337a7144c0e',
                        'acf-group_60337ef56b443',
                        'acf-group_602f93659dd67',

                    )
                ),
            );
        } else if ($pagetemplate == 'inflo-hi.php') {
            return array(
                'normal' => join(
                    ",",
                    array(
                        'acf-group_603652dc7bfbd',
                        'acf-group_60365409d3d40',
                        'acf-group_6053118639dd5',
                        'acf-group_60360dafeb3db',
                        'acf-group_602f68ad4d1e1',
                        'acf-group_602f93659dd67',

                    )
                ),
            );
        } else if ($pagetemplate == 'inflo_security.php') {
            return array(
                'normal' => join(
                    ",",
                    array(
                        'acf-group_60647c32af425',
                        'acf-group_60647c72a65d6',
                        'acf-group_6065cf1397578',
                        'acf-group_602f93659dd67',

                    )
                ),
            );
        } else if ($pagetemplate == 'info.php') {
            return array(
                'normal' => join(
                    ",",
                    array(
                        'acf-group_60337ef56b443',
                        'acf-group_602f93659dd67',

                    )
                ),
            );
        } else if ($pagetemplate == 'learn.php') {
            return array(
                'normal' => join(
                    ",",
                    array(
                        'acf-group_603381ba0c8ae',
                        'acf-group_60338384b8367',
                        'acf-group_603393d6efe82',
                        'acf-group_603398a05f9aa',
                        'acf-group_60339bcdc25fa',
                        'acf-group_60339e90d61ab',
                        'acf-group_602f93659dd67',
                    )
                ),
            );
        } else if ($pagetemplate == 'other_solutions_children.php') {
            return array(
                'normal' => join(
                    ",",
                    array(
                        'acf-group_602f68ad4d1e1',
                        'acf-group_604217960b406',
                        'acf-group_6036208773991',
                        'acf-group_60338384b8367',
                        'acf-group_602f93659dd67',
                    )
                ),
            );
        } else if ($pagetemplate == 'pricing-details.php') {
            return array(
                'normal' => join(
                    ",",
                    array(
                        'acf-group_6034ea502e821',
                        'acf-group_6066c8370cd1b',
                        'acf-group_605b0c0ec04e2',
                        'acf-group_602f83cd50429',
                        'acf-group_6053118639dd5',
                        'acf-group_60360dafeb3db',
                        'acf-group_602f68ad4d1e1',
                        'acf-group_60337ef56b443',
                        'acf-group_605b1449c2fe8',
                        'acf-group_602f93659dd67',
                    )
                ),
            );
        } else if ($pagetemplate == 'pricing.php') {
            return array(
                'normal' => join(
                    ",",
                    array(
                        'acf-group_6034ea502e821',
                        'acf-group_6034ede90ad6e',
                        'acf-group_60acdd0a2c9c1',
                        'acf-group_6053118639dd5',
                        'acf-group_6034f8c4a76c4',
                        'acf-group_602f68ad4d1e1',
                        'acf-group_60530dd53e609',
                        'acf-group_606c0550ab46b',
                        'acf-group_6035007c6dc10',
                        'acf-group_60337ef56b443',
                        'acf-group_602f93659dd67',
                    )
                ),
            );
        } else if ($pagetemplate == 'product.php') {
            return array(
                'normal' => join(
                    ",",
                    array(
                        'acf-group_6033af21da114',
                        'acf-group_60659482bdfe6',
                        'acf-group_60642e7bbbe7e',
                        'acf-group_6036208773991',
                        'acf-group_602f86f12875b',
                        'acf-group_602f68ad4d1e1',
                        'acf-group_604217960b406',
                        'acf-group_602f93659dd67',
                    )
                ),
            );
        } else if ($pagetemplate == 'solution-child.php') {
            return array(
                'normal' => join(
                    ",",
                    array(
                        'acf-group_603652dc7bfbd',
                        'acf-group_60365409d3d40',
                        'acf-group_602f83cd50429',
                        'acf-group_60365791ddf54',
                        'acf-group_602f93659dd67',
                    )
                ),
            );
        } else if ($pagetemplate == 'solutions-parent-new.php') {
            return array(
                'normal' => join(
                    ",",
                    array(
                        'acf-group_603652dc7bfbd',
                        'acf-group_60631f1711a21',
                        'acf-group_60642e7bbbe7e',
                        'acf-group_6036208773991',
                        'acf-group_60363d4d4da8d',
                        'acf-group_604b5f0d96bec',
                        'acf-group_602f93659dd67',
                    )
                ),
            );
        } else if ($pagetemplate == 'solutions-parent.php') {
            return array(
                'normal' => join(
                    ",",
                    array(
                        'acf-group_603632dd2779e',
                        'acf-group_60363429eb449',
                        'acf-group_604b5f0d96bec',
                        'acf-group_602f93659dd67',
                    )
                ),
            );
        } else if ($pagetemplate == 'thank-you.php') {
            return array(
                'normal' => join(
                    ",",
                    array(
                        'acf-group_6038a84856e23',
                        'acf-group_602f93659dd67',
                    )
                ),
            );
        } else if ($pagetemplate == 'use_case_listing.php') {
            return array(
                'normal' => join(
                    ",",
                    array(
                        'acf-group_6034cab623484',
                        'acf-group_6034cb4783dc2',
                        'acf-group_602f93659dd67',
                    )
                ),
            );
        }
    }
}

add_filter('get_user_option_meta-box-order_products', 'metabox_order_products');
add_filter('get_user_option_meta-box-order_modules', 'metabox_order_products');
function metabox_order_products($order)
{

    $this_post_id = $_GET['post'];

    $pagetemplate = get_post_meta($this_post_id, '_wp_page_template', true);
    if (!empty($pagetemplate)) {
        if ($pagetemplate == 'single-product.php') {
            return array(
                'normal' => join(
                    ",",
                    array(
                        'acf-group_6066f963d5c5c',
                        'acf-group_60360dafeb3db',
                        'acf-group_60361cfeeb500',
                        'acf-group_602f93659dd67',
                        'acf-group_60338384b8367',
                    )
                ),
            );
        }
    }
}

add_filter('get_user_option_meta-box-order_use_cases', 'metabox_order_use_cases');
function metabox_order_use_cases($order)
{

    $this_post_id = $_GET['post'];

    $pagetemplate = get_post_meta($this_post_id, '_wp_page_template', true);
    if (!empty($pagetemplate)) {
        if ($pagetemplate == 'use_case.php') {
            return array(
                'normal' => join(
                    ",",
                    array(
                        'acf-group_6033bf5ba8abe',
                        'acf-group_6033cb7d4bff4',
                        'acf-group_6033cd7a4019c',
                        'acf-group_6034b6a5a89e4',
                        'acf-group_602f93659dd67',
                        'acf-group_60360c0913051',
                    )
                ),
            );
        }
    }
}
/*order acf meta boxes end*/

function defer_parsing_of_js($url)
{
    if (is_user_logged_in()) return $url; //don't break WP Admin
    if (FALSE === strpos($url, '.js')) return $url;
    if (strpos($url, 'jquery.min.js')) return $url;
    return str_replace(' src', ' defer src', $url);
}
add_filter('script_loader_tag', 'defer_parsing_of_js', 10);

/* Allies Group - custom scripts */

// ACF Sync functionality
include 'library/acf-sync.php';

// Responsive Images
include 'library/responsive-images.php';
