
<div class="custom-popup" id="digital_audit_popup">
	<div class="popup-inner">
		<a href="#" class="close close-popup"></a>
		<div class="_white-form-holder digital-audit-form-holder">
			<h4 class="text-center">Register now to see the Digital Audit in action</h4>
			<!--[if lte IE 8]>
			<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
			<![endif]-->
			<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
			<script>
			  hbspt.forms.create({
				portalId: "7936749",
				formId: "ac7654fe-584e-4007-b8c1-5230bcc8d2f6"
			});
			</script>
		</div>
	</div>
</div>
</main>
		<footer id="footer">
			<div class="container">
				<div class="footer-inner">
					<div class="col logo-col">
						<strong class="logo">
							<a href="<?php echo esc_url( home_url() ); ?>">
								<img loading="lazy" src="https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/logo.png" alt="Inflo" width="84">
							</a>
						</strong>
						<ul class="network-list">
							<li>
								<a href="<?= esc_attr( get_option('gb_twitter'.$gf_prefix) ) ?>" target="_blank">
									<i class="fab fa-twitter"></i>
								</a>
							</li>
							<li>
								<a href="<?= esc_attr( get_option('gb_linkedin'.$gf_prefix) ) ?>" target="_blank">
									<i class="fab fa-linkedin-in"></i>
								</a>
							</li>
						</ul>
						<div class="email-link-holder">
							<a href="mailto:<?= esc_attr( get_option('gb_email'.$gf_prefix) ) ?>"><?= esc_attr( get_option('gb_email'.$gf_prefix) ) ?></a>
						</div>
						<div id="footer_hub_form" style="display: none;">
						<?php echo do_shortcode('[hubspot type=form portal=7936749 id=f83a3ac6-3b5f-4081-a02b-bc0d7f9d9a1c]'); ?>
						</div>
						<form action="#" class="subscribe-form" id="hub_sub">
							<div class="form-text">
								<p><?= esc_attr( get_option('gb_subtext'.$gf_prefix) ) ?></p>
							</div>
							<div class="single-form-row">
								<input type="email" class="form-control" placeholder="Email">
								<input type="submit" value="Subscribe" class="btn btn-success">
							</div>
						</form>

						<div id="footer_lang_switcher"> <?php
							$languages = icl_get_languages( 'skip_missing=0&orderby=custom' );
							$output    = "";
							if ( is_array( $languages ) ) {
								$curr_lang = get_locale();
								$flag = "";
								foreach ($languages as $lang) {
									if ($curr_lang === $lang['default_locale']) {
										$flag = '<img loading="lazy" src="https://dev.digilabs.ca/inflo/wp-content/uploads/2021/03/paper-plane.png" />';//'<img src=' . $lang['country_flag_url'] . ' />';
										$ltext = esc_html( $lang['native_name'] );
									}
								}
							    $output.= '<div class="wpml-language-nav"><a href="#" id="wpml_opener_footer">'. $flag  . ' <span class="lang-name">' . $ltext . '</span></a>';
							    $output.= '<div class="wpml-language-nav-sub-wrapper" style="display:none"><div class="wpml-language-nav-sub" >';
							    $output.= "<ul class='wpml-language-navigation'>";
							    foreach ( $languages as $lang ) {
							        $output.= "<li class='language_" . esc_attr( $lang['language_code'] ) . "'><a href='";

							         $output .= esc_url( $lang['url'] );

							        $output .= "'>";
							        $output.= "<span class='wpml-lang-flag'><img loading='lazy' title='" . esc_attr( $lang['native_name'] ) . "' src='" . esc_url( $lang['country_flag_url'] ) . "' /></span> ";
							        $output.= "<span class='wpml-lang-name'>" . esc_html( $lang['native_name'] ) . "</span>";
							        $output.= "</a></li>";
							    }

							    $output.= "</ul></div></div></div>";
							}

							echo $output; ?>
						</div>
					</div>
					<?php include_once('includes/footer_walker.php'); ?>
					<div class="col">

							<?php wp_nav_menu( array(
		                            'menu' => 'footer1',
		                            'container' => '',
		                            'depth' => '0',
		                            'walker' => new FooterWalker()
		                    ) ); ?>

					</div>
					<div class="col">
						<?php wp_nav_menu( array(
		                            'menu' => 'footer2',
		                            'container' => '',
		                            'depth' => '0',
		                            'walker' => new FooterWalker()
		                    ) ); ?>
					</div>
					<div class="col">
						<?php wp_nav_menu( array(
		                            'menu' => 'footer3',
		                            'container' => '',
		                            'depth' => '0',
		                            'walker' => new FooterWalker()
		                    ) ); ?>
					</div>
				</div>
			</div>
			<div class="copyright-holder">
				<div class="container">
					<p>&copy; <?= date('Y') ?> Inflo. All rights are reserved.</p>
				</div>
			</div>
		</footer>
	</div>

	<!-- simplex-noise -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/simplex-noise/2.4.0/simplex-noise.min.js"></script>

	<?php wp_footer(); ?>

</body>

</html>
