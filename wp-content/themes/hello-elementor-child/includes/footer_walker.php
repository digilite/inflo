<?php

class FooterWalker extends Walker_Nav_Menu {
    function start_el(  &$output, $item, $depth = 0, $args = null, $id = 0 ) { 
        
        if ($depth == 0) { ?>
            <h6 class="footer-subtitle"><a href="<?= $item->url ?>"><?= $item->title ?></a></h6>            
            <?php } 
            if($depth == 1) { ?>
                <li><a href="<?= $item->url ?>"><?= $item->title ?></a>
           <?php }
           if($depth == 2) {?>
            <li><a href="<?= $item->url ?>"><?= $item->title ?></a>
            <?php }
    }

    function start_lvl( &$output, $depth = 0, $args = null ) {
        if($depth == 0){ ?>
            <ul class="footer-nav-list">
        <?php }

        if($depth == 1){ ?>
            <ul class="footer-nav-sublist">
        <?php }
    }

    function end_el(&$output, $object, $depth = 0, $args = array())
    {
         if($depth == 1) { ?>
                </li>
           <?php }
           if($depth == 2) {?>
            </li>
            <?php
        }
    }

    function end_lvl(&$output, $depth = 0, $args = array())
    {
        if($depth == 1){ ?>
            </ul>
        <?php }

        if($depth == 2){ ?>
            </ul>
        <?php }
    }
}