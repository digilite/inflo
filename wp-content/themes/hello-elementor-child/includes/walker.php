<?php

class CustomWalker extends Walker_Nav_Menu {

    public $elems_count = 4;
    public $elems_end_count = 0;
    public $end_div = false;

    function start_el(  &$output, $item, $depth = 0, $args = null, $id = 0 ) { 
        
        if ($depth == 0) { ?>
            <li><a href="<?= $item->url ?>"><?= $item->title ?></a>
            <?php } 
            if($depth == 1) { ?>
                <?php $this->elems_count++; if($this->elems_count >= 4) {$this->elems_count = 0; ?> <div class="submenu-col"> <?php } ?>
                <?php
                    $add_style = '';
                    if($item->title == 'dummy') {$add_style = 'style="display:none;"';} ?>
                <div class="submenu-block"><h4 <?php echo $add_style; ?> ><a href="<?= $item->url ?>"><?= $item->title ?></a></h4>
           <?php }
           if($depth == 2) {?>
            <li><a href="<?= $item->url ?>"><?= $item->title ?></a>
            <?php }
    }

    function start_lvl( &$output, $depth = 0, $args = null ) {
        if($depth == 0){ $this->elems_count = 4; ?>
            <div class="submenu">
        
        <?php }
        if($depth == 1){ ?>
            <ul class="submenu-list">
        <?php }
    }

    function end_el(&$output, $object, $depth = 0, $args = array())
    {
        if ($depth == 0) { ?>
            </li>
            <?php } 
            if($depth == 1) { ?>
                <?php $this->elems_end_count++; if($this->elems_end_count >= 4) {$this->elems_end_count = 0; $this->end_div = true; ?> </div> <?php } else {$this->end_div = false;} ?>
                </div>
           <?php }
           if($depth == 2) {?>
            </li>
            <?php
        }
    }

    function end_lvl(&$output, $depth = 0, $args = array())
    {
        if($depth == 0){ ?>

            </div>

        <?php if(!$this->end_div) { $this->elems_end_count = 0; ?> </div> <?php }?>
            
        <?php }        
        if($depth == 1){ ?>
            </ul>
        <?php }
    }
}