<?php /* The template for displaying 404 pages (Not Found) */ ?>

<?php get_header(); ?>

<style>
	/*footer {
		display: none !important;
	}*/
</style>

<div class="page-not-found bg-light" style="background-image: url('https://dev.digilabs.ca/inflo/wp-content/uploads/2021/02/img-02-1.png');">
	<div class="container">
		<div class="text-holder">
			<h2 class="text-center"><?= esc_attr( get_option('404_title') ) ?></h2>
			<?= get_option('404_text') ?>
		</div>
		<?php get_search_form(); ?>
		<div class="text-center btn-block">
			<a href="<?= esc_attr( get_option('404_cta1_url') ) ?>" class="btn"><?= esc_attr( get_option('404_cta1_text') ) ?></a>
		</div>
		<div class="text-center link-block">
			<a href="<?= esc_attr( get_option('404_cta2_url') ) ?>"><?= esc_attr( get_option('404_cta2_text') ) ?></a>
		</div>
	</div>
</div>

<?php get_footer(); ?>
