## Inflo WordPress Theme
The Inflo website is built using a blend of elementor pages with custom page templates using ACF fields.

### WARNING
**The theme contains references to page ID's when using custom page templates to pull information.
This may cause the website to break if a page no longer exists or is modified in any way.**


### How to build css/js?
Currently, there is no webpack used for this build, it's all manual CSS and JS.

### Custom Post Types
The theme contains a few custom post types:

- Use Cases
- Products
- Modules (_No way to manage this via the backend of the website._)

### Custom Options
The theme also offers site customisation from a custom WordPress page in the backend under "Global Fields".

- Header CTA (Default, AU, US and CA)
- Site Login URL (Default, AU, US and CA)
- Social Media Links for LinkedIn and Twitter (Default, AU, US and CA)
- Email address (Default, AU, US and CA)
- Subtext _this one is an unknown_ (Default, AU, US and CA)
- Custom 404 customisation: text, title, cta options for button 1 and 2

### Plugins
This theme was handed over with the following plugins installed:

- Advanced Custom Fields (_We've upgraded this to the Pro version of the plugin._)
- Advanced Custom Fields Multilingual
- Akismet Anti-Spam (_Disabled_)
- Classic Editor
- Elementor
- Elementor Pro
- HubSpot All-In-One Marketing - Forms, Popups, Live Chat
- Imagify
- Redirection
- Smush (_Disabled_)
- Wordfence Security
- WPML Media
- WPML Multilingual CMS
- WPML String Translation
- Yoast SEO
- Yoast SEO Multilingual

